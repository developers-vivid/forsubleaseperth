<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class ProductsCondition extends Model {
    protected $table = 'products_condition';
    protected $primaryKey = 'condition_id';
    public $incrementing = false;
    protected $softDelete = true;

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'condition_id', 'name'
    ];

    protected $dates = [
        'deleted_at', 'created_at', 'updated_at',
    ];

    protected $hidden = [
        'condition_id',
    ];

    /**
     * To get conditions for app purposes only
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-25T10:06:25+0800]
     * @return [type] [description]
     */
    public static function getConditionsForApp(){
        return DB::table('products_condition as pc')
                    ->select('condition_id', 'name')
                    ->orderBy('condition_id', 'asc')
                    ->get();
    }


}
