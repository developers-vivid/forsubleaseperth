<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersCompany extends Model
{
  protected $table = 'users_company';
  protected $primaryKey = 'company_id';
  public $incrementing = false;
  protected $softDelete = true;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'company_id', 'user_id', 'name', 'abn', 'street', 'city', 'state', 'post_code', 'phone', 'logo',
  ];

  protected $dates = [
  	'created_at', 'updated_at',
  ];

  protected $hidden = [
  	'company_id',
  ];
}
