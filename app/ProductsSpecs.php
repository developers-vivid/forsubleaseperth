<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductsSpecs extends Model
{
  protected $table = 'products_specs';
  protected $primaryKey = 'product_specs_id';
  public $incrementing = false;
  protected $softDelete = true;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'product_specs_id', 
    'product_id', 
    'product_condition', 
    'product_brand', 
    'product_model', 
    // 'product_finish', 
    'product_categories', 
    'product_subcategories', 
    'product_year', 
    'product_colour',
    'product_made_in',
    'product_type', 
  ];

  protected $dates = [
  	'created_at', 'updated_at'
  ];

  protected $hidden = [
  	'product_specs_id', 'product_id'
  ];
}
