<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Session;

use App\Blogs;

class PagesController extends Controller {
    	
    /**
     * [__construct description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-13T09:54:30+0800]
     */
    public function __construct(){
    	// 
    }

    /**
     * [showAboutUs description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-15T08:52:33+0800]
     * @return [type] [description]
     */
    public function showAboutUs() {
    	return view('pages.about-us');
    }

    /**
     * [showFaqs description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-15T08:52:36+0800]
     * @return [type] [description]
     */
    public function showFaqs() {
        return view('pages.faqs');
    }

    /**
     * [showBlogs description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-15T08:52:38+0800]
     * @return [type] [description]
     */
    public function showBlogs() {
        $blogs = Blogs::getBlogs( null, true, false );
        return view('pages.blogs', compact('blogs'));
    }

    /**
     * [showWhyUseGP description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-15T08:52:40+0800]
     * @return [type] [description]
     */
    public function showWhyUseGP() {
        return view('pages.why-use-gp');
    }

    /**
     * [showPolicy description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-15T08:52:43+0800]
     * @return [type] [description]
     */
    public function showPolicy() {
        return view('pages.policy');
    }

    /**
     * [showShipping description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-15T08:52:45+0800]
     * @return [type] [description]
     */
    public function showShipping() {
        return view('pages.shipping');
    }

    /**
     * [showPackingItems description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-15T08:52:47+0800]
     * @return [type] [description]
     */
    public function showPackingItems() {
        return view('pages.packing-items');
    }

    /**
     * [showGearPhotography description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-15T08:52:50+0800]
     * @return [type] [description]
     */
    public function showGearPhotography() {
        return view('pages.gear-photography');
    }

    /**
     * [showGearTermsAndCondition description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-15T08:52:50+0800]
     * @return [type] [description]
     */
    public function showGearTermsAndCondition() {
        return view('pages.terms-and-condition');
    }

    /**
     * [showGearPhotography description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-15T08:52:50+0800]
     * @return [type] [description]
     */
    public function showSellingMusicGear() {
        return view('pages.selling-music-gear');
    }

    /**
     * [showMessageSuccess description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-15T08:52:50+0800]
     * @return [type] [description]
     */
    public function showMessageSuccess() {
        if( isset(Session::get('alert')->data['signedup']) && Session::get('alert')->data['signedup'] )
            return view('messages.signed-up');
        else
            return redirect()->route('home');
    }

}
