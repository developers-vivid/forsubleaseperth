<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Newsletter;

class NewsletterController extends Controller {
  
	public function __construct() {
		$this->middleware('auth', ['except' => ['subscribeUser']]);
	}

	/**
	 * To subscribe a user via email address
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-04-12T11:10:21+0800]
	 * @return [type] [description]
	 */
	public function subscribeUser( Request $request ) {
		if( $request->has('email') ) {
			if (! filter_var($request->email, FILTER_VALIDATE_EMAIL) === false)
				return \App\Helper::handleResponse('Successfully subscribed', false, ["response" => Newsletter::subscribe( $request->email )]);
			else
				return \App\Helper::handleResponse('The email you entered is not valid');
		}
	}

}
