<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class ApiRefController extends Controller {
    protected $theMethod, $response, $token, $build;

	/**
	 * [__construct description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-29T10:31:58+0800]
	 */
    public function __construct( Request $request ) {
    	$this->response = new \App\Message();
    	$this->token = substr(str_shuffle(config('gp_conf.alpha_num')), 0, 60);

        $this->build = [
            "device" => "android",
            "version" => "V0.5B0.1"
        ];
    }

    /**
     * [home description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-29T10:32:06+0800]
     * @return [type] [description]
     */
    public function home( Request $request, $api = null ){
    	if( ! is_null($api) ) {
	    	$this->theMethod = $api;
	        $newMethod = studly_case(str_replace("-", "_", $api));
	        if( method_exists($this, $newMethod) )
	            return $this->$newMethod( $request );
	        else
	        	return view('api.not-found-new', ['method' => $this->theMethod, 'message' => 'Method <strong>'. ucfirst($api) .'</strong> does not exist']);
    	}
    	return redirect()->route('apiDocHome', ['dashboard']);
    }

    /**
     * [Dashboard description]
     * Develop by Richmund Lofranco
     * @author RichmundLofranco <richmundlofanco@gmail.com> @2016-07-06T22:06:00+0800
     * @param  [type] $request [description]
     */
    protected function Dashboard( $request ) {
        $title = camel_case($this->theMethod);
        $method = $this->theMethod;
        $notes = [
            "Welcome to Forsublease API Reference.",
            "For more info please contact <a href='mailto:richmund@sushidigital.com.au' title='Richmund Lofranco'>richmund@sushidigital.com.au</a>",
        ];
        $pendingTasks = [
        	"Sample Pending Task",
       	];
        $questions = [
            "Sample question 1",
        ];

        if( $request->isMethod('post') ) {
            $data = array();

            $data['configuration'] = [
                "env"            => config('app.env'),
                "timezone"       => config('app.timezone'),
                "memory_limit"   => config('gp_conf.memory_limit'),
                "execution_time" => config('gp_conf.execution_time'),
                "menus"          => config('gp_conf.menus'),
            ];

            $data['notes'] = [
                // ...
            ];

            return $this->response->setMessage('Success')->setData($data)->setSuccess()->display();
        }

        return view('api.method_new', compact('title', 'method', 'notes', 'pendingTasks', 'questions'));
    }

    /**
     * [Settings description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-07-05T09:02:22+0800]
     * @param  [type] $request [description]
     */
    protected function Settings( $request ) {
    	$title = camel_case($this->theMethod);
        $method = $this->theMethod;
        $url = "EoUvsyjX0xYcSpIRWTew3ni9GrJda7PhmAZt6OKCgHuN4B2LfV";
        $notes = [
        	"Settings url is static"
        ];

        if( $request->isMethod('post') ) {
        	$data = array();

            $params = [];
            $data['request'] = array('api' => $this->theMethod, 'build' => $this->build, 'data' => $params);
            $data['response'] = array(
            	"status" => 'success',
            	"message" => trans('messages.api.settings.all'),
            	"api" => $this->theMethod,
            	"data" => array(
            		"env" => config('app.env'),
		            "timezone" => config('app.timezone'),
		    		"api" => [
		    			"version" => env('API_VERSION', 1),
		    			"url" => env('APP_URL_'. strtoupper(env('APP_ENV', 'local')), 'LOCAL') .'api/v'. env('API_VERSION', 1) .'/',
		    		],
                    "can" => config('gp_conf.can'),
		    		"images" => [
		    			"gear" => [
		    				"original" => env('APP_URL_'. strtoupper(env('APP_ENV', 'local')), 'LOCAL') . 'images/gears/',
		    				"thumbs" => env('APP_URL_'. strtoupper(env('APP_ENV', 'local')), 'LOCAL') . 'images/gears/thumbs',
		    			],
		    		],
		    		'mode'       => env('APP_ENV'),
		            'pusher_key' => config('gp_conf.pusher_id'),
		            'msgcntr'    => [
		                'channel' => config('gp_conf.pusher_msg_counter_id'),
		                'event' => config('gp_conf.pusher.msg_counter.'. strtolower(env('APP_ENV')))
		            ],
		            'sort'       => config('gp_conf.sort'),
		            'brands'     => \App\Helper::getBrands(),
		            'conditions' => \App\ProductsCondition::getConditionsForApp(), 
		            'countries'  => \App\Helper::getCountries(), 
		            'states'     => \App\Helper::getStates(), 
		            'categories' => \App\Helper::getCategories(),
		            'order_statuses' => \App\OrdersStatus::getStatuses()
            	)
            );

            return $this->response->setMessage('Success')->setData($data)->setSuccess()->display();
        }

        return view('api.method_new', compact('title', 'method', 'url', 'notes'));
    }

    /**
     * [List description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-03T15:01:02+0800]
     * @param  [type] $request [description]
     */
    protected function Gears( $request ){
    	$title = camel_case($this->theMethod);
        $method = $this->theMethod;
        $url = "gears";
        $notes = [
        	"Request <strong>sub_category</strong> value is optional"
        ];

        if( $request->isMethod('post') ) {
        	$data = array();

            $params = (object)[
                "category" => "acoustic-guitars",
		        "sub_category" => "",
		        "location" => "all",
		        "brand" => "all",
		        "price_min" => "",
		        "price_max" => "",
		        "type" => "all",
		        "sort" => "created_at|desc",
		        "page" => 1
            ];
            $data['request'] = array('api' => $this->theMethod, 'build' => $this->build, 'data' => $params);
            $data['response'] = array(
            	"status" => 'success',
            	"message" => trans('messages.api.gears.loaded'),
            	"api" => $this->theMethod,
            	"data" => array(
            		"total" => 7,
				    "total_pages" => 1,
				    "result" => array(
				      	[
					        "product_id" => "57a0357429ab2",
					        "product_title" => "brand001-model001-2001-red",
					        "created_at" => "2016-08-02 05:53:56",
					        "status" => "active",
					        "sold_at" => "",
					        "product_blurb" => "brand001-model001-2001-red",
					        "product_description" => "brand001 model001 2001 red description",
					        "product_price" => "135.00",
					        "product_sale_price" => "130.00",
					        "created_by" => "57a03573678cb",
					        "owner" => "Richmund Lofranco",
					        "owner_email" => "richmund@gmail.com",
					        "category_slug" => "acoustic-guitars",
					        "category_name" => "Acoustic Guitars",
					        "category_parent" => "-",
					        "gear_photo" => "deP1FERhYgiHpoOBWtzV.jpeg"
				    	], [
					        "product_id" => "5758d77249ebb",
					        "product_title" => "Martin Cowboy V 2006 Graphic",
					        "created_at" => "2016-06-09 02:41:54",
					        "status" => "active",
					        "sold_at" => "",
					        "product_blurb" => "martin-cowboy-v-2006-graphic",
					        "product_description" => "This used Cowboy V guitar is a fun and wonderful instrument, it is first and foremost a real Martin guitar. And as with all Martin guitars, tone, playability, and quality are integral to its design. It is # 14 of 500. The body is a compact 0 size, similar to a traditional tenor design and is great for travel. An adjustable, modified low profile 14-fret neck (23&quot; scale) was selected for easy playability. Gotoh Cosmo Black tuning machines. The Cowboy V makes use of Martin&#039;s highly advanced X technology, with a HPL (high pressure laminate) back and textured HPL sides. The top, also of durable HPL material, is reinforced with crowned cross brace and a Graphite Bowtie plate. Fingerboard and bridge are crafted of black Micarta, an excellent and environmentally friendly alternative to ebony. Nut and compensated saddle are shaped from Black Corian and TUSQ material, respectively.",
					        "product_price" => "499.00",
					        "product_sale_price" => "0.00",
					        "created_by" => "5757bb218a2c3",
					        "owner" => "Matilda  Prentice",
					        "owner_email" => "matilda01prentice@gmail.com",
					        "category_slug" => "acoustic-guitars",
					        "category_name" => "Acoustic Guitars",
					        "category_parent" => "-",
					        "gear_photo" => "9KS7MFeTA1z3xJIbNcsd.jpg"
					    ]
				    )
            	)
			);

            return $this->response->setMessage('Success')->setData($data)->setSuccess()->display();
        }

        return view('api.method_new', compact('title', 'method', 'notes', 'url'));
    }

    /**
     * [Search description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-03T15:11:50+0800]
     * @param  [type] $request [description]
     */
    protected function SearchGears( $request ) {
    	$title = camel_case($this->theMethod);
        $method = $this->theMethod;
        $url = "gears/search";
        $notes = [
        	"Request <strong>keyword</strong> value is <strong>required</strong>"
        ];

        if( $request->isMethod('post') ) {
        	$data = array();

            $params = (object)[
                "keyword" => "akai",
		        "type" => "all",
		        "location" => "all",
		        "brand" => "all",
		        "price_min" => "",
		        "price_max" => "",
		        "sort" => "created_at|desc",
		        "page" => 1
            ];
            $data['request'] = array('api' => $this->theMethod, 'build' => $this->build, 'data' => $params);
            $data['response'] = array(
            	"status" => 'success',
            	"message" => trans('messages.api.gears.loaded'),
            	"api" => $this->theMethod,
            	"data" => array(
            		"total"=> 2,
				    "total_pages"=> 1,
				    "result"=> [
				      	[
					        "product_id"=> "5783769466e85",
					        "product_title"=> "Akai MPC500 drum machine sampler  2000's Black",
					        "created_at"=> "2016-07-11 10:36:04",
					        "status"=> "active",
					        "sold_at"=> "",
					        "product_blurb"=> "akai-mpc500-drum-machine-sampler-2000s-black",
					        "product_description"=> "Used Akai MPC500. Extra thick pads. A few scratches but generally good nick. ",
					        "product_price"=> "375.00",
					        "product_sale_price"=> "0.00",
					        "created_by"=> "5783736ab0d23",
					        "owner"=> "Shaun McIlroy",
					        "owner_email"=> "shaun@gearplanet.com.au",
					        "gear_photo"=> "BG4fHhuLgzM7eROm0KpU.jpeg"
				    	], [
					        "product_id"=> "5781d1b5ba6c9",
					        "product_title"=> "Akai D-1 Shred-O-Matic Tube Distortion Pedal Discontinued with Expression - Rare",
					        "created_at"=> "2016-07-10 04:40:21",
					        "status"=> "active",
					        "sold_at"=> "",
					        "product_blurb"=> "akai-d-1-shred-o-matic-tube-distortion-pedal-discontinued-with-expression-rare",
					        "product_description"=> "Used Akai D1 Shred-O-Matic pedal in excellent working and cosmetic condition. Has a couple small nicks and scratches. You have to look hatd to find them. Check out the pictures. You probably wont even see them they are so small. Product Description The Akai Shred-O-Matic offers mind-bending 12AX7A tube and diode distortion in one unit. Using Shred-O-Matic's MIX function in either Tube or Diode allows player dynamics to customize the distortion. When playing softly, the signal is routed only through the tube section, resulting in a mellow, low-level distortion. Harder playing will mix the warm, low-level output of the tube section with the screaming output of the diode section. For maximum flexibility, 2 user selectable modes are also available, Smooth, providing a gradual change between tube and diode and Passion, providing an abrupt change. Shred-O-Matic features easy-to-see and easy-to-use controls including output level control and LED indicator for distortion mode. The foot switch turns the effects on and off and when the unit is off, and the internal circuitry allows the signal to bypass the unit, without coloring the sound. The Shred-O-Matic's foot pedal provides total control of either the output level or the drive level. Additionally, while operating in the Mix mode, the pedal can be used to switch between tube and diode distortion. Features Mind-bending 12AX7A tube sound plus diode distortion in one pedal. Pedal allows dynamic control of drive level, output level, and the ability to mix tube and diode distortion for shred customization. The Shred-O-Matic allows you to choose between 4 distortion modes=> TUBE - Provides the warm, fat sound of tube distortion. DIODE - Provides the harder, edgier sound of diode distortion. MIX - In both modes the mix is determined by player dynamics. When playing softly, the signal will be routed only through the tube section, resulting in a mellow, low-level distortion. Harder playing will mix the warm, low-level output of the tube section with the screaming output of the diode section. Two modes are available. SMOOTH - Gradual change between tube and diode. PASSION - Abrupt change between tube and diode. Pedal Assign - Selects the Foot Pedal assignment. In DRIVE, the Pedal controls the distortion level In OUTPUT, it controls the volume of the output signal. \n\nPlease feel free to ask any questions. I will do my best to answer them to the best of my ability and as quickly as possible. \n\nAll pictures are of the exact item that you will be receiving. Please look at the pictures to get as best idea that you can on the condition of any item. If you require additional pictures then I will do my best to get a quality photo for you.\n\nI do not accept returns unless an item you receive is incorrect. I do my best to be as accurate as I can in my descriptions and post lots of pictures of my items at different angles to ensure that you know exactly what you are buying.",
					        "product_price"=> "125.00",
					        "product_sale_price"=> "",
					        "created_by"=> "577f89c0ca329",
					        "owner"=> "Groovius Maximus",
					        "owner_email"=> "payments@gearplanet.com.au",
					        "gear_photo"=> "2T3YdmtKZXG6Cf5WvEkg.jpg"
				      	]
				    ]
				)
			);

            return $this->response->setMessage('Success')->setData($data)->setSuccess()->display();
        }

        return view('api.method_new', compact('title', 'method', 'notes', 'url'));
    }

    /**
     * [SignIn description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-04T08:54:26+0800]
     * @param  [type] $request [description]
     */
    protected function SignIn( $request ) {
    	$title = camel_case($this->theMethod);
        $method = $this->theMethod;
        $url = "users/sign-in";
        $notes = [
        	"Request <strong>type</strong> value could either <strong>local</strong> or <strong>google/facebook</strong>"
        ];

        if( $request->isMethod('post') ) {
        	$data = array();

            $params = (object)[
                "email" => "william01fiaschi@gmail.com",
		        "password" => "12345678",
		        "type" => "local"
            ];
            $data['request'] = array('api' => $this->theMethod, 'build' => $this->build, 'data' => $params);
            $data['response'] = array(
            	"status" => 'success',
            	"message" => trans('messages.api.user.login'),
            	"api" => $this->theMethod,
            	"data" => array(
            		"user_id" => "57835d4dbd83f",
				    "name" => "William Fiaschi",
				    "email" => "william01fiaschi@gmail.com",
				    "login_type" => "local",
				    "api_token" => "N7lVHp6jgzBky4oumwdacseGFOqn1EMY50SCAhxKib8tX2ZJfrQ9DUITPWRv",
				    "date_registered" => "2016-07-11 08:48:13",
				    "role_name" => "User",
				    "first_name" => "William",
				    "last_name" => "Fiaschi",
				    "telephone" => "",
				    "mobile" => "",
				    "company_name" => "",
				    "company_abn" => "",
				    "company_street" => "",
				    "company_city" => "",
				    "company_state" => "",
				    "company_post_code" => "",
				    "company_phone" => "",
				    "company_logo" => "",
				    "shipping_address_1" => "",
				    "shipping_address_2" => "",
				    "shipping_city" => "",
				    "shipping_state" => "",
				    "shipping_post_code" => "",
				    "total_unread_messages" => 0
				)
			);

            return $this->response->setMessage('Success')->setData($data)->setSuccess()->display();
        }

        return view('api.method_new', compact('title', 'method', 'notes', 'url'));
    }

    /**
     * [SignUp description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-04T09:10:47+0800]
     * @param  [type] $request [description]
     */
    protected function SignUp( $request ){
    	$title = camel_case($this->theMethod);
        $method = $this->theMethod;
        $url = "users/sign-up";
        $notes = [
        	"Request <strong>type</strong> value could either <strong>local</strong> or <strong>google/facebook</strong>",
        	"Request <strong>password</strong> and <strong>password_confirmation</strong> is <strong>OPTIONAL</strong> when <strong>type</strong> value is <strong>not local</strong>",
        	"<strong>RESPONSE DATA VALUE IS OBJECT WHEN TYPE IS NOT LOCAL, OTHERWISE EMPTY ARRAY</strong>"
        ];

        if( $request->isMethod('post') ) {
        	$data = array();

            $params = (object)[
                "first_name" => "Thomas",
		        "last_name" => "Cruz",
		        "email" => "thomas001211aa@gmail.com",
		        "password" => "123456",
		        "password_confirmation" => "123456",
		        "phone" => "",
		        "mobile" => "",
		        "seller" => "no",
		        "company_name" => "",
		        "company_city" => "",
		        "company_state" => "",
		        "company_post_code" => "",
		        "agreed" => "yes",
		        "type" => "facebook"
            ];
            $data['request'] = array('api' => $this->theMethod, 'build' => $this->build, 'data' => $params);
            $data['response'] = array(
            	"status" => 'success',
            	"message" => trans('messages.api.user.sign_up.'. ($params->type == 'local' ? 'local' : 'social_media')),
            	"api" => $this->theMethod,
            	"data" => array(
            		"user_id" => "57a295956e23c",
				    "name" => "Thomas Cruz",
				    "email" => "thomas001211aa@gmail.com",
				    "login_type" => "facebook",
				    "api_token" => "fVZPYoiGlKjX2zuA4OSL08pvQxy3eIMmh1d5NFn9wrc7CTED6sbktRHUWgqa",
				    "date_registered" => "2016-08-04 01:08:37",
				    "role_name" => "User",
				    "first_name" => "Thomas",
				    "last_name" => "Cruz",
				    "telephone" => "",
				    "mobile" => "",
				    "company_name" => "",
				    "company_abn" => "",
				    "company_street" => "",
				    "company_city" => "",
				    "company_state" => "",
				    "company_post_code" => "",
				    "company_phone" => "",
				    "company_logo" => "",
				    "shipping_address_1" => "",
				    "shipping_address_2" => "",
				    "shipping_city" => "",
				    "shipping_state" => "",
				    "shipping_post_code" => "",
				    "total_unread_messages" => 0
            	)
			);

            return $this->response->setMessage('Success')->setData($data)->setSuccess()->display();
        }

        return view('api.method_new', compact('title', 'method', 'notes', 'url'));
    }

    /**
     * [MyAds description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-04T09:27:37+0800]
     * @param  [type] $request [description]
     */
    protected function MyAds( $request ) {
    	$title = camel_case($this->theMethod);
        $method = $this->theMethod;
        $url = "gears/my-ads?api_token=". $this->token;
        $notes = [
        	"Request <strong>price_min</strong> and <strong>price_max</strong> value is <strong>optional</strong> but leave it an <strong>empty</strong> array"
        ];

        if( $request->isMethod('post') ) {
        	$data = array();

            $params = (object)[
                "status" => "all",
		        "type" => "all",
		        "location" => "all",
		        "brand" => "all",
		        "price_min" => "",
		        "price_max" => "",
		        "sort" => "created_at|desc",
		        "page" => 1
            ];
            $data['request'] = array('api' => $this->theMethod, 'build' => $this->build, 'data' => $params);
            $data['response'] = array(
            	"status" => 'success',
            	"message" => trans('messages.api.user.ads.loaded'),
            	"api" => $this->theMethod,
            	"data" => array(
            		"total" => 5,
				    "total_pages" => 1,
				    "result" => [
				      	[
					        "product_id" => "5758e41a9f9cf",
					        "product_blurb" => "isoacoustics-iso-l8r155-2016-black",
					        "product_title" => "IsoAcoustics ISO-L8R155 2016 Black",
					        "product_description" => "Like new, sold without box. Only available in the configuration shown (no extended support posts).",
					        "product_price" => "75.00",
					        "status" => "active",
					        "created_at" => "2016-06-09 03:35:54",
					        "sold_at" => "2016-06-15 08:34:04",
					        "owner" => "Ashton  Willoughby",
					        "owner_email" => "ashton01willoughby@gmail.com",
					        "gear_photo" => "SlAbwcN1MVhv7d4EjinT.jpg"
				    	], [
					        "product_id" => "5758e383db954",
					        "product_blurb" => "ibanez-tbx150h-150w-guitar-head-2015-black",
					        "product_title" => "Ibanez TBX150H 150W Guitar Head 2015 Black",
					        "product_description" => "Ibanez TBX150H 150W Guitar Amplifier Head. This amp is in very good working condition. Both channels are fully functional, with an audio path free from crackle, hiss, hum, or distortion.The LED indicators all function well. The cosmetic condition is very good with only some minor scuffing and scratches in the finish.\n",
					        "product_price" => "215.00",
					        "status" => "active",
					        "created_at" => "2016-06-09 03:33:23",
					        "sold_at" => "",
					        "owner" => "Ashton  Willoughby",
					        "owner_email" => "ashton01willoughby@gmail.com",
					        "gear_photo" => "reohXvWfn4LMOjEUwpJ7.jpg"
				      	]
				    ]
            	)
			);

            return $this->response->setMessage('Success')->setData($data)->setSuccess()->display();
        }

        return view('api.method_new', compact('title', 'method', 'notes', 'url'));
    }

    /**
     * [Details description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-04T09:57:27+0800]
     */
    protected function GearDetails( $request ) {
    	$title = camel_case($this->theMethod);
        $method = $this->theMethod;
        $url = "gears/details";
        $notes = [
        	"Request <strong>product_blurb</strong> field is required"
        ];

        if( $request->isMethod('post') ) {
        	$data = array();

            $params = (object)["product_blurb" => "ramirez-classical-guitar-estudio-classical-1968-peach"];
            $data['request'] = array('api' => $this->theMethod, 'build' => $this->build, 'data' => $params);
            $data['response'] = array(
            	"status" => 'success',
            	"message" => trans('messages.api.gears.loaded'),
            	"api" => $this->theMethod,
            	"data" => array(
            		"gears_details" => [
						"product_id" => "5757b5b0a3478",
						"product_blurb" => "ramirez-classical-guitar-estudio-classical-1968-peach",
						"product_title" => "Ramirez Classical Guitar Estudio classical 1968 peach",
						"product_description" => "Vintage rare spruce top 1968. All solid. Ebony with super frets. Tone to die for and easy play. Original case with keys. Fustero tuners. Well taken care of but played!",
						"product_price" => "1095.00",
						"product_sale_price" => "0.00",
						"shop_name" => "",
						"product_primary_photo" => "5757b5b0b27bb",
						"accept_offers" => "",
						"paypal_email" => "gearplanetbuyer001@gmail.com",
						"shopping_policy" => "About The Seller\n\nClassical Guitar Emporium \nMurfreesboro, TN, United States \n\n-Quick Responder\n\nAccepted Payment Methods \n-PayPal \n\nPayment Policy\nTo negotiate price, shipping, or payment options, please send a message or make an offer.\n\n",
						"status" => "active",
						"created_by" => "5757b08ba98b4",
						"sold_at" => "",
						"deleted_at" => "",
						"created_at" => "2016-06-08 06:05:36",
						"updated_at" => "2016-06-17 07:19:00",
						"product_brand" => "Ramirez Classical Guitar",
						"product_model" => "Estudio classical",
						"product_finish" => "",
						"product_year" => "1968",
						"product_colour" => "1968",
						"product_made_in" => "spain",
						"product_type" => "new",
						"category_id" => "56e626f24ce06",
						"sub_category_id" => "571890184b4bf",
						"condition_id" => "56e65261b3fa2",
						"product_condition" => "Very Good",
						"product_category" => "Acoustic Guitars",
						"photo_filename" => "NeEWuzZD6t1hjSd9b0Y2.jpg",
						"photo_title" => "Ramirez Classical Guitar Estudio classical 1968 Natural spruc",
						"shipping_country" => "Australia",
						"shipping_city" => "Perth",
						"shipping_state" => "qld",
						"shipping_policy" => "Shipping Policy\n\nShips from Murfreesboro, TN to:\nContinental U.S.$50.00\nEverywhere Else$100.00\n\nI will ship with tracking to the continental United States. Alaska, Hawaii, and international shipments may incur additional charges.\n\nReturn Policy\nReturn Window\nThis product can be returned within 7 days of receipt.\n\nGeneral Terms\nItems must be returned in original, as-shipped condition with all original packaging and no signs of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n\nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges.",
						"shipping_method" => "shipping",
						"name" => "Hazel Sellote",
						"email" => "sellotepas@gmail.com",
						"date_registered" => "2016-06-08 05:43:39",
						"first_name" => "Hazel",
						"last_name" => "Sellote",
						"mobile" => "",
						"company_name" => "Brownian",
						"company_abn" => "",
						"company_street" => "",
						"company_city" => "Perth",
						"company_state" => "wa",
						"company_post_code" => "",
						"company_phone" => "12345678",
						"company_logo" => "",
						"photos" => [
							[
								"photo_title" => "Ramirez Classical Guitar Estudio classical 1968 Natural spruc",
								"photo_filename" => "NeEWuzZD6t1hjSd9b0Y2.jpg"
							], [
								"photo_title" => "Ramirez Classical Guitar Estudio classical 1968 Natural spruc-2",
								"photo_filename" => "j4wVbJk3urovyNMgGaQS.jpg"
							], [
								"photo_title" => "Ramirez Classical Guitar Estudio classical 1968 Natural spruc-3",
								"photo_filename" => "nF4b3WiqhjsHlrA28euN.jpg"
							], [
								"photo_title" => "Ramirez Classical Guitar Estudio classical 1968 Natural spruc-4",
								"photo_filename" => "8z02pSCFXPxi6GRAHkuo.jpg"
							], [
								"photo_title" => "Ramirez Classical Guitar Estudio classical 1968 Natural spruc-5",
								"photo_filename" => "BGFi4uXLrWyTpfQE9S1v.jpg"
							]
						],
						"isOwner" => false,
						"shippingCost" => [
							[
								"product_shipping_cost_id" => "5763a4642a896",
								"location" => "Metro Areas",
								"cost" => "0.00"
							], [
								"product_shipping_cost_id" => "5763a4642bb51",
								"location" => "Regional Areas",
								"cost" => "0.00"
							]
						]
					],
					"related_items" => [
						[
							"product_title" => "Bourgeois L-DBO/S - Sunburst - Madagascar 2010 black",
							"product_blurb" => "bourgeois-l-dbos-sunburst-madagascar-2010-black",
							"created_by" => "5757bb5b2451e",
							"photo_filename" => "cNfGdm5uZ3RetjBM9SU4.jpg"
						], [
							"product_title" => "Jose Ramirez 130 Años Spruce 2016 Laquer",
							"product_blurb" => "jose-ramirez-130-anos-spruce-2016-laquer",
							"created_by" => "5757b8ebf31a3",
							"photo_filename" => "vYwLlkSynHVZRj89o04s.jpg"
						], [
							"product_title" => "Taylor 814ce Limited 2010 Natural",
							"product_blurb" => "taylor-814ce-limited-2010-natural",
							"created_by" => "5757b94202d2f",
							"photo_filename" => "sLzGVdSFTB1g5toYORHk.jpg"
						], [
							"product_title" => "Gibson J-45 1962 Cherry Sunburst",
							"product_blurb" => "gibson-j-45-1962-cherry-sunburst",
							"created_by" => "5757b9ae6ba8a",
							"photo_filename" => "9jegUslc0p2XaTJwYzZA.jpg"
						]
					]
            	)
			);

            return $this->response->setMessage('Success')->setData($data)->setSuccess()->display();
        }

        return view('api.method_new', compact('title', 'method', 'notes', 'url'));
    }

    /**
     * [SellGear description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-08T10:08:27+0800]
     * @param  [type] $request [description]
     */
    protected function SellGear( $request ) {
    	$title = camel_case($this->theMethod);
        $method = $this->theMethod;
        $url = "gears/sell?api_token=". $this->token;
        $notes = [
        	"Request <strong>sub_category</strong> field is optional.",
        	"Request <strong>sale_price</strong> field is optional. Just leave empty string.",
        	"Request <strong>shipping.cost</strong> field is optional. Just leave empty string.",
        	"Ad <strong>primary photo</strong> will be the first photo of <strong>photos</strong>.",
        ];

        if( $request->isMethod('post') ) {
        	$data = array();

            $params = (object)[
            	"category" => "56e626f24ce06",
		        "sub_category" => "571890184b4bf",
		        "condition" => "56e65261b3f8b",
		        "brand" => "brand001",
		        "model" => "model001",
		        "year" => "2001",
		        "colour" => "red",
		        "made" => "canada",
		        "type" => "new",
		        "listing_title" => "brand001 model001 2001 red 002",
		        "price" => 135,
		        "sale_price" => 130,
		        "paypal_email" => "gearplanetbuyer001@gmail.com",
		        "accept_offers" => "yes",
		        "description" => "brand001 model001 2001 red description",
		        "shopping_policy" => "brand001 model001 2001 red shopping policy",
		        "shipping"  => [
		            "city" => "city",
		            "state" => "nsw",
		            "policy" => "brand001 model001 2001 red shipping policy",
		            "method" => "shipping",
		            "cost" => 50
		        ],
		        "photos" => [
		            "data:image/jpeg;base64,/9j...", 
		            "data:image/jpeg;base64,/9j...", 
		            "data:image/jpeg;base64,/9j...",
		            "data:image/jpeg;base64,/9j..."
		        ],
		        "agree" => "yes"
            ];
            $data['request'] = array('api' => $this->theMethod, 'build' => $this->build, 'data' => $params);
            $data['response'] = array(
            	"status" => 'success',
            	"message" => trans('messages.gear.created'),
            	"api" => $this->theMethod,
            	"data" => [
            		"gear_id" => "57a7f587ae6ba",
    				"listing_title" => "brand001-model001-2001-red-002"
            	]
			);

            return $this->response->setMessage('Success')->setData($data)->setSuccess()->display();
        }

        return view('api.method_new', compact('title', 'method', 'notes', 'url'));
    }

    /**
     * [UpdateGear description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-08T12:49:33+0800]
     * @param  [type] $request [description]
     */
    protected function UpdateGear( $request ) {
    	$title = camel_case($this->theMethod);
        $method = $this->theMethod;
        $url = "gears/update?api_token=". $this->token;
        $notes = [
        	"This applies only if <strong>owner</strong> of the ad.",
        	"Request <strong>photos.added</strong> must be an <strong>array</strong> but <strong>optional</strong>. Value must be in a form of <strong>array</strong>. Just leave an empty array if no photo(s) to be added.",
        	"Request <strong>photos.delete</strong> must be an <strong>array</strong> but <strong>optional</strong>. Values must be the <strong>photo ids</strong>. Just leave an empty array if no photo(s) to be deleted.",
        	"Request <strong>photos.primary</strong> value must be <strong>(string) if fetched from photo_id</strong> or <strong>(integer) as index from photos.added array</strong>. Just leave an empty string if nothing to be set as <strong>primary photo</strong>."
        ];

        if( $request->isMethod('post') ) {
        	$data = array();

            $params = (object)[
            	"gear_id" => "5758e0ec9d692",
		        "category" => "56e626f24ce06",
		        "sub_category" => "571890184b4bf",
		        "condition" => "56e65261b3f8b",
		        "brand" => "brand001",
		        "model" => "model001",
		        "year" => "2001",
		        "colour" => "red",
		        "made" => "canada",
		        "type" => "new",
		        "listing_title" => "Gibson L-00 1941 Sunburst",
		        "price" => 135,
		        "sale_price" => 130,
		        "paypal_email" => "gearplanetbuyer001@gmail.com",
		        "accept_offers" => "yes",
		        "description" => "brand001 model001 2001 red description",
		        "shopping_policy" => "brand001 model001 2001 red shopping policy",
		        "shipping"  => [
		            "city" => "city",
		            "state" => "nsw",
		            "policy" => "brand001 model001 2001 red shipping policy",
		            "method" => "shipping",
		            "cost" => 50
		        ],
		        "photos" => [
		            "added" => [],
		            "deleted" => [],
		            "primary" => ""
		        ]
            ];
            $data['request'] = array('api' => $this->theMethod, 'build' => $this->build, 'data' => $params);
            $data['response'] = array(
            	"status" => 'success',
            	"message" => trans('messages.gear.updated'),
            	"api" => $this->theMethod,
            	"data" => []
			);

            return $this->response->setMessage('Success')->setData($data)->setSuccess()->display();
        }

        return view('api.method_new', compact('title', 'method', 'notes', 'url'));
    }

    /**
     * [CancelGear description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-08T12:51:01+0800]
     * @param  [type] $request [description]
     */
    protected function CancelGear( $request ) {
    	$title = camel_case($this->theMethod);
        $method = $this->theMethod;
        $url = "gears/cancel?api_token=". $this->token;
        $notes = ["Request <strong>gear_id</strong> field is required"];

        if( $request->isMethod('post') ) {
        	$data = array();

            $params = (object)["gear_id" => "5758e0ec9d692"];
            $data['request'] = array('api' => $this->theMethod, 'build' => $this->build, 'data' => $params);
            $data['response'] = array(
            	"status" => 'success',
            	"message" => trans('messages.gear.cancel.success'),
            	"api" => $this->theMethod,
            	"data" => []
			);

            return $this->response->setMessage('Success')->setData($data)->setSuccess()->display();
        }

        return view('api.method_new', compact('title', 'method', 'notes', 'url'));
    }

    /**
     * [DeleteGear description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-08T12:51:31+0800]
     * @param  [type] $request [description]
     */
    protected function DeleteGear( $request ) {
    	$title = camel_case($this->theMethod);
        $method = $this->theMethod;
        $url = "gears/delete?api_token=". $this->token;
        $notes = ["Request <strong>gear_id</strong> field is required"];

        if( $request->isMethod('post') ) {
        	$data = array();

            $params = (object)["gear_id" => "5758e0ec9d692"];
            $data['request'] = array('api' => $this->theMethod, 'build' => $this->build, 'data' => $params);
            $data['response'] = array(
            	"status" => 'success',
            	"message" => trans('messages.gear.delete.success'),
            	"api" => $this->theMethod,
            	"data" => []
			);

            return $this->response->setMessage('Success')->setData($data)->setSuccess()->display();
        }

        return view('api.method_new', compact('title', 'method', 'notes', 'url'));
    }

    /**
     * [ActivateGear description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-08T12:51:55+0800]
     * @param  [type] $request [description]
     */
    protected function ActivateGear( $request ) {
    	$title = camel_case($this->theMethod);
        $method = $this->theMethod;
        $url = "gears/activate?api_token=". $this->token;
        $notes = ["Request <strong>gear_id</strong> field is required"];

        if( $request->isMethod('post') ) {
        	$data = array();

            $params = (object)["gear_id" => "5758e0ec9d692"];
            $data['request'] = array('api' => $this->theMethod, 'build' => $this->build, 'data' => $params);
            $data['response'] = array(
            	"status" => 'success',
            	"message" => trans('messages.gear.activate.success'),
            	"api" => $this->theMethod,
            	"data" => []
			);

            return $this->response->setMessage('Success')->setData($data)->setSuccess()->display();
        }

        return view('api.method_new', compact('title', 'method', 'notes', 'url'));
    }

    /**
     * [ContactSeller description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-04T11:14:59+0800]
     * @param  [type] $request [description]
     */
    protected function ContactSeller( $request ) {
    	$title = camel_case($this->theMethod);
        $method = $this->theMethod;
        $url = "messages/contact/seller?api_token=". $this->token;
        $notes = [
        	"All fields are required"
        ];

        if( $request->isMethod('post') ) {
        	$data = array();

            $params = (object)[
                "seller_id" => "56fc9e052d05f",
		        "message" => "This is a sample message to Rich Lofr",
		        "socket_id" => "200405.5363483"
            ];
            $data['request'] = array('api' => $this->theMethod, 'build' => $this->build, 'data' => $params);
            $data['response'] = array(
            	"status" => 'success',
            	"message" => trans('messages.api.messages.sent'),
            	"api" => $this->theMethod,
            	"data" => array(
            		"message" => "57a2b35152d9a"
            	)
			);

            return $this->response->setMessage('Success')->setData($data)->setSuccess()->display();
        }

        return view('api.method_new', compact('title', 'method', 'notes', 'url'));
    }

    /**
     * [Send description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-04T11:17:05+0800]
     * @param  [type] $request [description]
     */
    protected function Send( $request ) {
    	$title = camel_case($this->theMethod);
        $method = $this->theMethod;
        $url = "messages/send?api_token=". $this->token;
        $notes = [
        	"All fields are required"
        ];

        if( $request->isMethod('post') ) {
        	$data = array();

            $params = (object)[
                "inbox_id" => "578ee4fea2244",
		        "seller_id" => "578c538e46670",
		        "message" => "Hello there 104.?",
		        "socket_id" => "201703.5998766"
            ];
            $data['request'] = array('api' => $this->theMethod, 'build' => $this->build, 'data' => $params);
            $data['response'] = array(
            	"status" => 'success',
            	"message" => trans('messages.api.messages.sent'),
            	"api" => $this->theMethod,
            	"data" => array(
	                "response" => true,
	                "message" => [
	                    "message_id" => "578f147f6193e",
	                    "inbox_id" => "578ee4fea2244",
	                    "sender_id" => "57835d4dbd83f",
	                    "receiver_id" => "578c538e46670",
	                    "message" => "Hello there.",
	                    "status" => "read",
	                    "created_at" => "2016-07-20 06:04:47",
	                    "updated_at" => "2016-07-20 06:04:48",
	                    "channel_id" => "emjkSiUqAPOBn7VJutbo",
	                    "receiver_name" => "Paul Jain",
	                    "receiver_email" => "paulj.sushi@gmail.com",
	                    "sender_name" => "William Fiaschi",
	                    "sender_email" => "william01fiaschi@gmail.com"
	                ]
            	)
			);

            return $this->response->setMessage('Success')->setData($data)->setSuccess()->display();
        }

        return view('api.method_new', compact('title', 'method', 'notes', 'url'));
    }

    /**
     * [Inbox description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-04T11:24:15+0800]
     * @param  [type] $request [description]
     */
    protected function Inbox( $request ) {
    	$title = camel_case($this->theMethod);
        $method = $this->theMethod;
        $url = "messages/inbox?api_token=". $this->token;
        $notes = [];

        if( $request->isMethod('post') ) {
        	$data = array();

            $params = [];
            $data['request'] = array('api' => $this->theMethod, 'build' => $this->build, 'data' => $params);
            $data['response'] = array(
            	"status" => 'success',
            	"message" => trans('messages.api.user.inbox'),
            	"api" => $this->theMethod,
            	"data" => array(
            		"event" => "kYiHxKcIFUfCW",
				    "inbox" => [
				      	[
					        "inbox_id" => "57a2af893dc55",
					        "channel_id" => "lKrVTMP1FD26vtsJX4B7",
					        "sender_id" => "5757bbce106d2",
					        "photo" => "http://localhost:8080/images/thumb-pic.png",
					        "name" => "Rich Lofr",
					        "email" => "richmund@gmail.com"
				      	]
				    ]
            	)
			);

            return $this->response->setMessage('Success')->setData($data)->setSuccess()->display();
        }

        return view('api.method_new', compact('title', 'method', 'notes', 'url'));
    }

    /**
     * [InboxMessages description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-04T11:32:03+0800]
     * @param  [type] $request [description]
     */
    protected function InboxMessages( $request ) {
    	$title = camel_case($this->theMethod);
        $method = $this->theMethod;
        $url = "messages/inbox-messages?api_token=". $this->token;
        $notes = [
        	"Request <strong>inbox_id</strong> field is required"
        ];

        if( $request->isMethod('post') ) {
        	$data = array();

            $params = (object)["inbox_id" => "578d7e133f643"];
            $data['request'] = array('api' => $this->theMethod, 'build' => $this->build, 'data' => $params);
            $data['response'] = array(
            	"status" => 'success',
            	"message" => trans('messages.api.user.inbox_message'),
            	"api" => $this->theMethod,
            	"data" => array(
            		"unread_messages" => 0,
				    "messages" => [
				      	[
					        "message_id" => "578d7e133fc34",
					        "sender_id" => "578c538e46670",
					        "receiver_id" => "57835d4dbd83f",
					        "message" => "Hello.\nInterested.",
					        "status" => "read",
					        "sent_at" => "2016-07-19 01:10:43"
				      	]
				    ]
            	)
			);

            return $this->response->setMessage('Success')->setData($data)->setSuccess()->display();
        }

        return view('api.method_new', compact('title', 'method', 'notes', 'url'));
    }

    /**
     * [MessageDetails description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-04T11:43:08+0800]
     * @param  [type] $request [description]
     */
    protected function InboxMessageDetails( $request ) {
    	$title = camel_case($this->theMethod);
        $method = $this->theMethod;
        $url = "messages/details?api_token=". $this->token;
        $notes = [
        	"Request <strong>message_id</strong> field is required"
        ];

        if( $request->isMethod('post') ) {
        	$data = array();

            $params = (object)["message_id" => "578d7e133fc34"];
            $data['request'] = array('api' => $this->theMethod, 'build' => $this->build, 'data' => $params);
            $data['response'] = array(
            	"status" => 'success',
            	"message" => trans('messages.api.user.message_details'),
            	"api" => $this->theMethod,
            	"data" => array(
            		"message_id" => "578d7e133fc34",
				    "sender_id" => "578c538e46670",
				    "receiver_id" => "57835d4dbd83f",
				    "message" => "Hello.\nInterested.",
				    "status" => "read",
				    "sent_at" => "2016-07-19 01:10:43"
            	)
			);

            return $this->response->setMessage('Success')->setData($data)->setSuccess()->display();
        }

        return view('api.method_new', compact('title', 'method', 'notes', 'url'));
    }

    /**
     * [SaveSearch description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-04T11:53:45+0800]
     * @param  [type] $request [description]
     */
    protected function SaveSearch( $request ) {
    	$title = camel_case($this->theMethod);
        $method = $this->theMethod;
        $url = "search/save?api_token=". $this->token;
        $notes = [
        	"All fields are required"
        ];

        if( $request->isMethod('post') ) {
        	$data = array();

            $params = (object)[
            	"url" =>"http://gearplanet.sushidigital.ph/search?keyword=gear1&location=nsw&brand=3rd+power&price_min=100&price_max=999&type=new&sort=created_at%7Cdesc",
        		"keyword" =>"gear1222"
            ];
            $data['request'] = array('api' => $this->theMethod, 'build' => $this->build, 'data' => $params);
            $data['response'] = array(
            	"status" => 'success',
            	"message" => trans('messages.api.search.saved'),
            	"api" => $this->theMethod,
            	"data" => []
			);

            return $this->response->setMessage('Success')->setData($data)->setSuccess()->display();
        }

        return view('api.method_new', compact('title', 'method', 'notes', 'url'));
    }

    /**
     * [DeleteSearch description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-05T08:22:14+0800]
     * @param  [type] $request [description]
     */
    protected function DeleteSearch( $request ) {
    	$title = camel_case($this->theMethod);
        $method = $this->theMethod;
        $url = "search/delete?api_token=". $this->token;
        $notes = [
        	"All fields are required"
        ];

        if( $request->isMethod('post') ) {
        	$data = array();

            $params = (object)["search_id" => "57a2bc1dea6f4"];
            $data['request'] = array('api' => $this->theMethod, 'build' => $this->build, 'data' => $params);
            $data['response'] = array(
            	"status" => 'success',
            	"message" => trans('messages.api.search.deleted'),
            	"api" => $this->theMethod,
            	"data" => []
			);

            return $this->response->setMessage('Success')->setData($data)->setSuccess()->display();
        }

        return view('api.method_new', compact('title', 'method', 'notes', 'url'));
    }

    /**
     * [SearchList description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-05T08:25:49+0800]
     * @param  [type] $request [description]
     */
    protected function SearchList( $request ) {
    	$title = camel_case($this->theMethod);
        $method = $this->theMethod;
        $url = "search/list?api_token=". $this->token;
        $notes = [];

        if( $request->isMethod('post') ) {
        	$data = array();

            $params = [];
            $data['request'] = array('api' => $this->theMethod, 'build' => $this->build, 'data' => $params);
            $data['response'] = array(
            	"status" => 'success',
            	"message" => trans('messages.api.search.loaded'),
            	"api" => $this->theMethod,
            	"data" => [
            		[
						"search_id" => "576362a80c283",
						"keyword" => "tes",
						"url" => "http://gearplanet.sushidigitaldemo.info/search?keyword=tes&location=&brand=&price_min=&price_max=&type=&sort=product_price%7Casc",
						"created_by" => "5757cc0ae2bf2",
						"created_at" => "2016-06-17 02:38:32",
						"updated_at" => "2016-06-17 02:38:32"
				    ], [
						"search_id" => "576362933b75f",
						"keyword" => "tes",
						"url" => "http://gearplanet.sushidigitaldemo.info/search?keyword=tes&page=2",
						"created_by" => "5757cc0ae2bf2",
						"created_at" => "2016-06-17 02:38:11",
						"updated_at" => "2016-06-17 02:38:23"
				    ]
            	]
			);

            return $this->response->setMessage('Success')->setData($data)->setSuccess()->display();
        }

        return view('api.method_new', compact('title', 'method', 'notes', 'url'));
    }

    /**
     * [CreateOrder description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-05T10:59:41+0800]
     * @param  [type] $request [description]
     */
    protected function CreateOrder( $request ) {
    	$title = camel_case($this->theMethod);
        $method = $this->theMethod;
        $url = "orders/create?api_token=". $this->token;
        $notes = [
            "Request <strong>paypal_correlation_id</strong>, <strong>tracking_code</strong> and <strong>notes</strong> fields are optional, but leave an empty string.",
        	"In request <strong>shipping -> address 2</strong> field is optional"
        ];

        if( $request->isMethod('post') ) {
        	$data = array();

            $params = (object)[
            	"invoice_id" => "invoiceid001",
		        "total" => 4.75,
		        "status" => "573e5b0202eb9",
		        "date_completed" => "2016-08-05 02:35:58",
		        "paypal_correlation_id" => "paypalcorrelationid001",
		        "paypal_pay_key" => "AP-7GC32445PW841084M",
		        "tracking_code" => "",
		        "notes" => "",
		        "products" => [
		            [
		                "product_id" => "5757bfc15b77c",
		                "price" => 4
		            ], [
		                "product_id" => "5757be522be2d",
		                "price" => 269
		            ], [
		                "product_id" => "5757c0580f2d3",
		                "price" => 360
		            ]
		        ],
		        "shipping" => [
		            "address1" => "address 1",
		            "address2" => "",
		            "city" => "city001",
		            "state" => "state",
		            "post_code" => "9123"   
		        ]
            ];
            $data['request'] = array('api' => $this->theMethod, 'build' => $this->build, 'data' => $params);
            $data['response'] = array(
            	"status" => 'success',
            	"message" => trans('messages.api.orders.success'),
            	"api" => $this->theMethod,
            	"data" => [
            		"message" => "Your order has been received and is now being processed. Your order details are shown below for your reference",
				    "order" => [
						"order_id" => "57a4030e6d85f",
						"customer_id" => "5757cc0ae2bf2",
						"invoice_id" => "invoiceid001",
						"total" => "4.75",
						"date_cancelled" => "",
						"date_completed" => "2016-08-05 02:35:58",
						"date_refunded" => "",
						"paypal_correlation_id" => "paypalcorrelationid001",
						"paypal_pay_key" => "AP-7GC32445PW841084M",
						"paypal_payer_email" => "",
						"paypal_payer_id" => "",
						"tracking_code" => "",
						"token" => "ZbNxKCAujqo58cJg0Y2fQPIUHDM4TXhV3Wi9F6OyetsB1wknvpamErSLGz7l",
						"notes" => "",
						"date_created" => "2016-08-05 03:07:58",
						"order_status" => "Completed",
						"shipping_address_1" => "address 1",
						"shipping_address_2" => "",
						"shipping_city" => "city001",
						"shipping_state" => "state",
						"shipping_post_code" => "9123",
						"transaction_id" => "",
						"transaction_status" => "",
						"receiver_amount" => "",
						"receiver_email" => "",
						"receiver_primary" => "",
						"receiver_invoice_id" => "",
						"receiver_payment_type" => "",
						"receiver_account_id" => "",
						"refunded_amount" => "",
						"sender_transaction_id" => "",
						"sender_transaction_status" => "",
						"customer_email" => "gab01penn@gmail.com",
						"customer_name" => "Gabrielle Pennefather",
						"customer_first_name" => "Gabrielle",
						"customer_last_name" => "Pennefather",
						"customer_telephone" => "(07) 5304 8021",
						"customer_mobile" => "",
						"ordered_products" => [
					        [
								"product_id" => "5757be522be2d",
								"product_title" => "Yamaha Oak custom 12\" tom 2014 black",
								"product_price" => "269.00",
								"owner_email" => "summer01white@gmail.com",
								"owner_name" => "Summer  White-Haney"
					        ], [
								"product_id" => "5757bfc15b77c",
								"product_title" => "James Tyler Studio Elite HD - Burning Water 2K 2016 maple",
								"product_price" => "4.00",
								"owner_email" => "howard@gmail.com",
								"owner_name" => "Howard Balls"
					        ], [
								"product_id" => "5757c0580f2d3",
								"product_title" => "DOD DOD 250 Analog Overdrive Preamp VINTAGE 70s black",
								"product_price" => "360.00",
								"owner_email" => "summer01white@gmail.com",
								"owner_name" => "Summer  White-Haney"
					        ]
				      	],
				      	"order_history" => [
					        [
					          "order_history_id" => "57a4030e6f778",
					          "description" => "Order has been successfully created via App",
					          "created_at" => "2016-08-05 03:07:58"
					        ]
				      	]
				    ]
            	]
			);

            return $this->response->setMessage('Success')->setData($data)->setSuccess()->display();
        }

        return view('api.method_new', compact('title', 'method', 'notes', 'url'));
    }

}
