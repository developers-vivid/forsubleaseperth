<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Vinkla\Pusher\PusherManager;
use Carbon\Carbon;
use Auth;

use \App\Helper as Hlpr;

use App\User;
use App\UsersInbox;
use App\UsersMessages;

class MessageController extends Controller {

	protected $pusher;

	/**
	 * [__construct description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-13T11:55:16+0800]
	 * @param  PusherManager $pusher [description]
	 */
	public function __construct( PusherManager $pusher ){
		$this->pusher = $pusher;
		$this->middleware('auth', ['except' => ['showContactSellerForm']]);
	}

	/**
	 * Message displayer
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-16T10:39:02+0800]
	 * @param  [type] $title   [description]
	 * @param  [type] $message [description]
	 * @param  [type] $status  [description]
	 * @return [type]          [description]
	 */
	public function showMessage($title, $message, $status) {
		return view('success.global-message', compact('title', 'message', 'status'));
	}

	/**
	 * [showChat description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-09-07T12:44:40+0800]
	 * @param  [type] $channelId [description]
	 * @return [type]            [description]
	 */
	public function showChat( $channelId ) {
		$inboxDetails = UsersInbox::getDetails( null, $channelId, Auth::user()->user_id );
		if( $inboxDetails ) {
			// dd($inboxDetails);
			$messages = UsersMessages::getInboxMessages( $inboxDetails->inbox_id );
			$userDetails = User::getFullDetails( $inboxDetails->receiver );

			// set to `read`
			UsersMessages::where(['inbox_id' => $inboxDetails->inbox_id])->update(['status' => 'read', 'updated_at' => Carbon::now()]);

			return view('messages.chat', compact('inboxDetails', 'messages', 'userDetails'));
		}

		return view('errors.no-record-found', ['message' => 'No record found']);
	}

	/**
	 * To show contact seller form
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-13T12:01:45+0800]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function showContactSellerForm( Request $request ){
		if( Auth::check() ) {
			if( $request->has('seller') && $request->seller != '' ) {
				$user = User::whereUserId($request->seller)->first();
				return Hlpr::handleResponse('Success', false, ['contact_form' => view('mail.contact-seller', ['user' => $user])->render()]);
			}
			return Hlpr::handleResponse('You must specify the receiver');
		}
		return Hlpr::handleResponse('You must login first before contacting the seller');
	}

	/**
	 * Ctrl to get user's messages
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-10T10:27:01+0800]
	 */
	public function showMessages(){
		$messages = UsersMessages::getUserInbox();
		return view('users.messages');
	}

	/**
	 * To get users senders
	 * This happens only in using pusher
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-13T12:43:21+0800]
	 * @return [type] [description]
	 */
	public function getUserSenders(){
		$userInbox = UsersMessages::getUserInbox();
		return Hlpr::handleResponse('User inbox has been successfully loaded.', false, ['inbox' => $userInbox, 'sender_html' => view('users.user.senders', ['messages' => $userInbox])->render()]);
	}

	/**
	 * Display message
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-10T13:34:16+0800]
	 * @param  [type] $inboxId [description]
	 * @return [type]          [description]
	 */
	public function displayInboxMessages( Request $request ){
		if( $request->has('inbox') && $request->inbox != '' ) {
			$inboxDetails = UsersMessages::getUserInbox( $request->inbox );
			$messages = UsersMessages::getInboxMessages( $request->inbox );

			// mark inbox messages into read
			UsersMessages::where(['inbox_id' => $request->inbox, 'receiver_id' => Auth::user()->user_id])->update(['status' => 'read', 'updated_at' => Carbon::now()]);

			return Hlpr::handleResponse('Messages has been successfully loaded.', false, ['user' => ($inboxDetails->receiver_id == Auth::user()->user_id ? $inboxDetails->sender_id : $inboxDetails->receiver_id), 'inbox' => $request->inbox, 'total_unread_messages' => UsersMessages::getTotalUsersUnReadMessages(), 'msg_html' => view('users.user.messages', ['inbox' => $inboxDetails, 'messages' => $messages])->render()]);
		}
		return Hlpr::handleResponse('Could not access messages. Please try again later.');
	}

	/**
	 * Handle send message
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-10T13:13:49+0800]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function sendMessage( Request $request ){
		if( $request->has('inbox') && $request->inbox != '' ) {
			return \App\Messaging::sendMessage( $this->pusher, $request->inbox, Auth::user()->user_id, $request->receiver, Auth::user()->name, $request->socket_id, $request->message );
		}
		return Hlpr::handleResponse( 'No inbox specified. Please try again later.' );
	}

	/**
	 * To send a message to seller
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-13T12:59:20+0800]
	 * @return [type] [description]
	 */
	public function sendMessageToSeller( Request $request ){
		if( $request->has('seller') && $request->has('message') ) {
			return \App\Messaging::contactSeller($this->pusher, Auth::user()->user_id, $request->seller, Auth::user()->name, $request->socket_id, $request->message);
		}
		else {
			return Hlpr::handleResponse( 'Error! Could not send a message. Please try again later.' );
		}
	}

	/**
	 * To get single message
	 * Serves as a template
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-13T12:59:07+0800]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function getSingleMessage( Request $request ){
		if( $request->has('message_id') && $request->message_id != '' ) {
			$theMsg = UsersMessages::getInboxMessages( null, $request->message_id );

			// set to `read` if on current inbox
			if( $request->has('update_msg') && strtolower($request->update_msg) == 'yes' )
				UsersMessages::where(['message_id' => $request->message_id])->update(['status' => 'read', 'updated_at' => Carbon::now()]);

			return Hlpr::handleResponse( 'Message has been successfully fetched.', false, ['message' => view('layouts.single-message', ['msg' => $theMsg[0]])->render()] );
		}
		else {
			return Hlpr::handleResponse( 'Error! Could not fetch the message. Please try again.' );
		}
	}

	/**
	 * To get users' message counter
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-16T08:20:02+0800]
	 * @return [type] [description]
	 */
	public function getMessageCounter( Request $request ){
		if( Auth::check() ){

			$inboxDetails = UsersInbox::whereInboxId( $request->inbox )->first();

			// set to `read` if on current inbox
			if( $request->has('msg_id') && $request->msg_id != '' )
				UsersMessages::where(['message_id' => $request->msg_id])->update(['status' => 'read', 'updated_at' => Carbon::now()]);

			return Hlpr::handleResponse('Success', false, ['channel' => $inboxDetails->channel_id, 'total' => UsersMessages::getTotalUsersUnReadMessages( (string)Auth::user()->user_id )]);
		}
	}
}
