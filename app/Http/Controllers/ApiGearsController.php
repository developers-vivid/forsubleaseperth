<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use Carbon\Carbon;
use Validator;
use Image;
use Eventviva\ImageResize;
use DB;

// helpers
use App\Helper as Hlpr;
use App\GpLog as Log;

use App\User;
use App\Products;
use App\ProductsSpecs;
use App\ProductsPhotos;
use App\ProductsShipping;

class ApiGearsController extends Controller {
    protected $data, $response, $user, $notification;

	/**
	 * [__construct description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-04T09:22:41+0800]
	 * @param  Request $request [description]
	 */
    public function __construct( Request $request ){
        $content = json_decode($request->getContent());
        $this->data = $content->data;
        $this->response = new \App\Message( (isset($content->api) && $content->api != '' ? $content->api : '') );

        if( $request->has('api_token') && $request->api_token != '' )
            $this->user = User::getFullDetails( Auth::guard('api')->user()->user_id );

        // $this->notification = new \App\Notif();
    }
    
    /**
     * [getGearsList description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-03T14:58:06+0800]
     * @return [type] [description]
     */
	public function getGearsList() {
		return $this->response->setMessage( trans('messages.api.gears.loaded') )->setData(Hlpr::spliceArr(Products::getGearsByCategory( $this->data->category, ($this->data->sub_category != '' ? $this->data->sub_category : null), $this->data->type, $this->data->location, $this->data->brand, $this->data->price_min, $this->data->price_max, $this->data->sort, true ), $this->data->page))->setSuccess()->display();
	}

	/**
	 * [getSearchedGear description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-03T15:09:57+0800]
	 * @return [type] [description]
	 */
	public function getSearchedGear() {
		return $this->response->setMessage( trans('messages.api.gears.loaded') )->setData(Hlpr::spliceArr(Products::searchGears( $this->data->keyword, $this->data->type, $this->data->location, $this->data->brand, $this->data->price_min, $this->data->price_max, $this->data->sort, true), $this->data->page))->setSuccess()->display();
	}

	/**
	 * [getMyGears description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-04T09:18:03+0800]
	 * @return [type] [description]
	 */
	public function getMyGears() {
		return $this->response->setMessage( trans('messages.api.user.ads.loaded') )->setData( Hlpr::spliceArr(Products::getMyAds($this->data->status, $this->data->type, $this->data->location, $this->data->brand, $this->data->price_min, $this->data->price_max, $this->data->sort, $this->user->user_id, true), $this->data->page) )->setSuccess()->display();
	}

	/**
	 * [getGearDetails description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-04T09:46:41+0800]
	 * @return [type] [description]
	 */
	public function getGearDetails() {
		$validator = Validator::make((array)$this->data, ["product_blurb" => 'required']);
        $error = $validator->errors();

        if( count($error) > 0 )
        	return $this->response->setMessage( $error->first() )->display();

        $gear = Products::getProductDetails(null, $this->data->product_blurb);
        if( is_object($gear) && count((array)$gear) > 0 ) {
            $relatedItems = Products::getRelatedItems( $gear->product_id, $gear->category_id );
            return $this->response->setMessage( trans('messages.api.gears.loaded') )->setData( ['gears_details' => $gear, 'related_items' => $relatedItems] )->setSuccess()->display();
        }

        return $this->response->setMessage( trans('messages.api.gears.not_found') )->display();
	}

	/**
	 * [sellGear description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-08T10:07:26+0800]
	 * @return [type] [description]
	 */
	public function sellGear() {
		$theRequest = (array)$this->data;
        $theRequest['listing_title'] = str_slug($theRequest['listing_title'], "-");
        $theRequest['shipping'] = (array)$theRequest['shipping'];

		$validator = Validator::make($theRequest, [
            "category"        => 'required',	// sub_category is optional
            "condition"       => 'required',
            "brand"           => 'required|max:100',
            "model"           => 'required|max:100',
            "year"            => 'required|max:4',
            "colour"          => 'required|max:30',
            "made"            => 'required|max:100',
            "type"            => 'required',
            "listing_title"   => 'required|max:255|unique:products,product_blurb',
            "price"           => 'required',
            "sale_price"      => 'required',
            "paypal_email"    => 'required|email|max:255',
            "description"     => 'required|min:10',
            "shopping_policy" => 'required|min:10',
            "shipping.city"   => 'required|max:100',
            "shipping.state"  => 'required|max:100',
            "shipping.policy" => 'required|min:10',
            "shipping.method" => 'required',
            "shipping.cost"   => 'required|numeric',
            "photos"          => 'required',
            "agree"           => 'required'
		]);
        $error = $validator->errors();

        if( count($error) > 0 )
            return $this->response->setMessage( $error->first() )->display();

        if( strtolower($this->data->agree) != 'yes' )
        	return $this->response->setMessage( trans('messages.user.must_agree.on_signup') )->display();

        $createdAt = Carbon::now();
        $listingTitle = str_slug($this->data->listing_title, "-");
        DB::beginTransaction();

        $gear = Products::create([
            'product_id'          => Hlpr::getUID(),
            'product_title'       => $this->data->listing_title,
            'product_blurb'       => $listingTitle,
            'product_description' => $this->data->description,
            'product_price'       => $this->data->price,
            'product_sale_price'  => ($this->data->sale_price == "" ? null : $this->data->sale_price),
            'shipping_cost'       => ($this->data->shipping->cost == "" ? null : $this->data->shipping->cost),
            'accept_offers'       => (strtolower($this->data->accept_offers) == "yes" ? 'yes' : 'no'),
            'paypal_email'        => $this->data->paypal_email,
            'shopping_policy'     => $this->data->shopping_policy,
            'created_by'          => $this->user->user_id,
            'created_at'          => $createdAt
        ]);

        // create product specs
        $gearSpec = ProductsSpecs::create([
            'product_specs_id'      => Hlpr::getUID(),
            'product_id'            => $gear->product_id,
            'product_condition'     => $this->data->condition,
            'product_brand'         => $this->data->brand,
            'product_model'         => $this->data->model,
            'product_finish'        => null,
            'product_categories'    => $this->data->category,
            'product_subcategories' => $this->data->sub_category,
            'product_year'          => $this->data->year,
            'product_colour'        => $this->data->colour,
            'product_made_in'       => $this->data->made,
            'product_type'          => $this->data->type,
            'created_at'            => $createdAt
        ]);

        $productShippingInfo = ProductsShipping::create([
            "product_shipping_id" => Hlpr::getUID(),
            "product_id" => $gear->product_id,
            "country" => 'Australia',
            "city" => $this->data->shipping->city,
            "state" => $this->data->shipping->state,
            "policy" => $this->data->shipping->policy,
            "method" => $this->data->shipping->method,
            "created_at" => $createdAt
        ]);

        // photos
        if( $this->data->photos != '' && count($this->data->photos) > 0 ) {
            for($i = 0; $i < count($this->data->photos); $i++){
                $ext = Hlpr::getMimeTypeFromBase64( $this->data->photos[$i] );
                if( ! is_null( $ext ) ) {
                    $img           = Image::make($this->data->photos[$i]);
                    $gearPhotoName = substr(str_shuffle(config('gp_conf.alpha_num')), 0, 20) .'.'. $ext;
                    $gearPath      = config('gp_conf.gear_photo_url');
                    $gearImage     = $gearPath . $gearPhotoName;

                    // save image
                    $img->save($gearImage);

                    if( file_exists( $gearImage ) ) {
                        $photo = ProductsPhotos::create([
                            "photo_id"       => Hlpr::getUID(),
                            "product_id"     => $gear->product_id,
                            "photo_title"    => $gear->product_title .' - '. $i,
                            "photo_filename" => $gearPhotoName,
                            "created_at"     => Carbon::now(),
                        ]);

                        // Create the product's primary photo
                        Products::where('product_id', $gear->product_id)->whereNull('product_primary_photo')->update(['product_primary_photo' => $photo->photo_id]);
                        
                        // create thumbnail 200x200
                        $image = new ImageResize($gearImage);
                        $image->crop(200, 200);
                        $image->save( $gearPath . 'thumbs' .config('gp_conf.ds'). $gearPhotoName);  
                    }
                }
            }
        }

        if( $gear && $gearSpec && $productShippingInfo ) {
            DB::commit();
            return $this->response->setMessage( trans('messages.gear.created') )->setData( ['gear_id' => $gear->product_id, 'listing_title' => $listingTitle] )->setSuccess()->display();
        }
        else {
            DB::rollback();
            return $this->response->setMessage( trans('messages.gear.not_created') )->display();            
        }
	}

    /**
     * [updateGear description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-08T11:15:36+0800]
     * @return [type] [description]
     */
    public function updateGear() {
        $theRequest = (array)$this->data;
        $theRequest['shipping'] = (array)$theRequest['shipping'];

        $validator = Validator::make($theRequest, [
            "gear_id"         => 'required',
            "category"        => 'required',    // sub_category is optional
            "condition"       => 'required',
            "brand"           => 'required|max:100',
            "model"           => 'required|max:100',
            "year"            => 'required|max:4',
            "colour"          => 'required|max:30',
            "made"            => 'required|max:100',
            "type"            => 'required',
            "listing_title"   => 'required|max:255',
            "price"           => 'required',
            "sale_price"      => 'required',
            "paypal_email"    => 'required|email|max:255',
            "description"     => 'required|min:10',
            "shopping_policy" => 'required|min:10',
            "shipping.city"   => 'required|max:100',
            "shipping.state"  => 'required|max:100',
            "shipping.policy" => 'required|min:10',
            "shipping.method" => 'required',
            "shipping.cost"   => 'required|numeric',
            "photos"          => 'required'
        ]);
        $error = $validator->errors();

        if( count($error) > 0 )
            return $this->response->setMessage( $error->first() )->display();

        $adDetails = Products::whereProductId( $this->data->gear_id )->first();
        if( ! $adDetails )
            return $this->response->setMessage( trans('messages.gear.not_found') )->display();
        
        if( Auth::guard('api')->user()->cannot('update-ad', $adDetails) )
            return $this->response->setMessage( trans('messages.gear.update_not_allowed') )->display();

        $listingTitle = str_slug($this->data->listing_title, "-");
        $ad = Products::whereProductBlurb( $listingTitle )->count();

        if( $ad > 1 )
            return $this->response->setMessage( trans('messages.gear.listing_title_taken') )->display();

        $updatedAt = Carbon::now();

        // update gear
        $gear = Products::where(['product_id' => $this->data->gear_id])->update([
            'product_title'       => $this->data->listing_title,
            'product_blurb'       => $listingTitle,
            'product_description' => $this->data->description,
            'product_price'       => $this->data->price,
            'product_sale_price'  => ($this->data->sale_price == "" ? null : $this->data->sale_price),
            'shipping_cost'       => ($this->data->shipping->cost == "" ? null : $this->data->shipping->cost),
            'accept_offers'       => (strtolower($this->data->accept_offers) == "yes" ? 'yes' : 'no'),
            'paypal_email'        => $this->data->paypal_email,
            'shopping_policy'     => $this->data->shopping_policy,
            'updated_at'          => $updatedAt            
        ]);

        // update product specs
        $gearSpec = ProductsSpecs::where(['product_id' => $this->data->gear_id])->update([
            'product_condition'     => $this->data->condition,
            'product_brand'         => $this->data->brand,
            'product_model'         => $this->data->model,
            'product_finish'        => null,
            'product_categories'    => $this->data->category,
            'product_subcategories' => $this->data->sub_category,
            'product_year'          => $this->data->year,
            'product_colour'        => $this->data->colour,
            'product_made_in'       => $this->data->made,
            'product_type'          => $this->data->type,
            'updated_at'            => $updatedAt
        ]);

        $productShippingInfo = ProductsShipping::where(['product_id' => $this->data->gear_id])->update([
            "country"             => 'Australia',
            "city"                => $this->data->shipping->city,
            "state"               => $this->data->shipping->state,
            "policy"              => $this->data->shipping->policy,
            "method"              => $this->data->shipping->method,
            "updated_at"          => $updatedAt
        ]);

        // added photos under photos->added
        $addedPhotos = [];
        if( isset($this->data->photos->added) && count($this->data->photos->added) > 0 ) {
            for($i = 0; $i < count($this->data->photos->added); $i++){
                $ext = Hlpr::getMimeTypeFromBase64( $this->data->photos->added[$i] );
                if( ! is_null( $ext ) ) {
                    $img           = Image::make($this->data->photos->added[$i]);
                    $gearPhotoName = substr(str_shuffle(config('gp_conf.alpha_num')), 0, 20) .'.'. $ext;
                    $gearPath      = config('gp_conf.gear_photo_url');
                    $gearImage     = $gearPath . $gearPhotoName;

                    // save image
                    $img->save($gearImage);

                    if( file_exists( $gearImage ) ) {
                        $photo = ProductsPhotos::create([
                            "photo_id"       => Hlpr::getUID(),
                            "product_id"     => $this->data->gear_id,
                            "photo_title"    => $this->data->listing_title .' - '. Hlpr::getUID(),
                            "photo_filename" => $gearPhotoName,
                            "created_at"     => Carbon::now(),
                        ]);

                        // add to added photos
                        $addedPhotos[] = $photo->photo_id;

                        // create thumbnail 200x200
                        $image = new ImageResize($gearImage);
                        $image->crop(200, 200);
                        $image->save( $gearPath . 'thumbs' .config('gp_conf.ds'). $gearPhotoName);  
                    }
                }
            }
        }

        // delete photos if has to be deleted
        if( isset($this->data->photos->deleted) && count($this->data->photos->deleted) > 0 ) {
            for( $i = 0; $i < count($this->data->photos->deleted); $i++ ){
                $deletePhoto = Hlpr::removeGearPhoto( $this->data->photos->deleted[$i] );
            }
        }

        // set primary photo is photos.primary has a value
        if( isset($this->data->photos->primary) && is_numeric($this->data->photos->primary) && isset($addedPhotos[$this->data->photos->primary]) )
            Products::where(['product_id' => $this->data->gear_id])->update(['product_primary_photo' => $addedPhotos[$this->data->photos->primary], 'updated_at' => Carbon::now()]);

        else if( isset($this->data->photos->primary) && is_string($this->data->photos->primary) && $this->data->photos->primary != "" )
            Products::where(['product_id' => $this->data->gear_id])->update(['product_primary_photo' => $this->data->photos->primary, 'updated_at' => Carbon::now()]);

        return $this->response->setMessage( trans('messages.gear.updated') )->setSuccess()->display();
    }

    /**
     * [cancelGear description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-08T11:15:47+0800]
     * @return [type] [description]
     */
    public function cancelGear() {
        $validator = Validator::make((array)$this->data, ["gear_id" => 'required']);
        $error = $validator->errors();

        if( count($error) > 0 )
            return $this->response->setMessage( $error->first() )->display();

        $ad = Products::where(['product_id' => $this->data->gear_id])->first();

        if( ! $ad )
            return $this->response->setMessage( trans('messages.gear.not_found') )->display();

        if( Auth::guard('api')->user()->cannot('cancel-ad', $ad) )
            return $this->response->setMessage( trans('messages.gear.cancel.not_allowed') )->display();

        $ad->status = 'cancelled';
        $ad->cancelled_at = Carbon::now();
        $ad->updated_at = Carbon::now();

        $cancelled = $ad->save();

        if( $cancelled )
            return $this->response->setMessage( trans('messages.gear.cancel.success') )->setSuccess()->display();
        else
            return $this->response->setMessage( trans('messages.gear.cancel.error') )->display();
    }

    /**
     * [deleteGear description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-08T11:15:58+0800]
     * @return [type] [description]
     */
    public function deleteGear() {
        $validator = Validator::make((array)$this->data, ["gear_id" => 'required']);
        $error = $validator->errors();

        if( count($error) > 0 )
            return $this->response->setMessage( $error->first() )->display();

        $ad = Products::where(['product_id' => $this->data->gear_id])->first();

        if( ! $ad )
            return $this->response->setMessage( trans('messages.gear.not_found') )->display();

        if( Auth::guard('api')->user()->cannot('delete-ad', $ad) )
            return $this->response->setMessage( trans('messages.gear.delete.not_allowed') )->display();

        $ad->status = 'inactive';
        $ad->cancelled_at = null;
        $ad->deleted_at = Carbon::now();
        $ad->updated_at = Carbon::now();

        $deleted = $ad->save();

        if( $deleted )
            return $this->response->setMessage( trans('messages.gear.delete.success') )->setSuccess()->display();
        else
            return $this->response->setMessage( trans('messages.gear.delete.error') )->display();
    }

    /**
     * [activateGear description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-08T11:16:12+0800]
     * @return [type] [description]
     */
    public function activateGear() {
        $validator = Validator::make((array)$this->data, ["gear_id" => 'required']);
        $error = $validator->errors();

        if( count($error) > 0 )
            return $this->response->setMessage( $error->first() )->display();

        $ad = Products::where(['product_id' => $this->data->gear_id])->first();

        if( ! $ad )
            return $this->response->setMessage( trans('messages.gear.not_found') )->display();

        if( Auth::guard('api')->user()->cannot('activate-ad', $ad) )
            return $this->response->setMessage( trans('messages.gear.activate.not_allowed') )->display();

        $ad->status = 'active';
        $ad->sold_at = null;
        $ad->deleted_at = null;
        $ad->cancelled_at = null;
        $ad->updated_at = Carbon::now();

        $deleted = $ad->save();

        if( $deleted )
            return $this->response->setMessage( trans('messages.gear.activate.success') )->setSuccess()->display();
        else
            return $this->response->setMessage( trans('messages.gear.activate.error') )->display();
    }

}
