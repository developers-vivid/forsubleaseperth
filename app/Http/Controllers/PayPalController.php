<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use PayPal\Core\PPHttpConfig;
use PayPal\Service\AdaptivePaymentsService;
use PayPal\Types\AP\PayRequest;
use PayPal\Types\AP\Receiver;
use PayPal\Types\AP\ReceiverList;
use PayPal\Types\Common\RequestEnvelope;

use App\Http\Requests;
use \App\GpLog as Log;

use PayPal;
use Redirect;

class PayPalController extends Controller {
    private $_apiContext;

    /**
     * [__construct description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-18T15:10:48+0800]
     */
	public function __construct() {
		$this->middleware('auth');
    }

    /**
     * PayPal Adaptive Payment configuration
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-18T15:10:53+0800]
     * @return [type] [description]
     */
    public function config() {
        return [
            'mode' => 'sandbox',
            'acct1.AppId' => 'APP-80W284485P519543T',
            'acct1.UserName' => 'richmund-facilitator_api1.sushidigital.com.au',
            'acct1.Password' => 'DWL92ZBZLYTVS325',
            'acct1.Signature' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31AhwR1UotWjj8kmOL8L9DGpmEnWtR',
        ];
    }

    /**
     * Redirect to PayPal
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-18T14:36:47+0800]
     * @param  string $payKey [description]
     * @return [type]         [description]
     */
    public function redirectToPayPal($payKey = '', $sandbox = true) {
        if( $sandbox )
            return redirect('https://www.sandbox.paypal.com/webapps/adaptivepayment/flow/pay?expType=light&payKey='.$payKey);
        else
            return redirect('https://www.paypal.com/webscr?cmd=_ap-payment&paykey=' . $payKey);
    }

    /**
     * Handles paypal success transactions
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-18T15:11:14+0800]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getDone(Request $request) {
        dd($request->all());
        // $id = $request->get('paymentId');
        // $token = $request->get('token');
        // $payer_id = $request->get('PayerID');

        // $payment = PayPal::getById($id, $this->_apiContext);

        // $paymentExecution = PayPal::PaymentExecution();

        // $paymentExecution->setPayerId($payer_id);
        // $executePayment = $payment->execute($paymentExecution, $this->_apiContext);

        // // Clear the shopping cart, write to database, send notifications, etc.

        // // Thank the user for the purchase
        // return view('checkout.done');
    }

    /**
     * Handles PayPal cancelled transactions
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-18T15:11:28+0800]
     * @return [type] [description]
     */
    public function getCancel() {
        // Curse and humiliate the user for cancelling this most sacred payment (yours)
        return view('checkout.cancel');
    }

    /**
     * Show checkout
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-18T14:37:09+0800]
     * @return [type] [description]
     */
    public function showCheckout() {
        /*$requestEnvelope = ['errorLanguage' => 'fr_FR'];
        $actionType = 'PAY';
        $cancelUrl = route('paypal::cancel');
        $returnUrl = route('paypal::done');

        $currencyCode = 'AUD';

        // Your request
        $_POST['receiverEmail'] = ['someone@example.com'];
        $_POST['receiverAmount'] = ['3.00'];

        $receiver = [];
        for ($i = 0; $i < count($_POST['receiverEmail']); ++$i) {
            // Parallel Payments
            $receiver[$i] = new Receiver();
            $receiver[$i]->email = $_POST['receiverEmail'][$i];
            $receiver[$i]->amount = $_POST['receiverAmount'][$i];
        }
        $receiverList = new ReceiverList($receiver);
        $payRequest = new PayRequest($requestEnvelope, $actionType, $cancelUrl,
            $currencyCode, $receiverList, $returnUrl);

        // Set the correct the value 1, 3, 4, ...
        // PPHttpConfig::$DEFAULT_CURL_OPTS[CURLOPT_SSLVERSION] = 4;

        $config = $this->config();
        $service = new AdaptivePaymentsService($config);
        $response = $service->Pay($payRequest);

        if (strtoupper($response->responseEnvelope->ack) == 'FAILURE') {
            dd($response->error);
        }
        if (strtoupper($response->responseEnvelope->ack) == 'SUCCESS') {
            return $this->redirect($response->payKey);
        }

        return view('payment.checkout');*/
        $ppLog = new Log('gp_paypal');

        try {
            PPHttpConfig::$DEFAULT_CURL_OPTS[CURLOPT_SSLVERSION] = 6;
            $payRequest = new PayRequest();
            $gearPrice = 9;
            $gpShares = ($gearPrice * config('gp_conf.prcnt_share'));
            $invoiceID = substr(str_shuffle(config('gp_conf.alpha_num')), 0, 60);

            $receiver = array();
            $receiver[0] = new Receiver();
            $receiver[0]->amount = $gpShares;
            $receiver[0]->email = "gearplanetbuyer001@gmail.com";
            $receiver[0]->invoiceId = $invoiceID;

            $receiver[1] = new Receiver();
            $receiver[1]->amount = ($gearPrice - $gpShares);
            $receiver[1]->email = "gearplanetbuyer002@gmail.com";
            $receiver[1]->primary = "true";
            $receiver[1]->invoiceId = $invoiceID;

            $receiverList = new ReceiverList($receiver);
            $payRequest->receiverList = $receiverList;

            $requestEnvelope = new RequestEnvelope("en_AU");
            $payRequest->requestEnvelope = $requestEnvelope;
            $payRequest->actionType = "PAY";
            $payRequest->cancelUrl = route('paypal::cancel');
            $payRequest->returnUrl = route('paypal::done');
            $payRequest->currencyCode = "AUD";
            $payRequest->ipnNotificationUrl = route('paypal::get-transaction');

            $adaptivePaymentsService = new AdaptivePaymentsService( config('gp_conf.pp_adaptive_payment') );
            $payResponse = $adaptivePaymentsService->Pay($payRequest);

            if ($payResponse->responseEnvelope->ack == "Success") {
                dd($payResponse);
                return $this->redirectToPayPal($payResponse->payKey);
            } else {
                foreach($payResponse->error as $error){
                    $ppLog->error('Adaptive Payments: ['. $error->errorId .']['. $error->category .' '. $error->severity .'! '. $error->message .']');
                }
                $ppLog->endLog();

                return redirect('/');
            }
        }   catch(\PayPal\Exception\PPConnectionException $e){
            $ppLog->error( $e->getMessage() )->endLog();
            echo $e->getMessage();
        }
    }

}
