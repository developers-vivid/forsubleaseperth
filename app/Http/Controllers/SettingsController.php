<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Session;

class SettingsController extends Controller {

	/**
	 * [__construct description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-04-27T13:27:57+0800]
	 */
	public function __construct() {

	}
  	
  /**
   * [changeSearchView description]
   * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
   * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-04-27T13:28:04+0800]
   * @return [type] [description]
   */
	public function changeSearchView( Request $request ) {
		if( $request->has('view') ) {
			if( ! Session::has('searchview') || strtolower(Session::get('searchview')) != strtolower($request->view) ) {
				Session::put('searchview', strtolower($request->view));
				return \App\Helper::handleResponse('Search view has been changed', false);
			}
		}
		return \App\Helper::handleResponse('Nothing to change in Search View');
	}

	public function getSubCategory( Request $request ){
		if( $request->has('category') ) {
			$options = \App\Categories::whereParent( $request->get('category') )->get();
			return \App\Helper::handleResponse('Success', false, ['html' => view('layouts.gears.sub-category-options', ['options' => $options])->render()]);
		}
		return \App\Helper::handleResponse('Nothing to fetch');
	}

}
