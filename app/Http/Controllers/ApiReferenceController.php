<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class ApiReferenceController extends Controller {
        
    protected $response, $theMethod;

    /**
     * [__construct description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-17T11:49:57+0800]
     */
    public function __construct() {
        // 
    }

    /**
     * Api Doc landing page
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-17T11:54:31+0800]
     * @return [type] [description]
     */
    public function index(){
    	return view('api.list');
    }

    /**
     * Handles app details
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-17T13:00:24+0800]
     * @param  [type] $method [description]
     * @return [type]         [description]
     */
    public function showApiDetails( Request $request, $method ){
        $this->theMethod = $method;
        $newMethod = studly_case(str_replace("-", "_", $method));
        if( method_exists($this, $newMethod) ) {
            return $this->$newMethod( $request );
        }
        else {
            return \App\Helper::handleResponse(ucfirst($method) .' method does not exist');
        }
    }

    /**
     * [GetCategories description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-17T13:12:03+0800]
     */
    protected function GetCategories( $request ){
        $title = 'Get Categories';
        $method = $this->theMethod;

        if( $request->isMethod('post') ) {
            $data = array();
            $data['request'] = array('api' => $this->theMethod, 'data' => []);
            $data['response'] = $this->getResponse('Categories has been loaded.', false, ['categories' => \App\Helper::getCategories()]);

            return \App\Helper::handleResponse( 'Success', false, $data );
        }
        return view('api.method', compact('title', 'method'));
    }

    /**
     * [GetGears description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-17T14:06:27+0800]
     * @param  [type] $request [description]
     */
    protected function GetGears( $request ){
        $title = 'Get Gears';
        $method = $this->theMethod;

        if( $request->isMethod('post') ) {
            $data = array();

            $params = (object)[
                "category"     => "acoustic-guitars",
                "sub_category" => "",
                "location"     => "all",
                "brand"        => "all",
                "price_min"    => "",
                "price_max"    => "",
                "type"         => "all",
                "sort"         => "created_at|desc",
                "page"         => 1
            ];
            $data['request'] = array('api' => $this->theMethod, 'data' => $params);
            $data['response'] = $this->getResponse('Gears has been loaded.', false, \App\Helper::spliceArr(\App\Products::getGearsByCategory( $params->category, ($params->sub_category != '' ? $params->sub_category : null), $params->type, $params->location, $params->brand, $params->price_min, $params->price_max, $params->sort, true ), $params->page));

            return \App\Helper::handleResponse( 'Success', false, $data );
        }
        return view('api.method', compact('title', 'method'));
    }

    /**
     * [SearchGears description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-17T14:07:00+0800]
     * @param  [type] $request [description]
     */
    protected function SearchGears( $request ){
        $title = 'Search Gears';
        $method = $this->theMethod;

        if( $request->isMethod('post') ) {
            $data = array();

            $params = (object)[
                "keyword"   => "g",
                "type"      => "all",
                "location"  => "all",
                "brand"     => "all",
                "price_min" => "",
                "price_max" => "",
                "sort"      => "created_at|desc",
                "page"      => 1
            ];
            $data['request'] = array('api' => $this->theMethod, 'data' => $params);
            $data['response'] = $this->getResponse('Gears has been loaded.', false, \App\Helper::spliceArr(\App\Products::searchGears( $params->keyword, $params->type, $params->location, $params->brand, $params->price_min, $params->price_max, $params->sort, true), $params->page));

            return \App\Helper::handleResponse( 'Success', false, $data );
        }
        return view('api.method', compact('title', 'method'));
    }

    /**
     * [SignIn description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-17T14:24:43+0800]
     * @param  [type] $request [description]
     */
    protected function SignIn( $request ) {
        $title = 'Sign In';
        $method = $this->theMethod;

        if( $request->isMethod('post') ) {
            $data = array();

            $params = (object)[
                "email" => "william01fiaschi@gmail.com",
                "password" => "12345678",
                "type" => "local"
            ];
            $data['request'] = array('api' => $this->theMethod, 'data' => $params);
            $data['response'] = $this->getResponse('User has been successfully signed in', false, [
                "user_id" => "57835d4dbd83f",
                "name" => "William Fiaschi",
                "email" => "william01fiaschi@gmail.com",
                "login_type" => "local",
                "api_token" => "N7lVHp6jgzBky4oumwdacseGFOqn1EMY50SCAhxKib8tX2ZJfrQ9DUITPWRv",
                "date_registered" => "2016-07-11 08:48:13",
                "role_name" => "User",
                "first_name" => "William",
                "last_name" => "Fiaschi",
                "telephone" => "",
                "mobile" => "",
                "company_name" => "",
                "company_abn" => "",
                "company_street" => "",
                "company_city" => "",
                "company_state" => "",
                "company_post_code" => "",
                "company_phone" => "",
                "company_logo" => "",
                "shipping_address_1" => "",
                "shipping_address_2" => "",
                "shipping_city" => "",
                "shipping_state" => "",
                "shipping_post_code" => "",
                "total_unread_messages" => 1
            ]);

            return \App\Helper::handleResponse( 'Success', false, $data );
        }
        return view('api.method', compact('title', 'method'));
    }

    /**
     * [SignUp description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-17T14:36:24+0800]
     * @param  [type] $request [description]
     */
    protected function SignUp( $request ){
        $title = 'Sign Up';
        $method = $this->theMethod;

        if( $request->isMethod('post') ) {
            $data = array();

            $params = (object)[
                "first_name"            => "Thomas",
                "last_name"             => "Cruz",
                "email"                 => "thomas001@gmail.com",
                "password"              => "123456",
                "password_confirmation" => "123456",
                "phone"                 => "",
                "mobile"                => "",
                "seller"                => "no",
                "company_name"          => "",
                "company_city"          => "",
                "company_state"         => "",
                "company_post_code"     => "",
                "agreed"                => "yes",
                "type"                  => "local"
            ];
            $data['request'] = array('api' => $this->theMethod, 'data' => $params);
            $data['response'] = $this->getResponse('You have successfully registered', false);

            return \App\Helper::handleResponse( 'Success', false, $data );
        }
        return view('api.method', compact('title', 'method'));
    }


    /**
     * [MyAds description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-17T14:38:27+0800]
     * @param  [type] $request [description]
     */
    protected function MyAds( $request ){
        $title = 'My Ads';
        $method = $this->theMethod;
        $token = "?api_token=W29vbAwOrThmpQkdLgoiEKZPHFlyqUtD0e4G5js3J7SYBXz16aIMC8fNRnu1c";

        if( $request->isMethod('post') ) {
            $data = array();

            $params = (object)[
                "status"    => "all",
                "type"      => "all",
                "location"  => "all",
                "brand"     => "all",
                "price_min" => "",
                "price_max" => "",
                "sort"      => "created_at|desc",
                "page"      => 1
            ];
            $data['request'] = array('api' => $this->theMethod, 'data' => $params);
            $data['response'] = $this->getResponse("User's ads/gears has been loaded", false, \App\Helper::spliceArr(\App\Products::getMyAds($params->status, $params->type, $params->location, $params->brand, $params->price_min, $params->price_max, $params->sort, '5757bac743d02', true), $params->page));

            return \App\Helper::handleResponse( 'Success', false, $data );
        }
        return view('api.method', compact('title', 'method', 'token'));
    }

    /**
     * [SellGear description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-17T15:05:43+0800]
     * @param  [type] $request [description]
     */
    protected function SellGear( $request ) {
        $title = 'Sell Gear';
        $method = $this->theMethod;
        $token = "?api_token=W29vbAwOrThmpQkdLgoiEKZPHFlyqUtD0e4G5js3J7SYBXz16aIMC8fNRnu1c";
        $notes = [
            "If api token is empty, <strong>account</strong> field is required. API token will get and validate the user details."
        ];

        if( $request->isMethod('post') ) {
            $data = array();

            $params = (object)[
                "category" => "56e626f24ce06",
                "sub_category" => "571890184b4bf",
                "condition" => "56e65261b3f8b",
                "brand" => "brand001",
                "model" => "model001",
                "year" => "2001",
                "colour" => "red",
                "made" => "canada",
                "type" => "new",
                "listing_title" => "brand001 model001 2001 red",
                "price" => 135,
                "sale_price" => 130,
                "paypal_email" => "gearplanetbuyer001@gmail.com",
                "accept_offers" => "yes",
                "description" => "brand001 model001 2001 red description",
                "shopping_policy" => "brand001 model001 2001 red shopping policy",
                "shipping_city" => "city",
                "shipping_state" => "nsw",
                "shipping_policy" => "brand001 model001 2001 red shipping policy",
                "shipping_method" => "shipping",
                "shipping_cost" => [
                    array(
                        "location" => "Metro Areas",
                        "cost" => 7.75
                    ), array(
                        "location" => "Regional Areas",
                        "cost" => 0
                    ), array(
                        "location" => "Custom Shipping",
                        "cost" => 5
                    )
                ],
                "photos" => [
                    "base64-image-1",
                    "base64-image-2",
                    "base64-image-3",
                    "base64-image-4"
                ],
                "account" => [
                    "first_name" => "",
                    "last_name" => "",
                    "email" => "",
                    "password" => ""
                ],
                "agree" => "yes"
            ];
            $data['request'] = array('api' => $this->theMethod, 'data' => $params);
            $data['response'] = $this->getResponse( trans('messages.gear_created'), false, ['gear_id' => '57624d70a0e00'] );

            return \App\Helper::handleResponse( 'Success', false, $data );
        }
        return view('api.method', compact('title', 'method', 'token', 'notes'));
    }

    /**
     * [Settings description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-17T14:44:31+0800]
     * @param  [type] $request [description]
     */
    protected function Settings( $request ){
        $title = 'Settings';
        $method = $this->theMethod;

        if( $request->isMethod('post') ) {
            $data = array();

            $params = [];
            $data['request'] = array('api' => $this->theMethod, 'data' => $params);
            $data['response'] = $this->getResponse('Settings has been loaded.', false, [
            'mode'       => env('APP_ENV'),
            // 'msgevnt'    => config('gp_conf.pusher.new_message.'. strtolower(env('APP_ENV'))),
            'pusher_key' => config('gp_conf.pusher_id'),
            'msgcntr'    => [
                'channel' => config('gp_conf.pusher_msg_counter_id'),
                'event' => config('gp_conf.pusher.msg_counter.'. strtolower(env('APP_ENV')))
            ],
            'sort'       => config('gp_conf.sort'),
            'brands'     => \App\Helper::getBrands(),
            'conditions' => \App\ProductsCondition::getConditionsForApp(), 
            'countries'  => \App\Helper::getCountries(), 
            'states'     => \App\Helper::getStates(), 
            'categories' => \App\Helper::getCategories()
        ]);

            return \App\Helper::handleResponse( 'Success', false, $data );
        }
        return view('api.method', compact('title', 'method'));
    }

    /**
     * [GearDetails description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-17T14:48:41+0800]
     * @param  [type] $request [description]
     */
    protected function GearDetails( $request ){
        $title = 'Gear Details';
        $method = $this->theMethod;

        if( $request->isMethod('post') ) {
            $data = array();

            $params = (object)[
                "product_blurb" => "ramirez-classical-guitar-estudio-classical-1968-peach"
            ];
            $data['request'] = array('api' => $this->theMethod, 'data' => $params);

            $gear = \App\Products::getProductDetails(null, $params->product_blurb);
            if( is_object($gear) && count((array)$gear) > 0 ) {
                $relatedItems = \App\Products::getRelatedItems( $gear->product_id, $gear->category_id );
                $data['response'] = $this->getResponse( 'Gear details has been loaded', false, ['gears_details' => $gear, 'related_items' => $relatedItems] );
            }            

            return \App\Helper::handleResponse( 'Success', false, $data );
        }
        return view('api.method', compact('title', 'method'));
    }

    /**
     * [ContactSeller description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-17T14:56:13+0800]
     * @param  [type] $request [description]
     */
    protected function ContactSeller( $request ){
        $title = 'Contact Seller';
        $method = $this->theMethod;
        $token = "?api_token=W29vbAwOrThmpQkdLgoiEKZPHFlyqUtD0e4G5js3J7SYBXz16aIMC8fNRnu1c";

        if( $request->isMethod('post') ) {
            $data = array();

            $params = (object)[
                "seller_id" => "56fc9e052d05f",
                "message"   => "This is a sample message to Rich Lofr",
                "socket_id" => "200405.5363483"
            ];
            $data['request'] = array('api' => $this->theMethod, 'data' => $params);
            $data['response'] = $this->getResponse( 'Message has been sent', false, ['message' => '5757bac743d02'] );
            return \App\Helper::handleResponse( 'Success', false, $data );
        }
        return view('api.method', compact('title', 'method', 'token'));
    }

    /**
     * [SendMessage description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-17T14:56:10+0800]
     * @param  [type] $request [description]
     */
    protected function SendMessage( $request ) {
        $title = 'Send Message';
        $method = $this->theMethod;
        $token = "?api_token=W29vbAwOrThmpQkdLgoiEKZPHFlyqUtD0e4G5js3J7SYBXz16aIMC8fNRnu1c";

        if( $request->isMethod('post') ) {
            $data = array();

            $params = (object)[
                "inbox_id"  => "56fc9e052d05f",
                "seller_id" => "56fc9e052d05f",
                "message"   => "This is a sample message",
                "socket_id" => "200405.5363483"
            ];

            $data['request'] = array('api' => $this->theMethod, 'data' => $params);
            $data['response'] = $this->getResponse( 'Message has been sent', false, [
                "response" => true,
                "message" => [
                    "message_id" => "578f147f6193e",
                    "inbox_id" => "578ee4fea2244",
                    "sender_id" => "57835d4dbd83f",
                    "receiver_id" => "578c538e46670",
                    "message" => "Hello there.",
                    "status" => "read",
                    "created_at" => "2016-07-20 06:04:47",
                    "updated_at" => "2016-07-20 06:04:48",
                    "channel_id" => "emjkSiUqAPOBn7VJutbo",
                    "receiver_name" => "Paul Jain",
                    "receiver_email" => "paulj.sushi@gmail.com",
                    "sender_name" => "William Fiaschi",
                    "sender_email" => "william01fiaschi@gmail.com"
                ]
            ]);
            return \App\Helper::handleResponse( 'Success', false, $data );
        }
        return view('api.method', compact('title', 'method', 'token'));
    }

    /**
     * [Inbox description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-07-14T17:22:06+0800]
     * @param  [type] $request [description]
     */
    protected function Inbox( $request ) {
        $title = 'Inbox';
        $method = $this->theMethod;
        $token = "?api_token=W29vbAwOrThmpQkdLgoiEKZPHFlyqUtD0e4G5js3J7SYBXz16aIMC8fNRnu1c";

        if( $request->isMethod('post') ) {
            $data = array();

            $params = [];

            $data['request'] = array('api' => $this->theMethod, 'data' => $params);
            $data['response'] = $this->getResponse( trans('messages.api.user.inbox'), false, [
                "event" => "kYiHxKcIFUfCW",
                "inbox" => [
                    [
                        "inbox_id" => "578d7e133f643",
                        "channel_id" => "WfyeirjRAMxCnougELGU",
                        "sender_id" => "578c538e46670",
                        "photo" => "http://localhost:8080/images/thumb-pic.png",
                        "name" => "Paul Jain",
                        "email" => "paulj.sushi@gmail.com"
                    ]
                ]
            ] );
            return \App\Helper::handleResponse( 'Success', false, $data );
        }
        return view('api.method', compact('title', 'method', 'token'));
    }

    /**
     * [InboxMessages description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-07-18T15:17:03+0800]
     * @param  [type] $request [description]
     */
    protected function InboxMessages( $request ) {
        $title = 'Inbox Messages';
        $method = $this->theMethod;
        $token = "?api_token=W29vbAwOrThmpQkdLgoiEKZPHFlyqUtD0e4G5js3J7SYBXz16aIMC8fNRnu1c";

        if( $request->isMethod('post') ) {
            $data = array();

            $params = (object)["inbox_id" => "57624cd4e569d"];

            $data['request'] = array('api' => $this->theMethod, 'data' => $params);
            $data['response'] = $this->getResponse( trans('messages.api.user.inbox_message'), false, [
                "unread_messages" => 0,
                "messages" => [
                    [
                        "message_id" => "578d7e133fc34",
                        "sender_id" => "578c538e46670",
                        "receiver_id" => "57835d4dbd83f",
                        "message" => "Hello.\nInterested.",
                        "status" => "read",
                        "sent_at" => "2016-07-19 01:10:43"
                    ]
                ]
            ] );
            return \App\Helper::handleResponse( 'Success', false, $data );
        }
        return view('api.method', compact('title', 'method', 'token'));
    }

    /**
     * [InboxMessageDetails description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-07-18T15:17:21+0800]
     * @param  [type] $request [description]
     */
    protected function InboxMessageDetails( $request ) {
        $title = 'Inbox Message Details';
        $method = $this->theMethod;
        $token = "?api_token=W29vbAwOrThmpQkdLgoiEKZPHFlyqUtD0e4G5js3J7SYBXz16aIMC8fNRnu1c";

        if( $request->isMethod('post') ) {
            $data = array();

            $params = (object)["message_id" => "57624d18d5ab2"];

            $data['request'] = array('api' => $this->theMethod, 'data' => $params);
            $data['response'] = $this->getResponse( trans('messages.api.user.message_details'), false, [
                    "message_id" => "57624d18d5ab2",
                    "sender_id" => "5757cd6ec2f31",
                    "receiver_id" => "5757bbce106d2",
                    "message" => "hi ash from phoebe",
                    "status" => "read",
                    "sent_at" => "2016-06-16 06:54:16"
                ]
            );
            return \App\Helper::handleResponse( 'Success', false, $data );
        }
        return view('api.method', compact('title', 'method', 'token'));
    }

    /**
     * [SaveSearch description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-07-19T15:28:19+0800]
     * @param  [type] $request [description]
     */
    protected function SaveSearch( $request ) {
        $title = 'Save Search';
        $method = $this->theMethod;
        $token = "?api_token=W29vbAwOrThmpQkdLgoiEKZPHFlyqUtD0e4G5js3J7SYBXz16aIMC8fNRnu1c";

        if( $request->isMethod('post') ) {
            $data = array();

            $params = (object)[
                "url" => "http://gearplanet.sushidigital.ph/search?keyword=gear1&location=nsw&brand=3rd+power&price_min=100&price_max=999&type=new&sort=created_at%7Cdesc",
                "keyword" => "gear1"
            ];

            $data['request'] = array('api' => $this->theMethod, 'data' => $params);
            $data['response'] = $this->getResponse( trans('messages.api.search.saved'), false);
            return \App\Helper::handleResponse( 'Success', false, $data );
        }
        return view('api.method', compact('title', 'method', 'token'));
    }

    /**
     * [DeleteSearch description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-07-19T15:30:33+0800]
     * @param  [type] $request [description]
     */
    protected function DeleteSearch( $request ) {
        $title = 'Delete Search';
        $method = $this->theMethod;
        $token = "?api_token=W29vbAwOrThmpQkdLgoiEKZPHFlyqUtD0e4G5js3J7SYBXz16aIMC8fNRnu1c";

        if( $request->isMethod('post') ) {
            $data = array();

            $params = (object)["search_id" => "578dd4d24e765"];

            $data['request'] = array('api' => $this->theMethod, 'data' => $params);
            $data['response'] = $this->getResponse( trans('messages.api.search.deleted'), false);
            return \App\Helper::handleResponse( 'Success', false, $data );
        }
        return view('api.method', compact('title', 'method', 'token'));
    }

    /**
     * [SearchList description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-07-19T15:37:32+0800]
     * @param  [type] $request [description]
     */
    protected function SearchList( $request ){
        $title = 'Search List';
        $method = $this->theMethod;
        $token = "?api_token=W29vbAwOrThmpQkdLgoiEKZPHFlyqUtD0e4G5js3J7SYBXz16aIMC8fNRnu1c";

        if( $request->isMethod('post') ) {
            $data = array();

            $params = [];

            $data['request'] = array('api' => $this->theMethod, 'data' => $params);
            $data['response'] = $this->getResponse( trans('messages.api.search.deleted'), false, [
                [
                    "search_id" => "578dd4477466e",
                    "keyword" => "gear",
                    "url" => "http://gearplanet.sushidigital.ph/search?keyword=gear&location=nsw&brand=3rd+power&price_min=100&price_max=999&type=new&sort=created_at%7Cdesc",
                    "created_by" => "57835d4dbd83f",
                    "created_at" => "2016-07-19 07:18:31",
                    "updated_at" => "2016-07-19 07:20:42"
                ]
            ]);
            return \App\Helper::handleResponse( 'Success', false, $data );
        }
        return view('api.method', compact('title', 'method', 'token'));
    }

    /**
     * [getResponse description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-17T13:12:06+0800]
     * @param  string  $message [description]
     * @param  boolean $error   [description]
     * @param  array   $data    [description]
     * @return [type]           [description]
     */
    protected function getResponse( $message = 'Oops! Something went wrong.', $error = true, $data = array() ){
        $type = ($error ? 'error' : 'success');
        return array(
            "status"  => ($error ? 'error' : 'success'),
            "api"     => $this->theMethod,
            "message" => $message,
            "data"    => $data
        );
    }

}
