<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;
use Carbon\Carbon;
use Validator;

// helper
use App\Helper as Hlpr;
use App\GpLog as Log;

use App\User;
use App\UsersSavedSearch;

class ApiSearchController extends Controller {
    protected $data, $response, $user, $notification;

	/**
	 * [__construct description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-04T11:50:00+0800]
	 * @param  Request $request [description]
	 */
    public function __construct( Request $request ){
        $content = json_decode($request->getContent());
        $this->data = $content->data;
        $this->response = new \App\Message( (isset($content->api) && $content->api != '' ? $content->api : '') );

        if( $request->has('api_token') && $request->api_token != '' )
            $this->user = User::getFullDetails( Auth::guard('api')->user()->user_id );

        // $this->notification = new \App\Notif();
    }

    /**
     * [saveASearch description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-04T11:51:04+0800]
     * @return [type] [description]
     */
    public function saveASearch() {
    	$validator = Validator::make((array)$this->data, ["url" => 'required|url', "keyword" => 'required']);
        $error = $validator->errors();

        if( count($error) > 0 )
        	return $this->response->setMessage( $error->first() )->display();

    	$searchDetails = UsersSavedSearch::where('keyword', 'like', '%'. $this->data->keyword .'%')->whereUrl($this->data->url)->whereCreatedBy($this->user->user_id)->first();
        if( count($searchDetails) > 0 ){
            UsersSavedSearch::where(['search_id' => $searchDetails->search_id, "created_by" => $this->user->user_id])->update([
                "keyword"    => $this->data->keyword,
                "url"        => $this->data->url,
                "updated_at" => Carbon::now()
            ]);
        }
        else {
            UsersSavedSearch::create([
                "search_id"  => Hlpr::getUID(),
                "keyword"    => $this->data->keyword,
                "url"        => $this->data->url,
                "created_by" => $this->user->user_id,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }

        return $this->response->setMessage( trans('messages.api.search.saved') )->setSuccess()->display();
    }

    /**
     * [deleteSearch description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-05T08:17:35+0800]
     * @return [type] [description]
     */
    public function deleteSearch() {
    	$validator = Validator::make((array)$this->data, ["search_id" => 'required']);
        $error = $validator->errors();

        if( count($error) > 0 )
        	return $this->response->setMessage( $error->first() )->display();

        $userDetails = UsersSavedSearch::where(['search_id' => $this->data->search_id, 'created_by' => $this->user->user_id])->first();
        if( $userDetails ){
            UsersSavedSearch::where(['search_id' => $this->data->search_id, 'created_by' => $this->user->user_id])->delete();
            return $this->response->setMessage( trans('messages.api.search.deleted') )->setSuccess()->display();
        }

        return $this->response->setMessage( trans('messages.api.search.not_exist') )->display();
    }

    /**
     * [getSearchList description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-05T08:23:31+0800]
     * @return [type] [description]
     */
    public function getSearchList() {
    	return $this->response->setMessage( trans('messages.api.search.loaded') )->setData( UsersSavedSearch::whereCreatedBy($this->user->user_id)->orderBy('updated_at', 'desc')->get() )->setSuccess()->display();
    }

}
