<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Input;

use App\Http\Requests;

use App\Products;

class AdsController extends Controller
{

	/**
	 * Construct
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-04-04T14:47:09+0800]
	 */
	public function __construct(){
		$this->middleware('auth');
	}

	/**
	 * User's ads/gears
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-04-04T14:47:18+0800]
	 * @return [type] [description]
	 */
	public function myAds(){
		$myAds = Products::getMyAds(
			(Input::has('status') ? Input::get('status') : 'all'), 
			(Input::has('type') ? Input::get('type') : 'all'), 
			(Input::has('location') ? Input::get('location') : 'all'), 
			(Input::has('brand') ? Input::get('brand') : 'all'), 
			(Input::has('price_min') ? Input::get('price_min') : ''), 
			(Input::has('price_max') ? Input::get('price_max') : ''), 
			(Input::has('sort') ? Input::get('sort') : 'created_at|desc'));
		return view('users.gears.my-ads', compact('myAds'));
	}
}
