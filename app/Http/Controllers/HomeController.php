<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use Auth;

use App\Helper as Hlpr;

use App\Products;
use App\UsersSavedSearch;

class HomeController extends Controller {

    /**
     * [__construct description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-09-01T15:00:57+0800]
     */
	public function __construct(){
		$this->middleware('auth', ['except' => ['index', 'splash']]);
	}

    /**
     * [index description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-09-01T15:01:05+0800]
     * @return [type] [description]
     */
    public function index() {
        $savedSearch = null;

        if( Auth::check() ) {
            $savedSearch = UsersSavedSearch::whereCreatedBy( Auth::user()->user_id )->orderBy('updated_at', 'desc')->first();
            if( $savedSearch ) {
                $savedSearch = Hlpr::setArrayFromUrl( $savedSearch->url );
                $savedSearch = Products::getSavedSearch( 
                    (isset($savedSearch->search['keyword']) && $savedSearch->search['keyword'] ? $savedSearch->search['keyword'] : ''),
                    (isset($savedSearch->search['type']) && $savedSearch->search['type'] ? $savedSearch->search['type'] : 'all'),
                    (isset($savedSearch->search['location']) && $savedSearch->search['location'] ? $savedSearch->search['location'] : 'all'),
                    (isset($savedSearch->search['brand']) && $savedSearch->search['brand'] ? $savedSearch->search['brand'] : 'all'),
                    (isset($savedSearch->search['price_min']) && $savedSearch->search['price_min'] ? $savedSearch->search['price_min'] : ''),
                    (isset($savedSearch->search['price_max']) && $savedSearch->search['price_max'] ? $savedSearch->search['price_max'] : ''),
                    (isset($savedSearch->search['sort']) && $savedSearch->search['sort'] ? $savedSearch->search['sort'] : 'created_at|desc')
                );
            }
        }
        $gears = Products::getHomeGears();

        return view( (env('APP_SPLASH') ? 'splash' : 'home'), compact('gears', 'savedSearch'));
    }

    /**
     * [splash description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-09-01T15:01:12+0800]
     * @return [type] [description]
     */
    public function splash() {
        return view('splash');
    }
}
