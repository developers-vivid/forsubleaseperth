<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;
use Auth;
use Carbon\Carbon;
use Validator;

// helper
use App\Helper as Hlpr;
use App\GpLog as Log;

use App\User;
use App\UsersShippingAddress;
use App\Orders;
use App\OrdersHistory;
use App\OrdersPaymentInfo;
use App\OrdersProducts;
use App\OrdersShippingInfo;

class ApiOrderController extends Controller {
    protected $data, $response, $user, $notification;

	/**
	 * [__construct description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-04T11:50:00+0800]
	 * @param  Request $request [description]
	 */
    public function __construct( Request $request ){
        $content = json_decode($request->getContent());
        $this->data = $content->data;
        $this->response = new \App\Message( (isset($content->api) && $content->api != '' ? $content->api : '') );

        if( $request->has('api_token') && $request->api_token != '' )
            $this->user = User::getFullDetails( Auth::guard('api')->user()->user_id );

        // $this->notification = new \App\Notif();
    }

    /**
     * [putOrders description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-05T09:51:33+0800]
     * @return [type] [description]
     */
    public function putOrders() {
    	// set to array
    	$this->data->shipping = (array)$this->data->shipping;

    	$validator = Validator::make((array)$this->data, [
			"invoice_id"            => 'required',
			"total"                 => 'required|numeric|min:1',
			"date_completed"        => 'required|date_format:"Y-m-d H:i:s"',
			// "paypal_correlation_id" => 'required',
			// "paypal_pay_key"        => 'required',
			"products"              => 'required',
			"shipping.address1"     => 'required',
			"shipping.city"         => 'required',
			"shipping.state"        => 'required',
			"shipping.post_code"    => 'required',
    	]);
        $error = $validator->errors();

        if( count($error) > 0 )
        	return $this->response->setMessage( $error->first() )->display();

        // set to object
    	$this->data->shipping = (object)$this->data->shipping;
    	$today = Carbon::now();

    	DB::beginTransaction();

    	// create order
    	$order = Orders::create([
    		"order_id" => Hlpr::getUID(),
    		"customer_id" => $this->user->user_id,
    		"invoice_id" => $this->data->invoice_id,
    		"total" => $this->data->total,
    		"status" => '573e5b0202eef',	// completed
    		"date_completed" => $this->data->date_completed,
    		"paypal_correlation_id" => $this->data->paypal_correlation_id,
    		"paypal_pay_key" => $this->data->paypal_pay_key,
    		"token" => substr(str_shuffle(config('gp_conf.alpha_num')), 0, 60),
    		"created_at" => $today,
    		"updated_at" => $today
    	]);

    	// create order history
    	OrdersHistory::create([
    		"order_history_id" => Hlpr::getUID(),
    		"order_id" => $order->order_id,
    		"description" => 'Order has been successfully created via mobile App',
    		"created_at" => $today,
    		"updated_at" => $today
    	]);

    	// create orders products
    	if( is_array($this->data->products) && count($this->data->products) > 0 ) {
    		foreach( $this->data->products as $prod ) {
		    	OrdersProducts::create([
		    		"order_product_id" => Hlpr::getUID(),
		    		"order_id" => $order->order_id,
		    		"product_id" => $prod->product_id,
		    		"price" => $prod->price,
		    		"qty" => 1,	// by default,
		    		"created_at" => $today,
		    		"updated_at" => $today
		    	]);
    		}
    	}
    	
        // set order shipping and update user shipping info
        Hlpr::updateOrderShippingInfo(
        	$this->data->shipping->address1,
        	$this->data->shipping->address2,
        	$this->data->shipping->city,
        	$this->data->shipping->state,
        	$this->data->shipping->post_code,
        	$this->user->user_id,
        	$order->order_id
        );

        if( $order ) {
        	DB::commit();
        	return $this->response->setMessage( trans('messages.api.orders.success') )->setData([
        		'message' => 'Your order has been received and is now being processed. Your order details are shown below for your reference', 
        		'order' => Orders::getOrderDetails( $order->order_id )
        	])->setSuccess()->display();
        }
       	else {
       		DB::rollBack();
       		return $this->response->setMessage( trans('messages.api.orders.error') )->display();
       	}
    }

}
