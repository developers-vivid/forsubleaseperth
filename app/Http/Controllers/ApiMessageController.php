<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Validator;
use Carbon\Carbon;
use Auth;
use Vinkla\Pusher\PusherManager;

// helper
use App\Helper as Hlpr;
use App\GpLog as Log;

use App\User;
use App\UsersMessages;

class ApiMessageController extends Controller {
    
    protected $data, $response, $user, $notification, $pusher;

	/**
	 * [__construct description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-04T10:52:33+0800]
	 * @param  Request $request [description]
	 */
    public function __construct( Request $request, PusherManager $pusher ){
    	$this->pusher = $pusher;

        $content = json_decode($request->getContent());
        $this->data = $content->data;
        $this->response = new \App\Message( (isset($content->api) && $content->api != '' ? $content->api : '') );

        if( $request->has('api_token') && $request->api_token != '' )
            $this->user = User::getFullDetails( Auth::guard('api')->user()->user_id );

        // $this->notification = new \App\Notif();
    }

	/**
	 * [contactSeller description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-04T10:52:23+0800]
	 * @return [type] [description]
	 */
	public function contactSeller() {
		$validator = Validator::make((array)$this->data, ["seller_id" => 'required', "message" => 'required', "socket_id" => 'required']);
        $error = $validator->errors();

        if( count($error) > 0 )
        	return $this->response->setMessage( $error->first() )->display();

        return \App\Messaging::contactSeller( $this->pusher, $this->user->user_id, $this->data->seller_id, $this->user->name, $this->data->socket_id, $this->data->message, 'contact-seller' );
	}

	/**
	 * [sendAMessage description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-04T11:10:52+0800]
	 * @return [type] [description]
	 */
	public function sendAMessage() {
		$validator = Validator::make((array)$this->data, ["inbox_id" => 'required', "seller_id" => 'required', "message" => 'required', "socket_id" => 'required']);
        $error = $validator->errors();

        if( count($error) > 0 )
        	return $this->response->setMessage( $error->first() )->display();

        return \App\Messaging::sendMessage( $this->pusher, $this->data->inbox_id, $this->user->user_id, $this->data->seller_id, $this->user->name, $this->data->socket_id, $this->data->message, true, 'send-message' );
	}

	/**
	 * [getUserInbox description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-04T11:21:55+0800]
	 * @return [type] [description]
	 */
	public function getUserInbox() {
		$userInbox = UsersMessages::getUserInbox( null, $this->user->user_id );
        $inbox = [];

        if( count($userInbox) > 0 ) {
            foreach($userInbox as $inbx) {
                $userDetails = User::whereUserId( ($this->user->user_id == $inbx->receiver_id ? $inbx->sender_id : $inbx->receiver_id ) )->first();
                $inbox[] = array(
					"inbox_id"   => $inbx->inbox_id,
					"channel_id" => $inbx->channel_id,
					"sender_id"  => $inbx->sender_id,
					"photo"      => url('images/thumb-pic.png'),
					"name"       => $userDetails->name,
					"email"      => $userDetails->email
                );
            }
        }

        return $this->response->setMessage( trans('messages.api.user.inbox') )->setData(['event' => config('gp_conf.pusher.new_message.'. strtolower(env('APP_ENV'))), 'inbox' => $inbox])->setSuccess()->display();
	}

	/**
	 * [getUserInboxMessages description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-04T11:29:34+0800]
	 * @return [type] [description]
	 */
	public function getUserInboxMessages() {
        $validator = Validator::make((array)$this->data, ["inbox_id" => 'required']);
        $error = $validator->errors();

        if( count($error) > 0 )
        	return $this->response->setMessage( $error->first() )->display();

        $totalUnread = UsersMessages::whereInboxId( $this->data->inbox_id )->whereReceiverId( $this->user->user_id )->whereStatus('un-read')->count();
        
        $messages = UsersMessages::getInboxMessages( $this->data->inbox_id, null, true );

        // update to read
        if( $totalUnread > 0 )
            UsersMessages::where(['inbox_id' => $this->data->inbox_id, 'receiver_id' => $this->user->user_id, 'status' => 'un-read'])->update(['status' => 'read', 'updated_at' => Carbon::now()]);
        
        return $this->response->setMessage( trans('messages.api.user.inbox_message') )->setData(['unread_messages' => $totalUnread, 'messages' => $messages])->setSuccess()->display();
	}

	/**
	 * [getUserInboxMessageDetails description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-04T11:36:35+0800]
	 * @return [type] [description]
	 */
	public function getUserInboxMessageDetails() {
		$validator = Validator::make((array)$this->data, ["message_id" => 'required']);
        $error = $validator->errors();

        if( count($error) > 0 )
        	return $this->response->setMessage( $error->first() )->display();

        $messageDetails = UsersMessages::getInboxMessages( null, $this->data->message_id, true );
        if( $messageDetails && $messageDetails[0]->status == 'unread' )
            UsersMessages::where(['message_id' => $this->data->message_id])->update(['status' => 'read', 'updated_at' => Carbon::now()]);

        $this->response->setMessage( trans('messages.api.user.message_details') )->setSuccess();
        if( $messageDetails )
        	$this->response->setData( $messageDetails[0] );

        return $this->response->display();
	}

}
