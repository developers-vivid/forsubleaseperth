<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use Input;
use Validator;
use Session;
use Carbon\Carbon;
use Eventviva\ImageResize;

use App\Helper as Hlpr;

use App\User;
use App\Blogs;
use App\BlogsComments;

class AdminBlogsController extends Controller {
    
	protected $response;

	/**
	 * [__construct description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-10-27T15:26:12+0800]
	 */
  	public function __construct(){
	    $this->middleware('auth');
	    $this->response = new \App\Message();
  	}

  	/**
  	 * [index description]
  	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
  	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-10-27T15:26:32+0800]
  	 * @return [type] [description]
  	 */
  	public function index( Request $request ) {
        $blogs = Blogs::getBlogs( (Input::has('keyword') && Input::get('keyword') != '' ? Input::get('keyword') : null) );
  		return view('admin.blogs.list', compact('blogs'));
  	}

    /**
     * [createBlog description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-10-28T09:55:29+0800]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function createBlog( Request $request ) {
        if( $request->isMethod('post') ) {
            $validator = Validator::make((array)$request->all(), [
                'title'   => 'required|unique:blogs',
                'content' => 'required',
                'photo'   => 'required'
            ]);

            $error = $validator->errors();

            if( count($error) > 0 )
                return $this->response->setMessage( Hlpr::getAllValidationError($error, false) )->display();

            if( $request->hasFile('photo') ) {
                $photo       = $request->file('photo');
                $imgOrigName = $photo->getClientOriginalName();
                $imgName     = Hlpr::getRandomHex() .'.'. $photo->getClientOriginalExtension();
                $imgStor     = config('gp_conf.photo.blog_path');
                $imgLocation = $imgStor . $imgName;

                if( file_exists($imgStor) ) {
                    $photo->move($imgStor, $imgName);   // move the file

                    if( file_exists($imgLocation) ) {
                        $blog = Blogs::create([
                            'blog_id'    => Hlpr::getUID(),
                            'title'      => $request->title,
                            'slug'       => str_slug($request->title, '-'),
                            'content'    => $request->content,
                            'photo'      => $imgName,
                            'status'     => $request->status,
                            'created_by' => Auth::user()->user_id,
                            'created_at' => Carbon::now()
                        ]);

                        // create thumbnail 347x220
                        $image = new ImageResize($imgLocation);
                        $image->crop(347, 220);
                        $image->save( $imgStor. 'thumbs' .config('gp_conf.ds'). $imgName);  

                        return $this->response->setMessage( trans('messages.blog.created') )->setSuccess()->setData(['redirect' => route('admin::updateBlog', [$blog->blog_id])])->display();        
                    }
                }
            }

            return $this->response->setMessage( trans('messages.blog.not_created') )->setSuccess()->display();
        }
        
        return view('admin.blogs.create-blog');
    }

    /**
     * [updateBlog description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-11-02T15:03:35+0800]
     * @param  Request $request [description]
     * @param  [type]  $blogId  [description]
     * @return [type]           [description]
     */
    public function updateBlog( Request $request, $blogId ) {
        $blog = Blogs::whereBlogId( $blogId )->first();

        if( $request->isMethod('post') ) {
            $validator = Validator::make((array)$request->all(), [
                'title'   => 'required',
                'content' => 'required'
            ]);

            $error = $validator->errors();

            if( count($error) > 0 )
                return $this->response->setMessage( Hlpr::getAllValidationError($error, false) )->display();

            if( Blogs::whereTitle( $request->title )->count() > 1 )
                return $this->response->setMessage( trans('messages.blog.exist') )->display();

            $newPhoto = null;
            if( $request->hasFile('photo') ) {
                @unlink(config('gp_conf.photo.blog_path') . $blog->photo);    // remove original image
                @unlink(config('gp_conf.photo.blog_path') . 'thumbs' . config('gp_conf.ds') . $blog->photo);    // remove thumbnail image

                $photo       = $request->file('photo');
                $imgOrigName = $photo->getClientOriginalName();
                $imgName     = Hlpr::getRandomHex() .'.'. $photo->getClientOriginalExtension();
                $imgStor     = config('gp_conf.photo.blog_path');
                $imgLocation = $imgStor . $imgName;

                if( file_exists($imgStor) ) {
                    $photo->move($imgStor, $imgName);   // move the file

                    if( file_exists($imgLocation) ) {
                        $newPhoto = $imgName;

                        // create thumbnail 347x220
                        $image = new ImageResize($imgLocation);
                        $image->crop(347, 220);
                        $image->save( $imgStor. 'thumbs' .config('gp_conf.ds'). $imgName);
                    }
                }
            }

            $blog->title      = $request->title;
            $blog->slug       = str_slug($request->title, '-');
            $blog->content    = $request->content;

            if( !is_null($newPhoto) )
                $blog->photo      = $newPhoto;

            $blog->status     = $request->status;
            $blog->updated_at = Carbon::now();
            $updated = $blog->save();
            
            return $this->response->setMessage( trans('messages.blog.updated') )->setSuccess()->display();
        }

        return view('admin.blogs.update-blog', compact('blog', 'blogId'));
    }

    /**
     * [deleteBlog description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-11-02T16:43:21+0800]
     * @param  Request $request [description]
     * @param  [type]  $blogId  [description]
     * @return [type]           [description]
     */
    public function deleteBlog( Request $request, $blogId ) {
        $blog = Blogs::whereBlogId( $blogId )->first();
        $deleted = null;

        if( $blog ) {
            @unlink(config('gp_conf.photo.blog_path') . $blog->photo);    // remove original image
            @unlink(config('gp_conf.photo.blog_path') . 'thumbs' . config('gp_conf.ds') . $blog->photo);    // remove thumbnail image

            $blogComments = BlogsComments::where(['blog_id' => $blogId])->forceDelete();
            $deleted = $blog->forceDelete();
        }

        $this->response->setMessage( trans(sprintf('messages.blog.%s', ($blog && $deleted ? 'deleted' : 'not_deleted'))) );
        if( $blog && $deleted )
            $this->response->setSuccess();

        $this->response->flash();

        return redirect()->route('admin::blogs');
    }

}
