<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PasswordResets extends Model {
	protected $table = 'password_resets';
	protected $primaryKey = 'password_reset_id';
	public $incrementing = false;
	protected $softDelete = true;

	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'password_reset_id',
		'email',
		'token',
	];

	protected $dates = [
		'created_at',
		'updated_at',
	];

	protected $hidden = [
	];
}
