<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;

class UsersInbox extends Model {
    protected $table = 'users_inbox';
    protected $primaryKey = 'inbox_id';
    public $incrementing = false;
    protected $softDelete = true;

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'inbox_id', 'channel_id', 'has_unread_messages', 'last_message_received',
    ];

    protected $dates = [
    	'created_at', 'updated_at',
    ];

    protected $hidden = [
    ];

    public function messages(){
        return $this->belongsTo('App\UsersInboxMessages', 'inbox_id');
    }

    /**
    * Get total unread messages
    * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
    * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-10T10:22:52+0800]
    * @return boolean [description]
    */
    public static function hasUnreadMessages(){
        return UsersInbox::where('owner_id', Auth::user()->user_id)->where('has_unread_messages', 1)->count();
    }

    /**
     * [getDetails description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-09-07T13:31:53+0800]
     * @param  [type] $inboxId   [description]
     * @param  [type] $requestor [description]
     * @return [type]            [description]
     */
    public static function getDetails( $inboxId = null, $channelId = null, $requestor ) {
        $inbox =  DB::table('users_inbox as ui')
                    ->select('ui.inbox_id', 'ui.channel_id', 'um.sender_id', 'um.receiver_id')
                    ->when((!is_null($inboxId) && is_null($channelId)), function($whenInboxId) use($inboxId){
                        return $whenInboxId->where('ui.inbox_id', $inboxId);
                    })
                    ->when((!is_null($channelId) && is_null($inboxId)), function($whenChannelId) use ($channelId){
                        return $whenChannelId->where('ui.channel_id', $channelId);                    
                    })
                    ->join('users_messages as um', 'ui.inbox_id', '=', 'um.inbox_id')
                    ->groupBy('um.inbox_id')
                    ->groupBy('um.sender_id')
                    ->groupBy('um.receiver_id')
                    ->first();

        if( $inbox ) {
            $inbox->sender = ($requestor == $inbox->sender_id ? $inbox->sender_id : $inbox->receiver_id);
            $inbox->receiver = ($requestor != $inbox->sender_id ? $inbox->sender_id : $inbox->receiver_id);
            
            unset($inbox->sender_id);
            unset($inbox->receiver_id);
        }

        return $inbox;

    }
}
