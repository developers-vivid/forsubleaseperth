<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use DB;

use App\Helper as Hlpr;

class Blogs extends Model
{
	protected $table = 'blogs';
	protected $primaryKey = 'blog_id';
	public $incrementing = false;
	protected $softDelete = true;

	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'blog_id',
		'title',
		'slug',
		'content',
		'photo',
		'status',
		'created_by',
	];

	protected $dates = [
		'deleted_at',
		'created_at',
		'updated_at',
	];

	protected $hidden = [
		'blog_id',
	];

	/**
	 * [getBlogs description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-11-02T16:07:59+0800]
	 * @param  boolean $paginate   [description]
	 * @param  boolean $fullAccess [description]
	 * @return [type]              [description]
	 */
	public static function getBlogs( $keyword = null, $paginate = true, $fullAccess = true ) {
		$blog = DB::table('blogs as blog')
				->select('blog.blog_id', 'blog.title', DB::raw("(SUBSTRING_INDEX( ".env('DB_PREFIX', 'gp_')."blog.content, ' ', 30 )) as blog_content"), 
					'blog.photo', Hlpr::nullToStr('user.name', 'author'), 'blog.created_at', 'blog.status')
				->join('users as user', 'blog.created_by', '=', 'user.user_id')
				->when(!$fullAccess, function($query){
					return $query->where('blog.status', 'active')
						->whereNull('blog.deleted_at');
				})
				->when((!is_null($keyword)), function($query) use($keyword){
					return $query
							->where('blog.title', 'like', '%'. $keyword .'%')
							->where('blog.content', 'like', '%'. $keyword .'%');
				})
				->orderBy('blog.created_at', 'desc');

		return $paginate ? $blog->paginate(config('gp_conf.pagination_limit.admin.blogs')) : $blog->get();
	}
}
