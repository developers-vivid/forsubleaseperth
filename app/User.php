<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

use App\Helper as Hlpr;

class User extends Authenticatable {
    protected $primaryKey = 'user_id';
    public $incrementing = false;

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'user_id', 'name', 'email', 'login_type', 'password', 'api_token', 'verification_token', 'role_id', 'seller', 'status',
    ];

    /**
    * The attributes excluded from the model's JSON form.
    *
    * @var array
    */
    protected $hidden = [
        'password', 'remember_token', 'user_id', 'verification_token'
    ];

    /**
    * Get user's full information
    * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
    * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-24T13:12:35+0800]
    * @return [type] [description]
    */
    public static function getFullDetails( $userID ){
        $query = DB::table('users')
                    ->select(
                        'users.user_id', 'users.name', 'users.email', 'users.login_type', 'users.api_token', 'users.created_at as date_registered',
                        'role.name as role_name',
                        'profile.first_name', Hlpr::nullToStr('profile.last_name'), Hlpr::nullToStr('profile.telephone'), Hlpr::nullToStr('profile.mobile'),
                        Hlpr::nullToStr('company.name', 'company_name'), Hlpr::nullToStr('company.abn', 'company_abn'), Hlpr::nullToStr('company.street', 'company_street'), Hlpr::nullToStr('company.city', 'company_city'), Hlpr::nullToStr('company.state', 'company_state'), Hlpr::nullToStr('company.post_code', 'company_post_code'), Hlpr::nullToStr('company.phone', 'company_phone'), Hlpr::nullToStr('company.logo', 'company_logo'),
                        Hlpr::nullToStr('shipping.address_1', 'shipping_address_1'), Hlpr::nullToStr('shipping.address_2', 'shipping_address_2'), Hlpr::nullToStr('shipping.city', 'shipping_city'), Hlpr::nullToStr('shipping.state', 'shipping_state'), Hlpr::nullToStr('shipping.post_code', 'shipping_post_code'),
                        DB::raw("(select count(messages.message_id) as total_unread_messages from ".env('DB_PREFIX', 'gp_')."users_messages as messages where (messages.receiver_id = ".env('DB_PREFIX', 'gp_')."users.user_id AND messages.status = 'un-read')) as total_unread_messages"),
                        Hlpr::nullToStr('settings.email_message_notification')
                    )
                    ->leftJoin('users_roles as role', 'users.role_id', '=', 'role.role_id')
                    ->leftJoin('users_profile as profile', 'users.user_id', '=', 'profile.user_id')
                    ->leftJoin('users_company as company', 'users.user_id', '=', 'company.user_id')
                    ->leftJoin('users_shipping_address as shipping', 'users.user_id', '=', 'shipping.user_id')
                    ->leftJoin('users_settings as settings', 'users.user_id', '=', 'settings.user_id')
                    ->where('users.user_id', $userID)
                    ->groupBy('users.user_id')
                    ->get();

        return count($query) > 0 ? $query[0] : null;
    }

    public function gears(){
        return $this->hasMany('App\Products', 'created_by');
    }

    public function profile(){
        return $this->hasOne('App\UsersProfile', 'user_id');
    }

    public function isAdmin() {
        return $this->role_id == "56aebc46f2823";
    }

    public function isSuperAdmin() {
        return $this->role_id == "56aebc46f281b";        
    }

    /**
    * Get user's full information by email
    * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
    * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-24T13:12:35+0800]
    * @return [type] [description]
    */
    public static function getFullDetailsEmail( $email ){
        $query = DB::table('users')
                    ->select(
                        'users.user_id', 'users.name', 'users.email', 'users.login_type', 'users.api_token', 'users.created_at as date_registered',
                        'role.name as role_name','verification_token',
                        'profile.first_name', Hlpr::nullToStr('profile.last_name'), Hlpr::nullToStr('profile.telephone'), Hlpr::nullToStr('profile.mobile'),
                        Hlpr::nullToStr('company.name', 'company_name'), Hlpr::nullToStr('company.abn', 'company_abn'), Hlpr::nullToStr('company.street', 'company_street'), Hlpr::nullToStr('company.city', 'company_city'), Hlpr::nullToStr('company.state', 'company_state'), Hlpr::nullToStr('company.post_code', 'company_post_code'), Hlpr::nullToStr('company.phone', 'company_phone'), Hlpr::nullToStr('company.logo', 'company_logo'),
                        Hlpr::nullToStr('shipping.address_1', 'shipping_address_1'), Hlpr::nullToStr('shipping.address_2', 'shipping_address_2'), Hlpr::nullToStr('shipping.city', 'shipping_city'), Hlpr::nullToStr('shipping.state', 'shipping_state'), Hlpr::nullToStr('shipping.post_code', 'shipping_post_code'),
                        DB::raw("(select count(messages.message_id) as total_unread_messages from ".env('DB_PREFIX', 'gp_')."users_messages as messages where (messages.receiver_id = ".env('DB_PREFIX', 'gp_')."users.user_id AND messages.status = 'un-read')) as total_unread_messages"),
                        Hlpr::nullToStr('settings.email_message_notification')
                    )
                    ->leftJoin('users_roles as role', 'users.role_id', '=', 'role.role_id')
                    ->leftJoin('users_profile as profile', 'users.user_id', '=', 'profile.user_id')
                    ->leftJoin('users_company as company', 'users.user_id', '=', 'company.user_id')
                    ->leftJoin('users_shipping_address as shipping', 'users.user_id', '=', 'shipping.user_id')
                    ->leftJoin('users_settings as settings', 'users.user_id', '=', 'settings.user_id')
                    ->where('users.email', $email)
                    ->groupBy('users.user_id')
                    ->get();

        return count($query) > 0 ? $query[0] : null;
    }
}
