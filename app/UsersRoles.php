<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersRoles extends Model
{
  protected $table = 'users_roles';
  protected $primaryKey = 'role_id';
  public $incrementing = false;
  protected $softDelete = true;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'role_id', 'name', 'description',
  ];

  protected $dates = [
  	'created_at', 'updated_at',
  ];

  protected $hidden = [
  	'role_id'
  ];
}
