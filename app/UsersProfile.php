<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersProfile extends Model
{
  protected $table = 'users_profile';
  protected $primaryKey = 'profile_id';
  public $incrementing = false;
  protected $softDelete = true;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'profile_id', 'user_id', 'first_name', 'last_name', 'telephone', 'mobile',
  ];

  protected $dates = [
  	'created_at', 'updated_at',
  ];

  protected $hidden = [
  	'profile_id',
  ];
}
