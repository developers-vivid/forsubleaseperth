<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrdersShippingInfo extends Model {
    protected $table = 'orders_shipping_info';
	protected $primaryKey = 'order_shipping_id';
	public $incrementing = false;
	protected $softDelete = true;

	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'order_shipping_id', 'order_id', 'address_1', 'address_2', 'city', 'state', 'post_code',
	];

	protected $dates = [
		'created_at', 'updated_at',
	];

	protected $hidden = [
	];
}
