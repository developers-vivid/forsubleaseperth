<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersShippingAddress extends Model {
	protected $table = 'users_shipping_address';
	protected $primaryKey = 'shipping_id';
	public $incrementing = false;
	protected $softDelete = true;

	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'shipping_id', 'user_id', 'address_1', 'address_2', 'city', 'state', 'post_code',
	];

	protected $dates = [
		'created_at', 'updated_at',
	];

	protected $hidden = [
	];
}
