<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use DB;

class ModelName extends Model
{
  protected $table = '';
  protected $primaryKey = '';
  public $incrementing = false;
  protected $softDelete = true;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    '',
  ];

  protected $dates = [
  ];

  protected $hidden = [
  ];
}