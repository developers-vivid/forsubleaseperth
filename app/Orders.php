<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Helper as Hlpr;

class Orders extends Model {
	protected $table = 'orders';
	protected $primaryKey = 'order_id';
	public $incrementing = false;
	protected $softDelete = true;

	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'order_id',
		'customer_id',
		'invoice_id',
		'total',
		'status',
		'date_cancelled',
		'date_completed',
		'date_refunded',
		'paypal_correlation_id',
		'paypal_pay_key',
		'paypal_payer_email',
		'paypal_payer_id',
		'tracking_code',
		'token',
		'notes',
	];

	protected $dates = [
		'created_at', 'updated_at',
	];

	protected $hidden = [

	];

	/**
	 * [getOrderDetails description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-13T08:34:55+0800]
	 * @return [type] [description]
	 */
	public static function getOrderDetails( $orderId ){
		$query = DB::table('orders as order')
					->select(
						'order.order_id', 'order.customer_id', 'order.invoice_id', 'order.total',
						Hlpr::nullToStr('order.date_cancelled'), Hlpr::nullToStr('order.date_completed'), Hlpr::nullToStr('order.date_refunded'), 
						'order.paypal_correlation_id', 'order.paypal_pay_key', Hlpr::nullToStr('order.paypal_payer_email'), Hlpr::nullToStr('order.paypal_payer_id'), 
						Hlpr::nullToStr('order.tracking_code'), Hlpr::nullToStr('order.token'), Hlpr::nullToStr('order.notes'), 'order.created_at as date_created', 
						'stat.name as order_status', 
						'ship.address_1 as shipping_address_1', 'ship.address_2 as shipping_address_2', 'ship.city as shipping_city', 'ship.state as shipping_state', 'ship.post_code as shipping_post_code',
						Hlpr::nullToStr('pay_info.transaction_id'), Hlpr::nullToStr('pay_info.transaction_status'), Hlpr::nullToStr('pay_info.receiver_amount'), Hlpr::nullToStr('pay_info.receiver_email'), 
						Hlpr::nullToStr('pay_info.receiver_primary'), Hlpr::nullToStr('pay_info.receiver_invoice_id'), Hlpr::nullToStr('pay_info.receiver_payment_type'), Hlpr::nullToStr('pay_info.receiver_account_id'), 
						Hlpr::nullToStr('pay_info.refunded_amount'), Hlpr::nullToStr('pay_info.sender_transaction_id'), Hlpr::nullToStr('pay_info.sender_transaction_status'),
						'user.email as customer_email', 'user.name as customer_name', 'up.first_name as customer_first_name', 'up.last_name as customer_last_name', Hlpr::nullToStr('up.telephone', 'customer_telephone'), Hlpr::nullToStr('up.mobile', 'customer_mobile'),
						Hlpr::nullToStr('opr.product_id', 'ordered_product')
					)
					->leftJoin('orders_status as stat', 'order.status', '=', 'stat.order_status_id')
					->leftJoin('orders_shipping_info as ship', 'order.order_id', '=', 'ship.order_id')
					->leftJoin('orders_payment_info as pay_info', 'order.order_id', '=', 'pay_info.order_id')
					->leftJoin('orders_products as opr', 'order.order_id', '=', 'opr.order_id')
					->leftJoin('users as user', 'order.customer_id', '=', 'user.user_id')
					->leftJoin('users_profile as up', 'order.customer_id', '=', 'up.user_id')
					->where('order.order_id', $orderId)
					->groupBy('order.order_id')
					->get();

		if( count($query) > 0 ) {
			foreach($query as $que) {
				$que->ordered_products = OrdersProducts::getOrderedProducts( $que->order_id );
				$que->order_history = OrdersHistory::getHistory( $que->order_id );
				$que->seller = Products::getProductOwner( $que->ordered_product );
			}
		}
		return count($query) > 0 ? $query[0] : null;
	}
}
