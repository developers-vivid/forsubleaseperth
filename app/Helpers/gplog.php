<?php

namespace App;

use Carbon\Carbon;

class GpLog {

	protected $filePath;

	/**
	 * File log constructor
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-19T13:29:19+0800]
	 * @param  [type] $filepath [description]
	 * @param  string $mode     [description]
	 */
	public function __construct( $filepath, $mode = 'a' ){
		$this->filePath = fopen(storage_path('logs/'. $filepath .'.log'), $mode);
		fwrite( $this->filePath, '-- '. Carbon::now() .' --' . PHP_EOL );
	}

	/**
	 * [write description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-19T13:57:51+0800]
	 * @param  [type] $message [description]
	 * @return [type]          [description]
	 */
	public function write( $message ){
		fwrite( $this->filePath, " -> ".$message . PHP_EOL );
		return $this;
	}

	/**
	 * [emergency description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-19T13:57:16+0800]
	 * @param  [type] $message [description]
	 * @return [type]          [description]
	 */
	public function emergency( $message ){
		fwrite( $this->filePath, " -> [EMERGENCY] ".$message . PHP_EOL );
		return $this;
	}

	/**
	 * [alert description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-19T13:57:20+0800]
	 * @param  [type] $message [description]
	 * @return [type]          [description]
	 */
	public function alert( $message ){
		fwrite( $this->filePath, " -> [ALERT] ".$message . PHP_EOL );
		return $this;
	}

	/**
	 * [critical description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-19T13:57:23+0800]
	 * @param  [type] $message [description]
	 * @return [type]          [description]
	 */
	public function critical( $message ){
		fwrite( $this->filePath, " -> [CRITICAL] ".$message . PHP_EOL );
		return $this;
	}

	/**
	 * [error description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-19T13:57:26+0800]
	 * @param  [type] $message [description]
	 * @return [type]          [description]
	 */
	public function error( $message ){
		fwrite( $this->filePath, " -> [ERROR] ".$message . PHP_EOL );
		return $this;
	}

	/**
	 * [warning description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-19T13:57:29+0800]
	 * @param  [type] $message [description]
	 * @return [type]          [description]
	 */
	public function warning( $message ){
		fwrite( $this->filePath, " -> [WARNING] ".$message . PHP_EOL );
		return $this;
	}

	/**
	 * [notice description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-19T13:57:32+0800]
	 * @param  [type] $message [description]
	 * @return [type]          [description]
	 */
	public function notice( $message ){
		fwrite( $this->filePath, " -> [NOTICE] ".$message . PHP_EOL );
		return $this;
	}

	/**
	 * [info description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-19T13:57:34+0800]
	 * @param  [type] $message [description]
	 * @return [type]          [description]
	 */
	public function info( $message ){
		fwrite( $this->filePath, " -> [INFO] ".$message . PHP_EOL );
		return $this;
	}

	/**
	 * [debug description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-19T13:57:37+0800]
	 * @param  [type] $message [description]
	 * @return [type]          [description]
	 */
	public function debug( $message ){
		fwrite( $this->filePath, " -> [DEBUG] ".$message . PHP_EOL );
		return $this;
	}

	/**
	 * [success description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-27T11:55:52+0800]
	 * @param  [type] $message [description]
	 * @return [type]          [description]
	 */
	public function success( $message ){
		fwrite( $this->filePath, " -> [SUCCESS] ".$message . PHP_EOL );
		return $this;
	}

	/**
	 * End log
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-19T13:29:35+0800]
	 * @return [type] [description]
	 */
	public function endLog(){
		fwrite( $this->filePath, '-- '. Carbon::now() .' --' . PHP_EOL . PHP_EOL );
    	fclose( $this->filePath );
	}
}