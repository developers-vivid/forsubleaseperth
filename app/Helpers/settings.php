<?php

namespace App;

use Carbon\Carbon;

use App\UserSettings;

class Settings {

	protected $userSettings;

	/**
	 * 
	 */
	public function __construct( $userId ) {
		$userSettings = UserSettings::whereUserId( $userId )->first();
		if( ! $userSettings ) {
			$userSettings = UserSettings::create([
				'settings_id' => \App\Helper::getUID(), 
				'user_id' => $userId, 
				'created_at' => Carbon::now()
			]);
		}
		$this->userSettings = $userSettings;
	}

	/**
	 * 
	 */
	public function setMessageNotification( $value = 'on' ) {
		$this->userSettings->email_message_notification = strtolower( $value );
		return $this;
	}

	/**
	 * 
	 */
	public function save() {
		$this->userSettings->updated_at = Carbon::now();
		return $this->userSettings->save();
	}

}