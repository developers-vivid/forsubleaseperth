<?php

namespace App;

use Session;
use DB;

class Message {

	protected $msgHndlr;

	/**
	 * [__construct description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-01T11:18:25+0800]
	 */
	public function __construct( $api = '' ){
		$this->msgHndlr = new \stdClass();
		$this->msgHndlr->message = '';

		if( $api != '' )
			$this->msgHndlr->api = $api;

		$this->msgHndlr->status = 'error';
		$this->msgHndlr->data = [];
	}

	/**
	 * [setApi description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-07-05T08:36:18+0800]
	 * @param  string $api [description]
	 */
	public function setApi( $api = '' ){
		$this->msgHndlr->api = $api;
		return $this;
	}

	/**
	 * [setMessage description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-01T11:30:49+0800]
	 * @param  [type] $msg [description]
	 */
	public function setMessage( $msg = '' ) {
		$this->msgHndlr->message = $msg;
		return $this;
	}

	/**
	 * [setData description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-01T11:32:43+0800]
	 */
	public function setData( $data = array() ) {
		$this->msgHndlr->data = $data;
		return $this;
	}

	/**
	 * [setError description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-01T11:30:53+0800]
	 */
	public function setError() {
		$this->msgHndlr->status = 'error';
		return $this;
	}

	/**
	 * [setSuccess description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-01T11:30:58+0800]
	 */
	public function setSuccess() {
		$this->msgHndlr->status = 'success';
		return $this;
	}

	/**
	 * [setWarning description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-01T11:31:00+0800]
	 */
	public function setWarning() {
		$this->msgHndlr->status = 'warning';
		return $this;
	}

	/**
	 * [setApiName description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-01T11:31:03+0800]
	 * @param  [type] $apiName [description]
	 */
	public function setApiName( $apiName ) {
		$this->msgHndlr->api = $apiName;
		return $this;
	}

	/**
	 * [flash description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-01T11:31:05+0800]
	 * @return [type] [description]
	 */
	public function flash() {
		Session::flash('alert', $this->msgHndlr);
		return $this;
	}

	/**
	 * [display description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-01T11:31:08+0800]
	 * @return [type] [description]
	 */
	public function display() {
		return response()->json($this->msgHndlr);
	}

	/**
	 * If and in josn format and data only
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-29T11:17:25+0800]
	 * @return [type] [description]
	 */
	public function show() {
		return $this->msgHndlr;
	}
}