<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

use App\Helper as Hlpr;
use Illuminate\Database\Eloquent\SoftDeletes;


class Products extends Model {
    
    use SoftDeletes;

    protected $table = 'products';
    protected $primaryKey = 'product_id';
    public $incrementing = false;
    protected $softDelete = true;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
    protected $fillable = [
        'product_id', 
        'product_blurb', 
        'product_title', 
        'product_description', 
        'product_price', 
        'product_sale_price', 
        'shipping_cost', 
        'shop_name', 
        'product_primary_photo', 
        'accept_offers', 
        'paypal_email', 
        'shopping_policy', 
        'status', 
        'created_by', 
        'sold_at'
    ];

    protected $dates = [
        'deleted_at', 
        'cancelled_at', 
        'created_at', 
        'updated_at',
    ];

    protected $hidden = [];

    public static function boot() {
        parent::boot();

        static::creating(function($model){
            foreach ($model->attributes as $key => $value) {
                $model->{$key} = empty($value) ? null : $value;
            }
        });

        static::updating(function($model){
            foreach ($model->attributes as $key => $value) {
                $model->{$key} = empty($value) ? null : $value;
            }
        });
    }

    /**
     * Search model
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-04-29T10:03:10+0800]
     * @param  [type] $keyword [description]
     * @return [type]          [description]
     */
    public static function searchGears( $keyword, $type, $location, $brand, $priceMin, $priceMax, $sort, $isMobile = false ) {
        $sort = explode("|", $sort);

        $query = DB::table('products as prod')
                    ->select(
                        'prod.product_id', 'prod.product_title', 'prod.created_at', 'prod.status', Hlpr::nullToStr('prod.sold_at'), Hlpr::nullToStr('prod.cancelled_at'), Hlpr::nullToStr('prod.deleted_at'), 'prod.product_blurb', 'prod.product_description', 'prod.product_price', Hlpr::nullToStr('prod.product_sale_price'), 'prod.created_by',
                        'u.name as owner', 'u.email as owner_email', 'photo.photo_filename as gear_photo')
                    ->when(($type != '' && $type != 'all'), function($whenType) use ($type){
                        return $whenType->where('specs.product_type', strtolower($type));
                    })
                    ->when(($location != '' && $location != 'all'), function($whenLocation) use ($location){
                        return $whenLocation->where('ship.state', strtolower($location));
                    })
                    ->when(($brand != '' && $brand != 'all'), function($whenBrand) use ($brand){
                        return $whenBrand->where('specs.product_brand', 'like', '%'. $brand .'%');
                    })
                    ->when(($priceMin != '' && $priceMax != ''), function($whenPrice) use ($priceMin, $priceMax){
                        return $whenPrice->whereBetween('prod.product_price', [$priceMin, $priceMax]);
                    })
                    ->when(($keyword != '' && $keyword != 'all'), function($whenKeyword) use ($keyword){
                        return $whenKeyword->where(function($where) use ($keyword) {
                            $where
                                ->where('prod.product_blurb', 'like', '%'. $keyword .'%')
                                ->orWhere('prod.product_title', 'like', '%'. $keyword .'%')
                                ->orWhere('prod.product_description', 'like', '%'. $keyword .'%');
                            });
                    })
                    ->leftJoin('products_photos as photo', 'photo.photo_id', '=', 'prod.product_primary_photo')
                    ->leftJoin('products_specs as specs', 'specs.product_id', '=', 'prod.product_id')
                    ->leftJoin('products_shipping as ship', 'ship.product_id', '=', 'prod.product_id')
                    ->leftJoin('products_shipping_cost as ship_cost', 'ship_cost.product_id', '=', 'prod.product_id')
                    ->leftJoin('users as u', 'u.user_id', '=', 'prod.created_by')
                    ->where(function($whereValidProduct){
                        $whereValidProduct
                            ->where('prod.status', 'active')
                            ->whereNull('prod.deleted_at')
                            ->whereNull('prod.cancelled_at')
                            ->whereNull('prod.sold_at');
                    })
                    ->groupBy('prod.product_id')
                    ->when( (is_array($sort) && count($sort) > 0 && isset($sort[0]) && in_array(strtolower($sort[0]), ['created_at', 'product_price'])), function($whenValidSort) use($sort){
                        return $whenValidSort->orderBy('prod.'. $sort[0], $sort[1]);
                    });

        return $isMobile ? $query->get() : $query->paginate((int)config('gp_conf.pagination'));
    }

    /**
     * [getSavedSearch description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-09-01T15:46:53+0800]
     * @param  [type] $keyword  [description]
     * @param  [type] $type     [description]
     * @param  [type] $location [description]
     * @param  [type] $brand    [description]
     * @param  [type] $priceMin [description]
     * @param  [type] $priceMax [description]
     * @param  [type] $sort     [description]
     * @return [type]           [description]
     */
    public static function getSavedSearch( $keyword, $type, $location, $brand, $priceMin, $priceMax, $sort, $productId = null ) {
        $sort = explode("|", urldecode($sort));

        $savedSearch =  DB::table('products as prod')
                        ->select(
                            'prod.product_id', 'prod.product_title', 'prod.created_at', 'prod.status', Hlpr::nullToStr('prod.sold_at'), Hlpr::nullToStr('prod.cancelled_at'), Hlpr::nullToStr('prod.deleted_at'), 'prod.product_blurb', 'prod.product_description', 'prod.product_price', Hlpr::nullToStr('prod.product_sale_price'), 'prod.created_by',
                            'u.name as owner', 'u.email as owner_email', 'photo.photo_filename as gear_photo')
                        ->when(($type != '' && $type != 'all'), function($whenType) use ($type){
                            return $whenType->where('specs.product_type', strtolower($type));
                        })
                        ->when(($location != '' && $location != 'all'), function($whenLocation) use ($location){
                            return $whenLocation->where('ship.state', strtolower($location));
                        })
                        ->when(($brand != '' && $brand != 'all'), function($whenBrand) use ($brand){
                            return $whenBrand->where('specs.product_brand', 'like', '%'. $brand .'%');
                        })
                        ->when(($priceMin != '' && $priceMax != ''), function($whenPrice) use ($priceMin, $priceMax){
                            return $whenPrice->whereBetween('prod.product_price', [$priceMin, $priceMax]);
                        })
                        ->when(($keyword != '' && $keyword != 'all'), function($whenKeyword) use ($keyword){
                            return $whenKeyword->where(function($where) use ($keyword) {
                                $where
                                    ->where('prod.product_blurb', 'like', '%'. $keyword .'%')
                                    ->orWhere('prod.product_title', 'like', '%'. $keyword .'%')
                                    ->orWhere('prod.product_description', 'like', '%'. $keyword .'%');
                                });
                        })
                        ->leftJoin('products_photos as photo', 'photo.photo_id', '=', 'prod.product_primary_photo')
                        ->leftJoin('products_specs as specs', 'specs.product_id', '=', 'prod.product_id')
                        ->leftJoin('products_shipping as ship', 'ship.product_id', '=', 'prod.product_id')
                        ->leftJoin('products_shipping_cost as ship_cost', 'ship_cost.product_id', '=', 'prod.product_id')
                        ->leftJoin('users as u', 'u.user_id', '=', 'prod.created_by')
                        ->where(function($whereValidProduct){
                            $whereValidProduct
                                ->where('prod.status', 'active')
                                ->whereNull('prod.deleted_at')
                                ->whereNull('prod.cancelled_at')
                                ->whereNull('prod.sold_at');
                        })
                        ->groupBy('prod.product_id')
                        ->when( (is_array($sort) && count($sort) > 0 && isset($sort[0]) && in_array(strtolower($sort[0]), ['created_at', 'product_price'])), function($whenValidSort) use($sort){
                            return $whenValidSort->orderBy('prod.'. $sort[0], $sort[1]);
                        })
                        ->when((!is_null($productId)), function($query) use($productId){
                            return $query->where('prod.product_id', $productId);
                        })
                        ->skip( 0 )
                        ->take( 10 );

        return is_null($productId) ? $savedSearch->get() : $savedSearch->first();
    }

    /**
     * Handles fetching gears in home page
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-03T09:11:22+0800]
     * @return [type] [description]
    */
    public static function getHomeGears(){
        $query = DB::table('products as prod')
                    ->select(
                        'prod.product_id', 'prod.product_title', 'prod.created_at', 'prod.status', Hlpr::nullToStr('prod.sold_at'), Hlpr::nullToStr('prod.cancelled_at'), Hlpr::nullToStr('prod.deleted_at'), 'prod.product_blurb', 'prod.product_description', 'prod.product_price', Hlpr::nullToStr('prod.product_sale_price'), 'prod.created_by',
                        'u.name as owner', 'u.email as owner_email', 'photo.photo_filename as gear_photo')
                    ->leftJoin('products_photos as photo', 'photo.photo_id', '=', 'prod.product_primary_photo')
                    ->leftJoin('products_specs as specs', 'specs.product_id', '=', 'prod.product_id')
                    ->leftJoin('users as u', 'u.user_id', '=', 'prod.created_by')
                    ->where(function($whereValidProduct){
                        $whereValidProduct
                            ->where('prod.status', 'active')
                            ->whereNull('prod.deleted_at')
                            ->whereNull('prod.cancelled_at')
                            ->whereNull('prod.sold_at');
                    })
                    ->orderBy('prod.created_at', 'desc');
                    // ->skip(0)
                    // ->take(9);

        return $query->paginate( (int)config('gp_conf.pagination') );
    }

    /**
     * To get single or a list of products
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-03-31T14:31:03+0800]
     * @param  [type] $productID       [description]
     * @param  [type] $productTitle    [description]
     * @param  [type] $productCategory [description]
     * @param  [type] $location        [description]
     * @param  [type] $brand           [description]
     * @param  [type] $type            [description]
     * @param  [type] $priceRange      [description]
     * @return [type]                  [description]
     */
    public static function getProductDetails( $productID = null, $productTitle = null, $productCategory = null, $productSubCategory = null, $location = null, $brand = null, $type = null, $priceRange = null, $myGear = false ){
        $query = DB::table('products as prod')
                    ->select(
                        'prod.product_id', 'prod.product_blurb', 'prod.product_title', Hlpr::nullToStr('prod.product_description'), 'prod.product_price', Hlpr::nullToStr('prod.product_sale_price'), Hlpr::nullToStr('prod.shipping_cost'), Hlpr::nullToStr('prod.shop_name'), Hlpr::nullToStr('prod.product_primary_photo'), Hlpr::nullToStr('prod.accept_offers'), 
                        'prod.paypal_email', Hlpr::nullToStr('prod.shopping_policy'), 'prod.status', 'prod.created_by', Hlpr::nullToStr('prod.sold_at'), Hlpr::nullToStr('prod.cancelled_at'), Hlpr::nullToStr('prod.deleted_at'), 'prod.created_at', Hlpr::nullToStr('prod.updated_at'),
                        'ps.product_brand', 'ps.product_model', Hlpr::nullToStr('ps.product_finish'), 'ps.product_year', 'ps.product_colour', 'ps.product_made_in', 'ps.product_type', 'ps.product_categories as category_id', Hlpr::nullToStr('ps.product_subcategories', 'sub_category_id'), 'ps.product_condition as condition_id', 
                        'pc.name as product_condition', 
                        'cat.name as product_category', 
                        'photo.photo_filename', 'photo.photo_title', 
                        'ship.country as shipping_country', 'ship.city as shipping_city', 'ship.state as shipping_state', 'ship.policy as shipping_policy', 'ship.method as shipping_method',
                        'u.name', 'u.email', 'u.status', 'u.created_at as date_registered', 
                        'up.first_name', 'up.last_name', Hlpr::nullToStr('up.mobile'), 
                        'uc.name as company_name', Hlpr::nullToStr('uc.abn', 'company_abn'), Hlpr::nullToStr('uc.street', 'company_street'), 'uc.city as company_city', 'uc.state as company_state', 
                        Hlpr::nullToStr('uc.post_code', 'company_post_code'), 'uc.phone as company_phone', Hlpr::nullToStr('uc.logo', 'company_logo')
                    )
                    ->when(( ! is_null($productID) ), function($whenProductId) use ($productID) {
                        return $whenProductId->where('prod.product_id', $productID);
                    })
                    ->when(( ! is_null($productTitle) ), function($whenProductTitle) use ($productTitle) {
                        return $whenProductTitle->where('prod.product_blurb', $productTitle);
                    })
                    ->when(( ! $myGear ), function($whenMyGear) use ($productTitle) {
                        return $whenMyGear
                                ->where(function($whereValidProduct){
                                    $whereValidProduct
                                        ->where('prod.status', 'active')
                                        ->whereNull('prod.deleted_at')
                                        ->whereNull('prod.cancelled_at')
                                        ->whereNull('prod.sold_at');
                                });
                                // ->where('prod.status', 'active')
                                // ->whereNull('prod.sold_at');
                    })
                    ->leftJoin('products_specs as ps', 'ps.product_id', '=', 'prod.product_id')
                    ->leftJoin('products_condition as pc', 'pc.condition_id', '=', 'ps.product_condition')
                    ->leftJoin('products_photos as photo', 'photo.photo_id', '=', 'prod.product_primary_photo')
                    ->leftJoin('products_shipping as ship', 'ship.product_id', '=', 'prod.product_id')
                    ->leftJoin('categories as cat', 'cat.category_id', '=', 'ps.product_categories')
                    ->leftJoin('users as u', 'u.user_id', '=', 'prod.created_by')
                    ->leftJoin('users_profile as up', 'up.user_id', '=', 'prod.created_by')
                    ->leftJoin('users_company as uc', 'uc.user_id', '=', 'prod.created_by')
                    // ->whereNull('prod.deleted_at')
                    ->orderBy('prod.created_at', 'desc');

        $result = $query->paginate( (int)config('gp_conf.pagination') );

        if( count($result) > 0 ) {
            foreach( $result as $res ) {
                $res->photos       = \App\ProductsPhotos::whereProductId($res->product_id)->select('photo_id', 'photo_title', 'photo_filename')->get();
                $res->isOwner      = (Auth::check() && Auth::user()->user_id == $res->created_by);
                $res->shippingCost = \App\ProductsShippingCost::whereProductId( $res->product_id )->select('product_shipping_cost_id', 'location', 'cost')->get();
            }
        }
        
        if( is_null($productCategory) )
            return count($result) > 0 ? $result[0] : (object)[];
        else
            return $result;
    }

    /**
     * To get related gear items
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-10T15:54:43+0800]
     * @param  [type] $productId [description]
     * @return [type]            [description]
     */
    public static function getRelatedItems( $productId, $categoryId ) {
        return DB::table('products_specs as spec')
                ->select('prod.product_title', 'prod.product_blurb', 'prod.created_by', 'photo.photo_filename')
                ->where(function($whereSpec) use($productId, $categoryId){
                    return $whereSpec
                            ->where('spec.product_id', '!=', $productId)
                            ->where('spec.product_categories', $categoryId);
                })
                ->leftJoin('products as prod', 'spec.product_id', '=', 'prod.product_id')
                ->leftJoin('products_photos as photo', 'photo.photo_id', '=', 'prod.product_primary_photo')
                ->where(function($whereValidProduct){
                    $whereValidProduct
                        ->where('prod.status', 'active')
                        ->whereNull('prod.deleted_at')
                        ->whereNull('prod.cancelled_at')
                        ->whereNull('prod.sold_at');
                })
                /*->where(function($whereValid){
                    return $whereValid
                            ->where('prod.status', 'active')
                            ->whereNull('prod.sold_at');
                })*/
                ->skip(0)
                ->take(4)
                ->get();
    }

    /**
     * Get product/gears via category
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-04-22T14:23:48+0800]
     * @param  [type] $category    [description]
     * @param  [type] $subCategory [description]
     * @return [type]              [description]
     */
    public static function getGearsByCategory( $category, $subCategory = null, $type, $location, $brand, $priceMin, $priceMax, $sort, $isMobile = false ){
        $sort = explode("|", $sort);

        $query = DB::table('products as prod')
                    ->select(
                        'prod.product_id', 'prod.product_title', 'prod.created_at', 'prod.status', Hlpr::nullToStr('prod.sold_at'), Hlpr::nullToStr('prod.cancelled_at'), Hlpr::nullToStr('prod.deleted_at'), 'prod.product_blurb', 'prod.product_description', 'prod.product_price', Hlpr::nullToStr('prod.product_sale_price'), 'prod.created_by',
                        'u.name as owner', 'u.email as owner_email', 'cat.slug as category_slug', 'cat.name as category_name', 'cat.parent as category_parent',
                        'photo.photo_filename as gear_photo')
                    ->leftJoin('products_specs as ps', 'ps.product_id', '=', 'prod.product_id')
                    ->leftJoin('products_condition as pc', 'pc.condition_id', '=', 'ps.product_condition')
                    ->leftJoin('products_shipping as ship', 'ship.product_id', '=', 'prod.product_id')
                    ->leftJoin('products_shipping_cost as ship_cost', 'ship_cost.product_id', '=', 'prod.product_id')
                    ->leftJoin('products_photos as photo', 'photo.photo_id', '=', 'prod.product_primary_photo')
                    ->when(( ! is_null($subCategory) && $subCategory != 'all' ), function($whenSubCategory) use ($subCategory){
                        return $whenSubCategory
                                ->leftJoin('categories as cat', 'cat.category_id', '=', 'ps.product_subcategories')
                                ->where('cat.slug', $subCategory);
                    })
                    ->when(( is_null($subCategory) || $subCategory == 'all' ), function($whenCategory) use ($category){
                        return $whenCategory
                                ->leftJoin('categories as cat', 'cat.category_id', '=', 'ps.product_categories')
                                ->where('cat.slug', $category);
                    })
                    ->when(($type != '' && $type != 'all'), function($whenType) use ($type){
                        return $whenType->where('ps.product_type', $type);
                    })
                    ->when(($location != '' && $location != 'all'), function($whenLocation) use ($location){
                        return $whenLocation->where('ship.state', strtolower($location));
                    })
                    ->when(($brand != '' && $brand != 'all' ), function($whenBrand) use ($brand){
                        return $whenBrand->where('ps.product_brand', 'like', '%'. $brand .'%');
                    })
                    ->when(($priceMin != '' && $priceMax != ''), function($whenPrice) use ($priceMin, $priceMax){
                        return $whenPrice->whereBetween('prod.product_price', [$priceMin, $priceMax]);
                    })
                    ->leftJoin('users as u', 'u.user_id', '=', 'prod.created_by')
                    ->leftJoin('users_profile as up', 'up.user_id', '=', 'prod.created_by')
                    ->leftJoin('users_company as uc', 'uc.user_id', '=', 'prod.created_by')
                    ->where(function($whereValidProduct){
                        $whereValidProduct
                            ->where('prod.status', 'active')
                            ->whereNull('prod.deleted_at')
                            ->whereNull('prod.cancelled_at')
                            ->whereNull('prod.sold_at');
                    })
                    /*->where('prod.status', 'active')
                    ->whereNull('prod.deleted_at')
                    ->whereNull('prod.sold_at')*/
                    // ->whereNull('pc.deleted_at')
                    // ->whereNull('cat.deleted_at')
                    ->groupBy('prod.product_id')
                    ->when( (is_array($sort) && count($sort) > 0 && isset($sort[0]) && in_array(strtolower($sort[0]), ['created_at', 'product_price'])), function($whenValidSort) use($sort){
                        return $whenValidSort->orderBy('prod.'. $sort[0], $sort[1]);
                    });

        return $isMobile ? $query->get() : $query->paginate((int)config('gp_conf.pagination'));
    }

    /**
     * Users' ads/gears
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-04-04T11:52:12+0800]
     * @return [type] [description]
     */
    public static function getMyAds( $status, $type, $location, $brand, $priceMin, $priceMax, $sort, $userID = null, $isMobile = false ) {
        $sort = explode("|", $sort);

        $query = DB::table('products as prod')
                    ->select(
                        'prod.product_id', 'prod.product_blurb', 'prod.product_title', 'prod.product_description', 'prod.product_price', 'prod.product_sale_price', 
                        'prod.status', 'prod.created_at', Hlpr::nullToStr('prod.sold_at'), Hlpr::nullToStr('prod.cancelled_at'), Hlpr::nullToStr('prod.deleted_at'),
                        'u.name as owner', 'u.email as owner_email', 'photo.photo_filename as gear_photo'
                    )
                    ->when(($status != '' && $status != 'all'), function($whenStatus) use ($status){
                        return $whenStatus->where('prod.status', strtolower($status));
                    })
                    ->when(($type != '' && $type != 'all'), function($whenType) use ($type){
                        return $whenType->where('specs.product_type', strtolower($type));
                    })
                    ->when(($location != '' && $location != 'all'), function($whenLocation) use ($location){
                        return $whenLocation->where('ship.state', strtolower($location));
                    })
                    ->when(($brand != '' && $brand != 'all'), function($whenBrand) use ($brand){
                        return $whenBrand->where('specs.product_brand', 'like', '%'. $brand .'%');
                    })
                    ->when(($priceMin != '' && $priceMax != ''), function($whenPrice) use ($priceMin, $priceMax){
                        return $whenPrice->whereBetween('prod.product_price', [$priceMin, $priceMax]);
                    })
                    ->when((!is_null($userID)), function($whenUserId) use($userID){
                        return $whenUserId->where('prod.created_by', $userID);
                    })
                    ->when((is_null($userID) || $userID == ''), function($whenUserId){
                        return $whenUserId->where('prod.created_by', Auth::user()->user_id);
                    })
                    ->leftJoin('products_photos as photo', 'photo.photo_id', '=', 'prod.product_primary_photo')
                    ->leftJoin('products_specs as specs', 'specs.product_id', '=', 'prod.product_id')
                    ->leftJoin('products_shipping as ship', 'ship.product_id', '=', 'prod.product_id')
                    ->leftJoin('products_shipping_cost as ship_cost', 'ship_cost.product_id', '=', 'prod.product_id')
                    ->leftJoin('users as u', 'u.user_id', '=', 'prod.created_by')
                    ->whereNull('prod.deleted_at')
                    ->groupBy('prod.product_id')
                    ->when( (is_array($sort) && count($sort) > 0 && isset($sort[0]) && in_array(strtolower($sort[0]), ['created_at', 'product_price'])), function($whenValidSort) use($sort){
                        return $whenValidSort->orderBy('prod.'. $sort[0], $sort[1]);
                    });

        return $isMobile ? $query->get() : $query->paginate((int)config('gp_conf.pagination'));
    }

    /**
     * To get all gear brands
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-04T10:21:16+0800]
     * @return [type] [description]
     */
    public static function getAllBrands(){
        return DB::table("products as prod")
                  ->select('specs.product_brand')
                  ->leftJoin('products_specs as specs', 'prod.product_id', '=', 'specs.product_id')
                  // ->groupBy('specs.product_id')
                  ->groupBy('specs.product_brand')
                  ->orderBy('specs.product_brand', 'asc')
                  ->get();
    }

    /**
     * To count total products
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-15T15:50:05+0800]
     * @param  [type] $categoryID    [description]
     * @param  [type] $subCategoryID [description]
     * @return [type]                [description]
     */
    public static function getTotalProducts( $categoryID, $subCategoryID = null ) {
        $query =    DB::table('products_specs as ps')
                    ->select( 'ps.product_id' )
                    ->when(( !is_null($subCategoryID) ), function($whenSubCategoryId) use($subCategoryID) {
                        return $whenSubCategoryId->where('ps.product_subcategories', $subCategoryID);
                    })
                    ->leftJoin('products as prod', 'ps.product_id', '=', 'prod.product_id')
                    ->where('ps.product_categories', $categoryID)
                    ->where(function($whereValidProduct){
                        $whereValidProduct
                            ->where('prod.status', 'active')
                            ->whereNull('prod.deleted_at')
                            ->whereNull('prod.cancelled_at')
                            ->whereNull('prod.sold_at');
                    })
                    /*->where(function($query){
                        $query->where('prod.status', 'active')
                          ->whereNull('prod.deleted_at')
                          ->whereNull('prod.sold_at');
                    })*/
                    ->groupBy('prod.product_id')
                    ->get();

        return count($query);
    }

    /**
     * [getProductOwner description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-11-03T10:44:06+0800]
     * @param  [type] $productId [description]
     * @return [type]            [description]
     */
    public static function getProductOwner( $productId ) {
        return DB::table('products as prod')
                ->select(Hlpr::nullToStr('user.email', 'seller_email'), Hlpr::nullToStr('user.name', 'seller_name'), 
                        Hlpr::nullToStr('uprof.first_name', 'seller_first_name'), Hlpr::nullToStr('uprof.last_name', 'seller_last_name'),
                        Hlpr::nullToStr('uprof.telephone', 'seller_telephone'), Hlpr::nullToStr('uprof.mobile', 'seller_mobile')
                    )
                ->leftJoin('users as user', 'prod.created_by', '=', 'user.user_id')
                ->leftJoin('users_profile as uprof', 'user.user_id', '=', 'uprof.user_id')
                ->where('prod.product_id', $productId)
                ->groupBy('user.user_id')
                ->first();
    }

    /**
     * Product Photos
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-03-31T09:07:16+0800]
     * @return [type] [description]
     */
    public function photos(){
        return $this->hasMany('App\ProductsPhotos', 'product_id');
    }

    /**
     * To get product specs
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-04T10:17:34+0800]
     * @return [type] [description]
     */
    public function specs(){
        return $this->hasOne('App\ProductsSpecs', 'product_id');
    }

    /**
     * Product Owner
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-03-31T09:53:50+0800]
     * @return [type] [description]
     */
    public function user(){
        return $this->belongsTo('App\User', 'created_by');
    }

    /**
     * To get product owner via product.created_by
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-04T10:17:42+0800]
     * @return [type] [description]
    */
    public function profile(){
        return $this->belongsTo('App\UsersProfile', 'created_by', 'user_id');
    }

    /**
     * To get product owner company via product.created_by
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-04T10:17:56+0800]
     * @return [type] [description]
     */
    public function company(){
        return $this->belongsTo('App\UsersCompany', 'created_by', 'user_id');
    }
}
