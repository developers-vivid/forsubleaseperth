<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

use ErrorException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Debug\Exception\FatalErrorException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {   
        $onLive = in_array(strtolower( app()->environment() ), ['staging', 'production']);
        $postParams = json_decode($request->getContent());

        // if( $request->isMethod('post') && (str_contains($request->url(), env('API_PREFIX', 'api/v'.env('API_VERSION', 1))) || str_contains($request->url(), 'EoUvsyjX0xYcSpIRWTew3ni9GrJda7PhmAZt6OKCgHuN4B2LfV')) && !is_null($postParams) ) {
        if( $request->isMethod('post') && (str_contains($request->url(), env('API_PREFIX', 'api/v'.env('API_VERSION', 1))) || str_contains($request->url(), 'EoUvsyjX0xYcSpIRWTew3ni9GrJda7PhmAZt6OKCgHuN4B2LfV')) ) {
            $msg = new \App\Message();
            $msg->setData(['datetime' => \Carbon\Carbon::now()]);    

            // brute force
            if( $e instanceof NotFoundHttpException || $e instanceof RuntimeException )
                return $msg->setMessage( trans('messages.api.error.page_not_found') )->display();

            if( $onLive ) {
                if( ($e instanceof FatalErrorException) || ($e instanceof QueryException) || ($e instanceof ReflectionException) || ($e instanceof BadMethodCallException) )
                    return $msg->setMessage( trans('messages.api.error.page_not_found') )->display();

                else if( $e instanceof MethodNotAllowedHttpException )
                    return $msg->setMessage( trans('messages.api.error.unrecognized_method') )->display();

                else if( $e instanceof ErrorException )
                    return $msg->setMessage( trans('messages.api.error.went_wrong') )->display();
            }

            return parent::render($request, $e);
        }
        else {
            // On Symfony\Component\Debug\Exception\FatalErrorException
            if ($e instanceof FatalErrorException && ! config('gp_conf.debug'))
                return response()->view('errors.404');

            // On Symfony\Component\HttpKernel\Exception\NotFoundHttpException
            else if ($e instanceof NotFoundHttpException && ! config('gp_conf.debug'))
                return response()->view('errors.404');

            // On Illuminate\Database\QueryException
            else if ($e instanceof QueryException && ! config('gp_conf.debug'))
                return response()->view('errors.404');

            // On Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException
            else if( $e instanceof MethodNotAllowedHttpException && ! config('gp_conf.debug') )
                return response()->view('errors.method-not-allowed');

            // On ErrorException
            else if( $e instanceof ErrorException && ! config('gp_conf.debug') )
                return response()->view('errors.error-exception');
            
            return parent::render($request, $e);            
        }
    }
}
