<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

use App\Helper as Hlpr;

class OrdersProducts extends Model {
	protected $table = 'orders_products';
	protected $primaryKey = 'order_product_id';
	public $incrementing = false;

	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'order_product_id', 'order_id', 'product_id', 'price', 'shipping_cost', 'qty', 'commission',
	];

	protected $dates = [
		'created_at', 'updated_at',
	];

	protected $hidden = [
	];

	/**
     * Get user ordered products
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-27T14:28:22+0800]
     * @param  [type] $orderID [description]
     * @return [type]          [description]
     */
    public static function getUserOrders( $orderID ){
        return DB::table('orders_products as op')
                    ->select('op.product_id', 
                        DB::raw('SUM('.config('gp_conf.db_prefix').'op.price * '. config('gp_conf.db_prefix') .'op.qty) as total'),
                        'op.shipping_cost', 'op.created_at', 'prod.paypal_email')
                    ->leftJoin('products as prod', 'op.product_id', '=', 'prod.product_id')
                    ->where('op.order_id', $orderID)
                    ->groupBy('prod.paypal_email')
                    ->get();
    }

    /**
     * To get full ordered products details
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-13T09:03:37+0800]
     * @param  [type] $orderId [description]
     * @return [type]          [description]
     */
    public static function getOrderedProducts( $orderId ) {
    	return DB::table('orders_products as op')
    			->select('prod.product_id', 'prod.product_title', 'op.price', 'op.shipping_cost', 'op.qty', 'op.commission', 'user.email as owner_email', 'user.name as owner_name')
    			->leftJoin('products as prod', 'op.product_id', '=', 'prod.product_id')
    			->leftJoin('users as user', 'prod.created_by', '=', 'user.user_id')
    			->where('op.order_id', $orderId)
    			->orderBy('op.product_id')
    			->get();
    }
}
