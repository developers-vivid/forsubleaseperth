<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use DB;

class BlogsComments extends Model
{
    protected $table = 'blogs_comments';
	protected $primaryKey = 'blog_comment_id';
	public $incrementing = false;
	protected $softDelete = true;

	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'blog_comment_id',
		'blog_id',
		'comment',
		'commented_by',
	];

	protected $dates = [
		'deleted_at',
		'created_at',
		'updated_at',
	];

	protected $hidden = [
		'blog_comment_id',
		'blog_id',
	];
}
