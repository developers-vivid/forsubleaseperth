<?php
	
	use Carbon\Carbon;
	// Author: RichmundLofranco

	return [

		// This will be the global date time format
		"datetimeformat" => 'd-m-Y h:i:s',

		// The Timezon
		"timezone" => 'Asia/Manila',

		// Maximum set of memory limit
		"memory_limit" => '5000M',

		// Maximum set of execution time
		"execution_time" => '5000',

		// Random String
		"alpha_num" => 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',

		// short_random
		"short_random" => 'abcdefghijklmnopqrstuvwxyz0123456789',

		// DIRECTORY_SEPARATOR
		"ds" => DIRECTORY_SEPARATOR,

		// Pagination limit
		"pagination" => 9,

		"pagination_limit" => [
			"admin" => [
				"blogs" => 10
			],
		],

		// Minimum Gear Price
		"gear_price" => [
			"min" => 50,
			"max" => 1500
		],

		// access
		"access" => [
			"logs" => env('APP_ACCESS_LOG', '0a9dc1f2-ec24-4696-97af-d0b5578c7ff2'),
		],

		// Gear Planet share percentage
		"prcnt_share" => 0.035,

		// db prefix
		"db_prefix" => env('DB_PREFIX', 'gp_'),

		// Debug mode
		"debug" => (env('APP_DEBUGGING', 'yes') == 'yes' ? true : false),

		// Site locality
		"isLocal" => (in_array(strtolower(env('APP_ENV')), ['local', 'development']) ? true : false),

		// Pusher ID
		"pusher_id" => '3f0904a9cce6d999a954',

		// can
		"can" => [
			"delete_ad" => env('ALLOW_DELETE_AD', false),
		],

		// pusher details
		"pusher" => [
			"new_message" => [
				"local"       => 'kYiHxKcIFUfCW',
				"development" => 'Qgs0IKMt5xfVw',
				"staging"     => 'iOLSmXBkdq5DC',
				"production"  => 'bwV4tnCzyBmex',
			],
			"msg_counter" => [
				"local"       => '6vtDrWFRlkmyn',
				"development" => '1ItjHAyr9acRY',
				"staging"     => 'ouEGF7PQT1Jwj',
				"production"  => '1muPpEOtfy2UF',
			],
		],

		// Pusher event
		"pusher_msg_counter_id" => 'Rgj5KumwVBndQkbily20',

		// Payment level
		"isBuyNow" => true,

		// set all user as seller
		"allseller" => true,

		// sort
		"sort" => [
			"" => '- Select -',
			"created_at|desc" => 'Most Recent First',
			"product_price|asc" => 'Price Low to High',
			"product_price|desc" => 'Price High to Low',
		],

		// Order status
		"orders" => [
			"status" => [
				"pendingPayment" => '573e5b0202eb9',
				"processing"     => '573e5b0202ee6',
				"onHold"         => '573e5b0202eeb',
				"completed"      => '573e5b0202eef',
				"cancelled"      => '573e5b0202ef3',
				"refunded"       => '573e5b0202ef9',
				"failed"         => '573e5b0202efc'
			],
		],

		// gear photo url
		"gear_photo_url" => public_path() .DIRECTORY_SEPARATOR. 'images' .DIRECTORY_SEPARATOR. 'gears' .DIRECTORY_SEPARATOR,

		"photo" => [
			"blog_path" => public_path() .DIRECTORY_SEPARATOR. 'images' .DIRECTORY_SEPARATOR. 'blogs' .DIRECTORY_SEPARATOR,
		],

		"require_token" => [
			"CreateGear",
			"MyAds",
			"ContactSeller",
			"SendMessage",
		],

		// PayPal Adaptive Payment
		"pp_adaptive_payment" => [
			'mode'            => env('PAYPAL_MODE', 'sandbox'),
			'acct1.AppId'     => env('PAYPAL_APPLICATION_ID', 'APP-80W284485P519543T'),
			'acct1.UserName'  => env('PAYPAL_USERID', 'richmund-facilitator_api1.sushidigital.com.au'),
			'acct1.Password'  => env('PAYPAL_PASSWORD', 'DWL92ZBZLYTVS325'),
			'acct1.Signature' => env('PAYPAL_SIGNATURE', 'AFcWxV21C7fd0v3bYYYRCpSSRl31AhwR1UotWjj8kmOL8L9DGpmEnWtR'),
		],

		// from http://stackoverflow.com/questions/10058768/php-apple-enhanced-push-notification-read-error-response
		"apns_status_code" => [
			0   => '0 - No errors encountered',
			1   => '1 - Processing Error',
			2   => '2 - Missing Device Token',
			3   => '3 - Missing Topic',
			4   => '4 - Missing Payload',
			5   => '5 - Invalid Token Size',
			6   => '6 - Invalid Topic Size',
			7   => '7 - Invalid Payload Size',
			8   => '8 - Invalid Token',
			255 => '255 - None (unknown)',
		],

		"apiguest" => [
			"dashboard",
			"settings",
			"categories",
			"gears",
			"search-gears",
			"sign-in",
			"sign-up",
			"gear-details",
		],
		"menus" => [
			"menu" => [
				"dashboard",
				"settings",
			],
			"gears" => [
				"gears",
				"search-gears",
				"my-ads",
				"gear-details",
				"sell-gear",
				"update-gear",
				"cancel-gear",
				"delete-gear",
				"activate-gear",
			],
			"users" => [
				"sign-in",
				"sign-up",
			],
			"messages" => [
				"contact-seller",
				"send-message",
				"inbox",
				"inbox-messages",
				"inbox-message-details",
			],
			"search" => [
				"save-search",
				"delete-search",
				"search-list",
			],
			"orders" => [
				"create-order",
			],
			"test" => [
				// for testing purposes only
	            "send-push-notification"
			],
		],
	];