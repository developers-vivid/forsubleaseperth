-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 07, 2018 at 07:33 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


-- --------------------------------------------------------

--
-- Table structure for table `gp_blogs`
--

CREATE TABLE `gp_blogs` (
  `blog_id` varchar(13) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `photo` varchar(50) NOT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `created_by` varchar(13) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gp_blogs`
--

INSERT INTO `gp_blogs` (`blog_id`, `title`, `slug`, `content`, `photo`, `status`, `created_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
('581999dca42fa', 'Totam mollitia a et ut', 'totam-mollitia-a-et-ut', 'Quaerat et necessitatibus dolorum voluptatem eaque ut. Reiciendis dicta quia enim ut. Eveniet saepe dolorem quos laborum minus. Quam omnis suscipit veniam et exercitationem voluptates molestias.\r\n\r\nHic quasi exercitationem voluptatem quis optio tempore beatae veritatis. Consectetur cum veritatis aut ducimus et voluptas. Amet facere ut doloremque illo. Corrupti vero consequatur iste possimus perferendis. Dolorum molestiae qui vel voluptatem unde illum.\r\n\r\nSit culpa voluptas quod qui dolor. Repellendus dolores eveniet reprehenderit rerum. Accusamus repellendus est officiis autem quaerat ipsum sit. Natus dolores dolor qui sapiente enim aspernatur excepturi qui. Provident nihil magnam tempora eos ipsum.\r\n\r\nRepellat qui eum nostrum et nobis nihil voluptates. Quo doloremque magni iusto incidunt itaque id occaecati. Vitae non doloribus eum inventore. Asperiores aspernatur ut atque nisi.\r\n\r\nAlias autem necessitatibus magnam rerum et cupiditate. Expedita optio omnis sed doloribus ut esse ab vero. Beatae vel incidunt assumenda eius perferendis reiciendis nihil dicta. In debitis aliquam harum similique incidunt voluptatibus.\r\n\r\nVoluptatem in repellendus unde nobis aperiam libero. Dolorem sunt sit culpa animi qui est. Aut architecto numquam unde autem sunt enim. Et omnis harum aspernatur omnis.\r\n\r\nQuia modi quam sed possimus earum numquam et libero. Odit ex rerum itaque. Quis cupiditate quia dolorem id praesentium fuga. Eveniet voluptatum fugiat tenetur non ratione.\r\n\r\nTemporibus ipsam corporis vitae illo quis ea. Non unde distinctio ut libero neque consequatur sed. Sed harum deserunt consequuntur ad pariatur.\r\n\r\nEaque ea iste voluptas. Sed minus voluptas cupiditate. Voluptatem tenetur pariatur sed cupiditate. Laborum in iste dolorem et.\r\n\r\nDicta velit aut error eum enim blanditiis voluptatem voluptate. Ut eveniet dolorem occaecati sed consectetur dolorem recusandae quis. Consequatur quia quia exercitationem numquam et voluptatem voluptatem. Ut consequatur dolores cumque. Cumque ducimus ea ea tempore commodi asperiores.', '4c4dc0c4-f0a8-4d80-8dd6-25b6fbb48b4d.jpg', 'active', '56e0d53867459', NULL, '2016-11-02 07:46:36', '2016-11-02 07:46:36'),
('5a6fc6a370e8f', 'Quisquam ad officia est error dolorem reprehenderit rem', 'quisquam-ad-officia-est-error-dolorem-reprehenderit-rem', 'Unde tempore cumque dolor ea. Qui harum odio temporibus tenetur repellendus similique. Iusto id voluptatem voluptates tempore quo. Illo similique culpa nostrum.\r\n\r\nNon deleniti voluptatem voluptatem dolorem voluptatum consequuntur dolor. Ut dignissimos provident libero quae quae repudiandae. Laborum veniam voluptates harum perferendis laboriosam.\r\n\r\nSunt sit accusamus eum incidunt. Tenetur recusandae sed adipisci ut. Quis officiis quis tempora alias.\r\n\r\nMinima repudiandae aut et necessitatibus nesciunt modi illum. Architecto architecto dolorum hic labore accusamus earum ab. Iste dicta accusamus perspiciatis assumenda. Laudantium eveniet qui et adipisci in.\r\n\r\nMagnam nesciunt quam occaecati quos laborum. Neque harum ullam vel quia sit ipsum. Debitis et iusto quo et.\r\n\r\nAsperiores asperiores dolore autem quo sed. Veritatis quibusdam rerum veniam ratione occaecati et doloribus neque. Accusantium alias rem rerum autem. Repudiandae aliquid qui vero.\r\n\r\nDolores molestias hic delectus nulla explicabo nulla magni. Porro eos temporibus ut sunt illo. Modi iure qui quia quo qui non.\r\n\r\nEt voluptates quaerat animi. Magnam aut maiores fugiat assumenda rem. Hic delectus est consequuntur tempora quam illum.\r\n\r\nOmnis sapiente deleniti dolorem rerum. Dicta numquam nesciunt nihil voluptatem minima. Earum culpa non ut autem et. Facilis eius quas hic tempora explicabo.\r\n\r\nPraesentium iste quia ad voluptas molestiae et accusamus. Adipisci esse alias quia quidem eum et at distinctio. Reprehenderit et consequatur optio rem nam excepturi.', '2733296b-264f-4c28-acc6-e18b7e85d1ee.jpg', 'active', '5a5ed2729be8b', NULL, '2018-01-29 17:13:07', '2018-01-29 17:13:07'),
('5a73d3c9e3b1a', 'Sed totam iure molestias ad', 'sed-totam-iure-molestias-ad', 'Qui veniam asperiores eveniet ullam ipsam voluptas. Facilis laborum ea quis minima et illum esse. Consectetur saepe voluptatem aut magnam dolores esse.\r\n\r\nAut deserunt consequuntur assumenda tempore iure earum. Tempore omnis explicabo officiis. Vitae id sed aut perferendis. Suscipit minus quo dolorem odit impedit nihil voluptatibus.\r\n\r\nQuae eveniet explicabo cumque voluptate dolor laborum. Ea consectetur architecto ipsam error.\r\n\r\nOmnis autem ipsa dignissimos reprehenderit in molestiae. Maxime consequuntur ea alias quos quas reprehenderit enim. Doloremque expedita et dolores quas nihil eos.\r\n\r\nEum non dolorem temporibus nesciunt alias. Doloribus consequatur aut non qui voluptas. Deserunt aliquid fuga dicta quia quia suscipit sint. Quaerat necessitatibus omnis explicabo.\r\n\r\nOdio assumenda quis ut nobis nisi vero. Explicabo eveniet non tenetur cum. Minus mollitia voluptatem aliquam commodi.\r\n\r\nEa recusandae non animi in voluptates. Ducimus optio dicta voluptatem harum vel. Quas fugit vel omnis voluptatibus deleniti rerum eius. Suscipit hic quos asperiores aut libero nobis.\r\n\r\nDebitis vitae sed consequatur totam autem explicabo. Minus ipsam mollitia magnam.\r\n\r\nSint id eligendi iure et neque. Facilis aut id est dolor. Omnis qui ut quas eos necessitatibus non omnis.\r\n\r\nDolore praesentium minus dolores modi sed cum. Placeat ullam provident doloribus et sit sunt quia error. Repudiandae blanditiis omnis tenetur fugiat. Omnis magnam architecto veniam blanditiis dicta.', '88e136d0-4c0e-42a1-8cbb-e0ce76fae812.jpg', 'active', '5a5ed2729be8b', NULL, '2018-02-01 18:58:17', '2018-02-01 18:58:17');

-- --------------------------------------------------------

--
-- Table structure for table `gp_blogs_comments`
--

CREATE TABLE `gp_blogs_comments` (
  `blog_comment_id` varchar(13) NOT NULL,
  `blog_id` varchar(13) NOT NULL,
  `comment` text,
  `commented_by` varchar(13) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gp_builds`
--

CREATE TABLE `gp_builds` (
  `build_id` varchar(13) NOT NULL,
  `version` varchar(30) NOT NULL,
  `device` enum('android','ios') NOT NULL DEFAULT 'android',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gp_builds`
--

INSERT INTO `gp_builds` (`build_id`, `version`, `device`, `created_at`, `updated_at`) VALUES
('57ad31880d237', 'V0.5B0.1', 'ios', '2016-08-12 10:17:51', NULL),
('57ad31880d23b', 'V0.5B0.1', 'android', '2016-08-12 10:17:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gp_cart`
--

CREATE TABLE `gp_cart` (
  `cart_id` varchar(13) NOT NULL,
  `token` varchar(40) NOT NULL,
  `product_id` varchar(13) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `shipping_cost` decimal(9,2) DEFAULT NULL,
  `qty` tinyint(3) NOT NULL DEFAULT '1',
  `expiration` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gp_cart`
--

INSERT INTO `gp_cart` (`cart_id`, `token`, `product_id`, `price`, `shipping_cost`, `qty`, `expiration`, `created_at`, `updated_at`) VALUES
('5757e32b86c59', 'dpc3XYHF0Mw3Ib0xBAtgkqqICwN5MosM39SUONHx', '5757ddfe391dd', '99.95', NULL, 1, '2016-06-09 09:19:39', '2016-06-08 09:19:39', '2016-06-08 09:19:39'),
('575e15ca62324', 'm4ZiATJ5xniwX1lbuXPQhCbaNqlYserJFRHJbj09', '5758e383db954', '215.00', NULL, 1, '2016-06-14 02:09:14', '2016-06-13 02:09:14', '2016-06-13 02:09:14'),
('575f723fb3bb2', 'vYIaJNPloaEiKofsSZHhxTiQ3RJ1nwtjqdaq0lL6', '5758e383db954', '215.00', NULL, 1, '2016-06-15 02:55:59', '2016-06-14 02:55:59', '2016-06-14 02:55:59'),
('5761219645118', 'VIkjUgTvkEkQ6tx8Mrd7mdHkMaev4kI7PEItiu6h', '5758dd33c86c2', '2850.00', NULL, 1, '2016-06-16 09:36:22', '2016-06-15 09:36:22', '2016-06-15 09:36:22'),
('57621ec12c148', 'POLamDJdAFIfLmTiD3MrEHZJz40gIEYfXFdqR7mI', '5757e63794d19', '310.00', NULL, 1, '2016-06-17 03:36:33', '2016-06-16 03:36:33', '2016-06-16 03:36:33'),
('57624bec27300', 'BpMygPqDYbC1TfkUMCWRRMhrRtSF0gsPRPOH5mOe', '5758db44b4238', '89.00', NULL, 1, '2016-06-17 06:49:16', '2016-06-16 06:49:16', '2016-06-16 06:49:16'),
('5763c7834453c', 'DsHjRon7CPzSC9uXSOYMup0qniZbiJxe5BipNts7', '5758dc480d86d', '2600.00', NULL, 1, '2016-06-18 09:48:51', '2016-06-17 09:48:51', '2016-06-17 09:48:51'),
('5765133759e43', 'ehvZ8JEVS0xb3IuWpjsnFlYIJ41bOE0rESLgxI2W', '5758dc480d86d', '2600.00', NULL, 1, '2016-06-19 09:24:07', '2016-06-18 09:24:07', '2016-06-18 09:24:07'),
('57693ef0d97c2', 'BzuK7421ytI9qhV1yDkyenjykSox42nCm9ALVZ54', '5758dc525fc7d', '2800.00', NULL, 1, '2016-06-22 13:19:44', '2016-06-21 13:19:44', '2016-06-21 13:19:44'),
('577e61f831ace', 'bd6CqZxDHLMGiwoONW60wUYXkv1UShl99fVjOG6X', '577e4cb2c7ed0', '500.00', NULL, 1, '2016-07-08 14:06:48', '2016-07-07 14:06:48', '2016-07-07 14:06:48'),
('577e6579d5443', '9alcjGUGJTaN1Ux3Ait9TY8PmeCeY0cxxMpezqba', '577e4f3d12ccd', '300.00', NULL, 1, '2016-07-08 14:21:45', '2016-07-07 14:21:45', '2016-07-07 14:21:45'),
('577f872b1485c', 'I2TJadyDR55gPFlXnKfvnQHOooGhcVmPzGv9YHqt', '577e4f3d12ccd', '300.00', NULL, 1, '2016-07-09 10:57:47', '2016-07-08 10:57:47', '2016-07-08 10:57:47'),
('577fc67e6beda', 'mmrgRxaOtFSS5wgLulXKdq7d1URmtjJIdhqNMvP2', '5758dc480d86d', '2600.00', NULL, 1, '2016-07-09 15:27:58', '2016-07-08 15:27:58', '2016-07-08 15:27:58'),
('57805fa085dfc', '0SoFzJ69Bnf7vw8V5Px6n3BlxLxA3vPtf3A1gwTq', '577e4f3d12ccd', '300.00', NULL, 1, '2016-07-10 02:21:20', '2016-07-09 02:21:20', '2016-07-09 02:21:20'),
('5780e0242d21f', 'GO9TYLXcF2DuQnOrhpmJLWpLswxcVJOI02dBtlsI', '577e4f3d12ccd', '300.00', NULL, 1, '2016-07-10 11:29:40', '2016-07-09 11:29:40', '2016-07-09 11:29:40'),
('578203e3d774c', 'RQYNaGVBVjnzTpse8yg0MfvHjt0ER4nGaK9qZ1LH', '5781d36b4c056', '400.00', NULL, 1, '2016-07-11 08:14:27', '2016-07-10 08:14:27', '2016-07-10 08:14:27'),
('578239a1a9b58', '3PpxbljISXtHiJj69tNItuOFGvr2uZQSXarNxkvw', '5757ddc6b27e3', '499.00', NULL, 1, '2016-07-11 12:03:45', '2016-07-10 12:03:45', '2016-07-10 12:03:45'),
('5785de3539ece', 'N0nxoocVIKGqWBG5khmiJKLtSeZwcnSgDMIUKnHA', '5757cad8b256c', '3400.00', NULL, 1, '2016-07-14 06:22:45', '2016-07-13 06:22:45', '2016-07-13 06:22:45'),
('57a151b3c91d2', 'xqpHBhr6GZRTRZQFt9So4p8h5DSewvX7xHtK9w33', '57a027783b4e9', '1200.00', NULL, 1, '2016-08-04 02:06:43', '2016-08-03 02:06:43', '2016-08-03 02:06:43'),
('57a1529b833a3', 'DoMb4gjkG7x6nUouymZah6sqEKxyyeLLlRuaCg2U', '5757cf0b5171f', '2500.00', NULL, 1, '2016-08-04 02:10:35', '2016-08-03 02:10:35', '2016-08-03 02:10:35'),
('57a198ede8860', 'MQ7Amj2vqaSgiH84RgvdhLh6V0Pir4CTZyuJqEXh', '57a1843b5f321', '125.00', NULL, 1, '2016-08-04 07:10:37', '2016-08-03 07:10:37', '2016-08-03 07:10:37'),
('57a2a22588937', 'NikkMAOiTd61oqKanm1rzZlbei9LUxAFUZRC9XHF', '5757e08c5348a', '195.00', NULL, 1, '2016-08-05 02:02:13', '2016-08-04 02:02:13', '2016-08-04 02:02:13'),
('57a3fadfa5782', 'qkBWJtv0AFc8LAniu66nCTVsnsewtWpHn4RIGPvY', '57a17e4515317', '1000.00', NULL, 1, '2016-08-06 02:33:03', '2016-08-05 02:33:03', '2016-08-05 02:33:03'),
('57aaeb47ad26a', 'k36OMxajGwF4NXwDjTtZLmjNQv0oRTHarfh0nrOn', '577e4f3d12ccd', '300.00', '20.00', 1, '2016-08-11 08:52:23', '2016-08-10 08:52:23', '2016-08-10 08:52:23'),
('57b15450159c5', 'KWExovQn6DWST94QenLlQQRaV018qwXbJG6V1Mnk', '5758d77249ebb', '499.00', '20.00', 1, '2016-08-16 05:34:08', '2016-08-15 05:34:08', '2016-08-15 05:34:08'),
('57bc0e2d09c7c', 'OrhRUrDaDnzJUWRDDf7el6PTkAtiJNOFjdFCinjx', '5758e2ac027d7', '98.00', '20.00', 1, '2016-08-24 08:49:49', '2016-08-23 08:49:49', '2016-08-23 08:49:49'),
('57bc0e766dd8f', 'LBTTc8cyxANXCMIMQ6hk6qjdj06VFl0591drwsIb', '5783769466e85', '375.00', '20.00', 1, '2016-08-24 08:51:02', '2016-08-23 08:51:02', '2016-08-23 08:51:02'),
('57bc474d45a1a', '5j5xc9sZXgeVpnYmYvLO72JtRcdz37QBjHP2QjMS', '578364d0455cc', '599.00', '20.00', 1, '2016-08-24 12:53:33', '2016-08-23 12:53:33', '2016-08-23 12:53:33'),
('57bd4ca298bfc', 'fCPEORn617W9axhpWJXk6E19dm5ROmn1WFdcHyGY', '5757e3b04e109', '2700.00', '20.00', 1, '2016-08-25 07:28:34', '2016-08-24 07:28:34', '2016-08-24 07:28:34'),
('57bd82b534e1f', 'AXXLbO9B51sw8o1o4SZdy8whbOoDkEjS8NbiDWWO', '5783769466e85', '375.00', '20.00', 1, '2016-08-25 11:19:17', '2016-08-24 11:19:17', '2016-08-24 11:19:17'),
('57ca2c9da9238', 'IKuoZXjwTn3ZtuXC1s1bLIs4hXMzfJDw0QFi7NDu', '5781d36b4c056', '400.00', '20.00', 1, '2016-09-04 01:51:25', '2016-09-03 01:51:25', '2016-09-03 01:51:25'),
('57cfbb857a264', 'Bv3IIOTsy1JL1LqB6lcavH6jPitqqneaPwxcnhOy', '5781ce616681d', '380.00', '20.00', 1, '2016-09-08 07:02:29', '2016-09-07 07:02:29', '2016-09-07 07:02:29'),
('57cfed9d78f0e', 'CIYVwrkkjmLtSdfhLBuZQ4co1hTdLz6HHkYPTQtY', '5757bda523572', '7.00', '20.00', 1, '2016-09-08 10:36:13', '2016-09-07 10:36:13', '2016-09-07 10:36:13'),
('57d8f194e3a87', 'DhLY2pQB192RRFzIylrWPlO4GY2Wd9oxyp5xV8qp', '5758dc480d86d', '2600.00', '20.00', 1, '2016-09-15 06:43:32', '2016-09-14 06:43:32', '2016-09-14 06:43:32'),
('57dbe32ebd0b4', 'rE2xPX0t8KZ4YAj7nFoCAAwzjlHBPkOJ7uquwQJv', '57c5711ca4891', '599.00', '35.00', 1, '2016-09-17 12:18:54', '2016-09-16 12:18:54', '2016-09-16 12:18:54'),
('57dcd184135fe', 'EixeNSeHKp92OQOr9RibYnY4PNc5YTUoJUt0SBLs', '57c5711ca4891', '599.00', '35.00', 1, '2016-09-18 05:15:48', '2016-09-17 05:15:48', '2016-09-17 05:15:48'),
('57e92e9d47db6', 'ihvmz1qcAbwcJZp1asAB8cUdT2Htf48A7XLVjcKe', '5781ce616681d', '380.00', '20.00', 1, '2016-09-27 14:20:13', '2016-09-26 14:20:13', '2016-09-26 14:20:13'),
('57ed09aa74468', 'JK8W39LjFtzkI3rCg6sCiFczPxQPXHrrodMwmgY7', '57c5711ca4891', '599.00', '35.00', 1, '2016-09-30 12:31:38', '2016-09-29 12:31:38', '2016-09-29 12:31:38'),
('57f8e776c7541', 'xRMd2bDI0d9ct4se62BJr01Eglymn4B1tCfCQspC', '5781f56070258', '1450.00', '20.00', 1, '2016-10-09 12:32:54', '2016-10-08 12:32:54', '2016-10-08 12:32:54'),
('58035e34633a0', 'UlYoRCcXeIbSQJyemtbtfIUq9X0GCjbzXdotmpXb', '5758dc525fc7d', '2800.00', '20.00', 1, '2016-10-17 11:02:12', '2016-10-16 11:02:12', '2016-10-16 11:02:12'),
('5804ae51afda5', 'NX8a3PHdltPFbnyOpVTaRrZoXRY18vq7mkAXoMxw', '5783769466e85', '375.00', '20.00', 1, '2016-10-18 10:56:17', '2016-10-17 10:56:17', '2016-10-17 10:56:17'),
('5804bbf64a392', 'XiMYi40t7xXIlZ164aCmGi58MRfR4xMlmllif9HP', '5757e3b04e109', '2700.00', '20.00', 1, '2016-10-18 11:54:30', '2016-10-17 11:54:30', '2016-10-17 11:54:30'),
('580db26c5ee75', 'LKojr8Q1gLtZXbfJ7v4E9yXnDAw8d75g00jeth6L', '57c4ff9c4955f', '0.20', '1.00', 1, '2016-10-25 07:04:12', '2016-10-24 07:04:12', '2016-10-24 07:04:12'),
('580ecd8ea6c77', '7dmZArSbqLJlYg7CTsl8nYIrHwcrFh4JBYSVmzIe', '57c4ff9c4955f', '0.20', '1.00', 1, '2016-10-26 03:12:14', '2016-10-25 03:12:14', '2016-10-25 03:12:14'),
('580ef6a9c7254', 'J2mW6ZiFYcNkD06igGfWDJySrJJienfUwXKEdNhf', '5757ff95c2c54', '149.99', '20.00', 1, '2016-10-26 06:07:37', '2016-10-25 06:07:37', '2016-10-25 06:07:37'),
('5811aa402c2a4', 'gNgcGLMKHhRpPj1gRgktvZtfD2c1t4CQqy6IxMxQ', '5783769466e85', '375.00', '20.00', 1, '2016-10-28 07:18:24', '2016-10-27 07:18:24', '2016-10-27 07:18:24'),
('5811c2cad8a74', 'wTag1Ex3wZsAlK7QNk09zrMa5FYy64H0G9tlbZHY', '5783769466e85', '375.00', '20.00', 1, '2016-10-28 09:03:06', '2016-10-27 09:03:06', '2016-10-27 09:03:06'),
('581987f4b70a0', '2xL4D7BIjI14OS5ZTwTZHjA2BDSszw3YvKuZdhnc', '5783769466e85', '375.00', '20.00', 1, '2016-11-03 06:30:12', '2016-11-02 06:30:12', '2016-11-02 06:30:12'),
('58199d54da6f2', 'OpaBGjfGhnlJQW6FNsaSvTCuzmlhVeqgcj3HEtVt', '578364d0455cc', '599.00', '20.00', 1, '2016-11-03 08:01:24', '2016-11-02 08:01:24', '2016-11-02 08:01:24'),
('581ab0862b4db', 'GEgQHLQ3batayHzEVID3elAUbmMBTQWk5DSu3SuF', '5783769466e85', '375.00', '20.00', 1, '2016-11-04 03:35:34', '2016-11-03 03:35:34', '2016-11-03 03:35:34'),
('581ab661ac906', 'Gwo4lw1RgE6kRK4NSK1LWQD4ddWnlax2iwUpJ598', '57c4ff9c4955f', '0.20', '1.00', 1, '2016-11-04 04:00:33', '2016-11-03 04:00:33', '2016-11-03 04:00:33'),
('5856500e48f74', 'dhtAavp0LwbV6OwaECYpx1nL83E9YLPANCmFwimV', '5783769466e85', '375.00', '20.00', 1, '2016-12-19 08:59:58', '2016-12-18 08:59:58', '2016-12-18 08:59:58'),
('588ecc0367427', 'MVWEB2KWtpgdmVlPKoiFGJIHDR6QtdE6PzS9BLuj', '5781d1b5ba6c9', '125.00', '20.00', 1, '2017-01-31 05:15:47', '2017-01-30 05:15:47', '2017-01-30 05:15:47'),
('59152a2446c41', 'Jywu5GM7x3ZhStGJjhVYiR6OHsKuSRsdg4x9r0Yi', '5758ce269848a', '225.00', '20.00', 1, '2017-05-13 03:21:08', '2017-05-12 03:21:08', '2017-05-12 03:21:08'),
('59818f94f30bb', 'R7rFPsjhlrAI5S0ETWUcUs1LDSwbuLZ2kMluGiIO', '5757df1ea5ddd', '350.00', '20.00', 1, '2017-08-03 08:38:44', '2017-08-02 08:38:44', '2017-08-02 08:38:44'),
('5a0c2fac91916', 'j5aDovJ5hyFo2zrYz0hlgZof0TZpzYlu9G8osmG0', '5987cb6d11f6f', '379.00', '0.00', 1, '2017-11-16 12:14:36', '2017-11-15 12:14:36', '2017-11-15 12:14:36'),
('5a698b64bb3da', 'TD8nONfnKZTxwW3cZeeQam18Yzb8cG6iVQMXkSMY', '5a6943237bbac', '343.00', '23.00', 1, '2018-01-26 07:46:44', '2018-01-24 23:46:44', '2018-01-24 23:46:44'),
('5a698bd60fd01', '5Wd2o3wcTpWMU9Z4SsnyihiFs605CFxwJlvviXcZ', '5781d1b5ba6c9', '125.00', '20.00', 1, '2018-01-26 07:48:38', '2018-01-24 23:48:38', '2018-01-24 23:48:38');

-- --------------------------------------------------------

--
-- Table structure for table `gp_categories`
--

CREATE TABLE `gp_categories` (
  `category_id` varchar(13) NOT NULL,
  `name` varchar(255) NOT NULL,
  `menu` varchar(50) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `order` tinyint(3) NOT NULL,
  `parent` varchar(13) DEFAULT '-',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gp_categories`
--

INSERT INTO `gp_categories` (`category_id`, `name`, `menu`, `slug`, `description`, `order`, `parent`, `deleted_at`, `created_at`, `updated_at`) VALUES
('56e626f24c620', 'House', 'House', 'house', 'House', 1, '-', NULL, '2016-03-14 02:50:26', '2018-01-25 03:55:47'),
('56e626f24cdd8', 'Apartment', 'Apartment', 'apartment', 'Apartment', 3, '-', NULL, '2016-03-14 02:50:26', '2018-01-25 03:55:55'),
('56e626f24cde4', 'Townhouse', 'Townhouse', 'townhouse', 'Townhouse', 2, '-', NULL, '2016-03-14 02:50:26', '2018-01-25 05:17:50'),
('56e626f24cdef', 'Villa', 'Villa', 'villa', 'Villa', 4, '-', NULL, '2016-03-14 02:50:26', '2018-01-25 03:56:00'),
('56e626f24cdf7', 'Other', 'Other', 'other', 'Other', 5, '-', NULL, '2016-03-14 02:50:26', '2018-01-25 03:56:04'),
('56e626f24cdff', 'Offices', 'Offices', 'offices', 'Offices', 0, '-', NULL, '2016-03-14 02:50:26', '2016-04-22 14:08:52');

-- --------------------------------------------------------

--
-- Table structure for table `gp_failed_jobs`
--

CREATE TABLE `gp_failed_jobs` (
  `id` int(10) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8_unicode_ci NOT NULL,
  `queue` text COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gp_jobs`
--

CREATE TABLE `gp_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gp_orders`
--

CREATE TABLE `gp_orders` (
  `order_id` varchar(13) NOT NULL,
  `customer_id` varchar(13) NOT NULL,
  `invoice_id` varchar(30) DEFAULT NULL,
  `total` decimal(9,2) NOT NULL,
  `status` varchar(13) DEFAULT '573e5b0202eb9',
  `date_cancelled` timestamp NULL DEFAULT NULL,
  `date_completed` timestamp NULL DEFAULT NULL,
  `date_refunded` timestamp NULL DEFAULT NULL,
  `paypal_correlation_id` varchar(100) DEFAULT NULL,
  `paypal_pay_key` varchar(100) DEFAULT NULL,
  `paypal_payer_email` varchar(255) DEFAULT NULL,
  `paypal_payer_id` varchar(13) DEFAULT NULL,
  `tracking_code` varchar(100) DEFAULT NULL,
  `token` varchar(60) DEFAULT NULL,
  `notes` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gp_orders`
--

INSERT INTO `gp_orders` (`order_id`, `customer_id`, `invoice_id`, `total`, `status`, `date_cancelled`, `date_completed`, `date_refunded`, `paypal_correlation_id`, `paypal_pay_key`, `paypal_payer_email`, `paypal_payer_id`, `tracking_code`, `token`, `notes`, `created_at`, `updated_at`) VALUES
('5757c07e711bb', '56fc9e052d05f', 'oDZvXPRmAngBOcFYW1Lht7SjaKf5I6', '4.00', '573e5b0202eb9', NULL, NULL, NULL, 'a314b2e71fd52', 'AP-32B05574F95669255', NULL, NULL, NULL, NULL, NULL, '2016-06-08 06:51:42', '2016-06-08 06:51:44'),
('5757c0cb398e9', '56fc9e052d05f', 'gpKTr9IdnCVlHYc8ifoqQuJkX5L1RW', '269.00', '573e5b0202eb9', NULL, NULL, NULL, '0a4052290d466', 'AP-5HE19977RK871122H', NULL, NULL, NULL, NULL, NULL, '2016-06-08 06:52:59', '2016-06-08 06:53:00'),
('5757dd330534e', '5757cc0ae2bf2', '14aEkSQNtu3jgnHYAIemrzhTvPbXCJ', '360.00', '573e5b0202eb9', NULL, NULL, NULL, '03d340c926965', 'AP-8M971524H4880474V', NULL, NULL, NULL, NULL, NULL, '2016-06-08 08:54:11', '2016-06-08 08:54:13'),
('5757ddac9a914', '5757cc0ae2bf2', '9dVWUEMB4jOoHRtXZlPvJS0zerykpu', '895.00', '573e5b0202eb9', NULL, NULL, NULL, 'ca88065b04a11', 'AP-47F16013TV778725U', NULL, NULL, NULL, NULL, NULL, '2016-06-08 08:56:12', '2016-06-08 08:56:15'),
('5757e21e03152', '56fc9e052d05f', '63FuweITMiPH8rLXgRdQbNcqZpSfaD', '169.00', '573e5b0202eb9', NULL, NULL, NULL, '07b36dcf19d91', 'AP-0TP597616R432332N', NULL, NULL, NULL, NULL, NULL, '2016-06-08 09:15:10', '2016-06-08 09:15:12'),
('5758eac1951c1', '56fc9e052d05f', 'ZwExWlX6FT2JcsdnAhevygj95bIVQa', '99.99', '573e5b0202eeb', '2016-06-09 05:41:04', NULL, NULL, '94034c9ba4043', 'AP-1XE09348DJ165733A', NULL, NULL, NULL, '', NULL, '2016-06-09 04:04:17', '2016-06-09 05:41:04'),
('5758eae9ec739', '5757cc0ae2bf2', 'MKwzmGXiS1kPEedfvOJ67cgRCq8trD', '99.99', '573e5b0202eef', '2016-10-26 07:08:19', NULL, NULL, '94a199bb92c1d', 'AP-2HE43821RN840335A', NULL, NULL, NULL, '', NULL, '2016-06-09 04:04:57', '2016-10-26 07:08:19'),
('575900e1201d7', '5757cc0ae2bf2', 'ovjRImeH9rJxP4hLlzT70X1ZUnq5Y2', '215.00', 'failed', '2016-06-09 05:56:41', NULL, NULL, '0a802c6b45369', 'AP-6HS477596H644512B', NULL, NULL, NULL, '', NULL, '2016-06-09 05:38:41', '2016-06-09 05:56:41'),
('5759020a2829e', '56fc9e052d05f', 'xp46AiGq83H5sCOQ0hkN9lTDouXt2a', '99.99', 'failed', '2016-06-09 05:44:36', NULL, NULL, '11cae416fd281', 'AP-5GU741105U819541M', NULL, NULL, NULL, '', NULL, '2016-06-09 05:43:38', '2016-06-09 05:44:36'),
('575902ed10ea9', '56fc9e052d05f', 'Dep2tfkjwLy49KGl5VnI7bc38YEguS', '99.99', '573e5b0202eef', NULL, '2016-06-09 05:49:03', NULL, 'ea4747acc4448', 'AP-4WE71148KY885982M', 'leo_1338405575_per@sushidigital.com.au', 'UPGFSHS653GEG', NULL, '', NULL, '2016-06-09 05:47:25', '2016-06-09 05:49:03'),
('57590aa2c0f19', '5757cc0ae2bf2', 'BjozQAmWbKtk57xciFUnH2uvfR9sad', '215.00', '573e5b0202eef', NULL, '2016-06-09 06:21:28', NULL, '93d9e29be48cc', 'AP-0F337930L4235815K', 'leo_1338405575_per@sushidigital.com.au', 'UPGFSHS653GEG', NULL, '', NULL, '2016-06-09 06:20:18', '2016-06-09 06:21:28'),
('57590b0c997bd', '5757cc0ae2bf2', 'DvcYW85LuhB3P7F2skgT9OmANltrGn', '599.00', '573e5b0202eef', NULL, '2016-06-09 06:22:40', NULL, 'ac63148ec35f0', 'AP-29R32172XE644431U', 'leo_1338405575_per@sushidigital.com.au', 'UPGFSHS653GEG', NULL, '', NULL, '2016-06-09 06:22:04', '2016-06-09 06:22:40'),
('57590bf9af099', '5757cc0ae2bf2', 'QRE4WKq97GFcwmjiYfZIgOHSDu5o3a', '149.99', '573e5b0202eef', NULL, '2016-06-09 06:27:32', NULL, 'afac22b58962d', 'AP-5BH09068N24236609', 'leo_1338405575_per@sushidigital.com.au', 'UPGFSHS653GEG', NULL, '', NULL, '2016-06-09 06:26:01', '2016-06-09 06:27:32'),
('57590c6fb9ce1', '5757cc0ae2bf2', 'mi2fMlCc0DNJhFLaW1XUBkdTEeGI5Y', '99.99', '573e5b0202eef', NULL, '2016-06-09 06:40:18', NULL, 'd114b77a743ca', 'AP-4FH91407RW4803303', 'leo_1338405575_per@sushidigital.com.au', 'UPGFSHS653GEG', NULL, '', NULL, '2016-06-09 06:27:59', '2016-06-09 06:40:18'),
('57590f5d1a3c1', '5757cc0ae2bf2', 'hgNtwJeDHnPy0TV1saFOZIrUlGMKW8', '25.19', '573e5b0202eeb', '2016-06-09 06:42:07', NULL, NULL, '62fb404eba971', 'AP-6PJ31704MR653231H', NULL, NULL, NULL, '', NULL, '2016-06-09 06:40:29', '2016-06-09 06:42:07'),
('57590fc9008f5', '5757cc0ae2bf2', 'Mc0rVlGpXwHf6EySzkhB5FOQaDZ9vY', '99.99', '573e5b0202eef', NULL, '2016-06-09 06:43:57', NULL, '9b073ce29ce36', 'AP-22J82877MA209780J', 'leo_1338405575_per@sushidigital.com.au', 'UPGFSHS653GEG', NULL, '', NULL, '2016-06-09 06:42:17', '2016-06-09 06:43:57'),
('575a81c405bb5', '5757ce7294790', 'InzM247qcg3BFwpsLZSleoEWPXyRH1', '25.19', '573e5b0202eef', NULL, '2016-06-10 09:02:09', NULL, 'a491820fe58a3', 'AP-1A651096HR398464P', 'leo_1338405575_per@sushidigital.com.au', 'UPGFSHS653GEG', NULL, '', NULL, '2016-06-10 09:00:52', '2016-06-10 09:02:09'),
('575e45e41d72f', '5757bc733bfff', 'C9xKEmihcVMdfzRZBePs7YolOHp2Fr', '895.00', '573e5b0202ef3', '2016-06-13 05:34:58', NULL, NULL, '550ecb1943afc', 'AP-4RW77151AP1506349', NULL, NULL, NULL, '', NULL, '2016-06-13 05:34:28', '2016-06-13 05:34:58'),
('575e53181ac10', '56fc9e052d05f', 'pbCVhta1yq89M2KXwIN34WjJ7SgmrY', '75.00', '573e5b0202ef3', '2016-06-13 06:31:43', NULL, NULL, 'eb76ddcb1935a', 'AP-9KF9244330528614U', NULL, NULL, NULL, '', NULL, '2016-06-13 06:30:48', '2016-06-13 06:31:43'),
('575e5ebe35e58', '56fc9e052d05f', '6VFqy91Xof3CUJMLBDGAYtxbaZi8T7', '99.99', '573e5b0202ef3', '2016-06-13 07:20:46', NULL, NULL, 'b520d756716a8', 'AP-21J51306H1302803V', NULL, NULL, NULL, '', NULL, '2016-06-13 07:20:30', '2016-06-13 07:20:46'),
('575e5ed09d93d', '5757cc0ae2bf2', 'REZ9u0T7VqLl2My5wN8AXOsnYrjdba', '149.99', '573e5b0202ef3', '2016-06-13 07:22:03', NULL, NULL, '7f739b4d75c86', 'AP-52C31372122111431', NULL, NULL, NULL, '', NULL, '2016-06-13 07:20:48', '2016-06-13 07:22:03'),
('575f8222a9e41', '5757cc0ae2bf2', 'DyChXOdmRBu3bxIoHaQFnqTKVcfZUp', '215.00', '573e5b0202ef3', '2016-06-14 04:11:43', NULL, NULL, 'd1864c9be4286', 'AP-1P369188UD937491W', NULL, NULL, NULL, '', NULL, '2016-06-14 04:03:46', '2016-06-14 04:11:43'),
('575f82b6738f9', '5757cc0ae2bf2', 'DZ5wkuLhKgWelEVnIxspN13zBf8Yr2', '75.00', '573e5b0202ef3', '2016-06-14 04:08:54', NULL, NULL, 'dfe2905ac0b32', 'AP-5EY58033E35500506', NULL, NULL, NULL, '', NULL, '2016-06-14 04:06:14', '2016-06-14 04:08:54'),
('576104df0fb7f', '5757cc0ae2bf2', '13v4lymjEh7ixBZzfctCSdDeuJ9aqV', '149.99', '573e5b0202ef3', '2016-06-15 07:34:15', NULL, NULL, 'ca56db9092604', 'AP-8K522741S5872071F', NULL, NULL, NULL, '', NULL, '2016-06-15 07:33:51', '2016-06-15 07:34:15'),
('576112d43761e', '5757cc0ae2bf2', 'vh0TOjdHFipyJZU2QcL9qVDM45noIK', '75.00', '573e5b0202eef', NULL, '2016-06-15 08:34:04', NULL, '51c3756437fbf', 'AP-1D215199CY665553A', 'leo_1338405575_per@sushidigital.com.au', 'UPGFSHS653GEG', NULL, '', NULL, '2016-06-15 08:33:24', '2016-06-15 08:34:04'),
('57611319513af', '5757cc0ae2bf2', 'NFQHV82qCf0BaJ4A6ejMWG9rg1TmOK', '849.99', '573e5b0202eef', NULL, '2016-06-15 08:35:10', NULL, '6d9821b118ce6', 'AP-0NT72176UP8866949', 'leo_1338405575_per@sushidigital.com.au', 'UPGFSHS653GEG', NULL, '', NULL, '2016-06-15 08:34:33', '2016-06-15 08:35:10'),
('57612120415c1', '5757cfdb6215a', '5nFqKRd3B7tvL6Mj9Q0cfmC1Ak2OGT', '25.19', '573e5b0202ee6', NULL, NULL, NULL, '1ef1ed6cb01b0', 'AP-6TE37401DR7107359', NULL, NULL, NULL, 'f7eiLv3j5qQg82JEHkxTXwuS4dRGVz0ZBp6DFm9lhcnOoCsUYNbWIPtrAy1K', NULL, '2016-06-15 09:34:24', '2016-06-15 09:34:26'),
('57612237752d3', '5757ccf71cdd1', 'S7JdBt16L3mz5ZokgAIUv8pq4cKXHQ', '1599.99', '573e5b0202ee6', NULL, NULL, NULL, '18480e47cc03a', 'AP-8UK41820LX264800X', NULL, NULL, NULL, 'LcaTOR5eli4Em3k0KCAHBubGI9roU1QnXtw7DSjZJy8F6Mps2YVWdghvxzqf', NULL, '2016-06-15 09:39:03', '2016-06-15 09:39:05'),
('576122ed72041', '5757cc0ae2bf2', 'uhoB2zxea6qSWUtVZAKmpOMRrvIsTN', '310.00', '573e5b0202ee6', NULL, NULL, NULL, '7472d1dc21829', 'AP-0M417608SF865133U', NULL, NULL, NULL, '3RFZmhS0KJTGArBnv4PYxtVHku7WlNoCg2q9iIb1payjOMLcEfe58sQXw6dD', NULL, '2016-06-15 09:42:05', '2016-06-15 09:42:07'),
('576212a679427', '5757cc0ae2bf2', 'PoQkUdN0i1bBLnqSFOMRIGZ5yHr6CX', '1164.60', '573e5b0202eef', NULL, '2016-06-16 02:46:10', NULL, '3c491d2eec02c', 'AP-1161647343745212G', 'leo_1338405575_per@sushidigital.com.au', 'UPGFSHS653GEG', NULL, '', NULL, '2016-06-16 02:44:54', '2016-06-16 02:46:10'),
('576213bf02619', '5757cc0ae2bf2', 'bgtG6ZsDPapB52UdYlu7NeTIXrEwLj', '9000.00', '573e5b0202eef', NULL, '2016-06-16 02:50:41', NULL, '84194c3ca0201', 'AP-90L40184N95651340', 'leo_1338405575_per@sushidigital.com.au', 'UPGFSHS653GEG', NULL, '', NULL, '2016-06-16 02:49:35', '2016-06-16 02:50:41'),
('576217b5522c4', '5757cc0ae2bf2', 'aKyUvArLIO2n01SNCbq5wYh7WTotXl', '99.99', '573e5b0202eef', NULL, '2016-06-16 03:07:52', NULL, '39cfd717a9c40', 'AP-1V510641KC753171E', 'leo_1338405575_per@sushidigital.com.au', 'UPGFSHS653GEG', NULL, '', NULL, '2016-06-16 03:06:29', '2016-06-16 03:07:52'),
('576218bec956d', '5757bb6decbb1', '3M7UnlNVAvPDcjohuk8YG61zLKWq9g', '25.19', '573e5b0202eef', NULL, '2016-06-16 03:12:04', NULL, '26f0fd066b10a', 'AP-5GY004454K077173B', 'leo_1338405575_per@sushidigital.com.au', 'UPGFSHS653GEG', NULL, '', NULL, '2016-06-16 03:10:54', '2016-06-16 03:12:04'),
('5762199058886', '5757bbce106d2', '6XO3CJmuYRBnt7SH2TsoexANzIFvjd', '2850.00', '573e5b0202eef', NULL, '2016-06-16 03:15:24', NULL, 'aea016113e70c', 'AP-0MM6361069888004G', 'leo_1338405575_per@sushidigital.com.au', 'UPGFSHS653GEG', NULL, '', NULL, '2016-06-16 03:14:24', '2016-06-16 03:15:24'),
('57621d30c573a', '5757bb6decbb1', '8i7oqh52zQcOJT4xIWFrZBbC3KH1am', '499.00', '573e5b0202eef', NULL, '2016-06-16 03:30:50', NULL, '835a1e435dae4', 'AP-3DD8173106200102R', 'leo_1338405575_per@sushidigital.com.au', 'UPGFSHS653GEG', NULL, '', NULL, '2016-06-16 03:29:52', '2016-06-16 03:30:50'),
('5762460d464ed', '5757cc0ae2bf2', 'r6m7hJHLCloXyb32psOg9EcQt0VfxF', '310.00', '573e5b0202eef', NULL, '2016-06-16 06:24:55', NULL, '59a9aa2593f76', 'AP-4U522522366369139', 'leo_1338405575_per@sushidigital.com.au', 'UPGFSHS653GEG', NULL, '', NULL, '2016-06-16 06:24:13', '2016-06-16 06:24:55'),
('576257136aafb', '5757cc0ae2bf2', 'bSlC2tmwni3NHx8ZTVaoB5LKI1dP9r', '56.00', '573e5b0202ef3', '2016-06-16 07:37:28', NULL, NULL, '53e48fd1cdc33', 'AP-32303543DA047182E', NULL, NULL, NULL, '', NULL, '2016-06-16 07:36:51', '2016-06-16 07:37:28'),
('5762818c9559c', '5757cc0ae2bf2', '27ZYd4FhRt8fOy9WeQSlzpHvTmqgGX', '895.00', '573e5b0202ef3', '2016-06-16 10:38:27', NULL, NULL, 'b888e9c73d7d8', 'AP-4BP85143NT6550421', NULL, NULL, NULL, '', NULL, '2016-06-16 10:38:04', '2016-06-16 10:38:27'),
('576358b062282', '5757cc0ae2bf2', 'ZiHpTAEg36NXP9o2BwxkD8R5SU0taK', '2270.00', '573e5b0202ef3', '2016-06-17 01:56:41', NULL, NULL, '5f8445abce51c', 'AP-26R21166B8574132N', NULL, NULL, NULL, '', NULL, '2016-06-17 01:56:00', '2016-06-17 01:56:41'),
('576358fa98ad9', '5757cc0ae2bf2', 'C2JzE8jKUqRi0vmeyugtb4swLBrAOD', '2700.00', '573e5b0202eef', NULL, '2016-06-17 01:58:36', NULL, 'bf3fca16d051a', 'AP-11F337898W766232U', 'leo_1338405575_per@sushidigital.com.au', 'UPGFSHS653GEG', NULL, '', NULL, '2016-06-17 01:57:14', '2016-06-17 01:58:36'),
('5764bf87e2470', '56fc9e052d05f', 'Nwm6pzfYPshAiBHI4xaJLbvU7E1eSd', '215.00', '573e5b0202ee6', NULL, NULL, NULL, '880fc1b490db3', 'AP-8EK48115D7253360R', NULL, NULL, NULL, '3ZFyB4DraPbY1UV9zGtik8LnfmsOwclCvodx6gJSTuqeQ0Kp7EhXHRWNMIj2', NULL, '2016-06-18 03:27:03', '2016-06-18 03:27:06'),
('577e5011a04ff', '577e48310c58d', 'h54fx3XTUmR8GstpbIJzvua9M2rn6d', '549.00', '573e5b0202ef3', '2016-07-07 12:51:19', NULL, NULL, '2c1b194b82a3f', 'AP-9GU94926LM219410B', NULL, NULL, NULL, '', NULL, '2016-07-07 12:50:25', '2016-07-07 12:51:19'),
('577e532304bf4', '577e48310c58d', 'hJXsPtBfqHAlyxYS76V5DrIz4e2390', '449.00', '573e5b0202ee6', NULL, NULL, NULL, '3395a91343bd9', 'AP-8M766141CB290182H', NULL, NULL, NULL, 'EN7wHfsO5VmWKjxc1k2MCaFXPGSJei3ulrp60nYvBLgoZQUbthRTAydIq9zD', NULL, '2016-07-07 13:03:31', '2016-07-07 13:03:32'),
('57805c8e8d882', '577e48310c58d', 'kxVpsEHGZrf0uXL7lyqYPObvtKmFia', '12300.00', '573e5b0202ef3', '2016-07-09 02:09:04', NULL, NULL, 'c47d21289a714', 'AP-1WL80961U0012614Y', NULL, NULL, NULL, '', NULL, '2016-07-09 02:08:14', '2016-07-09 02:09:04'),
('57877584c752d', '577e48310c58d', 'gsSO96oRt7Qzljqmp5N3T8EvCcLbuV', '2600.00', '573e5b0202ee6', NULL, NULL, NULL, '874186719bec8', 'AP-2X46316013571933N', NULL, NULL, NULL, 'ojDMe0QUmbqd57w9iJLph1BZCuVckXNx2lGtaPSATEg8W6YHzOIfvrRsy4nK', NULL, '2016-07-14 11:20:36', '2016-07-14 11:20:38'),
('57a96f2668d8a', '5757bbce106d2', 'EIUM8SleLoW3KH97Pijd5ar2nhCm0N', '82.50', '573e5b0202ef3', '2016-08-09 05:59:58', NULL, NULL, 'a84e2d6937e72', 'AP-7B256642LG778501G', NULL, NULL, NULL, '', NULL, '2016-08-09 05:50:30', '2016-08-09 05:59:58'),
('57a9728b525ec', '5757bbce106d2', 'rjJU09bMZgIfFwNnH5CTWQ4oSGcDku', '82.50', '573e5b0202eef', NULL, '2016-08-09 06:30:24', NULL, '850442261a96a', 'AP-76852171G89675019', 'richmund-buyer@sushidigital.com.au', 'W4DW3NMBBH2MQ', NULL, '', NULL, '2016-08-09 06:04:59', '2016-08-09 06:30:24'),
('57a97e684ce27', '5757ce2636dfd', 'eElDd8JS6Vq9rOhIRtn5AzbamHwTvL', '1399.99', '573e5b0202eef', NULL, '2016-08-09 07:01:06', NULL, 'ab43d860132aa', 'AP-0KY030441V2970911', 'leo_1338405575_per@sushidigital.com.au', 'UPGFSHS653GEG', NULL, '', NULL, '2016-08-09 06:55:36', '2016-08-09 07:01:06'),
('57b1a5b0a9462', '577e48310c58d', 'iFQTMla4EosD0dCm5ubwcvRVIZOLf6', '109.00', '573e5b0202ee6', NULL, NULL, NULL, '8d7633176d21d', 'AP-50622634L21512013', NULL, NULL, NULL, 'NrIt4cjTwsG6Hdo5k2vDxngJmFEYaLK9QMzCSPyB38WVAO7lXe1Zf0bpqRuU', NULL, '2016-08-15 11:21:20', '2016-08-15 11:21:22'),
('57b1a624b29f6', '5783736ab0d23', '2aV4rjSYK6odcT8GRMpqDO59A7ykIb', '1470.00', '573e5b0202ef3', '2016-08-15 11:23:52', NULL, NULL, '4c0abfc277c43', 'AP-8B3262351X694461D', NULL, NULL, NULL, '', NULL, '2016-08-15 11:23:16', '2016-08-15 11:23:52'),
('57b1a869609a2', '57b1a7776e17a', 'Ykoqy7eutBrOiEhzJQwLmTCIUKPndH', '1470.00', '573e5b0202ee6', NULL, NULL, NULL, 'cec5e07520249', 'AP-85M84612FS214400U', NULL, NULL, NULL, 'Ab2LznMp5eqOd6yfk1NDRtJZCu7IgalKFSv04TYiUmhP8wQXs3VjH9WBGExo', NULL, '2016-08-15 11:32:57', '2016-08-15 11:32:58'),
('57b1a8c0aa3dc', '57b1a7776e17a', 'TvCwxt1jYHMVkqUN9mDdz67WJ2IcS3', '1470.00', '573e5b0202ef3', '2016-08-15 11:35:39', NULL, NULL, '7e8fce1b4dc89', 'AP-0EV62856YF3536421', NULL, NULL, NULL, '', NULL, '2016-08-15 11:34:24', '2016-08-15 11:35:39'),
('57b1ae4aefd61', '57b1aba23598d', 'zNXcOaWgqtAMB0CGhs5LI71fD8HEn9', '789.00', '573e5b0202ef3', '2016-08-15 11:59:42', NULL, NULL, '93e07353ac409', 'AP-26K57707YN2712847', NULL, NULL, NULL, '', NULL, '2016-08-15 11:58:02', '2016-08-15 11:59:42'),
('57b1b062409e2', '57b1aba23598d', 'xq6bg40k2nZWhHvw1RIz3NQK7tcJXa', '789.00', '573e5b0202ee6', NULL, NULL, NULL, 'b7a3652ee29', 'AP-24C519877A834051D', NULL, NULL, NULL, 'RyAbciF7VCpzXglKJxHLGIsWqumN16oYd8TefDMtUPw0kQrB934Ejah2S5nO', NULL, '2016-08-15 12:06:58', '2016-08-15 12:06:59'),
('57b1b0979ec3d', '57b1aba23598d', 'mlMYTSGDkNhp2qjibatoCUKP3u89wX', '789.00', '573e5b0202ef3', '2016-08-15 12:08:17', NULL, NULL, 'a88e113d5bb77', 'AP-69W821333U0101435', NULL, NULL, NULL, '', NULL, '2016-08-15 12:07:51', '2016-08-15 12:08:17'),
('57b28f035858f', '57835d4dbd83f', 'jJ5yYxnzwgcte06kZ7RhrCSQGVBsN8', '789.00', '573e5b0202eef', NULL, '2016-08-16 03:58:10', NULL, '9f380db11f4d7', 'AP-2C851070K37919505', 'leo_1338405575_per@sushidigital.com.au', 'UPGFSHS653GEG', NULL, '', NULL, '2016-08-16 03:56:51', '2016-08-16 03:58:10'),
('57b5922fab30f', '57b1a7776e17a', 'EDQsW6FPmehty8GXogfdTKVZuzbHJ4', '869.99', '573e5b0202eef', NULL, '2016-08-18 10:48:40', NULL, '39ffeb7a680f5', 'AP-68Y99746TK233815B', 'richmund-buyer@sushidigital.com.au', 'W4DW3NMBBH2MQ', NULL, '', NULL, '2016-08-18 10:47:11', '2016-08-18 10:48:40'),
('57b826f1052a8', '57b1a7776e17a', 'Vbw8sDNmfRhx3c4kT6gEl9PHarZKBA', '520.00', '573e5b0202ef3', '2016-08-20 09:52:26', NULL, NULL, 'e0947c0ac2801', 'AP-6LT47287J2031664K', NULL, NULL, NULL, '', NULL, '2016-08-20 09:46:25', '2016-08-20 09:52:26'),
('57b8288c0727f', '57b1a7776e17a', 'NGwEoA2WfhkRy91rKLglJQdiTuC6Sv', '520.00', '573e5b0202eef', NULL, '2016-08-20 09:55:14', NULL, 'b2a106ecbc863', 'AP-9S943180JL2877803', 'richmund-buyer@sushidigital.com.au', 'W4DW3NMBBH2MQ', NULL, '', NULL, '2016-08-20 09:53:16', '2016-08-20 09:55:14'),
('57bc2811c7aea', '57bc138572cf7', '9n6o72KTWVGvyHhazCEOsYkXFx1g5q', '1419.99', '573e5b0202eef', NULL, '2016-08-23 10:42:28', NULL, '87d39cf88298', 'AP-1XF12030SL101015T', 'leo_1338405575_per@sushidigital.com.au', 'UPGFSHS653GEG', NULL, '', NULL, '2016-08-23 10:40:17', '2016-08-23 10:42:28'),
('57c4272c28507', '5783736ab0d23', 'vnMFZiyq3VT2sfcxOmr0GE68el41RI', '78.24', '573e5b0202eef', NULL, '2016-08-29 12:15:44', NULL, 'd820e9bee8467', 'AP-3G005571JK144491X', 'richmund-buyer@sushidigital.com.au', 'W4DW3NMBBH2MQ', NULL, '', NULL, '2016-08-29 12:14:36', '2016-08-29 12:15:44'),
('57c5714d73ab9', '5783736ab0d23', 'wuDnsrGZ3FlQOWdfEmIBK6gajSXqL5', '118.00', '573e5b0202eef', NULL, '2016-08-30 11:44:10', NULL, '197087203bc4f', 'AP-0RS46457AB213754P', 'richmund-buyer@sushidigital.com.au', 'W4DW3NMBBH2MQ', NULL, '', NULL, '2016-08-30 11:43:09', '2016-08-30 11:44:10'),
('57c7d25df206f', '578d9ba2ea31b', '7OklwIxn2sfMeHc5J4UFaP9ZKuVADE', '1.20', '573e5b0202eef', NULL, '2016-09-01 07:02:36', NULL, '80a3c0df92e29', 'AP-7UL613937F529854B', 'leo_1338405575_per@sushidigital.com.au', 'UPGFSHS653GEG', NULL, '', NULL, '2016-09-01 07:01:49', '2016-09-01 07:02:36'),
('57c973b07113c', '577e48310c58d', 'Gy0KZeOAs2UH43SRnrBQh8g6WwcPtN', '1515.00', '573e5b0202eef', NULL, '2016-09-02 12:46:49', NULL, '33c855b738d69', 'AP-7C347445YS4498827', 'richmund-buyer@sushidigital.com.au', 'W4DW3NMBBH2MQ', NULL, '', NULL, '2016-09-02 12:42:24', '2016-09-02 12:46:49'),
('57ce63e7118f2', '57c7f3dff0df7', 'SVUQact0bJLdnG2hwZ75lDP39BvWmp', '1.20', '573e5b0202eef', NULL, '2016-09-06 06:37:29', NULL, 'c76b1c1ed51c8', 'AP-9WR048445T4196442', 'leo_1338405575_per@sushidigital.com.au', 'UPGFSHS653GEG', NULL, '', NULL, '2016-09-06 06:36:23', '2016-09-06 06:37:29'),
('57ce9807e13f3', '57c7f3dff0df7', 'ZtuB7AhejRb29OQKf4Fz0vIyMUJGrN', '1.20', '573e5b0202ee6', NULL, NULL, NULL, 'd815f136a3b34', 'AP-9XF812956R905933F', NULL, NULL, NULL, 'os4Wf6l5V1hXwpmParyGkjKJQ8BTZEIuH9LDe7zY2C0nRNvSxgUqFitdAOM3', NULL, '2016-09-06 10:18:47', '2016-09-06 10:18:49'),
('57ce9b7a0f65d', '57c7f3dff0df7', '23X6JFb4SjoOMDnwZYWVKeCpBr0syR', '1.20', '573e5b0202ef3', '2016-09-06 10:34:32', NULL, NULL, 'dbf1247cbc83a', 'AP-2MS8476279345601R', NULL, NULL, NULL, '', NULL, '2016-09-06 10:33:30', '2016-09-06 10:34:32'),
('57cea1ed4f9d2', '57c7f3dff0df7', 'Te0SjNs3AXEQMkKRCuvDgBp5xqh86w', '1.20', '573e5b0202ee6', NULL, NULL, NULL, '9f1cf1528a06', 'AP-44A60385C25561622', NULL, NULL, NULL, '2rBgf1FJjSQOm4MtcbTWXvUyeLhNZ3HszqlV6wxaIGAYKkounC98RP0ip5D7', NULL, '2016-09-06 11:01:01', '2016-09-06 11:01:04'),
('57cfc2bd28a06', '5757bbce106d2', 'jUo72z4WyvleER6Fs5rPXhtcIJBDT3', '699.00', '573e5b0202ef3', '2016-09-07 07:44:19', NULL, NULL, '2ba04228e0a8d', 'AP-9SH46529495610449', NULL, NULL, NULL, '', NULL, '2016-09-07 07:33:17', '2016-09-07 07:44:19'),
('57cfc3ae2afff', '57835a6698899', 'cWhym9qDPJ7Btf4TI5ZaibMpSQ8jRF', '1350.00', '573e5b0202eef', NULL, '2016-09-07 07:38:03', NULL, '7d23ed0ad2f42', 'AP-7S4541216Y352345M', 'richmund-buyer@sushidigital.com.au', 'W4DW3NMBBH2MQ', NULL, '', NULL, '2016-09-07 07:37:18', '2016-09-07 07:38:03'),
('57d104fd40555', '57835a6698899', 'CTQxduUNZWE8zRlskb43VMAvPXhiYt', '95.00', '573e5b0202ef3', '2016-09-08 06:34:53', NULL, NULL, '217edc5df0b12', 'AP-3SX86748YH197103W', NULL, NULL, NULL, '', NULL, '2016-09-08 06:28:13', '2016-09-08 06:34:53'),
('57d106c9d9e1d', '57835a6698899', 'GwzxyLn4D1JY9OBm07pgFIjZc8u6SK', '95.00', '573e5b0202eef', NULL, '2016-09-08 06:42:08', NULL, '46436bd9ad1b', 'AP-85B49496L6756791E', 'richmund-buyer@sushidigital.com.au', 'W4DW3NMBBH2MQ', NULL, '', NULL, '2016-09-08 06:35:53', '2016-09-08 06:42:08'),
('57d10daf5537a', '57835a6698899', 'lEwzkOhdQDFYPW8uxNHqemvJCfZIX5', '500.00', '573e5b0202ef3', '2016-09-08 07:14:46', NULL, NULL, '6595751918efe', 'AP-20N94386FL125915R', NULL, NULL, NULL, '', NULL, '2016-09-08 07:05:19', '2016-09-08 07:14:46'),
('57d110098d5b8', '57835a6698899', 'qKJRirDCljHt0BX8LwSYsW5ZVEIhOd', '500.00', '573e5b0202eef', NULL, '2016-09-08 07:16:19', NULL, '7c0531fc50bb7', 'AP-9KW3389880134493J', 'richmund-buyer@sushidigital.com.au', 'W4DW3NMBBH2MQ', NULL, '', NULL, '2016-09-08 07:15:21', '2016-09-08 07:16:19'),
('57d8b52abe41b', '57835a6698899', 'zMIT45DqdkBJ2pbxP0jsGX6AFfEtOc', '800.00', '573e5b0202ee6', NULL, NULL, NULL, '5c1ba7846d4ec', 'AP-8UB08921LU466211J', NULL, NULL, NULL, '874rEqDnIgbSxVcTJkXvsNH3a5Y6PtiM9Cu0eRUGO1Ld2zjwKhflmQyBoFpW', NULL, '2016-09-14 02:25:46', '2016-09-14 02:25:48'),
('57d8b8434ce88', '57835a6698899', 'kSjOEBd1JNzP9gCl0Ga5UKtVyZr84m', '800.00', '573e5b0202ef3', '2016-09-14 02:39:27', NULL, NULL, '31d5c142f17db', 'AP-9GE73412FS922403M', NULL, NULL, NULL, '', NULL, '2016-09-14 02:38:59', '2016-09-14 02:39:27'),
('57d8b8cda615f', '57835a6698899', 'JUTzAZm3Pflb8O7LMdNQeq0a9RwEgi', '800.00', '573e5b0202ee6', NULL, NULL, NULL, '2109cc08630a1', 'AP-249948067L530302X', NULL, NULL, NULL, 'VmrnZiDlzQpvFCxq0tLy12NOgH6Tsboa7dSPfWuEY43G95IRXkKUe8hjBcJM', NULL, '2016-09-14 02:41:17', '2016-09-14 02:41:19'),
('57d8ba02f2f3e', '57835a6698899', 'dFjoMAkV29ZvQcTXWxKwOs1puBq6g0', '800.00', '573e5b0202eef', NULL, '2016-09-14 02:55:03', NULL, 'c2c610bbb1794', 'AP-7NA746839G1303709', 'richmund-buyer@sushidigital.com.au', 'W4DW3NMBBH2MQ', NULL, '', NULL, '2016-09-14 02:46:26', '2016-09-14 02:55:03'),
('57db9d3a93660', '5757ba73cef90', 'WJzfwSOBQan2NPXuymZHTIeYDq90sV', '500.00', '573e5b0202eef', NULL, '2016-09-16 07:21:48', NULL, '5bac536354115', 'AP-1XU90371YS789151H', 'richmund-buyer@sushidigital.com.au', 'W4DW3NMBBH2MQ', NULL, '', NULL, '2016-09-16 07:20:26', '2016-09-16 07:21:48'),
('57dbae48e0257', '5757ba73cef90', 'OIqznlu0eEpGxhNyr5vXwag974K3Zk', '500.00', '573e5b0202eef', NULL, '2016-09-16 08:34:59', NULL, 'efa1abcaa0b51', 'AP-03447862P0675472G', 'richmund-buyer@sushidigital.com.au', 'W4DW3NMBBH2MQ', NULL, '', NULL, '2016-09-16 08:33:12', '2016-09-16 08:34:59'),
('5809c3b87cfe0', '5757b7619536a', 'lP3fk2BIHpA7K1XqaGZNTDeUQuhzSb', '634.00', '573e5b0202eef', NULL, '2016-10-21 07:40:53', NULL, '6dfe910a4715c', 'AP-6C56837228574021L', 'leo_1338405575_per@sushidigital.com.au', 'UPGFSHS653GEG', NULL, '', NULL, '2016-10-21 07:28:56', '2016-10-21 07:40:53'),
('5811c37f546e3', '57c7f3dff0df7', 'kN2VYnLFyp658So1lexbAiUfQ3sqPg', '619.00', '573e5b0202eef', NULL, '2016-10-27 09:06:52', NULL, '98b877f22c45', 'AP-2U300525RC141342N', 'leo_1338405575_per@sushidigital.com.au', 'UPGFSHS653GEG', NULL, '', NULL, '2016-10-27 09:06:07', '2016-10-27 09:06:52'),
('5819a7000f75b', '5811a0a68a5cc', 'oSh9Vyk70etiArIZpTPbclwj3UmnKW', '1419.99', '573e5b0202eef', NULL, '2016-11-02 08:43:32', NULL, '363d421bb2c0f', 'AP-7VX12700KD335204P', 'leo_1338405575_per@sushidigital.com.au', 'UPGFSHS653GEG', NULL, '', NULL, '2016-11-02 08:42:40', '2016-11-02 08:43:32'),
('581ab0d62fedc', '57c7f3dff0df7', '13H9olg2sTeSD7fLZVtpM05dXzKGqI', '619.00', '573e5b0202eef', NULL, '2016-11-03 03:38:06', NULL, 'cdb4a004e348b', 'AP-2L5174542L105220G', 'leo_1338405575_per@sushidigital.com.au', 'UPGFSHS653GEG', NULL, NULL, NULL, '2016-11-03 03:36:54', '2016-11-03 03:38:06'),
('581ab524877c5', '57c7f3dff0df7', 'WrLQ4DsAzRvJiEn587KFxP6kgVSCbN', '619.00', '573e5b0202eef', NULL, '2016-11-03 04:01:55', NULL, '555e6ffd4654b', 'AP-44A441409Y6575823', 'leo_1338405575_per@sushidigital.com.au', 'UPGFSHS653GEG', NULL, NULL, NULL, '2016-11-03 03:55:16', '2016-11-03 04:01:55'),
('581ab883d3db6', '57c7f3dff0df7', 'W179F3VcEUKxlGDkJqd6tOfeyrA2NX', '619.00', '573e5b0202eef', NULL, '2016-11-03 04:10:30', NULL, 'd1096dd794797', 'AP-6UX79880RT666683D', 'leo_1338405575_per@sushidigital.com.au', 'UPGFSHS653GEG', NULL, NULL, NULL, '2016-11-03 04:09:39', '2016-11-03 04:10:30'),
('58564edc70345', '577e48310c58d', 'ZSha6v9uqgKxdctr4OQJUPmf3FTG0o', '2820.00', '573e5b0202ef3', '2016-12-18 08:58:19', NULL, NULL, '6c87798d3d4d4', 'AP-6HU29986D3995044M', NULL, NULL, NULL, NULL, NULL, '2016-12-18 08:54:52', '2016-12-18 08:58:19'),
('5981892611793', '5981889078112', 'RfiAkn4SaEysBb6hTlWqgIu2PHCZKG', '595.00', '573e5b0202ee6', NULL, NULL, NULL, '6d92d30ed9d67', 'AP-17S56023KH132124E', NULL, NULL, NULL, '1td6DqC4xF8fIlBNTmEZu2sHOpe9avyXScLjgVKWnMw37iPh05AUJkYQGzbo', NULL, '2017-08-02 08:11:18', '2017-08-02 08:11:19'),
('5987cdb598411', '5987c88324398', 'g1irjMp5eXqOJPA6By9zaQYCWV2SHc', '595.00', '573e5b0202ee6', NULL, NULL, NULL, '71ddbdd79c914', 'AP-9ES03376BA7037047', NULL, NULL, NULL, '4qmg5zNOVkH2y96dIlCBjMT0PUwDvirX71oKJex3WcpaZsfthbFu8GnSAERQ', NULL, '2017-08-07 02:17:25', '2017-08-07 02:17:27');

-- --------------------------------------------------------

--
-- Table structure for table `gp_orders_history`
--

CREATE TABLE `gp_orders_history` (
  `order_history_id` varchar(13) NOT NULL,
  `order_id` varchar(13) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gp_orders_history`
--

INSERT INTO `gp_orders_history` (`order_history_id`, `order_id`, `description`, `created_at`, `updated_at`) VALUES
('5757c07e72599', '5757c07e711bb', 'New order created', '2016-06-08 06:51:42', '2016-06-08 06:51:42'),
('5757c0cb3c2af', '5757c0cb398e9', 'New order created', '2016-06-08 06:52:59', '2016-06-08 06:52:59'),
('5757dd33065f0', '5757dd330534e', 'New order created', '2016-06-08 08:54:11', '2016-06-08 08:54:11'),
('5757ddac9b97b', '5757ddac9a914', 'New order created', '2016-06-08 08:56:12', '2016-06-08 08:56:12'),
('5757e21e04564', '5757e21e03152', 'New order created', '2016-06-08 09:15:10', '2016-06-08 09:15:10'),
('5758eac19655c', '5758eac1951c1', 'New order created', '2016-06-09 04:04:17', '2016-06-09 04:04:17'),
('5758eae9eea43', '5758eae9ec739', 'New order created', '2016-06-09 04:04:57', '2016-06-09 04:04:57'),
('575900e121502', '575900e1201d7', 'New order created', '2016-06-09 05:38:41', '2016-06-09 05:38:41'),
('5759020a29800', '5759020a2829e', 'New order created', '2016-06-09 05:43:38', '2016-06-09 05:43:38'),
('575902ed12bc4', '575902ed10ea9', 'New order created', '2016-06-09 05:47:25', '2016-06-09 05:47:25'),
('57590aa2c27e7', '57590aa2c0f19', 'New order created', '2016-06-09 06:20:18', '2016-06-09 06:20:18'),
('57590b0c9aa7c', '57590b0c997bd', 'New order created', '2016-06-09 06:22:04', '2016-06-09 06:22:04'),
('57590bf9b075b', '57590bf9af099', 'New order created', '2016-06-09 06:26:01', '2016-06-09 06:26:01'),
('57590c6fbb44b', '57590c6fb9ce1', 'New order created', '2016-06-09 06:27:59', '2016-06-09 06:27:59'),
('57590f5d1db00', '57590f5d1a3c1', 'New order created', '2016-06-09 06:40:29', '2016-06-09 06:40:29'),
('57590fc901908', '57590fc9008f5', 'New order created', '2016-06-09 06:42:17', '2016-06-09 06:42:17'),
('575a81c407080', '575a81c405bb5', 'New order created', '2016-06-10 09:00:52', '2016-06-10 09:00:52'),
('575e45e41eb6b', '575e45e41d72f', 'New order created', '2016-06-13 05:34:28', '2016-06-13 05:34:28'),
('575e4602652ee', '575e45e41d72f', 'Order has been cancelled', '2016-06-13 05:34:58', '2016-06-13 05:34:58'),
('575e531820510', '575e53181ac10', 'New order created', '2016-06-13 06:30:48', '2016-06-13 06:30:48'),
('575e534f41af7', '575e53181ac10', 'Order has been cancelled', '2016-06-13 06:31:43', '2016-06-13 06:31:43'),
('575e5ebe375be', '575e5ebe35e58', 'New order created', '2016-06-13 07:20:30', '2016-06-13 07:20:30'),
('575e5ece7ddf9', '575e5ebe35e58', 'Order has been cancelled', '2016-06-13 07:20:46', '2016-06-13 07:20:46'),
('575e5ed09eb03', '575e5ed09d93d', 'New order created', '2016-06-13 07:20:48', '2016-06-13 07:20:48'),
('575e5f1bac21f', '575e5ed09d93d', 'Order has been cancelled', '2016-06-13 07:22:03', '2016-06-13 07:22:03'),
('575f8222ab912', '575f8222a9e41', 'New order created', '2016-06-14 04:03:46', '2016-06-14 04:03:46'),
('575f82b67488f', '575f82b6738f9', 'New order created', '2016-06-14 04:06:14', '2016-06-14 04:06:14'),
('575f835677c9c', '575f82b6738f9', 'Order has been cancelled', '2016-06-14 04:08:54', '2016-06-14 04:08:54'),
('575f83ff6f16f', '575f8222a9e41', 'Order has been cancelled', '2016-06-14 04:11:43', '2016-06-14 04:11:43'),
('576104df11d80', '576104df0fb7f', 'New order created', '2016-06-15 07:33:51', '2016-06-15 07:33:51'),
('576104f78ba5d', '576104df0fb7f', 'Order has been cancelled', '2016-06-15 07:34:15', '2016-06-15 07:34:15'),
('576112d438b4d', '576112d43761e', 'New order created', '2016-06-15 08:33:24', '2016-06-15 08:33:24'),
('576112fcf3955', '576112d43761e', 'Order has been completed', '2016-06-15 08:34:04', '2016-06-15 08:34:04'),
('57611319523fa', '57611319513af', 'New order created', '2016-06-15 08:34:33', '2016-06-15 08:34:33'),
('5761133ed841b', '57611319513af', 'Order has been completed', '2016-06-15 08:35:10', '2016-06-15 08:35:10'),
('5761212043427', '57612120415c1', 'New order created', '2016-06-15 09:34:24', '2016-06-15 09:34:24'),
('576122377666f', '57612237752d3', 'New order created', '2016-06-15 09:39:03', '2016-06-15 09:39:03'),
('576122ed732dc', '576122ed72041', 'New order created', '2016-06-15 09:42:05', '2016-06-15 09:42:05'),
('576212a67a72c', '576212a679427', 'New order created', '2016-06-16 02:44:54', '2016-06-16 02:44:54'),
('576212f2e28f5', '576212a679427', 'Order has been completed', '2016-06-16 02:46:10', '2016-06-16 02:46:10'),
('576213bf037d6', '576213bf02619', 'New order created', '2016-06-16 02:49:35', '2016-06-16 02:49:35'),
('57621401bee4d', '576213bf02619', 'Order has been completed', '2016-06-16 02:50:41', '2016-06-16 02:50:41'),
('576217b553ad6', '576217b5522c4', 'New order created', '2016-06-16 03:06:29', '2016-06-16 03:06:29'),
('5762180867a8d', '576217b5522c4', 'Order has been completed', '2016-06-16 03:07:52', '2016-06-16 03:07:52'),
('576218becaffc', '576218bec956d', 'New order created', '2016-06-16 03:10:54', '2016-06-16 03:10:54'),
('57621904a7ac0', '576218bec956d', 'Order has been completed', '2016-06-16 03:12:04', '2016-06-16 03:12:04'),
('57621990599b7', '5762199058886', 'New order created', '2016-06-16 03:14:24', '2016-06-16 03:14:24'),
('576219cc343ff', '5762199058886', 'Order has been completed', '2016-06-16 03:15:24', '2016-06-16 03:15:24'),
('57621d30c6a2c', '57621d30c573a', 'New order created', '2016-06-16 03:29:52', '2016-06-16 03:29:52'),
('57621d6a2a80a', '57621d30c573a', 'Order has been completed', '2016-06-16 03:30:50', '2016-06-16 03:30:50'),
('5762460d47f47', '5762460d464ed', 'New order created', '2016-06-16 06:24:13', '2016-06-16 06:24:13'),
('5762463702606', '5762460d464ed', 'Order has been completed', '2016-06-16 06:24:55', '2016-06-16 06:24:55'),
('576257136c622', '576257136aafb', 'New order created', '2016-06-16 07:36:51', '2016-06-16 07:36:51'),
('5762573893c41', '576257136aafb', 'Order has been cancelled', '2016-06-16 07:37:28', '2016-06-16 07:37:28'),
('5762818c9752a', '5762818c9559c', 'New order created', '2016-06-16 10:38:04', '2016-06-16 10:38:04'),
('576281a342334', '5762818c9559c', 'Order has been cancelled', '2016-06-16 10:38:27', '2016-06-16 10:38:27'),
('576358b06cce6', '576358b062282', 'New order created', '2016-06-17 01:56:00', '2016-06-17 01:56:00'),
('576358d9cabde', '576358b062282', 'Order has been cancelled', '2016-06-17 01:56:41', '2016-06-17 01:56:41'),
('576358fa9a6ed', '576358fa98ad9', 'New order created', '2016-06-17 01:57:14', '2016-06-17 01:57:14'),
('5763594cda541', '576358fa98ad9', 'Order has been completed', '2016-06-17 01:58:36', '2016-06-17 01:58:36'),
('5764bf87f3135', '5764bf87e2470', 'New order created', '2016-06-18 03:27:03', '2016-06-18 03:27:03'),
('577e5011aa069', '577e5011a04ff', 'New order created', '2016-07-07 12:50:25', '2016-07-07 12:50:25'),
('577e504755a93', '577e5011a04ff', 'Order has been cancelled', '2016-07-07 12:51:19', '2016-07-07 12:51:19'),
('577e53230686b', '577e532304bf4', 'New order created', '2016-07-07 13:03:31', '2016-07-07 13:03:31'),
('57805c8e96dbe', '57805c8e8d882', 'New order created', '2016-07-09 02:08:14', '2016-07-09 02:08:14'),
('57805cc04bb29', '57805c8e8d882', 'Order has been cancelled', '2016-07-09 02:09:04', '2016-07-09 02:09:04'),
('57877584cc56b', '57877584c752d', 'New order created', '2016-07-14 11:20:36', '2016-07-14 11:20:36'),
('57a96f2672066', '57a96f2668d8a', 'New order created', '2016-08-09 05:50:30', '2016-08-09 05:50:30'),
('57a9715ef2a2f', '57a96f2668d8a', 'Order has been cancelled', '2016-08-09 05:59:58', '2016-08-09 05:59:58'),
('57a9728b54282', '57a9728b525ec', 'New order created', '2016-08-09 06:04:59', '2016-08-09 06:04:59'),
('57a9752780ba9', '57a9728b525ec', 'Order has been completed', '2016-08-09 06:16:07', '2016-08-09 06:16:07'),
('57a9760a807a8', '57a9728b525ec', 'Order has been completed', '2016-08-09 06:19:54', '2016-08-09 06:19:54'),
('57a97880d3db8', '57a9728b525ec', 'Order has been completed', '2016-08-09 06:30:24', '2016-08-09 06:30:24'),
('57a97e684e9e7', '57a97e684ce27', 'New order created', '2016-08-09 06:55:36', '2016-08-09 06:55:36'),
('57a97fb28ee54', '57a97e684ce27', 'Order has been completed', '2016-08-09 07:01:06', '2016-08-09 07:01:06'),
('57b1a5b0abe20', '57b1a5b0a9462', 'New order created', '2016-08-15 11:21:20', '2016-08-15 11:21:20'),
('57b1a624b454a', '57b1a624b29f6', 'New order created', '2016-08-15 11:23:16', '2016-08-15 11:23:16'),
('57b1a648bc6c0', '57b1a624b29f6', 'Order has been cancelled', '2016-08-15 11:23:52', '2016-08-15 11:23:52'),
('57b1a869623c8', '57b1a869609a2', 'New order created', '2016-08-15 11:32:57', '2016-08-15 11:32:57'),
('57b1a8c0abdb5', '57b1a8c0aa3dc', 'New order created', '2016-08-15 11:34:24', '2016-08-15 11:34:24'),
('57b1a90b482b2', '57b1a8c0aa3dc', 'Order has been cancelled', '2016-08-15 11:35:39', '2016-08-15 11:35:39'),
('57b1ae4af1945', '57b1ae4aefd61', 'New order created', '2016-08-15 11:58:02', '2016-08-15 11:58:02'),
('57b1aeae50d2f', '57b1ae4aefd61', 'Order has been cancelled', '2016-08-15 11:59:42', '2016-08-15 11:59:42'),
('57b1b06242428', '57b1b062409e2', 'New order created', '2016-08-15 12:06:58', '2016-08-15 12:06:58'),
('57b1b097a081a', '57b1b0979ec3d', 'New order created', '2016-08-15 12:07:51', '2016-08-15 12:07:51'),
('57b1b0b12575a', '57b1b0979ec3d', 'Order has been cancelled', '2016-08-15 12:08:17', '2016-08-15 12:08:17'),
('57b28f035f715', '57b28f035858f', 'New order created', '2016-08-16 03:56:51', '2016-08-16 03:56:51'),
('57b28f52ab263', '57b28f035858f', 'Order has been completed', '2016-08-16 03:58:10', '2016-08-16 03:58:10'),
('57b5922faceb7', '57b5922fab30f', 'New order created', '2016-08-18 10:47:11', '2016-08-18 10:47:11'),
('57b59288bf429', '57b5922fab30f', 'Order has been completed', '2016-08-18 10:48:40', '2016-08-18 10:48:40'),
('57b826f111072', '57b826f1052a8', 'New order created', '2016-08-20 09:46:25', '2016-08-20 09:46:25'),
('57b8285a72182', '57b826f1052a8', 'Order has been cancelled', '2016-08-20 09:52:26', '2016-08-20 09:52:26'),
('57b8288c09043', '57b8288c0727f', 'New order created', '2016-08-20 09:53:16', '2016-08-20 09:53:16'),
('57b8290207356', '57b8288c0727f', 'Order has been completed', '2016-08-20 09:55:14', '2016-08-20 09:55:14'),
('57bc2811c9807', '57bc2811c7aea', 'New order created', '2016-08-23 10:40:17', '2016-08-23 10:40:17'),
('57bc2894bc57d', '57bc2811c7aea', 'Order has been completed', '2016-08-23 10:42:28', '2016-08-23 10:42:28'),
('57c4272c32a7e', '57c4272c28507', 'New order created', '2016-08-29 12:14:36', '2016-08-29 12:14:36'),
('57c427709a0a7', '57c4272c28507', 'Order has been completed', '2016-08-29 12:15:44', '2016-08-29 12:15:44'),
('57c5714d8083f', '57c5714d73ab9', 'New order created', '2016-08-30 11:43:09', '2016-08-30 11:43:09'),
('57c5718a7fc0d', '57c5714d73ab9', 'Order has been completed', '2016-08-30 11:44:10', '2016-08-30 11:44:10'),
('57c7d25df3d58', '57c7d25df206f', 'New order created', '2016-09-01 07:01:49', '2016-09-01 07:01:49'),
('57c7d28c04405', '57c7d25df206f', 'Order has been completed', '2016-09-01 07:02:36', '2016-09-01 07:02:36'),
('57c973b0735b5', '57c973b07113c', 'New order created', '2016-09-02 12:42:24', '2016-09-02 12:42:24'),
('57c974b9bd2b5', '57c973b07113c', 'Order has been completed', '2016-09-02 12:46:49', '2016-09-02 12:46:49'),
('57ce63e713c3f', '57ce63e7118f2', 'New order created', '2016-09-06 06:36:23', '2016-09-06 06:36:23'),
('57ce64294a40b', '57ce63e7118f2', 'Order has been completed', '2016-09-06 06:37:29', '2016-09-06 06:37:29'),
('57ce9807e3836', '57ce9807e13f3', 'New order created', '2016-09-06 10:18:47', '2016-09-06 10:18:47'),
('57ce9b7a11522', '57ce9b7a0f65d', 'New order created', '2016-09-06 10:33:30', '2016-09-06 10:33:30'),
('57ce9bb8ec2af', '57ce9b7a0f65d', 'Order has been cancelled', '2016-09-06 10:34:32', '2016-09-06 10:34:32'),
('57cea1ed51a2c', '57cea1ed4f9d2', 'New order created', '2016-09-06 11:01:01', '2016-09-06 11:01:01'),
('57cfc2bd2a54d', '57cfc2bd28a06', 'New order created', '2016-09-07 07:33:17', '2016-09-07 07:33:17'),
('57cfc3ae2caf7', '57cfc3ae2afff', 'New order created', '2016-09-07 07:37:18', '2016-09-07 07:37:18'),
('57cfc3db4b2c1', '57cfc3ae2afff', 'Order has been completed', '2016-09-07 07:38:03', '2016-09-07 07:38:03'),
('57cfc553caffd', '57cfc2bd28a06', 'Order has been cancelled', '2016-09-07 07:44:19', '2016-09-07 07:44:19'),
('57d104fd42169', '57d104fd40555', 'New order created', '2016-09-08 06:28:13', '2016-09-08 06:28:13'),
('57d1068d828d3', '57d104fd40555', 'Order has been cancelled', '2016-09-08 06:34:53', '2016-09-08 06:34:53'),
('57d106c9dbae1', '57d106c9d9e1d', 'New order created', '2016-09-08 06:35:53', '2016-09-08 06:35:53'),
('57d108401691e', '57d106c9d9e1d', 'Order has been completed', '2016-09-08 06:42:08', '2016-09-08 06:42:08'),
('57d10daf57081', '57d10daf5537a', 'New order created', '2016-09-08 07:05:19', '2016-09-08 07:05:19'),
('57d10fe68601b', '57d10daf5537a', 'Order has been cancelled', '2016-09-08 07:14:46', '2016-09-08 07:14:46'),
('57d110098f50f', '57d110098d5b8', 'New order created', '2016-09-08 07:15:21', '2016-09-08 07:15:21'),
('57d11043b7677', '57d110098d5b8', 'Order has been completed', '2016-09-08 07:16:19', '2016-09-08 07:16:19'),
('57d8b52abffc8', '57d8b52abe41b', 'New order created', '2016-09-14 02:25:46', '2016-09-14 02:25:46'),
('57d8b8434eb96', '57d8b8434ce88', 'New order created', '2016-09-14 02:38:59', '2016-09-14 02:38:59'),
('57d8b85f42843', '57d8b8434ce88', 'Order has been cancelled', '2016-09-14 02:39:27', '2016-09-14 02:39:27'),
('57d8b8cda8033', '57d8b8cda615f', 'New order created', '2016-09-14 02:41:17', '2016-09-14 02:41:17'),
('57d8ba0300c49', '57d8ba02f2f3e', 'New order created', '2016-09-14 02:46:27', '2016-09-14 02:46:27'),
('57d8bc071f4f9', '57d8ba02f2f3e', 'Order has been completed', '2016-09-14 02:55:03', '2016-09-14 02:55:03'),
('57db9d3a95144', '57db9d3a93660', 'New order created', '2016-09-16 07:20:26', '2016-09-16 07:20:26'),
('57db9d8c12edf', '57db9d3a93660', 'Order has been completed', '2016-09-16 07:21:48', '2016-09-16 07:21:48'),
('57dbae48e1f94', '57dbae48e0257', 'New order created', '2016-09-16 08:33:12', '2016-09-16 08:33:12'),
('57dbaeb3ad140', '57dbae48e0257', 'Order has been completed', '2016-09-16 08:34:59', '2016-09-16 08:34:59'),
('5809c3b87f0d7', '5809c3b87cfe0', 'New order created', '2016-10-21 07:28:56', '2016-10-21 07:28:56'),
('5809c685adff6', '5809c3b87cfe0', 'Order has been completed', '2016-10-21 07:40:53', '2016-10-21 07:40:53'),
('58105663c3b63', '5758eae9ec739', 'Order has been completed', '2016-10-26 07:08:19', '2016-10-26 07:08:19'),
('5811c37f565f1', '5811c37f546e3', 'New order created', '2016-10-27 09:06:07', '2016-10-27 09:06:07'),
('5811c3acc3b64', '5811c37f546e3', 'Order has been completed', '2016-10-27 09:06:52', '2016-10-27 09:06:52'),
('5819a7001125e', '5819a7000f75b', 'New order created', '2016-11-02 08:42:40', '2016-11-02 08:42:40'),
('5819a7349e275', '5819a7000f75b', 'Order has been completed', '2016-11-02 08:43:32', '2016-11-02 08:43:32'),
('581ab0d6319bf', '581ab0d62fedc', 'New order created', '2016-11-03 03:36:54', '2016-11-03 03:36:54'),
('581ab11ee1bca', '581ab0d62fedc', 'Order has been completed', '2016-11-03 03:38:06', '2016-11-03 03:38:06'),
('581ab524894b7', '581ab524877c5', 'New order created', '2016-11-03 03:55:16', '2016-11-03 03:55:16'),
('581ab552e66cd', '581ab524877c5', 'Order has been completed', '2016-11-03 03:56:02', '2016-11-03 03:56:02'),
('581ab5ddcb4fa', '581ab524877c5', 'Order has been completed', '2016-11-03 03:58:21', '2016-11-03 03:58:21'),
('581ab66b9b2c6', '581ab524877c5', 'Order has been completed', '2016-11-03 04:00:43', '2016-11-03 04:00:43'),
('581ab68d30b96', '581ab524877c5', 'Order has been completed', '2016-11-03 04:01:17', '2016-11-03 04:01:17'),
('581ab6b3f34ce', '581ab524877c5', 'Order has been completed', '2016-11-03 04:01:55', '2016-11-03 04:01:55'),
('581ab883d5a97', '581ab883d3db6', 'New order created', '2016-11-03 04:09:39', '2016-11-03 04:09:39'),
('581ab8b637e39', '581ab883d3db6', 'Order has been completed', '2016-11-03 04:10:30', '2016-11-03 04:10:30'),
('58564edc71d0e', '58564edc70345', 'New order created', '2016-12-18 08:54:52', '2016-12-18 08:54:52'),
('58564faba54dd', '58564edc70345', 'Order has been cancelled', '2016-12-18 08:58:19', '2016-12-18 08:58:19'),
('598189261851d', '5981892611793', 'New order created', '2017-08-02 08:11:18', '2017-08-02 08:11:18'),
('5987cdb59a557', '5987cdb598411', 'New order created', '2017-08-07 02:17:25', '2017-08-07 02:17:25');

-- --------------------------------------------------------

--
-- Table structure for table `gp_orders_payment_info`
--

CREATE TABLE `gp_orders_payment_info` (
  `order_payment_id` varchar(13) NOT NULL,
  `order_id` varchar(13) NOT NULL,
  `transaction_id` varchar(100) DEFAULT NULL,
  `transaction_status` varchar(100) DEFAULT NULL,
  `receiver_amount` decimal(9,2) DEFAULT NULL,
  `receiver_email` varchar(100) DEFAULT NULL,
  `receiver_primary` enum('true','false') DEFAULT NULL,
  `receiver_invoice_id` varchar(30) DEFAULT NULL,
  `receiver_payment_type` varchar(100) DEFAULT NULL,
  `receiver_account_id` varchar(100) DEFAULT NULL,
  `refunded_amount` decimal(9,2) DEFAULT NULL,
  `pending_refund` varchar(20) DEFAULT NULL,
  `sender_transaction_id` varchar(100) DEFAULT NULL,
  `sender_transaction_status` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gp_orders_payment_info`
--

INSERT INTO `gp_orders_payment_info` (`order_payment_id`, `order_id`, `transaction_id`, `transaction_status`, `receiver_amount`, `receiver_email`, `receiver_primary`, `receiver_invoice_id`, `receiver_payment_type`, `receiver_account_id`, `refunded_amount`, `pending_refund`, `sender_transaction_id`, `sender_transaction_status`, `created_at`, `updated_at`) VALUES
('575901704f77f', '5758eac1951c1', NULL, 'created', '89.99', 'chard_1338406363_biz@sushidigital.com.au', 'true', 'ZwExWlX6FT2JcsdnAhevygj95bIVQa', 'service', '3TEDYG6B48RMQ', NULL, 'false', NULL, NULL, '2016-06-09 05:41:04', '2016-06-09 05:41:04'),
('575901704fefa', '5758eac1951c1', NULL, 'created', '10.00', 'gearplanetbuyer002@gmail.com', 'false', 'ZwExWlX6FT2JcsdnAhevygj95bIVQa', 'service', '79SPH4LHB5HV6', NULL, 'false', NULL, NULL, '2016-06-09 05:41:04', '2016-06-09 05:41:04'),
('5759034fa5b5b', '575902ed10ea9', '34565887VJ8018515', 'completed', '89.99', 'gearplanetbuyer001@gmail.com', 'true', 'Dep2tfkjwLy49KGl5VnI7bc38YEguS', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '60H96281R37148036', 'completed', '2016-06-09 05:49:03', '2016-06-09 05:49:03'),
('5759034fa66b0', '575902ed10ea9', '36356609EK824011X', 'completed', '10.00', 'gearplanetbuyer002@gmail.com', 'false', 'Dep2tfkjwLy49KGl5VnI7bc38YEguS', 'service', '79SPH4LHB5HV6', '0.00', 'false', '2MW41371KL878045E', 'completed', '2016-06-09 05:49:03', '2016-06-09 05:49:03'),
('57590ae8b43c8', '57590aa2c0f19', '5SF933793X644362H', 'completed', '193.50', 'gearplanetbuyer001@gmail.com', 'true', 'BjozQAmWbKtk57xciFUnH2uvfR9sad', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '52906314VK430973F', 'completed', '2016-06-09 06:21:28', '2016-06-09 06:21:28'),
('57590ae8b4ccc', '57590aa2c0f19', '31T98854A9820284N', 'completed', '21.50', 'gearplanetbuyer002@gmail.com', 'false', 'BjozQAmWbKtk57xciFUnH2uvfR9sad', 'service', '79SPH4LHB5HV6', '0.00', 'false', '6XL10210EV3836004', 'completed', '2016-06-09 06:21:28', '2016-06-09 06:21:28'),
('57590b30cc710', '57590b0c997bd', '5FX009006E676854K', 'completed', '539.10', 'gearplanetbuyer001@gmail.com', 'true', 'DvcYW85LuhB3P7F2skgT9OmANltrGn', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '49N20100HN127632R', 'completed', '2016-06-09 06:22:40', '2016-06-09 06:22:40'),
('57590b30cd0ff', '57590b0c997bd', '0J229475WC699325U', 'completed', '59.90', 'gearplanetbuyer002@gmail.com', 'false', 'DvcYW85LuhB3P7F2skgT9OmANltrGn', 'service', '79SPH4LHB5HV6', '0.00', 'false', '9MN01211G0469071F', 'completed', '2016-06-09 06:22:40', '2016-06-09 06:22:40'),
('57590c5437415', '57590bf9af099', '4UX7001579178470G', 'completed', '134.99', 'gearplanetbuyer001@gmail.com', 'true', 'QRE4WKq97GFcwmjiYfZIgOHSDu5o3a', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '0GA72474UT3934120', 'completed', '2016-06-09 06:27:32', '2016-06-09 06:27:32'),
('57590c5438196', '57590bf9af099', '0M487491F24083440', 'completed', '15.00', 'gearplanetbuyer002@gmail.com', 'false', 'QRE4WKq97GFcwmjiYfZIgOHSDu5o3a', 'service', '79SPH4LHB5HV6', '0.00', 'false', '5J8997619B070654L', 'completed', '2016-06-09 06:27:32', '2016-06-09 06:27:32'),
('57590f52f4179', '57590c6fb9ce1', '462105095X024923J', 'completed', '89.99', 'gearplanetbuyer001@gmail.com', 'true', 'mi2fMlCc0DNJhFLaW1XUBkdTEeGI5Y', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '3RW04008XE5800421', 'completed', '2016-06-09 06:40:18', '2016-06-09 06:40:18'),
('57590f530083c', '57590c6fb9ce1', '3R673646SD0398705', 'completed', '10.00', 'gearplanetbuyer002@gmail.com', 'false', 'mi2fMlCc0DNJhFLaW1XUBkdTEeGI5Y', 'service', '79SPH4LHB5HV6', '0.00', 'false', '4DE17419MF7753355', 'completed', '2016-06-09 06:40:19', '2016-06-09 06:40:19'),
('57590fbfa9be5', '57590f5d1a3c1', NULL, 'created', '22.67', 'gearplanetbuyer001@gmail.com', 'true', 'hgNtwJeDHnPy0TV1saFOZIrUlGMKW8', 'service', 'UXLW2SJYF7SFG', NULL, 'false', NULL, NULL, '2016-06-09 06:42:07', '2016-06-09 06:42:07'),
('57590fbfaa3fa', '57590f5d1a3c1', NULL, 'created', '2.52', 'gearplanetbuyer002@gmail.com', 'false', 'hgNtwJeDHnPy0TV1saFOZIrUlGMKW8', 'service', '79SPH4LHB5HV6', NULL, 'false', NULL, NULL, '2016-06-09 06:42:07', '2016-06-09 06:42:07'),
('5759102dd4ed9', '57590fc9008f5', '8AR4661569262101S', 'completed', '99.99', 'gearplanetbuyer001@gmail.com', 'true', 'Mc0rVlGpXwHf6EySzkhB5FOQaDZ9vY', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '6YK679069E7350436', 'completed', '2016-06-09 06:43:57', '2016-06-09 06:43:57'),
('5759102dd56b2', '57590fc9008f5', '01812320YP541632C', 'completed', '10.00', 'gearplanetbuyer002@gmail.com', 'false', 'Mc0rVlGpXwHf6EySzkhB5FOQaDZ9vY', 'service', '79SPH4LHB5HV6', '0.00', 'false', '9BR1476047467653B', 'completed', '2016-06-09 06:43:57', '2016-06-09 06:43:57'),
('575a821196e45', '575a81c405bb5', '0XR53744P4907742W', 'completed', '25.19', 'gearplanetbuyer001@gmail.com', 'true', 'InzM247qcg3BFwpsLZSleoEWPXyRH1', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '1M1608987X4203249', 'completed', '2016-06-10 09:02:09', '2016-06-10 09:02:09'),
('575a821197ddf', '575a81c405bb5', '61D132536B3808915', 'completed', '2.52', 'gearplanetbuyer002@gmail.com', 'false', 'InzM247qcg3BFwpsLZSleoEWPXyRH1', 'service', '79SPH4LHB5HV6', '0.00', 'false', '9JH29808VL572141N', 'completed', '2016-06-10 09:02:09', '2016-06-10 09:02:09'),
('575e4602632de', '575e45e41d72f', NULL, 'created', '895.00', 'gearplanetbuyer001@gmail.com', 'true', 'C9xKEmihcVMdfzRZBePs7YolOHp2Fr', 'service', 'UXLW2SJYF7SFG', NULL, 'false', NULL, NULL, '2016-06-13 05:34:58', '2016-06-13 05:34:58'),
('575e460263d76', '575e45e41d72f', NULL, 'created', '89.50', 'gearplanetbuyer002@gmail.com', 'false', 'C9xKEmihcVMdfzRZBePs7YolOHp2Fr', 'service', '79SPH4LHB5HV6', NULL, 'false', NULL, NULL, '2016-06-13 05:34:58', '2016-06-13 05:34:58'),
('575e5ece7c460', '575e5ebe35e58', NULL, 'created', '99.99', 'gearplanetbuyer001@gmail.com', 'true', '6VFqy91Xof3CUJMLBDGAYtxbaZi8T7', 'service', 'UXLW2SJYF7SFG', NULL, 'false', NULL, NULL, '2016-06-13 07:20:46', '2016-06-13 07:20:46'),
('575e5ece7ce4a', '575e5ebe35e58', NULL, 'created', '10.00', 'gearplanetbuyer002@gmail.com', 'false', '6VFqy91Xof3CUJMLBDGAYtxbaZi8T7', 'service', '79SPH4LHB5HV6', NULL, 'false', NULL, NULL, '2016-06-13 07:20:46', '2016-06-13 07:20:46'),
('575e5efdbc6b3', '575e5ed09d93d', '1YG4073554219440W', 'completed', '149.99', 'gearplanetbuyer001@gmail.com', 'true', 'REZ9u0T7VqLl2My5wN8AXOsnYrjdba', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '0U419214M5577480G', 'completed', '2016-06-13 07:21:33', '2016-06-13 07:21:33'),
('575e5efdbd1b9', '575e5ed09d93d', '69N10352GK882722R', 'completed', '15.00', 'gearplanetbuyer002@gmail.com', 'false', 'REZ9u0T7VqLl2My5wN8AXOsnYrjdba', 'service', '79SPH4LHB5HV6', '0.00', 'false', '13010936E3397611H', 'completed', '2016-06-13 07:21:33', '2016-06-13 07:21:33'),
('575f83ff6c1af', '575f8222a9e41', NULL, 'created', '215.00', 'gearplanetbuyer001@gmail.com', 'true', 'DyChXOdmRBu3bxIoHaQFnqTKVcfZUp', 'service', 'UXLW2SJYF7SFG', NULL, 'false', NULL, NULL, '2016-06-14 04:11:43', '2016-06-14 04:11:43'),
('575f83ff6d450', '575f8222a9e41', NULL, 'created', '21.50', 'gearplanetbuyer002@gmail.com', 'false', 'DyChXOdmRBu3bxIoHaQFnqTKVcfZUp', 'service', '79SPH4LHB5HV6', NULL, 'false', NULL, NULL, '2016-06-14 04:11:43', '2016-06-14 04:11:43'),
('576104f78a566', '576104df0fb7f', NULL, 'created', '149.99', 'gearplanetbuyer001@gmail.com', 'true', '13v4lymjEh7ixBZzfctCSdDeuJ9aqV', 'service', 'UXLW2SJYF7SFG', NULL, 'false', NULL, NULL, '2016-06-15 07:34:15', '2016-06-15 07:34:15'),
('576104f78adb9', '576104df0fb7f', NULL, 'created', '15.00', 'gearplanetbuyer002@gmail.com', 'false', '13v4lymjEh7ixBZzfctCSdDeuJ9aqV', 'service', '79SPH4LHB5HV6', NULL, 'false', NULL, NULL, '2016-06-15 07:34:15', '2016-06-15 07:34:15'),
('576112fcf05d7', '576112d43761e', '2GB00056FT995492D', 'completed', '75.00', 'gearplanetbuyer001@gmail.com', 'true', 'vh0TOjdHFipyJZU2QcL9qVDM45noIK', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '5BS94371K66569318', 'completed', '2016-06-15 08:34:04', '2016-06-15 08:34:04'),
('576112fcf0f08', '576112d43761e', '56F148121P3706038', 'completed', '7.50', 'gearplanetbuyer002@gmail.com', 'false', 'vh0TOjdHFipyJZU2QcL9qVDM45noIK', 'service', '79SPH4LHB5HV6', '0.00', 'false', '4YE97730WF958123A', 'completed', '2016-06-15 08:34:04', '2016-06-15 08:34:04'),
('5761133ed6d99', '57611319513af', '49729033WH248035M', 'completed', '849.99', 'gearplanetbuyer001@gmail.com', 'true', 'NFQHV82qCf0BaJ4A6ejMWG9rg1TmOK', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '26E37287T21780111', 'completed', '2016-06-15 08:35:10', '2016-06-15 08:35:10'),
('5761133ed770e', '57611319513af', '54880337VC5841034', 'completed', '85.00', 'gearplanetbuyer002@gmail.com', 'false', 'NFQHV82qCf0BaJ4A6ejMWG9rg1TmOK', 'service', '79SPH4LHB5HV6', '0.00', 'false', '0W336669Y1149392P', 'completed', '2016-06-15 08:35:10', '2016-06-15 08:35:10'),
('5761214af2770', '57612120415c1', '9RY995724C151270L', 'completed', '25.19', 'gearplanetbuyer001@gmail.com', 'true', '5nFqKRd3B7tvL6Mj9Q0cfmC1Ak2OGT', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '2WW97394TN7746643', 'completed', '2016-06-15 09:35:06', '2016-06-15 09:35:06'),
('5761214af307c', '57612120415c1', '9GM39224VV612670S', 'completed', '2.52', 'gearplanetbuyer002@gmail.com', 'false', '5nFqKRd3B7tvL6Mj9Q0cfmC1Ak2OGT', 'service', '79SPH4LHB5HV6', '0.00', 'false', '5EC9030033697835V', 'completed', '2016-06-15 09:35:06', '2016-06-15 09:35:06'),
('57612162ee38f', '57612120415c1', '9RY995724C151270L', 'completed', '25.19', 'gearplanetbuyer001@gmail.com', 'true', '5nFqKRd3B7tvL6Mj9Q0cfmC1Ak2OGT', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '2WW97394TN7746643', 'completed', '2016-06-15 09:35:30', '2016-06-15 09:35:30'),
('57612162eec53', '57612120415c1', '9GM39224VV612670S', 'completed', '2.52', 'gearplanetbuyer002@gmail.com', 'false', '5nFqKRd3B7tvL6Mj9Q0cfmC1Ak2OGT', 'service', '79SPH4LHB5HV6', '0.00', 'false', '5EC9030033697835V', 'completed', '2016-06-15 09:35:30', '2016-06-15 09:35:30'),
('576122117eaf9', '57612120415c1', '9RY995724C151270L', 'completed', '25.19', 'gearplanetbuyer001@gmail.com', 'true', '5nFqKRd3B7tvL6Mj9Q0cfmC1Ak2OGT', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '2WW97394TN7746643', 'completed', '2016-06-15 09:38:25', '2016-06-15 09:38:25'),
('576122117f3a6', '57612120415c1', '9GM39224VV612670S', 'completed', '2.52', 'gearplanetbuyer002@gmail.com', 'false', '5nFqKRd3B7tvL6Mj9Q0cfmC1Ak2OGT', 'service', '79SPH4LHB5HV6', '0.00', 'false', '5EC9030033697835V', 'completed', '2016-06-15 09:38:25', '2016-06-15 09:38:25'),
('5761227c44dd2', '57612237752d3', '9DL30592ME4608927', 'completed', '1599.99', 'gearplanetbuyer001@gmail.com', 'true', 'S7JdBt16L3mz5ZokgAIUv8pq4cKXHQ', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '0B723526KK5879705', 'completed', '2016-06-15 09:40:12', '2016-06-15 09:40:12'),
('5761227c4567c', '57612237752d3', '7RS701968D552592U', 'completed', '160.00', 'gearplanetbuyer002@gmail.com', 'false', 'S7JdBt16L3mz5ZokgAIUv8pq4cKXHQ', 'service', '79SPH4LHB5HV6', '0.00', 'false', '50431467AV452425B', 'completed', '2016-06-15 09:40:12', '2016-06-15 09:40:12'),
('5761231457a58', '576122ed72041', '0RK959269A616530B', 'completed', '310.00', 'gearplanetbuyer001@gmail.com', 'true', 'uhoB2zxea6qSWUtVZAKmpOMRrvIsTN', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '3E505895EJ091142T', 'completed', '2016-06-15 09:42:44', '2016-06-15 09:42:44'),
('57612314582d7', '576122ed72041', '5C149983JW0145417', 'completed', '31.00', 'gearplanetbuyer002@gmail.com', 'false', 'uhoB2zxea6qSWUtVZAKmpOMRrvIsTN', 'service', '79SPH4LHB5HV6', '0.00', 'false', '0RJ54225T8519623Y', 'completed', '2016-06-15 09:42:44', '2016-06-15 09:42:44'),
('576212f2e0e1e', '576212a679427', '3A7775472G8389815', 'completed', '1164.60', 'gearplanetbuyer001@gmail.com', 'true', 'PoQkUdN0i1bBLnqSFOMRIGZ5yHr6CX', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '6HC45193JS462814T', 'completed', '2016-06-16 02:46:10', '2016-06-16 02:46:10'),
('576212f2e1b63', '576212a679427', '05U65300LK7023647', 'completed', '116.46', 'gearplanetbuyer002@gmail.com', 'false', 'PoQkUdN0i1bBLnqSFOMRIGZ5yHr6CX', 'service', '79SPH4LHB5HV6', '0.00', 'false', '8DN663422N410635N', 'completed', '2016-06-16 02:46:10', '2016-06-16 02:46:10'),
('57621401bd58f', '576213bf02619', '0DK605379B335750F', 'completed', '9000.00', 'gearplanetbuyer001@gmail.com', 'true', 'bgtG6ZsDPapB52UdYlu7NeTIXrEwLj', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '8AJ58806T19988107', 'completed', '2016-06-16 02:50:41', '2016-06-16 02:50:41'),
('57621401bdfc5', '576213bf02619', '3NF48737M0610220L', 'completed', '900.00', 'gearplanetbuyer002@gmail.com', 'false', 'bgtG6ZsDPapB52UdYlu7NeTIXrEwLj', 'service', '79SPH4LHB5HV6', '0.00', 'false', '29F38160E73151603', 'completed', '2016-06-16 02:50:41', '2016-06-16 02:50:41'),
('57621808664d6', '576217b5522c4', '1FU12452DT7974736', 'completed', '99.99', 'gearplanetbuyer001@gmail.com', 'true', 'aKyUvArLIO2n01SNCbq5wYh7WTotXl', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '2M130127PL953910R', 'completed', '2016-06-16 03:07:52', '2016-06-16 03:07:52'),
('5762180866d95', '576217b5522c4', '30R28880W5873394F', 'completed', '10.00', 'gearplanetbuyer002@gmail.com', 'false', 'aKyUvArLIO2n01SNCbq5wYh7WTotXl', 'service', '79SPH4LHB5HV6', '0.00', 'false', '8MJ90869UE095432E', 'completed', '2016-06-16 03:07:52', '2016-06-16 03:07:52'),
('57621904a62bc', '576218bec956d', '1LB40795V94721828', 'completed', '25.19', 'gearplanetbuyer001@gmail.com', 'true', '3M7UnlNVAvPDcjohuk8YG61zLKWq9g', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '9E738141JF840891P', 'completed', '2016-06-16 03:12:04', '2016-06-16 03:12:04'),
('57621904a6cca', '576218bec956d', '76J39933464551624', 'completed', '2.52', 'gearplanetbuyer002@gmail.com', 'false', '3M7UnlNVAvPDcjohuk8YG61zLKWq9g', 'service', '79SPH4LHB5HV6', '0.00', 'false', '4DS1804099251632V', 'completed', '2016-06-16 03:12:04', '2016-06-16 03:12:04'),
('576219cc32e4a', '5762199058886', '0RH70519WE4119033', 'completed', '2850.00', 'gearplanetbuyer001@gmail.com', 'true', '6XO3CJmuYRBnt7SH2TsoexANzIFvjd', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '9X145485EL5318714', 'completed', '2016-06-16 03:15:24', '2016-06-16 03:15:24'),
('576219cc336c7', '5762199058886', '292795415M675363A', 'completed', '285.00', 'gearplanetbuyer002@gmail.com', 'false', '6XO3CJmuYRBnt7SH2TsoexANzIFvjd', 'service', '79SPH4LHB5HV6', '0.00', 'false', '7UH01400SF288361C', 'completed', '2016-06-16 03:15:24', '2016-06-16 03:15:24'),
('57621d6a27343', '57621d30c573a', '82764786PA621493X', 'completed', '499.00', 'gearplanetbuyer001@gmail.com', 'true', '8i7oqh52zQcOJT4xIWFrZBbC3KH1am', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '88866020DD362690P', 'completed', '2016-06-16 03:30:50', '2016-06-16 03:30:50'),
('57621d6a29a5d', '57621d30c573a', '0VA34452BA449833R', 'completed', '49.90', 'gearplanetbuyer002@gmail.com', 'false', '8i7oqh52zQcOJT4xIWFrZBbC3KH1am', 'service', '79SPH4LHB5HV6', '0.00', 'false', '2MU19652XM603460E', 'completed', '2016-06-16 03:30:50', '2016-06-16 03:30:50'),
('57624636f1c9f', '5762460d464ed', '9H640518MM6984624', 'completed', '310.00', 'gearplanetbuyer001@gmail.com', 'true', 'r6m7hJHLCloXyb32psOg9EcQt0VfxF', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '5K525675KT9380648', 'completed', '2016-06-16 06:24:54', '2016-06-16 06:24:54'),
('57624636f34cb', '5762460d464ed', '3PK43586WL359422P', 'completed', '31.00', 'gearplanetbuyer002@gmail.com', 'false', 'r6m7hJHLCloXyb32psOg9EcQt0VfxF', 'service', '79SPH4LHB5HV6', '0.00', 'false', '9WG073861D819060E', 'completed', '2016-06-16 06:24:54', '2016-06-16 06:24:54'),
('576257388f63e', '576257136aafb', NULL, 'created', '56.00', 'gearplanetbuyer001@gmail.com', 'true', 'bSlC2tmwni3NHx8ZTVaoB5LKI1dP9r', 'service', 'UXLW2SJYF7SFG', NULL, 'false', NULL, NULL, '2016-06-16 07:37:28', '2016-06-16 07:37:28'),
('5762573891002', '576257136aafb', NULL, 'created', '5.60', 'gearplanetbuyer002@gmail.com', 'false', 'bSlC2tmwni3NHx8ZTVaoB5LKI1dP9r', 'service', '79SPH4LHB5HV6', NULL, 'false', NULL, NULL, '2016-06-16 07:37:28', '2016-06-16 07:37:28'),
('576281a33e058', '5762818c9559c', NULL, 'created', '895.00', 'gearplanetbuyer001@gmail.com', 'true', '27ZYd4FhRt8fOy9WeQSlzpHvTmqgGX', 'service', 'UXLW2SJYF7SFG', NULL, 'false', NULL, NULL, '2016-06-16 10:38:27', '2016-06-16 10:38:27'),
('576281a33f7e8', '5762818c9559c', NULL, 'created', '89.50', 'gearplanetbuyer002@gmail.com', 'false', '27ZYd4FhRt8fOy9WeQSlzpHvTmqgGX', 'service', '79SPH4LHB5HV6', NULL, 'false', NULL, NULL, '2016-06-16 10:38:27', '2016-06-16 10:38:27'),
('576358d9c60d2', '576358b062282', NULL, 'created', '2270.00', 'gearplanetbuyer001@gmail.com', 'true', 'ZiHpTAEg36NXP9o2BwxkD8R5SU0taK', 'service', 'UXLW2SJYF7SFG', NULL, 'false', NULL, NULL, '2016-06-17 01:56:41', '2016-06-17 01:56:41'),
('576358d9c7e0e', '576358b062282', NULL, 'created', '227.00', 'gearplanetbuyer002@gmail.com', 'false', 'ZiHpTAEg36NXP9o2BwxkD8R5SU0taK', 'service', '79SPH4LHB5HV6', NULL, 'false', NULL, NULL, '2016-06-17 01:56:41', '2016-06-17 01:56:41'),
('5763594cd644e', '576358fa98ad9', '7CB77444FG068734D', 'completed', '2700.00', 'gearplanetbuyer001@gmail.com', 'true', 'C2JzE8jKUqRi0vmeyugtb4swLBrAOD', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '3RJ048726V710611P', 'completed', '2016-06-17 01:58:36', '2016-06-17 01:58:36'),
('5763594cd7c9d', '576358fa98ad9', '8DM353881G004120K', 'completed', '270.00', 'gearplanetbuyer002@gmail.com', 'false', 'C2JzE8jKUqRi0vmeyugtb4swLBrAOD', 'service', '79SPH4LHB5HV6', '0.00', 'false', '7SF962430P3983428', 'completed', '2016-06-17 01:58:36', '2016-06-17 01:58:36'),
('577e50475164b', '577e5011a04ff', NULL, 'created', '549.00', 'gearplanetbuyer001@gmail.com', 'true', 'h54fx3XTUmR8GstpbIJzvua9M2rn6d', 'service', 'UXLW2SJYF7SFG', NULL, 'false', NULL, NULL, '2016-07-07 12:51:19', '2016-07-07 12:51:19'),
('577e504752e84', '577e5011a04ff', NULL, 'created', '54.90', 'gearplanetbuyer002@gmail.com', 'false', 'h54fx3XTUmR8GstpbIJzvua9M2rn6d', 'service', '79SPH4LHB5HV6', NULL, 'false', NULL, NULL, '2016-07-07 12:51:19', '2016-07-07 12:51:19'),
('57805cc047d59', '57805c8e8d882', NULL, 'created', '12300.00', 'gearplanetbuyer001@gmail.com', 'true', 'kxVpsEHGZrf0uXL7lyqYPObvtKmFia', 'service', 'UXLW2SJYF7SFG', NULL, 'false', NULL, NULL, '2016-07-09 02:09:04', '2016-07-09 02:09:04'),
('57805cc04963a', '57805c8e8d882', NULL, 'created', '1230.00', 'gearplanetbuyer002@gmail.com', 'false', 'kxVpsEHGZrf0uXL7lyqYPObvtKmFia', 'service', '79SPH4LHB5HV6', NULL, 'false', NULL, NULL, '2016-07-09 02:09:04', '2016-07-09 02:09:04'),
('57a975277c4be', '57a9728b525ec', '2W974966JU021243G', 'completed', '82.50', 'gearplanetbuyer001@gmail.com', 'true', 'rjJU09bMZgIfFwNnH5CTWQ4oSGcDku', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '0WR328707N861115M', 'completed', '2016-08-09 06:16:07', '2016-08-09 06:16:07'),
('57a975277dea0', '57a9728b525ec', '7GA97333GV260091D', 'completed', '2.89', 'gearplanetbuyer002@gmail.com', 'false', 'rjJU09bMZgIfFwNnH5CTWQ4oSGcDku', 'service', '79SPH4LHB5HV6', '0.00', 'false', '2FM43797KU3487128', 'completed', '2016-08-09 06:16:07', '2016-08-09 06:16:07'),
('57a9760a7bfaf', '57a9728b525ec', '2W974966JU021243G', 'completed', '82.50', 'gearplanetbuyer001@gmail.com', 'true', 'rjJU09bMZgIfFwNnH5CTWQ4oSGcDku', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '0WR328707N861115M', 'completed', '2016-08-09 06:19:54', '2016-08-09 06:19:54'),
('57a9760a7daf7', '57a9728b525ec', '7GA97333GV260091D', 'completed', '2.89', 'gearplanetbuyer002@gmail.com', 'false', 'rjJU09bMZgIfFwNnH5CTWQ4oSGcDku', 'service', '79SPH4LHB5HV6', '0.00', 'false', '2FM43797KU3487128', 'completed', '2016-08-09 06:19:54', '2016-08-09 06:19:54'),
('57a97880cfa2b', '57a9728b525ec', '2W974966JU021243G', 'completed', '82.50', 'gearplanetbuyer001@gmail.com', 'true', 'rjJU09bMZgIfFwNnH5CTWQ4oSGcDku', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '0WR328707N861115M', 'completed', '2016-08-09 06:30:24', '2016-08-09 06:30:24'),
('57a97880d1450', '57a9728b525ec', '7GA97333GV260091D', 'completed', '2.89', 'gearplanetbuyer002@gmail.com', 'false', 'rjJU09bMZgIfFwNnH5CTWQ4oSGcDku', 'service', '79SPH4LHB5HV6', '0.00', 'false', '2FM43797KU3487128', 'completed', '2016-08-09 06:30:24', '2016-08-09 06:30:24'),
('57a97fb28a7f1', '57a97e684ce27', '7PY578429X5818644', 'completed', '1399.99', 'gearplanetbuyer001@gmail.com', 'true', 'eElDd8JS6Vq9rOhIRtn5AzbamHwTvL', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '9AG88006BM133945J', 'completed', '2016-08-09 07:01:06', '2016-08-09 07:01:06'),
('57a97fb28c16f', '57a97e684ce27', '7LA67270NP354552L', 'completed', '49.00', 'gearplanetbuyer002@gmail.com', 'false', 'eElDd8JS6Vq9rOhIRtn5AzbamHwTvL', 'service', '79SPH4LHB5HV6', '0.00', 'false', '3Y9665245Y391535A', 'completed', '2016-08-09 07:01:06', '2016-08-09 07:01:06'),
('57b1a648b7cd9', '57b1a624b29f6', NULL, 'created', '1470.00', 'gearplanetbuyer001@gmail.com', 'true', '2aV4rjSYK6odcT8GRMpqDO59A7ykIb', 'service', 'UXLW2SJYF7SFG', NULL, 'false', NULL, NULL, '2016-08-15 11:23:52', '2016-08-15 11:23:52'),
('57b1a648b993a', '57b1a624b29f6', NULL, 'created', '51.45', 'gearplanetbuyer002@gmail.com', 'false', '2aV4rjSYK6odcT8GRMpqDO59A7ykIb', 'service', '79SPH4LHB5HV6', NULL, 'false', NULL, NULL, '2016-08-15 11:23:52', '2016-08-15 11:23:52'),
('57b1a90b43f4d', '57b1a8c0aa3dc', NULL, 'created', '1470.00', 'gearplanetbuyer001@gmail.com', 'true', 'TvCwxt1jYHMVkqUN9mDdz67WJ2IcS3', 'service', 'UXLW2SJYF7SFG', NULL, 'false', NULL, NULL, '2016-08-15 11:35:39', '2016-08-15 11:35:39'),
('57b1a90b457b0', '57b1a8c0aa3dc', NULL, 'created', '51.45', 'gearplanetbuyer002@gmail.com', 'false', 'TvCwxt1jYHMVkqUN9mDdz67WJ2IcS3', 'service', '79SPH4LHB5HV6', NULL, 'false', NULL, NULL, '2016-08-15 11:35:39', '2016-08-15 11:35:39'),
('57b1aeae4c65c', '57b1ae4aefd61', NULL, 'created', '789.00', 'gearplanetbuyer001@gmail.com', 'true', 'zNXcOaWgqtAMB0CGhs5LI71fD8HEn9', 'service', 'UXLW2SJYF7SFG', NULL, 'false', NULL, NULL, '2016-08-15 11:59:42', '2016-08-15 11:59:42'),
('57b1aeae4e0f4', '57b1ae4aefd61', NULL, 'created', '27.62', 'gearplanetbuyer002@gmail.com', 'false', 'zNXcOaWgqtAMB0CGhs5LI71fD8HEn9', 'service', '79SPH4LHB5HV6', NULL, 'false', NULL, NULL, '2016-08-15 11:59:42', '2016-08-15 11:59:42'),
('57b1b0b121236', '57b1b0979ec3d', NULL, 'created', '789.00', 'gearplanetbuyer001@gmail.com', 'true', 'mlMYTSGDkNhp2qjibatoCUKP3u89wX', 'service', 'UXLW2SJYF7SFG', NULL, 'false', NULL, NULL, '2016-08-15 12:08:17', '2016-08-15 12:08:17'),
('57b1b0b122d0a', '57b1b0979ec3d', NULL, 'created', '27.62', 'gearplanetbuyer002@gmail.com', 'false', 'mlMYTSGDkNhp2qjibatoCUKP3u89wX', 'service', '79SPH4LHB5HV6', NULL, 'false', NULL, NULL, '2016-08-15 12:08:17', '2016-08-15 12:08:17'),
('57b28f52a6db0', '57b28f035858f', '94H26438L9337715F', 'completed', '789.00', 'gearplanetbuyer001@gmail.com', 'true', 'jJ5yYxnzwgcte06kZ7RhrCSQGVBsN8', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '4WR14174UK643112J', 'completed', '2016-08-16 03:58:10', '2016-08-16 03:58:10'),
('57b28f52a8659', '57b28f035858f', '5MW06929YS9275113', 'completed', '27.62', 'gearplanetbuyer002@gmail.com', 'false', 'jJ5yYxnzwgcte06kZ7RhrCSQGVBsN8', 'service', '79SPH4LHB5HV6', '0.00', 'false', '9V533453BV7630521', 'completed', '2016-08-16 03:58:10', '2016-08-16 03:58:10'),
('57b59288bb18f', '57b5922fab30f', '5M599724NM045454V', 'completed', '869.99', 'gearplanetbuyer001@gmail.com', 'true', 'EDQsW6FPmehty8GXogfdTKVZuzbHJ4', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '1DN22988MF2623451', 'completed', '2016-08-18 10:48:40', '2016-08-18 10:48:40'),
('57b59288bcb69', '57b5922fab30f', '05A30009DJ007140F', 'completed', '30.45', 'gearplanetbuyer002@gmail.com', 'false', 'EDQsW6FPmehty8GXogfdTKVZuzbHJ4', 'service', '79SPH4LHB5HV6', '0.00', 'false', '8KV04038AV6080506', 'completed', '2016-08-18 10:48:40', '2016-08-18 10:48:40'),
('57b8290202e38', '57b8288c0727f', '90J162439F220705E', 'completed', '520.00', 'gearplanetbuyer001@gmail.com', 'true', 'NGwEoA2WfhkRy91rKLglJQdiTuC6Sv', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '8PT83591Y98087601', 'completed', '2016-08-20 09:55:14', '2016-08-20 09:55:14'),
('57b82902049a9', '57b8288c0727f', '9H177605LP305322D', 'completed', '18.20', 'gearplanetbuyer002@gmail.com', 'false', 'NGwEoA2WfhkRy91rKLglJQdiTuC6Sv', 'service', '79SPH4LHB5HV6', '0.00', 'false', '50B077330X2971801', 'completed', '2016-08-20 09:55:14', '2016-08-20 09:55:14'),
('57bc2894b81b9', '57bc2811c7aea', '27D64195WK649004H', 'completed', '1419.99', 'gearplanetbuyer001@gmail.com', 'true', '9n6o72KTWVGvyHhazCEOsYkXFx1g5q', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '5PN116334M817452X', 'completed', '2016-08-23 10:42:28', '2016-08-23 10:42:28'),
('57bc2894b9c55', '57bc2811c7aea', '3Y126038YK9658308', 'completed', '49.70', 'gearplanetbuyer002@gmail.com', 'false', '9n6o72KTWVGvyHhazCEOsYkXFx1g5q', 'service', '79SPH4LHB5HV6', '0.00', 'false', '5JN17097LB890852L', 'completed', '2016-08-23 10:42:28', '2016-08-23 10:42:28'),
('57c4277095f04', '57c4272c28507', '55M7341307310740K', 'completed', '78.24', 'gearplanetbuyer001@gmail.com', 'true', 'vnMFZiyq3VT2sfcxOmr0GE68el41RI', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '4XE07925GS210734K', 'completed', '2016-08-29 12:15:44', '2016-08-29 12:15:44'),
('57c4277097757', '57c4272c28507', '6MV18131PF729733W', 'completed', '2.74', 'gearplanetbuyer002@gmail.com', 'false', 'vnMFZiyq3VT2sfcxOmr0GE68el41RI', 'service', '79SPH4LHB5HV6', '0.00', 'false', '2UW75811H2122790H', 'completed', '2016-08-29 12:15:44', '2016-08-29 12:15:44'),
('57c5718a7b452', '57c5714d73ab9', '93X32070DR408911C', 'completed', '118.00', 'gearplanetbuyer001@gmail.com', 'true', 'wuDnsrGZ3FlQOWdfEmIBK6gajSXqL5', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '11W92319S30096150', 'completed', '2016-08-30 11:44:10', '2016-08-30 11:44:10'),
('57c5718a7cf8f', '57c5714d73ab9', '33585981KM232600F', 'completed', '4.13', 'gearplanetbuyer002@gmail.com', 'false', 'wuDnsrGZ3FlQOWdfEmIBK6gajSXqL5', 'service', '79SPH4LHB5HV6', '0.00', 'false', '6W8646486G931322F', 'completed', '2016-08-30 11:44:10', '2016-08-30 11:44:10'),
('57c7d28bf3b2f', '57c7d25df206f', '2RX215412S3236945', 'completed', '1.20', 'gearplanetbuyer001@gmail.com', 'true', '7OklwIxn2sfMeHc5J4UFaP9ZKuVADE', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '9WY98709KY526073D', 'completed', '2016-09-01 07:02:35', '2016-09-01 07:02:35'),
('57c7d28c01ab8', '57c7d25df206f', '9UB67631W20826424', 'completed', '0.04', 'gearplanetbuyer002@gmail.com', 'false', '7OklwIxn2sfMeHc5J4UFaP9ZKuVADE', 'service', '79SPH4LHB5HV6', '0.00', 'false', '43426445VM319742V', 'completed', '2016-09-01 07:02:36', '2016-09-01 07:02:36'),
('57c974b9b8359', '57c973b07113c', '1EW46894CF1859135', 'completed', '1515.00', 'gearplanetbuyer001@gmail.com', 'true', 'Gy0KZeOAs2UH43SRnrBQh8g6WwcPtN', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '7SH620233K623334D', 'completed', '2016-09-02 12:46:49', '2016-09-02 12:46:49'),
('57c974b9ba652', '57c973b07113c', '03G72221RK894430S', 'completed', '53.03', 'gearplanetbuyer002@gmail.com', 'false', 'Gy0KZeOAs2UH43SRnrBQh8g6WwcPtN', 'service', '79SPH4LHB5HV6', '0.00', 'false', '3T55112372520291S', 'completed', '2016-09-02 12:46:49', '2016-09-02 12:46:49'),
('57ce642945ef9', '57ce63e7118f2', '9RB70063P5688773E', 'completed', '1.20', 'gearplanetbuyer001@gmail.com', 'true', 'SVUQact0bJLdnG2hwZ75lDP39BvWmp', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '75863178RB495100T', 'completed', '2016-09-06 06:37:29', '2016-09-06 06:37:29'),
('57ce642947a05', '57ce63e7118f2', '1C310487TR231420C', 'completed', '0.04', 'gearplanetbuyer002@gmail.com', 'false', 'SVUQact0bJLdnG2hwZ75lDP39BvWmp', 'service', '79SPH4LHB5HV6', '0.00', 'false', '6A745147ED479535Y', 'completed', '2016-09-06 06:37:29', '2016-09-06 06:37:29'),
('57ce9bb8e7c94', '57ce9b7a0f65d', NULL, 'created', '1.20', 'gearplanetbuyer001@gmail.com', 'true', '23X6JFb4SjoOMDnwZYWVKeCpBr0syR', 'service', 'UXLW2SJYF7SFG', NULL, 'false', NULL, NULL, '2016-09-06 10:34:32', '2016-09-06 10:34:32'),
('57ce9bb8e9af7', '57ce9b7a0f65d', NULL, 'created', '0.04', 'gearplanetbuyer002@gmail.com', 'false', '23X6JFb4SjoOMDnwZYWVKeCpBr0syR', 'service', '79SPH4LHB5HV6', NULL, 'false', NULL, NULL, '2016-09-06 10:34:32', '2016-09-06 10:34:32'),
('57cfc3db46de2', '57cfc3ae2afff', '5RR80367EG485222V', 'completed', '1350.00', 'gearplanetbuyer001@gmail.com', 'true', 'cWhym9qDPJ7Btf4TI5ZaibMpSQ8jRF', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '13F48697RL5970339', 'completed', '2016-09-07 07:38:03', '2016-09-07 07:38:03'),
('57cfc3db48819', '57cfc3ae2afff', '5VJ47566C8776445U', 'completed', '47.25', 'gearplanetbuyer002@gmail.com', 'false', 'cWhym9qDPJ7Btf4TI5ZaibMpSQ8jRF', 'service', '79SPH4LHB5HV6', '0.00', 'false', '2C2856077G952363M', 'completed', '2016-09-07 07:38:03', '2016-09-07 07:38:03'),
('57cfc553c709a', '57cfc2bd28a06', NULL, 'created', '699.00', 'gearplanetbuyer001@gmail.com', 'true', 'jUo72z4WyvleER6Fs5rPXhtcIJBDT3', 'service', 'UXLW2SJYF7SFG', NULL, 'false', NULL, NULL, '2016-09-07 07:44:19', '2016-09-07 07:44:19'),
('57cfc553c89ca', '57cfc2bd28a06', NULL, 'created', '24.47', 'gearplanetbuyer002@gmail.com', 'false', 'jUo72z4WyvleER6Fs5rPXhtcIJBDT3', 'service', '79SPH4LHB5HV6', NULL, 'false', NULL, NULL, '2016-09-07 07:44:19', '2016-09-07 07:44:19'),
('57d1068d7e4a4', '57d104fd40555', NULL, 'created', '95.00', 'gearplanetbuyer001@gmail.com', 'true', 'CTQxduUNZWE8zRlskb43VMAvPXhiYt', 'service', 'UXLW2SJYF7SFG', NULL, 'false', NULL, NULL, '2016-09-08 06:34:53', '2016-09-08 06:34:53'),
('57d1068d7ff96', '57d104fd40555', NULL, 'created', '3.33', 'gearplanetbuyer002@gmail.com', 'false', 'CTQxduUNZWE8zRlskb43VMAvPXhiYt', 'service', '79SPH4LHB5HV6', NULL, 'false', NULL, NULL, '2016-09-08 06:34:53', '2016-09-08 06:34:53'),
('57d1084012294', '57d106c9d9e1d', '6BT26892NV096894Y', 'completed', '95.00', 'gearplanetbuyer001@gmail.com', 'true', 'GwzxyLn4D1JY9OBm07pgFIjZc8u6SK', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '1R246535C20275352', 'completed', '2016-09-08 06:42:08', '2016-09-08 06:42:08'),
('57d1084013b49', '57d106c9d9e1d', '4T6079307C835163T', 'completed', '3.33', 'gearplanetbuyer002@gmail.com', 'false', 'GwzxyLn4D1JY9OBm07pgFIjZc8u6SK', 'service', '79SPH4LHB5HV6', '0.00', 'false', '4DD855957X952632W', 'completed', '2016-09-08 06:42:08', '2016-09-08 06:42:08'),
('57d11043b33b0', '57d110098d5b8', '6LH95076B8481603M', 'completed', '500.00', 'gearplanetbuyer001@gmail.com', 'true', 'qKJRirDCljHt0BX8LwSYsW5ZVEIhOd', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '82Y451617G285660H', 'completed', '2016-09-08 07:16:19', '2016-09-08 07:16:19'),
('57d11043b4d1f', '57d110098d5b8', '4BR5307253693323L', 'completed', '17.50', 'gearplanetbuyer002@gmail.com', 'false', 'qKJRirDCljHt0BX8LwSYsW5ZVEIhOd', 'service', '79SPH4LHB5HV6', '0.00', 'false', '24P6528722087862T', 'completed', '2016-09-08 07:16:19', '2016-09-08 07:16:19'),
('57d8b85f3e6ca', '57d8b8434ce88', NULL, 'created', '800.00', 'gearplanetbuyer001@gmail.com', 'true', 'kSjOEBd1JNzP9gCl0Ga5UKtVyZr84m', 'service', 'UXLW2SJYF7SFG', NULL, 'false', NULL, NULL, '2016-09-14 02:39:27', '2016-09-14 02:39:27'),
('57d8b85f40026', '57d8b8434ce88', NULL, 'created', '28.00', 'gearplanetbuyer002@gmail.com', 'false', 'kSjOEBd1JNzP9gCl0Ga5UKtVyZr84m', 'service', '79SPH4LHB5HV6', NULL, 'false', NULL, NULL, '2016-09-14 02:39:27', '2016-09-14 02:39:27'),
('57d8bc071a366', '57d8ba02f2f3e', '4Y990239E0772332Y', 'completed', '800.00', 'gearplanetbuyer001@gmail.com', 'true', 'dFjoMAkV29ZvQcTXWxKwOs1puBq6g0', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '9PR41468WT9327106', 'completed', '2016-09-14 02:55:03', '2016-09-14 02:55:03'),
('57d8bc071c8da', '57d8ba02f2f3e', '03W292710G050622M', 'completed', '28.00', 'gearplanetbuyer002@gmail.com', 'false', 'dFjoMAkV29ZvQcTXWxKwOs1puBq6g0', 'service', '79SPH4LHB5HV6', '0.00', 'false', '8HV81815EU6490258', 'completed', '2016-09-14 02:55:03', '2016-09-14 02:55:03'),
('57db9d8c0ebdb', '57db9d3a93660', '1X0426135V067892D', 'completed', '500.00', 'gearplanetbuyer001@gmail.com', 'true', 'WJzfwSOBQan2NPXuymZHTIeYDq90sV', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '0MS26385UM8726826', 'completed', '2016-09-16 07:21:48', '2016-09-16 07:21:48'),
('57db9d8c10618', '57db9d3a93660', '6UR71502BT4067109', 'completed', '17.50', 'gearplanetbuyer002@gmail.com', 'false', 'WJzfwSOBQan2NPXuymZHTIeYDq90sV', 'service', '79SPH4LHB5HV6', '0.00', 'false', '6KX31654VB482622E', 'completed', '2016-09-16 07:21:48', '2016-09-16 07:21:48'),
('57dbaeb3a8ef4', '57dbae48e0257', '82651657NL663990N', 'completed', '500.00', 'gearplanetbuyer001@gmail.com', 'true', 'OIqznlu0eEpGxhNyr5vXwag974K3Zk', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '2XD34132SG2959348', 'completed', '2016-09-16 08:34:59', '2016-09-16 08:34:59'),
('57dbaeb3aaa0f', '57dbae48e0257', '879200637H0572744', 'completed', '17.50', 'gearplanetbuyer002@gmail.com', 'false', 'OIqznlu0eEpGxhNyr5vXwag974K3Zk', 'service', '79SPH4LHB5HV6', '0.00', 'false', '15C46431WB422211B', 'completed', '2016-09-16 08:34:59', '2016-09-16 08:34:59'),
('5809c685a9a6e', '5809c3b87cfe0', '47963100FW8944749', 'completed', '634.00', 'gearplanetbuyer001@gmail.com', 'true', 'lP3fk2BIHpA7K1XqaGZNTDeUQuhzSb', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '0B104600B9303674P', 'completed', '2016-10-21 07:40:53', '2016-10-21 07:40:53'),
('5809c685ab6d8', '5809c3b87cfe0', '7BP69933BD0499443', 'completed', '22.19', 'gearplanetbuyer002@gmail.com', 'false', 'lP3fk2BIHpA7K1XqaGZNTDeUQuhzSb', 'service', '79SPH4LHB5HV6', '0.00', 'false', '7H937702GR509460W', 'completed', '2016-10-21 07:40:53', '2016-10-21 07:40:53'),
('58105663bf5d0', '5758eae9ec739', NULL, NULL, '89.99', 'chard_1338406363_biz@sushidigital.com.au', 'true', 'MKwzmGXiS1kPEedfvOJ67cgRCq8trD', 'service', '3TEDYG6B48RMQ', NULL, 'false', NULL, NULL, '2016-10-26 07:08:19', '2016-10-26 07:08:19'),
('58105663c0f7c', '5758eae9ec739', NULL, NULL, '10.00', 'gearplanetbuyer002@gmail.com', 'false', 'MKwzmGXiS1kPEedfvOJ67cgRCq8trD', 'service', '79SPH4LHB5HV6', NULL, 'false', NULL, NULL, '2016-10-26 07:08:19', '2016-10-26 07:08:19'),
('5811c3acbfa44', '5811c37f546e3', '54J552831W014273F', 'completed', '619.00', 'gearplanetbuyer001@gmail.com', 'true', 'kN2VYnLFyp658So1lexbAiUfQ3sqPg', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '5KV192502B5380802', 'completed', '2016-10-27 09:06:52', '2016-10-27 09:06:52'),
('5811c3acc1220', '5811c37f546e3', '4W2423963B4679740', 'completed', '21.67', 'gearplanetbuyer002@gmail.com', 'false', 'kN2VYnLFyp658So1lexbAiUfQ3sqPg', 'service', '79SPH4LHB5HV6', '0.00', 'false', '70T13281BK270320W', 'completed', '2016-10-27 09:06:52', '2016-10-27 09:06:52'),
('5819a73499b87', '5819a7000f75b', '10118267LN305711F', 'completed', '1419.99', 'gearplanetbuyer001@gmail.com', 'true', 'oSh9Vyk70etiArIZpTPbclwj3UmnKW', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '27X15530BU0029805', 'completed', '2016-11-02 08:43:32', '2016-11-02 08:43:32'),
('5819a7349b9ae', '5819a7000f75b', '2ED13890DH869405C', 'completed', '49.70', 'gearplanetbuyer002@gmail.com', 'false', 'oSh9Vyk70etiArIZpTPbclwj3UmnKW', 'service', '79SPH4LHB5HV6', '0.00', 'false', '0P911778W3610134K', 'completed', '2016-11-02 08:43:32', '2016-11-02 08:43:32'),
('581ab11edc9fa', '581ab0d62fedc', '5D025365HY0541733', 'completed', '619.00', 'gearplanetbuyer001@gmail.com', 'true', '13H9olg2sTeSD7fLZVtpM05dXzKGqI', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '93922574C8580815M', 'completed', '2016-11-03 03:38:06', '2016-11-03 03:38:06'),
('581ab11ede1aa', '581ab0d62fedc', '00348636MX048860E', 'completed', '21.67', 'gearplanetbuyer002@gmail.com', 'false', '13H9olg2sTeSD7fLZVtpM05dXzKGqI', 'service', '79SPH4LHB5HV6', '0.00', 'false', '2SC30348RR918100S', 'completed', '2016-11-03 03:38:06', '2016-11-03 03:38:06'),
('581ab552e2b55', '581ab524877c5', '6PK340766M8602938', 'completed', '619.00', 'gearplanetbuyer001@gmail.com', 'true', 'WrLQ4DsAzRvJiEn587KFxP6kgVSCbN', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '3TB22462JK5056547', 'completed', '2016-11-03 03:56:02', '2016-11-03 03:56:02'),
('581ab552e4383', '581ab524877c5', '08D70453M0307945J', 'completed', '21.67', 'gearplanetbuyer002@gmail.com', 'false', 'WrLQ4DsAzRvJiEn587KFxP6kgVSCbN', 'service', '79SPH4LHB5HV6', '0.00', 'false', '7TU24393D5474484L', 'completed', '2016-11-03 03:56:02', '2016-11-03 03:56:02'),
('581ab5ddc6f38', '581ab524877c5', '6PK340766M8602938', 'completed', '619.00', 'gearplanetbuyer001@gmail.com', 'true', 'WrLQ4DsAzRvJiEn587KFxP6kgVSCbN', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '3TB22462JK5056547', 'completed', '2016-11-03 03:58:21', '2016-11-03 03:58:21'),
('581ab5ddc873b', '581ab524877c5', '08D70453M0307945J', 'completed', '21.67', 'gearplanetbuyer002@gmail.com', 'false', 'WrLQ4DsAzRvJiEn587KFxP6kgVSCbN', 'service', '79SPH4LHB5HV6', '0.00', 'false', '7TU24393D5474484L', 'completed', '2016-11-03 03:58:21', '2016-11-03 03:58:21'),
('581ab66b96b80', '581ab524877c5', '6PK340766M8602938', 'completed', '619.00', 'gearplanetbuyer001@gmail.com', 'true', 'WrLQ4DsAzRvJiEn587KFxP6kgVSCbN', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '3TB22462JK5056547', 'completed', '2016-11-03 04:00:43', '2016-11-03 04:00:43'),
('581ab66b98463', '581ab524877c5', '08D70453M0307945J', 'completed', '21.67', 'gearplanetbuyer002@gmail.com', 'false', 'WrLQ4DsAzRvJiEn587KFxP6kgVSCbN', 'service', '79SPH4LHB5HV6', '0.00', 'false', '7TU24393D5474484L', 'completed', '2016-11-03 04:00:43', '2016-11-03 04:00:43'),
('581ab68d2c380', '581ab524877c5', '6PK340766M8602938', 'completed', '619.00', 'gearplanetbuyer001@gmail.com', 'true', 'WrLQ4DsAzRvJiEn587KFxP6kgVSCbN', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '3TB22462JK5056547', 'completed', '2016-11-03 04:01:17', '2016-11-03 04:01:17'),
('581ab68d2dc4d', '581ab524877c5', '08D70453M0307945J', 'completed', '21.67', 'gearplanetbuyer002@gmail.com', 'false', 'WrLQ4DsAzRvJiEn587KFxP6kgVSCbN', 'service', '79SPH4LHB5HV6', '0.00', 'false', '7TU24393D5474484L', 'completed', '2016-11-03 04:01:17', '2016-11-03 04:01:17'),
('581ab6b3ef137', '581ab524877c5', '6PK340766M8602938', 'completed', '619.00', 'gearplanetbuyer001@gmail.com', 'true', 'WrLQ4DsAzRvJiEn587KFxP6kgVSCbN', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '3TB22462JK5056547', 'completed', '2016-11-03 04:01:55', '2016-11-03 04:01:55'),
('581ab6b3f0ca0', '581ab524877c5', '08D70453M0307945J', 'completed', '21.67', 'gearplanetbuyer002@gmail.com', 'false', 'WrLQ4DsAzRvJiEn587KFxP6kgVSCbN', 'service', '79SPH4LHB5HV6', '0.00', 'false', '7TU24393D5474484L', 'completed', '2016-11-03 04:01:55', '2016-11-03 04:01:55'),
('581ab8b63375f', '581ab883d3db6', '9R629473LP4210722', 'completed', '619.00', 'gearplanetbuyer001@gmail.com', 'true', 'W179F3VcEUKxlGDkJqd6tOfeyrA2NX', 'service', 'UXLW2SJYF7SFG', '0.00', 'false', '54W05493TC148574H', 'completed', '2016-11-03 04:10:30', '2016-11-03 04:10:30'),
('581ab8b635162', '581ab883d3db6', '26E22396VG1226603', 'completed', '21.67', 'gearplanetbuyer002@gmail.com', 'false', 'W179F3VcEUKxlGDkJqd6tOfeyrA2NX', 'service', '79SPH4LHB5HV6', '0.00', 'false', '8TR18874S89028245', 'completed', '2016-11-03 04:10:30', '2016-11-03 04:10:30'),
('58564faba11eb', '58564edc70345', NULL, 'created', '2820.00', 'gearplanetbuyer001@gmail.com', 'true', 'ZSha6v9uqgKxdctr4OQJUPmf3FTG0o', 'service', 'UXLW2SJYF7SFG', NULL, 'false', NULL, NULL, '2016-12-18 08:58:19', '2016-12-18 08:58:19'),
('58564faba2ebd', '58564edc70345', NULL, 'created', '98.70', 'gearplanetbuyer002@gmail.com', 'false', 'ZSha6v9uqgKxdctr4OQJUPmf3FTG0o', 'service', '79SPH4LHB5HV6', NULL, 'false', NULL, NULL, '2016-12-18 08:58:19', '2016-12-18 08:58:19');

-- --------------------------------------------------------

--
-- Table structure for table `gp_orders_products`
--

CREATE TABLE `gp_orders_products` (
  `order_product_id` varchar(13) NOT NULL,
  `order_id` varchar(13) NOT NULL,
  `product_id` varchar(13) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `shipping_cost` decimal(9,2) DEFAULT NULL,
  `qty` tinyint(3) NOT NULL,
  `commission` decimal(9,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gp_orders_shipping_info`
--

CREATE TABLE `gp_orders_shipping_info` (
  `order_shipping_id` varchar(13) NOT NULL,
  `order_id` varchar(13) DEFAULT NULL,
  `address_1` varchar(100) DEFAULT NULL,
  `address_2` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(15) DEFAULT NULL,
  `post_code` varchar(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gp_orders_shipping_info`
--

INSERT INTO `gp_orders_shipping_info` (`order_shipping_id`, `order_id`, `address_1`, `address_2`, `city`, `state`, `post_code`, `created_at`, `updated_at`) VALUES
('5757c07e71ebd', '5757c07e711bb', 'address 1', 'address 2', 'city', 'qld', '6532', '2016-06-08 06:51:42', '2016-06-08 06:51:42'),
('5757c0cb3a741', '5757c0cb398e9', 'address 1', 'address 2', 'city', 'qld', '6532', '2016-06-08 06:52:59', '2016-06-08 06:52:59'),
('5757dd33061b9', '5757dd330534e', 'Test St.', 'testing', 'Perth', 'wa', '6000', '2016-06-08 08:54:11', '2016-06-08 08:54:11'),
('5757ddac9b5ec', '5757ddac9a914', 'Test St.', 'testing', 'Perth', 'wa', '6000', '2016-06-08 08:56:12', '2016-06-08 08:56:12'),
('5757e21e041a9', '5757e21e03152', 'address 1', 'address 2', 'city', 'qld', '6532', '2016-06-08 09:15:10', '2016-06-08 09:15:10'),
('5758eac19616b', '5758eac1951c1', 'address 1', 'address 2', 'city', 'qld', '6532', '2016-06-09 04:04:17', '2016-06-09 04:04:17'),
('5758eae9ed670', '5758eae9ec739', 'Test St.', 'sushi testing', 'Perth', 'wa', '6000', '2016-06-09 04:04:57', '2016-06-09 04:04:57'),
('575900e12108c', '575900e1201d7', 'Test St.', 'sushi testing', 'Perth', 'wa', '6000', '2016-06-09 05:38:41', '2016-06-09 05:38:41'),
('5759020a290e0', '5759020a2829e', 'address 1', 'address 2', 'city', 'qld', '6532', '2016-06-09 05:43:38', '2016-06-09 05:43:38'),
('575902ed125b7', '575902ed10ea9', 'address 1', 'address 2', 'city', 'qld', '6532', '2016-06-09 05:47:25', '2016-06-09 05:47:25'),
('57590aa2c2080', '57590aa2c0f19', 'Test St.', 'sushi testing', 'Perth', 'wa', '6000', '2016-06-09 06:20:18', '2016-06-09 06:20:18'),
('57590b0c9a648', '57590b0c997bd', 'Test St.', 'sushi testing', 'Perth', 'wa', '6000', '2016-06-09 06:22:04', '2016-06-09 06:22:04'),
('57590bf9b0146', '57590bf9af099', 'Test St.', 'sushi testing', 'Perth', 'wa', '6000', '2016-06-09 06:26:01', '2016-06-09 06:26:01'),
('57590c6fbaecc', '57590c6fb9ce1', 'Test St.', 'sushi testing', 'Perth', 'wa', '6000', '2016-06-09 06:27:59', '2016-06-09 06:27:59'),
('57590f5d1c5a7', '57590f5d1a3c1', 'Test St.', 'sushi testing', 'Perth', 'wa', '6000', '2016-06-09 06:40:29', '2016-06-09 06:40:29'),
('57590fc90156a', '57590fc9008f5', 'Test St.', 'sushi testing', 'Perth', 'wa', '6000', '2016-06-09 06:42:17', '2016-06-09 06:42:17'),
('575a81c406c69', '575a81c405bb5', '85 Chatsworth Drive', '', 'CANNING MILLS', 'wa', '6111', '2016-06-10 09:00:52', '2016-06-10 09:00:52'),
('575e45e41e743', '575e45e41d72f', '', '', '', '', '', '2016-06-13 05:34:28', '2016-06-13 05:34:28'),
('575e53181fe2f', '575e53181ac10', 'address 1', 'address 2', 'city', 'qld', '6532', '2016-06-13 06:30:48', '2016-06-13 06:30:48'),
('575e5ebe36db8', '575e5ebe35e58', 'address 1', 'address 2', 'city', 'qld', '6532', '2016-06-13 07:20:30', '2016-06-13 07:20:30'),
('575e5ed09e7ac', '575e5ed09d93d', 'Test St.', 'sushi testing', 'Perth', 'wa', '6000', '2016-06-13 07:20:48', '2016-06-13 07:20:48'),
('575f8222ab11c', '575f8222a9e41', 'Test St.', 'sushi testing', 'Perth', 'wa', '6000', '2016-06-14 04:03:46', '2016-06-14 04:03:46'),
('575f82b674557', '575f82b6738f9', 'Test St.', 'sushi testing', 'Perth', 'wa', '6000', '2016-06-14 04:06:14', '2016-06-14 04:06:14'),
('576104df11064', '576104df0fb7f', 'Test St.', '', 'Perth', 'wa', '6000', '2016-06-15 07:33:51', '2016-06-15 07:33:51'),
('576112d438752', '576112d43761e', 'Test St.', '', 'Perth', 'wa', '6000', '2016-06-15 08:33:24', '2016-06-15 08:33:24'),
('576113195206d', '57611319513af', 'Test St.', '', 'Perth', 'wa', '6000', '2016-06-15 08:34:33', '2016-06-15 08:34:33'),
('5761212042c47', '57612120415c1', '17 Jacabina Court', '', 'RUSSELL VALE', 'nsw', '2517', '2016-06-15 09:34:24', '2016-06-15 09:34:24'),
('57612237762df', '57612237752d3', '59 Jacolite Street', '', 'MELALEUCA', 'wa', '6065', '2016-06-15 09:39:03', '2016-06-15 09:39:03'),
('576122ed72fb3', '576122ed72041', 'Test St.', '', 'Perth', 'wa', '6000', '2016-06-15 09:42:05', '2016-06-15 09:42:05'),
('576212a67a34e', '576212a679427', 'Test St.', '', 'Perth', 'wa', '6000', '2016-06-16 02:44:54', '2016-06-16 02:44:54'),
('576213bf034af', '576213bf02619', 'Test St.', '', 'Perth', 'wa', '6000', '2016-06-16 02:49:35', '2016-06-16 02:49:35'),
('576217b5536f1', '576217b5522c4', 'Test St.', '', 'Perth', 'wa', '6000', '2016-06-16 03:06:29', '2016-06-16 03:06:29'),
('576218becab97', '576218bec956d', '90 Chatsworth Drive', '', 'ASCOT', 'wa', '6104', '2016-06-16 03:10:54', '2016-06-16 03:10:54'),
('5762199059650', '5762199058886', '2 Foreshore Road', '', 'HAZELMERE', 'wa', '6055', '2016-06-16 03:14:24', '2016-06-16 03:14:24'),
('57621d30c6662', '57621d30c573a', '90 Chatsworth Drive', '', 'ASCOT', 'wa', '6104', '2016-06-16 03:29:52', '2016-06-16 03:29:52'),
('5762460d479ae', '5762460d464ed', 'Test St.', '', 'Perth', 'wa', '6000', '2016-06-16 06:24:13', '2016-06-16 06:24:13'),
('576257136c094', '576257136aafb', 'Test St.', '', 'Perth', 'wa', '6000', '2016-06-16 07:36:51', '2016-06-16 07:36:51'),
('5762818c96d8a', '5762818c9559c', 'Test St.', '', 'Perth', 'wa', '6000', '2016-06-16 10:38:04', '2016-06-16 10:38:04'),
('576358b06c74b', '576358b062282', 'Test St.', '', 'Perth', 'wa', '6000', '2016-06-17 01:56:00', '2016-06-17 01:56:00'),
('576358fa9a118', '576358fa98ad9', 'Test St.', '', 'Perth', 'wa', '6000', '2016-06-17 01:57:14', '2016-06-17 01:57:14'),
('5764bf87f2b85', '5764bf87e2470', 'address 1', 'address 2', 'city', 'qld', '6532', '2016-06-18 03:27:03', '2016-06-18 03:27:03'),
('577e5011a9afa', '577e5011a04ff', '762 Beaufort st', '', 'Mount Lawley', 'wa', '6050', '2016-07-07 12:50:25', '2016-07-07 12:50:25'),
('577e53230620a', '577e532304bf4', '762 Beaufort st', '', 'Mount Lawley', 'wa', '6050', '2016-07-07 13:03:31', '2016-07-07 13:03:31'),
('57805c8e9668b', '57805c8e8d882', '762 Beaufort st', '', 'Mount Lawley', 'wa', '6050', '2016-07-09 02:08:14', '2016-07-09 02:08:14'),
('57877584cbf82', '57877584c752d', '762 Beaufort st', '', 'Mount Lawley', 'wa', '6050', '2016-07-14 11:20:36', '2016-07-14 11:20:36'),
('57a96f2671ad5', '57a96f2668d8a', 'add 001', 'add 002', 'city', 'sa', '5487', '2016-08-09 05:50:30', '2016-08-09 05:50:30'),
('57a9728b53c1d', '57a9728b525ec', 'first add', 'second add', 'perth', 'sa', '1231', '2016-08-09 06:04:59', '2016-08-09 06:04:59'),
('57a97e684e43a', '57a97e684ce27', '92 Banksia Street', '', 'ZUYTDORP', 'wa', '6536', '2016-08-09 06:55:36', '2016-08-09 06:55:36'),
('57b1a5b0ab82e', '57b1a5b0a9462', '762 Beaufort st', '', 'Mount Lawley', 'wa', '6050', '2016-08-15 11:21:20', '2016-08-15 11:21:20'),
('57b1a624b400d', '57b1a624b29f6', '762 Beaufort st', '', 'Mount Lawley', 'wa', '6050', '2016-08-15 11:23:16', '2016-08-15 11:23:16'),
('57b1a86961e35', '57b1a869609a2', '762 Beaufort st', '', 'Mount Lawley', 'wa', '6050', '2016-08-15 11:32:57', '2016-08-15 11:32:57'),
('57b1a8c0ab850', '57b1a8c0aa3dc', '762 beaufort st', '', 'Mount lawley', 'wa', '6050', '2016-08-15 11:34:24', '2016-08-15 11:34:24'),
('57b1ae4af13ed', '57b1ae4aefd61', '762 Beaufort st', '', 'Mount Lawley', 'wa', '6050', '2016-08-15 11:58:02', '2016-08-15 11:58:02'),
('57b1b06241ec8', '57b1b062409e2', '762 Beaufort st', '', 'Mount Lawley', 'wa', '6050', '2016-08-15 12:06:58', '2016-08-15 12:06:58'),
('57b1b097a0220', '57b1b0979ec3d', '762 Beaufort st', '', 'Mount Lawley', 'wa', '6050', '2016-08-15 12:07:51', '2016-08-15 12:07:51'),
('57b28f035f0ba', '57b28f035858f', 'Sushi Testing', '', 'Perth', 'nsw', '6041', '2016-08-16 03:56:51', '2016-08-16 03:56:51'),
('57b5922fac8ff', '57b5922fab30f', '762 Beaufort', '', 'mount lawley', 'wa', '6050', '2016-08-18 10:47:11', '2016-08-18 10:47:11'),
('57b826f110acf', '57b826f1052a8', '762 Beaufort st', '', 'mount lawley', 'wa', '6050', '2016-08-20 09:46:25', '2016-08-20 09:46:25'),
('57b8288c089fd', '57b8288c0727f', '762 Beaufort st', '', 'mount lawley', 'wa', '6050', '2016-08-20 09:53:16', '2016-08-20 09:53:16'),
('57bc2811c90e6', '57bc2811c7aea', '1233 Horsea St', '', 'Perth', 'wa', '6041', '2016-08-23 10:40:17', '2016-08-23 10:40:17'),
('57c4272c324d6', '57c4272c28507', '762 Beaufort st', '', 'Mount Lawley', 'wa', '6050', '2016-08-29 12:14:36', '2016-08-29 12:14:36'),
('57c5714d801e0', '57c5714d73ab9', '762 Beaufort st', '', 'Mount Lawley', 'wa', '6050', '2016-08-30 11:43:09', '2016-08-30 11:43:09'),
('57c7d25df3757', '57c7d25df206f', 'Sushi Digital St.', '', 'Perth', 'wa', '6041', '2016-09-01 07:01:49', '2016-09-01 07:01:49'),
('57c973b072d4e', '57c973b07113c', '762 Beaufort St', '', 'Mt Lawley', 'wa', '6050', '2016-09-02 12:42:24', '2016-09-02 12:42:24'),
('57ce63e71343f', '57ce63e7118f2', 'Sushi St.', '', 'Perth', 'wa', '6041', '2016-09-06 06:36:23', '2016-09-06 06:36:23'),
('57ce9807e2fb7', '57ce9807e13f3', 'Hello st', '', 'Perth', 'wa', '6041', '2016-09-06 10:18:47', '2016-09-06 10:18:47'),
('57ce9b7a10ea1', '57ce9b7a0f65d', 'Hello St.', '', 'Perth', 'wa', '6041', '2016-09-06 10:33:30', '2016-09-06 10:33:30'),
('57cea1ed513fa', '57cea1ed4f9d2', 'Perth', '', 'Perth', 'wa', '6041', '2016-09-06 11:01:01', '2016-09-06 11:01:01'),
('57cfc2bd29fd7', '57cfc2bd28a06', 'test', '', 'Perth', 'nsw', '6000', '2016-09-07 07:33:17', '2016-09-07 07:33:17'),
('57cfc3ae2c58c', '57cfc3ae2afff', 'abc321', 'abc321', 'cc', 'qld', '12312', '2016-09-07 07:37:18', '2016-09-07 07:37:18'),
('57d104fd41bc9', '57d104fd40555', 'add 1', 'add 2', 'perth', 'nsw', '12312', '2016-09-08 06:28:13', '2016-09-08 06:28:13'),
('57d106c9db4a3', '57d106c9d9e1d', 'add1', 'add2', 'pertj', 'qld', '12312', '2016-09-08 06:35:53', '2016-09-08 06:35:53'),
('57d10daf56987', '57d10daf5537a', 'first address', 'second address', 'perth', 'nsw', '1244', '2016-09-08 07:05:19', '2016-09-08 07:05:19'),
('57d110098ee7d', '57d110098d5b8', 'address 1', 'address 2', 'perth', 'nsw', '3415', '2016-09-08 07:15:21', '2016-09-08 07:15:21'),
('57d8b52abfa43', '57d8b52abe41b', 'testing street1', 'testing street2', 'Sydney', 'nsw', '121212', '2016-09-14 02:25:46', '2016-09-14 02:25:46'),
('57d8b8434e600', '57d8b8434ce88', 'carolina street', 'carolina street', 'Sydney', 'nsw', '2148', '2016-09-14 02:38:59', '2016-09-14 02:38:59'),
('57d8b8cda7a29', '57d8b8cda615f', 'carolina street', 'carolina in my mind street', 'Sydney', 'nsw', '2148', '2016-09-14 02:41:17', '2016-09-14 02:41:17'),
('57d8ba030055d', '57d8ba02f2f3e', 'carol street', 'carolina street', 'Sydney', 'nsw', '2148', '2016-09-14 02:46:27', '2016-09-14 02:46:27'),
('57db9d3a94bd2', '57db9d3a93660', 'Address 1 centered head', 'Address 2 centered head', 'Sydney', 'qld', '4324234', '2016-09-16 07:20:26', '2016-09-16 07:20:26'),
('57dbae48e19bd', '57dbae48e0257', 'Address one and final', 'Address two and final', 'sydney', 'wa', '5646', '2016-09-16 08:33:12', '2016-09-16 08:33:12'),
('5809c3b87e963', '5809c3b87cfe0', 'test', 'test', 'test', 'wa', '12345', '2016-10-21 07:28:56', '2016-10-21 07:28:56'),
('5811c37f56066', '5811c37f546e3', 'Mango St', '', 'Perth', 'wa', '6041', '2016-10-27 09:06:07', '2016-10-27 09:06:07'),
('5819a70010ce9', '5819a7000f75b', '20 Seninis Road', '', 'RITE ISLAND', 'qld', '4807', '2016-11-02 08:42:40', '2016-11-02 08:42:40'),
('581ab0d631404', '581ab0d62fedc', '58 Moruya Road', '', 'GUNDILLION', 'nsw', '2622', '2016-11-03 03:36:54', '2016-11-03 03:36:54'),
('581ab52488ef6', '581ab524877c5', '58 Moruya Road', '', 'GUNDILLION', 'nsw', '2622', '2016-11-03 03:55:16', '2016-11-03 03:55:16'),
('581ab883d54ac', '581ab883d3db6', '58 Moruya Road', '', 'GUNDILLION', 'nsw', '2622', '2016-11-03 04:09:39', '2016-11-03 04:09:39'),
('58564edc717dd', '58564edc70345', '40 third ave', '', 'Mount hawthorn', 'wa', '6050', '2016-12-18 08:54:52', '2016-12-18 08:54:52'),
('598189261680b', '5981892611793', '351 Hay Street', '', 'Subiaco', 'wa', '6008', '2017-08-02 08:11:18', '2017-08-02 08:11:18'),
('5987cdb599d71', '5987cdb598411', '99 Hay Street', '', 'Subiaco', 'wa', '6008', '2017-08-07 02:17:25', '2017-08-07 02:17:25');

-- --------------------------------------------------------

--
-- Table structure for table `gp_orders_status`
--

CREATE TABLE `gp_orders_status` (
  `order_status_id` varchar(13) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gp_orders_status`
--

INSERT INTO `gp_orders_status` (`order_status_id`, `name`, `created_at`, `updated_at`) VALUES
('573e5b0202eb9', 'Pending Payment', '2016-05-20 08:33:55', NULL),
('573e5b0202ee6', 'Processing', '2016-05-20 08:33:55', NULL),
('573e5b0202eeb', 'On Hold', '2016-05-20 08:33:55', NULL),
('573e5b0202eef', 'Completed', '2016-05-20 08:33:55', NULL),
('573e5b0202ef3', 'Cancelled', '2016-05-20 08:33:55', NULL),
('573e5b0202ef9', 'Refunded', '2016-05-20 08:33:55', NULL),
('573e5b0202efc', 'Failed', '2016-05-20 08:33:55', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gp_password_resets`
--

CREATE TABLE `gp_password_resets` (
  `password_reset_id` varchar(13) NOT NULL,
  `email` varchar(255) NOT NULL,
  `token` varchar(40) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gp_password_resets`
--

INSERT INTO `gp_password_resets` (`password_reset_id`, `email`, `token`, `created_at`, `updated_at`) VALUES
('57982763df4da', 'elskid@gmail.com', 'vfn-42r8x31qpdztjnbukc6vgime07oa5lfs', '2016-07-27 03:15:47', '2016-07-27 03:15:47');

-- --------------------------------------------------------

--
-- Table structure for table `gp_products`
--

CREATE TABLE `gp_products` (
  `product_id` varchar(13) NOT NULL,
  `product_blurb` varchar(255) NOT NULL,
  `product_title` varchar(255) NOT NULL,
  `product_description` text,
  `product_price` decimal(9,2) NOT NULL,
  `product_sale_price` decimal(9,2) DEFAULT NULL,
  `shipping_cost` decimal(9,2) DEFAULT NULL,
  `shop_name` varchar(255) DEFAULT NULL,
  `product_primary_photo` varchar(13) DEFAULT NULL,
  `accept_offers` enum('yes','no') DEFAULT 'no',
  `paypal_email` varchar(255) NOT NULL,
  `shopping_policy` text,
  `status` enum('active','inactive','sold','cancelled') DEFAULT 'active',
  `created_by` varchar(13) NOT NULL,
  `sold_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `cancelled_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gp_products`
--

INSERT INTO `gp_products` (`product_id`, `product_blurb`, `product_title`, `product_description`, `product_price`, `product_sale_price`, `shipping_cost`, `shop_name`, `product_primary_photo`, `accept_offers`, `paypal_email`, `shopping_policy`, `status`, `created_by`, `sold_at`, `deleted_at`, `cancelled_at`, `created_at`, `updated_at`) VALUES
('5a6ed790a7a5d', 'werw-wew-we-wewe', 'werw wew we wewe', 'df', '34.00', '343.00', '232.00', NULL, '5a6ed790e2159', 'no', 'email@email.com', 'df', 'active', '5a5ed593321a5', NULL, NULL, NULL, '2018-01-29 00:13:04', '2018-01-29 00:13:04'),
('5a6fe1e751d3e', 'ere', 'ere', 'rer', '34.00', NULL, NULL, NULL, '5a6fe1e7870a6', 'no', 'sgsz@gmail.com', 'erer', 'active', '5a5ed593321a5', NULL, NULL, NULL, '2018-01-29 19:09:27', '2018-01-29 19:09:27'),
('5a6fe62de4a85', 'officespace', 'officespace', 'wewsds', '232.00', NULL, NULL, NULL, '5a6fe62e1fdfd', 'no', 'aaha@gmail.com', 'wew', 'active', '5a5ed593321a5', NULL, NULL, NULL, '2018-01-29 19:27:41', '2018-01-29 19:39:39');

-- --------------------------------------------------------

--
-- Table structure for table `gp_products_condition`
--

CREATE TABLE `gp_products_condition` (
  `condition_id` varchar(13) NOT NULL,
  `name` varchar(100) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gp_products_condition`
--

INSERT INTO `gp_products_condition` (`condition_id`, `name`, `deleted_at`, `created_at`, `updated_at`) VALUES
('56e65261b3799', 'Brand New', NULL, '2016-03-14 05:55:45', NULL),
('56e65261b3f8b', 'Mint', NULL, '2016-03-14 05:55:45', NULL),
('56e65261b3f97', 'Excellent', NULL, '2016-03-14 05:55:45', '2016-03-31 10:29:38'),
('56e65261b3fa2', 'Very Good', NULL, '2016-03-14 05:55:45', NULL),
('56e65261b3fa9', 'Good', NULL, '2016-03-14 05:55:45', NULL),
('56e65261b3fb1', 'Fair', NULL, '2016-03-14 05:55:45', NULL),
('56e65261b3fba', 'Poor', NULL, '2016-03-14 05:55:45', NULL),
('56e65261b3fc2', 'For Parts/Repair', NULL, '2016-03-14 05:55:45', '2016-08-09 03:15:10');

-- --------------------------------------------------------

--
-- Table structure for table `gp_products_photos`
--

CREATE TABLE `gp_products_photos` (
  `photo_id` varchar(13) NOT NULL,
  `product_id` varchar(13) NOT NULL,
  `photo_title` varchar(255) NOT NULL,
  `photo_filename` varchar(30) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gp_products_photos`
--

INSERT INTO `gp_products_photos` (`photo_id`, `product_id`, `photo_title`, `photo_filename`, `created_at`, `updated_at`) VALUES
('5757b5b0b27bb', '5757b5b0a3478', 'Ramirez Classical Guitar Estudio classical 1968 Natural spruc', 'NeEWuzZD6t1hjSd9b0Y2.jpg', '2016-06-08 06:05:36', '2016-06-08 06:05:36'),
('5757b5b0bc4a2', '5757b5b0a3478', 'Ramirez Classical Guitar Estudio classical 1968 Natural spruc-2', 'j4wVbJk3urovyNMgGaQS.jpg', '2016-06-08 06:05:36', '2016-06-08 06:05:36'),
('5757b5b0c9d85', '5757b5b0a3478', 'Ramirez Classical Guitar Estudio classical 1968 Natural spruc-3', 'nF4b3WiqhjsHlrA28euN.jpg', '2016-06-08 06:05:36', '2016-06-08 06:05:36'),
('5757b5b0d4972', '5757b5b0a3478', 'Ramirez Classical Guitar Estudio classical 1968 Natural spruc-4', '8z02pSCFXPxi6GRAHkuo.jpg', '2016-06-08 06:05:36', '2016-06-08 06:05:36'),
('5757b5b0dd449', '5757b5b0a3478', 'Ramirez Classical Guitar Estudio classical 1968 Natural spruc-5', 'BGFi4uXLrWyTpfQE9S1v.jpg', '2016-06-08 06:05:36', '2016-06-08 06:05:36'),
('5757bc41d010b', '5757bc41ae3bc', 'PRS Guitars Zach Meyers Signature', 'KrbcmByL2XxdPJWkGzI6.jpg', '2016-06-08 06:33:37', '2016-06-08 06:33:37'),
('5757bc41da6f5', '5757bc41ae3bc', 'PRS Guitars Zach Meyers Signature-2', '1Tps9ANFMJkRQyPDijvm.jpg', '2016-06-08 06:33:37', '2016-06-08 06:33:37'),
('5757bc41e5aa5', '5757bc41ae3bc', 'PRS Guitars Zach Meyers Signature-3', 'Xq2DPaAx401MiktNhfmp.jpg', '2016-06-08 06:33:37', '2016-06-08 06:33:37'),
('5757bc41ed49c', '5757bc41ae3bc', 'PRS Guitars Zach Meyers Signature-4', 'Jep1H6O3QsLCSFrztyio.jpg', '2016-06-08 06:33:37', '2016-06-08 06:33:37'),
('5757bc4201463', '5757bc41ae3bc', 'PRS Guitars Zach Meyers Signature-5', 'jch2wpOkqB56Ve8NRLCM.jpg', '2016-06-08 06:33:38', '2016-06-08 06:33:38'),
('5757bc420abf4', '5757bc41ae3bc', 'PRS Guitars Zach Meyers Signature-6', 'sv7IN5T6grXxhlFWqaoi.jpg', '2016-06-08 06:33:38', '2016-06-08 06:33:38'),
('5757bda533cc7', '5757bda523572', 'Fender Master Built 1963', 'gFmZUcNsqJfM6CDOw1id.jpg', '2016-06-08 06:39:33', '2016-06-08 06:39:33'),
('5757be5236627', '5757be522be2d', 'Yamaha Oak custom 12 tom 2014 Musashi Black yess absolute-1', 'HfORTocSgUvhJd1NylCI.jpg', '2016-06-08 06:42:26', '2016-06-08 06:42:26'),
('5757be5248b15', '5757be522be2d', 'Yamaha Oak custom 12 tom 2014 Musashi Black yess absolute-2', 'f8ajzPrH9oceKUCvNWF3.jpg', '2016-06-08 06:42:26', '2016-06-08 06:42:26'),
('5757be5251195', '5757be522be2d', 'Yamaha Oak custom 12 tom 2014 Musashi Black yess absolute-3', 'jOYLX76Cp4dP8UfMSBTZ.jpg', '2016-06-08 06:42:26', '2016-06-08 06:42:26'),
('5757bfc1634ea', '5757bfc15b77c', 'James Tyler Studio Elite HD -1', 'F6eJQO4Mf8jydoNhvcVZ.jpg', '2016-06-08 06:48:33', '2016-06-08 06:48:33'),
('5757bfc189260', '5757bfc15b77c', 'James Tyler Studio Elite HD -2', 'G1iPa5g2EUMtqwJdkLjz.jpg', '2016-06-08 06:48:33', '2016-06-08 06:48:33'),
('5757bfc193e49', '5757bfc15b77c', 'James Tyler Studio Elite HD -3', 'BwXErTcYSobp2ylzVsmC.jpg', '2016-06-08 06:48:33', '2016-06-08 06:48:33'),
('5757bfc19c8e7', '5757bfc15b77c', 'James Tyler Studio Elite HD -4', 'B4qgS8cwroOL2fzhHAml.jpg', '2016-06-08 06:48:33', '2016-06-08 06:48:33'),
('5757bfc1a47e8', '5757bfc15b77c', 'James Tyler Studio Elite HD -5', 'CfGY631corSNJxBU29Ku.jpg', '2016-06-08 06:48:33', '2016-06-08 06:48:33'),
('5757c05816e64', '5757c0580f2d3', 'Vintage Grey DOD 250 Overdrive Preamp 70s-1', '5iSFLsbgpyC10lWAGj4z.jpg', '2016-06-08 06:51:04', '2016-06-08 06:51:04'),
('5757c0581f0d0', '5757c0580f2d3', 'Vintage Grey DOD 250 Overdrive Preamp 70s-2', 'q9vGnsgydV7Z2pXFH85f.jpg', '2016-06-08 06:51:04', '2016-06-08 06:51:04'),
('5757c05826c29', '5757c0580f2d3', 'Vintage Grey DOD 250 Overdrive Preamp 70s-3', '0Xa5P4V13WfrgbymYkRF.jpg', '2016-06-08 06:51:04', '2016-06-08 06:51:04'),
('5757c0582e329', '5757c0580f2d3', 'Vintage Grey DOD 250 Overdrive Preamp 70s-4', 'cFMwsrUEHfazGe5v3oOR.jpg', '2016-06-08 06:51:04', '2016-06-08 06:51:04'),
('5757c05835bf2', '5757c0580f2d3', 'Vintage Grey DOD 250 Overdrive Preamp 70s-5', 'bhyIHgl0mzPDoKaQCU8j.jpg', '2016-06-08 06:51:04', '2016-06-08 06:51:04'),
('5757c2856e6d0', '5757c28562494', 'Vintage Ross Stereo analog delay 1978 Yellow original USA-1', 'KHwM1NGVUDCmhIjqvbcQ.jpg', '2016-06-08 07:00:21', '2016-06-08 07:00:21'),
('5757c28577768', '5757c28562494', 'Vintage Ross Stereo analog delay 1978 Yellow original USA-2', 'S4hP9EFLZTRnBHXz7rM8.jpg', '2016-06-08 07:00:21', '2016-06-08 07:00:21'),
('5757c2a0d87e3', '5757c2a0cf7b4', 'Experience Guitars Eleanor 5 2016 Buckeye Burl - 1', '9nre6YfuHxlKTyiXpBUD.jpg', '2016-06-08 07:00:48', '2016-06-08 07:00:48'),
('5757c2a0e148c', '5757c2a0cf7b4', 'Experience Guitars Eleanor 5 2016 Buckeye Burl - 2', 'Ceq7AmZ1oB5wGRNXndzx.jpg', '2016-06-08 07:00:48', '2016-06-08 07:00:48'),
('5757c2a0e9365', '5757c2a0cf7b4', 'Experience Guitars Eleanor 5 2016 Buckeye Burl - 3', 'L4mcAbMkVnfOWPzv06N3.jpg', '2016-06-08 07:00:48', '2016-06-08 07:00:48'),
('5757c2a0f086d', '5757c2a0cf7b4', 'Experience Guitars Eleanor 5 2016 Buckeye Burl - 4', '4gHz5lBX6ahsLiRqAbpc.jpg', '2016-06-08 07:00:48', '2016-06-08 07:00:48'),
('5757c2a104d22', '5757c2a0cf7b4', 'Experience Guitars Eleanor 5 2016 Buckeye Burl - 5', 'wmkns6MJbRUP7Z84gTta.jpg', '2016-06-08 07:00:49', '2016-06-08 07:00:49'),
('5757c4466108c', '5757c4465aa91', 'Zildjian 22 K Ride-1', '7wBgRQvIYeMWG1CZ6Juk.jpg', '2016-06-08 07:07:50', '2016-06-08 07:07:50'),
('5757c44669bef', '5757c4465aa91', 'Zildjian 22 K Ride-2', 'LMCsgpO6IwlXZaf9EmWd.jpg', '2016-06-08 07:07:50', '2016-06-08 07:07:50'),
('5757c494c5282', '5757c4949c432', 'electric guitars', 'YyvSHCRgqBFcW64Za9Ul.jpg', '2016-06-08 07:09:08', '2016-06-08 07:09:08'),
('5757c57b7abe8', '5757c57b5d975', 'Yamaha Oak custom 7 x 14 snare drum Silver Sparkle original MIJ-1', 'Kw3ybuBp1GMrvZ7YAdh0.jpg', '2016-06-08 07:12:59', '2016-06-08 07:12:59'),
('5757c57b8a216', '5757c57b5d975', 'Yamaha Oak custom 7 x 14 snare drum Silver Sparkle original MIJ-2', '0CsNZhoYXl3wi9LrWbqV.jpg', '2016-06-08 07:12:59', '2016-06-08 07:12:59'),
('5757c57b9485f', '5757c57b5d975', 'Yamaha Oak custom 7 x 14 snare drum Silver Sparkle original MIJ-3', 'VhdN4HGyBSLmnDz7obE8.jpg', '2016-06-08 07:12:59', '2016-06-08 07:12:59'),
('5757c57b9ea1a', '5757c57b5d975', 'Yamaha Oak custom 7 x 14 snare drum Silver Sparkle original MIJ-4', 'FxGus5TCMQqpi1roZl0E.jpg', '2016-06-08 07:12:59', '2016-06-08 07:12:59'),
('5757c57ba99e1', '5757c57b5d975', 'Yamaha Oak custom 7 x 14 snare drum Silver Sparkle original MIJ-5', 'LPEbwHxklMRpsJ1YQGhr.jpg', '2016-06-08 07:12:59', '2016-06-08 07:12:59'),
('5757c69b9eb5d', '5757c69b978d2', 'Racked PAIR Universal Audio or Urei LA-4 stereo compressor vintage Blackface limiting amplifier-1', 'iMYIAxwks6JKmEHOvG9X.jpg', '2016-06-08 07:17:47', '2016-06-08 07:17:47'),
('5757c715f2216', '5757c715e62a2', 'drum', 'nhT8DbEkf9HOpmgr2YqL.jpg', '2016-06-08 07:19:49', '2016-06-08 07:19:49'),
('5757c76b1c762', '5757c76b0fcf2', 'Grundig or Sennheiser GDSM-202 Vintage Stereo Dynamic Microphone 60\'s-1', 'iEDh0FVAKtxceQH37RaL.jpg', '2016-06-08 07:21:15', '2016-06-08 07:21:15'),
('5757c76b24ea7', '5757c76b0fcf2', 'Grundig or Sennheiser GDSM-202 Vintage Stereo Dynamic Microphone 60\'s-2', 'hKT3fxXE5pjeAlrIqSQt.jpg', '2016-06-08 07:21:15', '2016-06-08 07:21:15'),
('5757c8fb7a418', '5757c8fb7426a', '3rd Power Dream Solo 4 \'68 Plexi Black-1', 'VLorqpRzMg2mhw3HO4WX.jpg', '2016-06-08 07:27:55', '2016-06-08 07:27:55'),
('5757c8fb828b8', '5757c8fb7426a', '3rd Power Dream Solo 4 \'68 Plexi Black-2', 'D6MO7IbT3dLZVWwJhjsH.jpg', '2016-06-08 07:27:55', '2016-06-08 07:27:55'),
('5757c8fb8a77d', '5757c8fb7426a', '3rd Power Dream Solo 4 \'68 Plexi Black-3', 'Eq3sAHcBLnxtj0ou6e1O.jpg', '2016-06-08 07:27:55', '2016-06-08 07:27:55'),
('5757c8fb920de', '5757c8fb7426a', '3rd Power Dream Solo 4 \'68 Plexi Black-4', 'ibr7QnpwPDs58KVNB6t3.jpg', '2016-06-08 07:27:55', '2016-06-08 07:27:55'),
('5757c906b1eb7', '5757c906a945c', 'acoustic guitar', 'cNfGdm5uZ3RetjBM9SU4.jpg', '2016-06-08 07:28:06', '2016-06-08 07:28:06'),
('5757ca0ceff06', '5757ca0ce7b0e', 'Angel AX-25N2 Glockenspiel Blue (B)-1', 'HlDi9rvqCSpz0kGbPuIc.jpg', '2016-06-08 07:32:28', '2016-06-08 07:32:28'),
('5757ca0d05e8f', '5757ca0ce7b0e', 'Angel AX-25N2 Glockenspiel Blue (B)-2', 'h0bPtyHQOYzquBU2T8pW.jpg', '2016-06-08 07:32:29', '2016-06-08 07:32:29'),
('5757ca0d0e359', '5757ca0ce7b0e', 'Angel AX-25N2 Glockenspiel Blue (B)-3', 'zrJUm6EOygnScsNpaZBt.jpg', '2016-06-08 07:32:29', '2016-06-08 07:32:29'),
('5757ca0d15ea5', '5757ca0ce7b0e', 'Angel AX-25N2 Glockenspiel Blue (B)-4', 'tgoaTrcZHyu5C9Q03fKk.jpg', '2016-06-08 07:32:29', '2016-06-08 07:32:29'),
('5757ca0d1e918', '5757ca0ce7b0e', 'Angel AX-25N2 Glockenspiel Blue (B)-5', '1kdHL4xEiKSuJV85rGA2.jpg', '2016-06-08 07:32:29', '2016-06-08 07:32:29'),
('5757ca83c0327', '5757ca83b493d', 'Fender Vibrolux Reverb 1968 - 1', '61qrM0zNpcLF5SdJxlQn.jpg', '2016-06-08 07:34:27', '2016-06-08 07:34:27'),
('5757ca83c93e7', '5757ca83b493d', 'Fender Vibrolux Reverb 1968 - 2', 'plaxtITRPA97jniJeyzH.jpg', '2016-06-08 07:34:27', '2016-06-08 07:34:27'),
('5757ca83d2889', '5757ca83b493d', 'Fender Vibrolux Reverb 1968 - 3', 'jQZ7RdDiTP2erOptLMgW.jpg', '2016-06-08 07:34:27', '2016-06-08 07:34:27'),
('5757ca83dace5', '5757ca83b493d', 'Fender Vibrolux Reverb 1968 - 4', 'MRleBD8xb7TdGrI4S16m.jpg', '2016-06-08 07:34:27', '2016-06-08 07:34:27'),
('5757ca83e2df0', '5757ca83b493d', 'Fender Vibrolux Reverb 1968 - 5', 'jNWgCe8hzt2Mxd40mYuB.jpg', '2016-06-08 07:34:27', '2016-06-08 07:34:27'),
('5757cad8ce84e', '5757cad8b256c', 'Experience Guitars Eleanor 5 2016 Buckeye Burl-1', 'Y6QqD8JohwtSGyb7ZKdv.jpg', '2016-06-08 07:35:52', '2016-06-08 07:35:52'),
('5757cad8d89a3', '5757cad8b256c', 'Experience Guitars Eleanor 5 2016 Buckeye Burl-2', 'WEriqajXCRGOgywJNuz7.jpg', '2016-06-08 07:35:52', '2016-06-08 07:35:52'),
('5757cad8e1857', '5757cad8b256c', 'Experience Guitars Eleanor 5 2016 Buckeye Burl-3', 'Lk8U5hvNBMyQ6cfDPb0Y.jpg', '2016-06-08 07:35:52', '2016-06-08 07:35:52'),
('5757cad8e9790', '5757cad8b256c', 'Experience Guitars Eleanor 5 2016 Buckeye Burl-4', 'qWDi6c8khVsmPlOIZMjG.jpg', '2016-06-08 07:35:52', '2016-06-08 07:35:52'),
('5757cad8f23e6', '5757cad8b256c', 'Experience Guitars Eleanor 5 2016 Buckeye Burl-5', '8PBzdgcT5DLJwxRkjmsN.jpg', '2016-06-08 07:35:52', '2016-06-08 07:35:52'),
('5757cb903b6be', '5757cb901a907', 'proaudio', 'LIPdYT2GOS9i3DcFNsCp.jpg', '2016-06-08 07:38:56', '2016-06-08 07:38:56'),
('5757ccb1de7bc', '5757ccb1a4f93', 'Selmer AS500 Alto Saxophone Brass-1', 'cC4IUGmO9PsoMaugSziB.jpg', '2016-06-08 07:43:45', '2016-06-08 07:43:45'),
('5757ccb1e7c03', '5757ccb1a4f93', 'Selmer AS500 Alto Saxophone Brass-2', 'GDKbrZjip4nhTPSIH5Ow.jpg', '2016-06-08 07:43:45', '2016-06-08 07:43:45'),
('5757ccb1f1049', '5757ccb1a4f93', 'Selmer AS500 Alto Saxophone Brass-3', 'NAaLZEwP3yMYIFdcnzks.jpg', '2016-06-08 07:43:45', '2016-06-08 07:43:45'),
('5757ccb204f05', '5757ccb1a4f93', 'Selmer AS500 Alto Saxophone Brass-4', '7t5h8TqoYzUD3FjkPCX4.jpg', '2016-06-08 07:43:46', '2016-06-08 07:43:46'),
('5757ccb224e5d', '5757ccb1a4f93', 'Selmer AS500 Alto Saxophone Brass-5', 'IrxFkqs8D0XcdOQhAljH.jpg', '2016-06-08 07:43:46', '2016-06-08 07:43:46'),
('5757ccbdedd6c', '5757ccbddfb7d', 'amps', 'uQp2Cbwl3I0qvYgKdB7f.jpg', '2016-06-08 07:43:57', '2016-06-08 07:43:57'),
('5757cd317735a', '5757cd316f107', 'Fender1966 Bassman Blackface Vintage Electric Guitar Tube Amplifier Amp Head - 1', 'H6bB2rSKN7fYRDsk4TVp.jpg', '2016-06-08 07:45:53', '2016-06-08 07:45:53'),
('5757cd317ffea', '5757cd316f107', 'Fender1966 Bassman Blackface Vintage Electric Guitar Tube Amplifier Amp Head - 2', '1Zm85KNaRF7Ot6MUHPCp.jpg', '2016-06-08 07:45:53', '2016-06-08 07:45:53'),
('5757cd3186f0a', '5757cd316f107', 'Fender1966 Bassman Blackface Vintage Electric Guitar Tube Amplifier Amp Head - 3', '34QUpvnSs6OYa5hLwzRg.jpg', '2016-06-08 07:45:53', '2016-06-08 07:45:53'),
('5757cd318e128', '5757cd316f107', 'Fender1966 Bassman Blackface Vintage Electric Guitar Tube Amplifier Amp Head - 4', 'mwqy2PDFM3fYenrj5bEG.jpg', '2016-06-08 07:45:53', '2016-06-08 07:45:53'),
('5757cd3195991', '5757cd316f107', 'Fender1966 Bassman Blackface Vintage Electric Guitar Tube Amplifier Amp Head - 5', 'N8zyiun6XKJEdBp0rYUh.jpg', '2016-06-08 07:45:53', '2016-06-08 07:45:53'),
('5757cd8e59a3d', '5757cd8e518f5', 'Jose Ramirez 130 Años Spruce 2016', 'vYwLlkSynHVZRj89o04s.jpg', '2016-06-08 07:47:26', '2016-06-08 07:47:26'),
('5757ceb479b5b', '5757ceb469e5a', 'effect and pedals', 'LinocCH3MvuITrySYbUf.jpg', '2016-06-08 07:52:20', '2016-06-08 07:52:20'),
('5757cf0b5d0cd', '5757cf0b5171f', 'Taylor 814ce Limited 2010 Natural-12-Fret or Short-Scale-1', 'sLzGVdSFTB1g5toYORHk.jpg', '2016-06-08 07:53:47', '2016-06-08 07:53:47'),
('5757cf0b6582e', '5757cf0b5171f', 'Taylor 814ce Limited 2010 Natural-12-Fret or Short-Scale-2', '396PoxeMnG0LRtVuU7qC.jpg', '2016-06-08 07:53:47', '2016-06-08 07:53:47'),
('5757cf0b8e2d6', '5757cf0b5171f', 'Taylor 814ce Limited 2010 Natural-12-Fret or Short-Scale-3', '7Mgbfok05OC8Ev4NaHIL.jpg', '2016-06-08 07:53:47', '2016-06-08 07:53:47'),
('5757cf0b97311', '5757cf0b5171f', 'Taylor 814ce Limited 2010 Natural-12-Fret or Short-Scale-4', 'cNfMQeC5ubYx2vghIEsd.jpg', '2016-06-08 07:53:47', '2016-06-08 07:53:47'),
('5757cf0b9f7be', '5757cf0b5171f', 'Taylor 814ce Limited 2010 Natural-12-Fret or Short-Scale-5', 'kf8t5QV37dEMYLAir2xm.jpg', '2016-06-08 07:53:47', '2016-06-08 07:53:47'),
('5757cf25f13e8', '5757cf25e884b', 'HH Electronic V-S Musician Reverb Amplifier - 1', '0nf8K6bwkcJiGR5yOgZL.jpg', '2016-06-08 07:54:13', '2016-06-08 07:54:13'),
('5757cf2603d9c', '5757cf25e884b', 'HH Electronic V-S Musician Reverb Amplifier - 2', 'Kqks7G4tiyr3avHRmf5O.jpg', '2016-06-08 07:54:14', '2016-06-08 07:54:14'),
('5757cf260b0b3', '5757cf25e884b', 'HH Electronic V-S Musician Reverb Amplifier - 3', 'D54Gx6OkzFa10nqZlYiA.jpg', '2016-06-08 07:54:14', '2016-06-08 07:54:14'),
('5757cf261317d', '5757cf25e884b', 'HH Electronic V-S Musician Reverb Amplifier - 4', 'Oi2pwdyVJxBbrjC8g70L.jpg', '2016-06-08 07:54:14', '2016-06-08 07:54:14'),
('5757d04d1f60a', '5757d04d133a2', 'Folk Instruments', 'niaIqE1SZKO78mCAof3h.jpg', '2016-06-08 07:59:09', '2016-06-08 07:59:09'),
('5757d0a32cbe1', '5757d0a322708', 'Native Instruments Maschine MK2 Groove Production Studio, Black Native Instruments Maschine MK2 Groo-1', '8l1pbECjAxftRrwGa9uQ.jpg', '2016-06-08 08:00:35', '2016-06-08 08:00:35'),
('5757d0a3346ea', '5757d0a322708', 'Native Instruments Maschine MK2 Groove Production Studio, Black Native Instruments Maschine MK2 Groo-2', 'lTPL9tnq6kVEeA7vQjwM.jpg', '2016-06-08 08:00:35', '2016-06-08 08:00:35'),
('5757d0a33c7d5', '5757d0a322708', 'Native Instruments Maschine MK2 Groove Production Studio, Black Native Instruments Maschine MK2 Groo-3', 'NkxaqO0Zp9r1BR2o7clJ.jpg', '2016-06-08 08:00:35', '2016-06-08 08:00:35'),
('5757d1a57ab5e', '5757d1a57267b', 'Pioneer DJM-500 2002 Black-1', 'tTN8BXesl1ERAMUqcd7u.jpg', '2016-06-08 08:04:53', '2016-06-08 08:04:53'),
('5757d1a593457', '5757d1a57267b', 'Pioneer DJM-500 2002 Black-2', 'rq6hkl29yBzoDdxsHJON.jpg', '2016-06-08 08:04:53', '2016-06-08 08:04:53'),
('5757d1a59bd41', '5757d1a57267b', 'Pioneer DJM-500 2002 Black-3', 'S1OqZi3jIxrXyWF0tGuN.jpg', '2016-06-08 08:04:53', '2016-06-08 08:04:53'),
('5757d1a5cfd18', '5757d1a57267b', 'Pioneer DJM-500 2002 Black-4', 'KTJEWQvhxs9zIHyijq3u.jpg', '2016-06-08 08:04:53', '2016-06-08 08:04:53'),
('5757d1a5d8424', '5757d1a57267b', 'Pioneer DJM-500 2002 Black-5', '4KfnqJlR9VgDINvdyEXU.jpg', '2016-06-08 08:04:53', '2016-06-08 08:04:53'),
('5757daf9853f1', '5757daf9705c1', 'Keyboards and Synths', '3trIDjA4UnBva7WkTN1m.jpg', '2016-06-08 08:44:41', '2016-06-08 08:44:41'),
('5757db494bb40', '5757db493aaa9', 'Becker 2000A Symphony Series 14 Viola Outfit with Case and Bow', 'ZA3OzDJaVv089xXu6pk2.jpg', '2016-06-08 08:46:01', '2016-06-08 08:46:01'),
('5757dc6b26453', '5757dc6b1cc1d', 'Band and Orchestra', 'aeKy2RspSYVfFXwGL3D4.jpg', '2016-06-08 08:50:51', '2016-06-08 08:50:51'),
('5757dc8034c8a', '5757dc802f480', 'JOYO JMT-03 Clip On Tuner and Metronome with Colour Display', 'tKZnz27I6uwUg0RyDFqA.jpg', '2016-06-08 08:51:12', '2016-06-08 08:51:12'),
('5757ddc6b99af', '5757ddc6b27e3', 'Used 1991 Mancuso F Model Electric Mandolin-1', '9bAjZE8sIQwXvdNuKVzY.jpg', '2016-06-08 08:56:38', '2016-06-08 08:56:38'),
('5757ddc6c1e34', '5757ddc6b27e3', 'Used 1991 Mancuso F Model Electric Mandolin-2', 'u0Pezg45BkLjs7rxEOwQ.jpg', '2016-06-08 08:56:38', '2016-06-08 08:56:38'),
('5757ddc6c9f9a', '5757ddc6b27e3', 'Used 1991 Mancuso F Model Electric Mandolin-3', 'MkewLH6qZfaU54bg2h8G.jpg', '2016-06-08 08:56:38', '2016-06-08 08:56:38'),
('5757ddc6d1f3e', '5757ddc6b27e3', 'Used 1991 Mancuso F Model Electric Mandolin-4', 'sCqodjVKcOGHAZUkvuJ2.jpg', '2016-06-08 08:56:38', '2016-06-08 08:56:38'),
('5757ddc6da02e', '5757ddc6b27e3', 'Used 1991 Mancuso F Model Electric Mandolin-5', 'bYnyKz9XOR14WoZmFxAv.jpg', '2016-06-08 08:56:38', '2016-06-08 08:56:38'),
('5757ddfe41bac', '5757ddfe391dd', 'Home Audio', 'IuBgPkaVQFJtfs4L1rRc.jpg', '2016-06-08 08:57:34', '2016-06-08 08:57:34'),
('5757df1eae270', '5757df1ea5ddd', 'Royal Hawaiian Soprano koa ukulele 1930s Project in need of full restoration-1', '4O2N1p3JR6s987uh0YaV.jpg', '2016-06-08 09:02:22', '2016-06-08 09:02:22'),
('5757df1eb83f0', '5757df1ea5ddd', 'Royal Hawaiian Soprano koa ukulele 1930s Project in need of full restoration-2', 'ruz6lPLeGXiq8S4h0bE7.jpg', '2016-06-08 09:02:22', '2016-06-08 09:02:22'),
('5757df1ec0178', '5757df1ea5ddd', 'Royal Hawaiian Soprano koa ukulele 1930s Project in need of full restoration-3', 'gUi9PqMyRKEBAxf1tDVO.jpg', '2016-06-08 09:02:22', '2016-06-08 09:02:22'),
('5757df1ec878e', '5757df1ea5ddd', 'Royal Hawaiian Soprano koa ukulele 1930s Project in need of full restoration-4', 'OroZvT7s8uf9WxJCAEhi.jpg', '2016-06-08 09:02:22', '2016-06-08 09:02:22'),
('5757df1ed0331', '5757df1ea5ddd', 'Royal Hawaiian Soprano koa ukulele 1930s Project in need of full restoration-5', '4Lz2Uns6ydFBRkfMg1tN.jpg', '2016-06-08 09:02:22', '2016-06-08 09:02:22'),
('5757e08c658d0', '5757e08c5348a', 'Regal Tiple 1930s to 1950s-1', 'fMk7iSgaphoKRmOy438r.jpg', '2016-06-08 09:08:28', '2016-06-08 09:08:28'),
('5757e08c71033', '5757e08c5348a', 'Regal Tiple 1930s to 1950s-2', 'odXnIV49zGmH60vQLFlO.jpg', '2016-06-08 09:08:28', '2016-06-08 09:08:28'),
('5757e08c79707', '5757e08c5348a', 'Regal Tiple 1930s to 1950s-3', 'KdOJ0YTifqAoRCbHBLNx.jpg', '2016-06-08 09:08:28', '2016-06-08 09:08:28'),
('5757e08c8176a', '5757e08c5348a', 'Regal Tiple 1930s to 1950s-4', 'cgFMk6GJ29uzpa1ehqB4.jpg', '2016-06-08 09:08:28', '2016-06-08 09:08:28'),
('5757e08c8985c', '5757e08c5348a', 'Regal Tiple 1930s to 1950s-5', 'Re894gwquoU71rxBFpbZ.jpg', '2016-06-08 09:08:28', '2016-06-08 09:08:28'),
('5757e1ec6cb2a', '5757e1ec63fb8', 'THE FEATHER MANDRIOLA MANDOLIN PICKUP with FLEXIBLE MICRO-GOOSE NECK by Myers Pickups-1', 'lH0NngLBA9fcoWCPapsk.jpg', '2016-06-08 09:14:20', '2016-06-08 09:14:20'),
('5757e1ec75b0f', '5757e1ec63fb8', 'THE FEATHER MANDRIOLA MANDOLIN PICKUP with FLEXIBLE MICRO-GOOSE NECK by Myers Pickups-2', 'or3hYcF1EMxv7OPfnjIU.jpg', '2016-06-08 09:14:20', '2016-06-08 09:14:20'),
('5757e1ec7cecd', '5757e1ec63fb8', 'THE FEATHER MANDRIOLA MANDOLIN PICKUP with FLEXIBLE MICRO-GOOSE NECK by Myers Pickups-3', 'azNe2kgDbSYOGtTi1quR.jpg', '2016-06-08 09:14:20', '2016-06-08 09:14:20'),
('5757e1ec83cf2', '5757e1ec63fb8', 'THE FEATHER MANDRIOLA MANDOLIN PICKUP with FLEXIBLE MICRO-GOOSE NECK by Myers Pickups-4', 'pBdMjRyPCrgvwoNbIa4m.jpg', '2016-06-08 09:14:20', '2016-06-08 09:14:20'),
('5757e1ec8afad', '5757e1ec63fb8', 'THE FEATHER MANDRIOLA MANDOLIN PICKUP with FLEXIBLE MICRO-GOOSE NECK by Myers Pickups-5', 'pE7ZMOFxDd2PWCq0SnGy.jpg', '2016-06-08 09:14:20', '2016-06-08 09:14:20'),
('5757e3b053ab2', '5757e3b04e109', 'Gibson J-45 1962 Cherry Sunburst-1', '9jegUslc0p2XaTJwYzZA.jpg', '2016-06-08 09:21:52', '2016-06-08 09:21:52'),
('5757e3b05d47e', '5757e3b04e109', 'Gibson J-45 1962 Cherry Sunburst-2', 'J62TnBiKVCyOx4cg871D.jpg', '2016-06-08 09:21:52', '2016-06-08 09:21:52'),
('5757e3b065118', '5757e3b04e109', 'Gibson J-45 1962 Cherry Sunburst-3', 'M2FYWK8qxQTbhoZpgUOt.jpg', '2016-06-08 09:21:52', '2016-06-08 09:21:52'),
('5757e3b06ca38', '5757e3b04e109', 'Gibson J-45 1962 Cherry Sunburst-4', 'qlfNUREVrHMxGBSePF19.jpg', '2016-06-08 09:21:52', '2016-06-08 09:21:52'),
('5757e3b073c63', '5757e3b04e109', 'Gibson J-45 1962 Cherry Sunburst-5', '9TCBP8UMYHqv07urmIKD.jpg', '2016-06-08 09:21:52', '2016-06-08 09:21:52'),
('5757e50fe4f1a', '5757e50fde599', '80\'s Sonor Phonic Black Drums 3-piece-1', 'vP827sriCWbd9D1AJMhH.jpg', '2016-06-08 09:27:43', '2016-06-08 09:27:43'),
('5757e50fecc22', '5757e50fde599', '80\'s Sonor Phonic Black Drums 3-piece-2', 'Ccnu2ygoxbljdHSZqX1U.jpg', '2016-06-08 09:27:43', '2016-06-08 09:27:43'),
('5757e51000cbb', '5757e50fde599', '80\'s Sonor Phonic Black Drums 3-piece-3', 'mTgxeL8lyGoZ21aDFSAp.jpg', '2016-06-08 09:27:44', '2016-06-08 09:27:44'),
('5757e510083d1', '5757e50fde599', '80\'s Sonor Phonic Black Drums 3-piece-4', 'Mo9anP5TDgeGAvZEIYFz.jpg', '2016-06-08 09:27:44', '2016-06-08 09:27:44'),
('5757e5101094b', '5757e50fde599', '80\'s Sonor Phonic Black Drums 3-piece-5', '6ZGYu0DHVLBefxbErdyU.jpg', '2016-06-08 09:27:44', '2016-06-08 09:27:44'),
('5757e637a0197', '5757e63794d19', 'Vintage 1977 B&B EQF-1 Parametric Equalizer 500 Series Module Early-1', 'VgiW8CyZq0dSmBjzhUDE.jpg', '2016-06-08 09:32:39', '2016-06-08 09:32:39'),
('5757e637a8a44', '5757e63794d19', 'Vintage 1977 B&B EQF-1 Parametric Equalizer 500 Series Module Early-2', 'BU0vaVZtJ1NgjMAhf9G5.jpg', '2016-06-08 09:32:39', '2016-06-08 09:32:39'),
('5757e637b1134', '5757e63794d19', 'Vintage 1977 B&B EQF-1 Parametric Equalizer 500 Series Module Early-3', 'SIAn61xbfc3LDFoYrtmU.jpg', '2016-06-08 09:32:39', '2016-06-08 09:32:39'),
('5757ff95d7165', '5757ff95c2c54', 'New Platinum Samples Accent Ocean Way Drums', 'FIw4uZgWY9Dz7x6LrMiG.jpg', '2016-06-08 11:20:53', '2016-06-08 11:20:53'),
('575802b4133a5', '575802b3b8829', 'New BAE 8CM 8-Channel Summing Mixer 10-Series Rack Chassis', '8rZk27wSmdctabI5xv4P.jpg', '2016-06-08 11:34:12', '2016-06-08 11:34:12'),
('5758046cc283d', '5758046cb97f5', 'Grundig-Sennheiser GDSM-202-1', '2b5AO7cY0rzRmQxUK1SW.jpg', '2016-06-08 11:41:32', '2016-06-08 11:41:32'),
('5758046ccb7a9', '5758046cb97f5', 'Grundig-Sennheiser GDSM-202-2', 'vQewFZscp2Ri9t3YxVDB.jpg', '2016-06-08 11:41:32', '2016-06-08 11:41:32'),
('575805fe8e78a', '575805fe82044', 'Vintage 1977 B&B EQF-1 Parametric Equalizer 500 Series Module Early', 'iaSCTxvzqbtL0d3DjBO8.jpg', '2016-06-08 11:48:14', '2016-06-08 11:48:14'),
('575805fe9d586', '575805fe82044', 'Vintage 1977 B&B EQF-1 Parametric Equalizer 500 Series Module Early-2', 'fm5LEJQnZ1OU9G6bKcRV.jpg', '2016-06-08 11:48:14', '2016-06-08 11:48:14'),
('575805feac0b0', '575805fe82044', 'Vintage 1977 B&B EQF-1 Parametric Equalizer 500 Series Module Early-3', '50vFkcC9hNExejGmZPgr.jpg', '2016-06-08 11:48:14', '2016-06-08 11:48:14'),
('575807dc7654c', '575807dc11832', 'B-52 ACTPRO-1515 HD-1', 'y0rTLUbRpqD5GvMIBnQe.jpg', '2016-06-08 11:56:12', '2016-06-08 11:56:12'),
('575807dc82c85', '575807dc11832', 'B-52 ACTPRO-1515 HD-2', 'I4ZBAOGhXRzK7P8bW1dE.jpg', '2016-06-08 11:56:12', '2016-06-08 11:56:12'),
('575807dc8b315', '575807dc11832', 'B-52 ACTPRO-1515 HD-3', 'pb90qdFY8mkuSHiVnlAL.jpg', '2016-06-08 11:56:12', '2016-06-08 11:56:12'),
('5758bd70a9f42', '5758bd7085fa4', 'Behringer Xenyx 1002B 2015 Charcoal Gray-1', 'FU8zrw2C5KtRfhOc9gjN.jpg', '2016-06-09 00:50:56', '2016-06-09 00:50:56'),
('5758bd70b43f9', '5758bd7085fa4', 'Behringer Xenyx 1002B 2015 Charcoal Gray-2', 'UKSeoAr39lDRt8ZQdviP.jpg', '2016-06-09 00:50:56', '2016-06-09 00:50:56'),
('5758bd70bd081', '5758bd7085fa4', 'Behringer Xenyx 1002B 2015 Charcoal Gray-3', 'NtdW46QqXFeg0blAxjU5.jpg', '2016-06-09 00:50:56', '2016-06-09 00:50:56'),
('5758bd70c4f13', '5758bd7085fa4', 'Behringer Xenyx 1002B 2015 Charcoal Gray-4', 'QzpFcOxyhog2XHGdJ14i.jpg', '2016-06-09 00:50:56', '2016-06-09 00:50:56'),
('5758bea4455bf', '5758bea43f4c5', 'PA System Mackie SA1530z Active High-Definition Professional Speaker-1', 'uenEFsAajDZzvtG21fbg.jpg', '2016-06-09 00:56:04', '2016-06-09 00:56:04'),
('5758bea44f492', '5758bea43f4c5', 'PA System Mackie SA1530z Active High-Definition Professional Speaker-2', 'v0HeWfpFsKLR6COT3xa4.jpg', '2016-06-09 00:56:04', '2016-06-09 00:56:04'),
('5758bea456da4', '5758bea43f4c5', 'PA System Mackie SA1530z Active High-Definition Professional Speaker-3', 'CqwaTLIuFd72c3MnKPez.jpg', '2016-06-09 00:56:04', '2016-06-09 00:56:04'),
('5758bea465810', '5758bea43f4c5', 'PA System Mackie SA1530z Active High-Definition Professional Speaker-4', 'Z4NOyHVU3edra2Qim1kl.jpg', '2016-06-09 00:56:04', '2016-06-09 00:56:04'),
('5758bea46d128', '5758bea43f4c5', 'PA System Mackie SA1530z Active High-Definition Professional Speaker-5', 'sFvY3aVP9WkxNjUCwgc4.jpg', '2016-06-09 00:56:04', '2016-06-09 00:56:04'),
('5758c093d6b30', '5758c093c6e68', 'rivera los lobottom sub 2 2005 jbl speakers-1', 'nt4xMVgW9aIqZFwSEdlC.jpg', '2016-06-09 01:04:19', '2016-06-09 01:04:19'),
('5758c093e16b5', '5758c093c6e68', 'rivera los lobottom sub 2 2005 jbl speakers-2', 'UwaEqr5HzQL0kJsOFXV2.jpg', '2016-06-09 01:04:19', '2016-06-09 01:04:19'),
('5758c093eb0c9', '5758c093c6e68', 'rivera los lobottom sub 2 2005 jbl speakers-3', 'IC92RSGwZvHXeucErgdW.jpg', '2016-06-09 01:04:19', '2016-06-09 01:04:19'),
('5758c093f319c', '5758c093c6e68', 'rivera los lobottom sub 2 2005 jbl speakers-4', '4a7yNfP3Mr8xVRLhGeXT.jpg', '2016-06-09 01:04:19', '2016-06-09 01:04:19'),
('5758c09406ed3', '5758c093c6e68', 'rivera los lobottom sub 2 2005 jbl speakers-5', 'CTRdXGS5p23L6lP4DJvn.jpg', '2016-06-09 01:04:20', '2016-06-09 01:04:20'),
('5758c0e704286', '5758c0e6e6e5a', 'Keyboards and Synths2', 'qUmJPCESXcBY7kAwgGWF.jpg', '2016-06-09 01:05:43', '2016-06-09 01:05:43'),
('5758c1bf7fbff', '5758c1bf73ef7', 'KORG ToneWorks PANDORA PX1T Personal Multi Effect Processor 1995 Japan PX1-1', 'JhSiXH2UnlvsdO8GmV6K.jpg', '2016-06-09 01:09:19', '2016-06-09 01:09:19'),
('5758c1bfd4397', '5758c1bf73ef7', 'KORG ToneWorks PANDORA PX1T Personal Multi Effect Processor 1995 Japan PX1-2', 'mCWq7ebM05jVlpYI1oAk.jpg', '2016-06-09 01:09:19', '2016-06-09 01:09:19'),
('5758c1c032b78', '5758c1bf73ef7', 'KORG ToneWorks PANDORA PX1T Personal Multi Effect Processor 1995 Japan PX1-3', 'nQW3m5tGlTI9kSuzf7vr.jpg', '2016-06-09 01:09:20', '2016-06-09 01:09:20'),
('5758c1c03d459', '5758c1bf73ef7', 'KORG ToneWorks PANDORA PX1T Personal Multi Effect Processor 1995 Japan PX1-4', 'PprxnKA2MXBJmCt5Lyzw.jpg', '2016-06-09 01:09:20', '2016-06-09 01:09:20'),
('5758c1c0463fe', '5758c1bf73ef7', 'KORG ToneWorks PANDORA PX1T Personal Multi Effect Processor 1995 Japan PX1-5', 'f0BbYqOnFNZkTwJXtLIs.jpg', '2016-06-09 01:09:20', '2016-06-09 01:09:20'),
('5758c1da67b00', '5758c1da5f662', 'Keyboards and Synths3', 'MjTS1I78OKgcGumxn3QX.jpg', '2016-06-09 01:09:46', '2016-06-09 01:09:46'),
('5758c2aba4631', '5758c2ab5275a', 'Pig Hog PH186R - 18.6 Tour Grade 1\'4 Right Angle Guitar Cable Black or White', 'IaLAUYeo4yxBJOTjEdsF.jpg', '2016-06-09 01:13:15', '2016-06-09 01:13:15'),
('5758c4d2e61ef', '5758c4d2d83fa', 'Keyboards and Synths4', 'EY4QtIHdTCpzM6UkWJx1.jpg', '2016-06-09 01:22:26', '2016-06-09 01:22:26'),
('5758c5446c398', '5758c5446299f', 'Duesenberg D-Bass - 1', 'nLyDltkim8CgbMFvRc2Q.jpg', '2016-06-09 01:24:20', '2016-06-09 01:24:20'),
('5758c54475be2', '5758c5446299f', 'Duesenberg D-Bass - 2', 'gYnSZL3iPz1AmQxeW2rH.jpg', '2016-06-09 01:24:20', '2016-06-09 01:24:20'),
('5758c5447de57', '5758c5446299f', 'Duesenberg D-Bass - 3', 'MBV1X5674aqtFeKmPhvH.jpg', '2016-06-09 01:24:20', '2016-06-09 01:24:20'),
('5758c544d00e3', '5758c5446299f', 'Duesenberg D-Bass - 4', 'BzhC1tpI2y7NEO0m9YaJ.jpg', '2016-06-09 01:24:20', '2016-06-09 01:24:20'),
('5758c544d7571', '5758c5446299f', 'Duesenberg D-Bass - 5', 'k63yMQc8JzXAFExjwogf.jpg', '2016-06-09 01:24:20', '2016-06-09 01:24:20'),
('5758c5b1edbb6', '5758c5b1dab96', 'Keyboards and Synths5', 'LJugNcMZ7VjPq1CvQHAE.jpg', '2016-06-09 01:26:09', '2016-06-09 01:26:09'),
('5758c64e8c5eb', '5758c64e75d1d', 'Henry Heller Peruvian Style Vegan Dreadnaught Acoustic Guitar Bag 2016 Green-1', 'MxNeEgoJaL6i59KCjuBr.jpg', '2016-06-09 01:28:46', '2016-06-09 01:28:46'),
('5758c64ec0307', '5758c64e75d1d', 'Henry Heller Peruvian Style Vegan Dreadnaught Acoustic Guitar Bag 2016 Green-2', 'HSeFz51p6BasKWtN0fIY.jpg', '2016-06-09 01:28:46', '2016-06-09 01:28:46'),
('5758c64ec93b4', '5758c64e75d1d', 'Henry Heller Peruvian Style Vegan Dreadnaught Acoustic Guitar Bag 2016 Green-3', 'v0WGmYQz5DxI97f1EwXO.jpg', '2016-06-09 01:28:46', '2016-06-09 01:28:46'),
('5758c64ed2f87', '5758c64e75d1d', 'Henry Heller Peruvian Style Vegan Dreadnaught Acoustic Guitar Bag 2016 Green-4', 'tmClZLFBzQ5v1MeuOk3a.jpg', '2016-06-09 01:28:46', '2016-06-09 01:28:46'),
('5758c64edd9f4', '5758c64e75d1d', 'Henry Heller Peruvian Style Vegan Dreadnaught Acoustic Guitar Bag 2016 Green-5', 'IeM9umrNWY02LabK7ndS.jpg', '2016-06-09 01:28:46', '2016-06-09 01:28:46'),
('5758c6eb579cb', '5758c6eb4e101', 'Keyboards and Synths6', '5namhpCGf8zvdcusURqH.jpg', '2016-06-09 01:31:23', '2016-06-09 01:31:23'),
('5758c715c10b6', '5758c715baf3b', 'Henry Heller HGB-D1 Dreadnought Guitar Soft Case-1', 'J8Oz1FvdM7oLXscZAKfr.jpg', '2016-06-09 01:32:05', '2016-06-09 01:32:05'),
('5758c715caf09', '5758c715baf3b', 'Henry Heller HGB-D1 Dreadnought Guitar Soft Case-2', 'qPgFZU3I4mpHOQjE5nWT.jpg', '2016-06-09 01:32:05', '2016-06-09 01:32:05'),
('5758c7d17dea4', '5758c7d1761aa', 'Fender F logo tuners 1966 Nickel original cbs tuner vintage strat tele coronado jazzmaster jaguar-1', 'HCd91lsu4KNg5VYzfiwL.jpg', '2016-06-09 01:35:13', '2016-06-09 01:35:13'),
('5758c7d188a58', '5758c7d1761aa', 'Fender F logo tuners 1966 Nickel original cbs tuner vintage strat tele coronado jazzmaster jaguar-2', 'ECBae4bcyZIz3kMA0SuU.jpg', '2016-06-09 01:35:13', '2016-06-09 01:35:13'),
('5758c84a031e0', '5758c849f0a87', 'Keyboards and Synths7', 'j8tzpLDXTZvon7NkAFW4.jpg', '2016-06-09 01:37:14', '2016-06-09 01:37:14'),
('5758c87e2c311', '5758c87e25bc4', 'Aftermarket MK-0115-003 Guitar or Bass Knobs Flat-Top Barrel Pair USA Solid Shaft Pots - Black', 'iLdM6FlQkNp3Jm4ncaey.jpg', '2016-06-09 01:38:06', '2016-06-09 01:38:06'),
('5758c93f2271f', '5758c93f1835c', 'NOS Matched Quad (4) National Union NU JAN-CNU-7193 Black Plate Vacuum Tubes', '5AJfbaN1Flh7gXOu3PkD.jpg', '2016-06-09 01:41:19', '2016-06-09 01:41:19'),
('5758c96cadf9c', '5758c96c9fa9d', 'Keyboards and Synths8', 'SXyFCmaWA0B3orEOdRcJ.jpg', '2016-06-09 01:42:04', '2016-06-09 01:42:04'),
('5758ca24deac8', '5758ca24ca553', 'Keyboards and Synths9', 'HfU4uq5JXrQOCcBSwPno.jpg', '2016-06-09 01:45:08', '2016-06-09 01:45:08'),
('5758ca30e801a', '5758ca30e00fa', 'Meyer UP Junior Black -- Pair -- Make OFFER, must sell! Like NEW!-1', 'qmVeNKl4ItWSsZLTDHwj.jpg', '2016-06-09 01:45:20', '2016-06-09 01:45:20'),
('5758ca30f2f51', '5758ca30e00fa', 'Meyer UP Junior Black -- Pair -- Make OFFER, must sell! Like NEW!-2', 'Dc9Re0Yd6kuN4aUECxOB.jpg', '2016-06-09 01:45:20', '2016-06-09 01:45:20'),
('5758ca3106594', '5758ca30e00fa', 'Meyer UP Junior Black -- Pair -- Make OFFER, must sell! Like NEW!-3', 'EwaJT9kPX73GxoKslNYe.jpg', '2016-06-09 01:45:21', '2016-06-09 01:45:21'),
('5758ca310dbcc', '5758ca30e00fa', 'Meyer UP Junior Black -- Pair -- Make OFFER, must sell! Like NEW!-4', 'S6cyNxWdAo0mEzDkFeis.jpg', '2016-06-09 01:45:21', '2016-06-09 01:45:21'),
('5758ca31154e4', '5758ca30e00fa', 'Meyer UP Junior Black -- Pair -- Make OFFER, must sell! Like NEW!-5', 'SaJbcpiRXmIZW52Fx8nk.jpg', '2016-06-09 01:45:21', '2016-06-09 01:45:21'),
('5758cb12caf87', '5758cb12b97cf', 'Keyboards and Synths10', 'c71ZTtSiwXEBevI43HW5.jpg', '2016-06-09 01:49:06', '2016-06-09 01:49:06'),
('5758cc5456d62', '5758cc544f496', 'Audix D-Flex Dual Pivot Microphone Clamp', 'Ei5DkJdgMxAKZvOHbsFw.jpg', '2016-06-09 01:54:28', '2016-06-09 01:54:28'),
('5758ce26a3c2a', '5758ce269848a', 'Artley 17S Clarinet Made in the USA-1', '4HXnwh8qBdQp6P2aImjU.jpg', '2016-06-09 02:02:14', '2016-06-09 02:02:14'),
('5758ce26adc42', '5758ce269848a', 'Artley 17S Clarinet Made in the USA-2', 'DyXWVRhQ6eZUntGAE13x.jpg', '2016-06-09 02:02:14', '2016-06-09 02:02:14'),
('5758ce26b536f', '5758ce269848a', 'Artley 17S Clarinet Made in the USA-3', 'DvmgK2N647BbQjlySC0r.jpg', '2016-06-09 02:02:14', '2016-06-09 02:02:14'),
('5758ce26bcc37', '5758ce269848a', 'Artley 17S Clarinet Made in the USA-4', 'VM7kWsPOvRwULECoJjgK.jpg', '2016-06-09 02:02:14', '2016-06-09 02:02:14'),
('5758ce26c4756', '5758ce269848a', 'Artley 17S Clarinet Made in the USA-5', 'oQI1kmxcdnzegXGpEqTW.jpg', '2016-06-09 02:02:14', '2016-06-09 02:02:14'),
('5758cef10778a', '5758cef0d16bc', 'Vuillaume a’ Paris-1', '280C4FLxMmV1vNRcWpze.jpg', '2016-06-09 02:05:37', '2016-06-09 02:05:37'),
('5758cef112db2', '5758cef0d16bc', 'Vuillaume a’ Paris-2', 'yfvMEjsOcH9hlD06KrWI.jpg', '2016-06-09 02:05:37', '2016-06-09 02:05:37'),
('5758cef1657a5', '5758cef0d16bc', 'Vuillaume a’ Paris-3', 'WbhkH4exVIp5rU29wgG0.jpg', '2016-06-09 02:05:37', '2016-06-09 02:05:37'),
('5758cef16d2d1', '5758cef0d16bc', 'Vuillaume a’ Paris-4', 'eac5hpNMBEAn20sCIqfi.jpg', '2016-06-09 02:05:37', '2016-06-09 02:05:37'),
('5758cef174e1c', '5758cef0d16bc', 'Vuillaume a’ Paris-5', 'AGZP508OJKeFUg4zRwo6.jpg', '2016-06-09 02:05:37', '2016-06-09 02:05:37'),
('5758d023c43b1', '5758d023ba0f3', 'ESP MA 250 BE STR See Through Red-1', 'uaWjNvB8X7UPgcFYrq6t.jpg', '2016-06-09 02:10:43', '2016-06-09 02:10:43'),
('5758d023cebbc', '5758d023ba0f3', 'ESP MA 250 BE STR See Through Red-2', 'C8i12ZqwBgTJ3KRj9WNU.jpg', '2016-06-09 02:10:43', '2016-06-09 02:10:43'),
('5758d023d66cf', '5758d023ba0f3', 'ESP MA 250 BE STR See Through Red-3', 'AqIWvRHaCJKpExrDZX0m.jpg', '2016-06-09 02:10:43', '2016-06-09 02:10:43'),
('5758d023de5ad', '5758d023ba0f3', 'ESP MA 250 BE STR See Through Red-4', 'eBc3mWSROjy2wthsElgn.jpg', '2016-06-09 02:10:43', '2016-06-09 02:10:43'),
('5758d023e8dc7', '5758d023ba0f3', 'ESP MA 250 BE STR See Through Red-5', 'fC4I1XeLtArqhdcMb9Oi.jpg', '2016-06-09 02:10:43', '2016-06-09 02:10:43'),
('5758d1b3a489a', '5758d1b39dd8a', 'ESP Amaze-CTM Ocean Blue-1', 'YZOAuTd6jCkEyX9L5Use.jpg', '2016-06-09 02:17:23', '2016-06-09 02:17:23'),
('5758d1b3aebf5', '5758d1b39dd8a', 'ESP Amaze-CTM Ocean Blue-2', 'HTFumwYvg1JkQMbC5zpr.jpg', '2016-06-09 02:17:23', '2016-06-09 02:17:23'),
('5758d1b3b6516', '5758d1b39dd8a', 'ESP Amaze-CTM Ocean Blue-3', 'u3MF0ZQ7djW6HoKLOfRE.jpg', '2016-06-09 02:17:23', '2016-06-09 02:17:23'),
('5758d1b3bde1c', '5758d1b39dd8a', 'ESP Amaze-CTM Ocean Blue-4', 'JRmLaQ2TuB43UeqvXZKW.jpg', '2016-06-09 02:17:23', '2016-06-09 02:17:23'),
('5758d1b3c5135', '5758d1b39dd8a', 'ESP Amaze-CTM Ocean Blue-5', 'mIQBAcOPS0s4lvWNMt1x.jpg', '2016-06-09 02:17:23', '2016-06-09 02:17:23'),
('5758d36290f60', '5758d36284ff2', 'Washburn HOBA House of Blues Acoustic Guitar with Gig Bag-1', 'lQbTKMNq3WZGgeRsFhId.jpg', '2016-06-09 02:24:34', '2016-06-09 02:24:34'),
('5758d36299822', '5758d36284ff2', 'Washburn HOBA House of Blues Acoustic Guitar with Gig Bag-2', 'EAiWSkNjx4yz3eI5HdvX.jpg', '2016-06-09 02:24:34', '2016-06-09 02:24:34'),
('5758d362a16c8', '5758d36284ff2', 'Washburn HOBA House of Blues Acoustic Guitar with Gig Bag-3', '1XHZCeJqgbFUvGW7B3Or.jpg', '2016-06-09 02:24:34', '2016-06-09 02:24:34'),
('5758d362abcf3', '5758d36284ff2', 'Washburn HOBA House of Blues Acoustic Guitar with Gig Bag-4', 'ictCxUoqT7n8HlkS19QW.jpg', '2016-06-09 02:24:34', '2016-06-09 02:24:34'),
('5758d362b3dbb', '5758d36284ff2', 'Washburn HOBA House of Blues Acoustic Guitar with Gig Bag-5', 'cUyv6fhRoOnSMP7iC1Ks.jpg', '2016-06-09 02:24:34', '2016-06-09 02:24:34'),
('5758d4e5d737e', '5758d4e5cd685', 'Danelectro 12-String Electric Guitar Dark Aqua-1', 'o5198xut6U4NXpRF7bms.jpg', '2016-06-09 02:31:01', '2016-06-09 02:31:01'),
('5758d4e5dfe6d', '5758d4e5cd685', 'Danelectro 12-String Electric Guitar Dark Aqua-2', 'o6DqANyGUjOMgSsx95LJ.jpg', '2016-06-09 02:31:01', '2016-06-09 02:31:01'),
('5758d4e5e89c7', '5758d4e5cd685', 'Danelectro 12-String Electric Guitar Dark Aqua-3', 'CasFWuoJeg8hcE4pLXZ5.jpg', '2016-06-09 02:31:01', '2016-06-09 02:31:01'),
('5758d77251b79', '5758d77249ebb', 'Martin Cowboy V # 14 of 500 Limited Edition includes gig bag-1', '9KS7MFeTA1z3xJIbNcsd.jpg', '2016-06-09 02:41:54', '2016-06-09 02:41:54'),
('5758d7725ac24', '5758d77249ebb', 'Martin Cowboy V # 14 of 500 Limited Edition includes gig bag-2', 'urq7oxzJ0N5V31OU2yld.jpg', '2016-06-09 02:41:54', '2016-06-09 02:41:54'),
('5758d77261f31', '5758d77249ebb', 'Martin Cowboy V # 14 of 500 Limited Edition includes gig bag-3', 'q8tkwE3sVmZrxPbAKTY7.jpg', '2016-06-09 02:41:54', '2016-06-09 02:41:54'),
('5758d7727de18', '5758d77249ebb', 'Martin Cowboy V # 14 of 500 Limited Edition includes gig bag-4', 'bliLSa4sWPxEMOA9FyvK.jpg', '2016-06-09 02:41:54', '2016-06-09 02:41:54'),
('5758d77285017', '5758d77249ebb', 'Martin Cowboy V # 14 of 500 Limited Edition includes gig bag-5', '7QVZlABjpXadD9oczrI6.jpg', '2016-06-09 02:41:54', '2016-06-09 02:41:54'),
('5758da290b759', '5758da28f2dcc', 'Vintage 80s Flanger - Yamaha FL-100 Made in Japan-1', 'IqoWncA7YEQ4GZmuer0v.jpg', '2016-06-09 02:53:29', '2016-06-09 02:53:29'),
('5758da29173e6', '5758da28f2dcc', 'Vintage 80s Flanger - Yamaha FL-100 Made in Japan-2', 'dPcUvjGFrJ2of4uIh56m.jpg', '2016-06-09 02:53:29', '2016-06-09 02:53:29'),
('5758da29686b2', '5758da28f2dcc', 'Vintage 80s Flanger - Yamaha FL-100 Made in Japan-3', 'egDUdLAVfEhtXwNn2kM9.jpg', '2016-06-09 02:53:29', '2016-06-09 02:53:29'),
('5758da297270e', '5758da28f2dcc', 'Vintage 80s Flanger - Yamaha FL-100 Made in Japan-4', '0CEkAcW2OBTYd8m1iPuH.jpg', '2016-06-09 02:53:29', '2016-06-09 02:53:29'),
('5758da2979c4e', '5758da28f2dcc', 'Vintage 80s Flanger - Yamaha FL-100 Made in Japan-5', 'BIEOgm6vTR357H14cWYu.jpg', '2016-06-09 02:53:29', '2016-06-09 02:53:29'),
('5758db44bd6f2', '5758db44b4238', 'TC Electronic Corona Stereo Chorus-1', 'k29QYWS81UM3vXHoyhxl.jpg', '2016-06-09 02:58:12', '2016-06-09 02:58:12'),
('5758db44c99b3', '5758db44b4238', 'TC Electronic Corona Stereo Chorus-2', '4rp0KXRW6AeELDa5oOnT.jpg', '2016-06-09 02:58:12', '2016-06-09 02:58:12'),
('5758db44d321f', '5758db44b4238', 'TC Electronic Corona Stereo Chorus-3', '5aPvpFuYBNHo8Mrd7i1t.jpg', '2016-06-09 02:58:12', '2016-06-09 02:58:12'),
('5758db44dc751', '5758db44b4238', 'TC Electronic Corona Stereo Chorus-4', 'qFwQx9NJ0Ofjm8gXSYd5.jpg', '2016-06-09 02:58:12', '2016-06-09 02:58:12'),
('5758dc482f402', '5758dc480d86d', 'drum2', 'AHO9mES356n7Fjek4VYW.jpg', '2016-06-09 03:02:32', '2016-06-09 03:02:32'),
('5758dc527333d', '5758dc525fc7d', 'Fender Jazz Bass 1978 Natural-1', 'jasD6tci0S7rnQ8UVpdf.jpg', '2016-06-09 03:02:42', '2016-06-09 03:02:42'),
('5758dc527d676', '5758dc525fc7d', 'Fender Jazz Bass 1978 Natural-2', '8YdHluBEaOesfk57X0iR.jpg', '2016-06-09 03:02:42', '2016-06-09 03:02:42'),
('5758dc5284c15', '5758dc525fc7d', 'Fender Jazz Bass 1978 Natural-3', 'q9ReJ21vEzLuo04NklDM.jpg', '2016-06-09 03:02:42', '2016-06-09 03:02:42'),
('5758dc528c137', '5758dc525fc7d', 'Fender Jazz Bass 1978 Natural-4', 'VN3E7Xods0ObYl1SypcL.jpg', '2016-06-09 03:02:42', '2016-06-09 03:02:42'),
('5758dc5293a5c', '5758dc525fc7d', 'Fender Jazz Bass 1978 Natural-5', 'ut7OrolN84FmiPy1vI5T.jpg', '2016-06-09 03:02:42', '2016-06-09 03:02:42'),
('5758dd33d1696', '5758dd33c86c2', 'Barker B1 Vertical Bass-1', '1K9uARrCxaSUHYEgtzN6.jpg', '2016-06-09 03:06:27', '2016-06-09 03:06:27'),
('5758dd3433f7d', '5758dd33c86c2', 'Barker B1 Vertical Bass-2', 'gIJoXxDnUbeMR0Cr7qQm.jpg', '2016-06-09 03:06:28', '2016-06-09 03:06:28'),
('5758dd343e349', '5758dd33c86c2', 'Barker B1 Vertical Bass-3', '37bORmQ2yVKDi4v9dPn5.jpg', '2016-06-09 03:06:28', '2016-06-09 03:06:28'),
('5758dd34467f2', '5758dd33c86c2', 'Barker B1 Vertical Bass-4', 'EK2NRmwQ7oBtrIbZ6kVd.jpg', '2016-06-09 03:06:28', '2016-06-09 03:06:28'),
('5758dd344eb1c', '5758dd33c86c2', 'Barker B1 Vertical Bass-5', 'xDZQwpE2rHFbNGAoa6fm.jpg', '2016-06-09 03:06:28', '2016-06-09 03:06:28'),
('5758dd91d31f1', '5758dd91cc13a', 'drum3', 'r3oAkHPvpbOaEVdDeB5K.jpg', '2016-06-09 03:08:01', '2016-06-09 03:08:01'),
('5758de522cbfe', '5758de5225dee', 'Hammond B3 1969 with a Leslie 122 RV-1', 'TKCJica1AkPDzN4I9xQu.jpg', '2016-06-09 03:11:14', '2016-06-09 03:11:14'),
('5758de5236a05', '5758de5225dee', 'Hammond B3 1969 with a Leslie 122 RV-2', 'yjfW0g3bmuqo285YdHM7.jpg', '2016-06-09 03:11:14', '2016-06-09 03:11:14'),
('5758de523df28', '5758de5225dee', 'Hammond B3 1969 with a Leslie 122 RV-3', 'RbiCrhwEe0kpxAmuGajO.jpg', '2016-06-09 03:11:14', '2016-06-09 03:11:14'),
('5758dfaa6a456', '5758dfaa5d747', 'Korg MS-20', 'gV9TOYhZ5K7riuQWPN2D.jpg', '2016-06-09 03:16:58', '2016-06-09 03:16:58'),
('5758e0eca671c', '5758e0ec9d692', '1941 Gibson TG-00 Tenor L-00 Pre-War Acoustic Guitar LAST ONE SHIPPED! RARE with Chipboard Case-1', 'M0DyCn2dLOQAaZTWvJ1x.jpg', '2016-06-09 03:22:20', '2016-06-09 03:22:20'),
('5758e0ecb33d8', '5758e0ec9d692', '1941 Gibson TG-00 Tenor L-00 Pre-War Acoustic Guitar LAST ONE SHIPPED! RARE with Chipboard Case-2', 'kVyZ64O1DcJWwia9vCTl.jpg', '2016-06-09 03:22:20', '2016-06-09 03:22:20'),
('5758e0ecbbefb', '5758e0ec9d692', '1941 Gibson TG-00 Tenor L-00 Pre-War Acoustic Guitar LAST ONE SHIPPED! RARE with Chipboard Case-3', 'Oyfl4JmRZkX5deBtuE6g.jpg', '2016-06-09 03:22:20', '2016-06-09 03:22:20'),
('5758e0ecc49d6', '5758e0ec9d692', '1941 Gibson TG-00 Tenor L-00 Pre-War Acoustic Guitar LAST ONE SHIPPED! RARE with Chipboard Case-4', '0ixFSzaMyJnIpkQmOHAK.jpg', '2016-06-09 03:22:20', '2016-06-09 03:22:20'),
('5758e0eccd65c', '5758e0ec9d692', '1941 Gibson TG-00 Tenor L-00 Pre-War Acoustic Guitar LAST ONE SHIPPED! RARE with Chipboard Case-5', 'wqS9nJ56TvHAdCOgmzhY.jpg', '2016-06-09 03:22:20', '2016-06-09 03:22:20'),
('5758e1be8bb25', '5758e1be8348f', 'Black Lion Auteur PreAmp', 'npTwjOukthx5qQCEUdgS.jpg', '2016-06-09 03:25:50', '2016-06-09 03:25:50'),
('5758e2ac0acbb', '5758e2ac027d7', 'Meinl Percussion Headliner Series Wood Bongos - Vintage Sunburst-1', 'uJlvFLIprbT3UNSWOjxh.jpg', '2016-06-09 03:29:48', '2016-06-09 03:29:48'),
('5758e2ac127ec', '5758e2ac027d7', 'Meinl Percussion Headliner Series Wood Bongos - Vintage Sunburst-2', 'Cwkz9gQKEb1T0B4AOJFe.jpg', '2016-06-09 03:29:48', '2016-06-09 03:29:48'),
('5758e2ac199b7', '5758e2ac027d7', 'Meinl Percussion Headliner Series Wood Bongos - Vintage Sunburst-3', '8fAkJQp7hDFvEMme1nsj.jpg', '2016-06-09 03:29:48', '2016-06-09 03:29:48'),
('5758e2ac21608', '5758e2ac027d7', 'Meinl Percussion Headliner Series Wood Bongos - Vintage Sunburst-4', '1JjOFVKcv4Ntkm5wozi9.jpg', '2016-06-09 03:29:48', '2016-06-09 03:29:48'),
('5758e2ac3c746', '5758e2ac027d7', 'Meinl Percussion Headliner Series Wood Bongos - Vintage Sunburst-5', 'AMcKxSBLtZ1ujVm2XnkE.jpg', '2016-06-09 03:29:48', '2016-06-09 03:29:48'),
('5758e383e3b9e', '5758e383db954', 'Ibanez TBX150H 150W Guitar Amplifier Head-1', 'reohXvWfn4LMOjEUwpJ7.jpg', '2016-06-09 03:33:23', '2016-06-09 03:33:23'),
('5758e38423d18', '5758e383db954', 'Ibanez TBX150H 150W Guitar Amplifier Head-2', 'zBNEkWtabg7uK5TjMeRd.jpg', '2016-06-09 03:33:24', '2016-06-09 03:33:24'),
('5758e3842b81f', '5758e383db954', 'Ibanez TBX150H 150W Guitar Amplifier Head-3', 'y8en6PrVQBFoTJ7tp4WY.jpg', '2016-06-09 03:33:24', '2016-06-09 03:33:24'),
('5758e38433179', '5758e383db954', 'Ibanez TBX150H 150W Guitar Amplifier Head-4', 'nArOKmkDZUYqC0zFgNx8.jpg', '2016-06-09 03:33:24', '2016-06-09 03:33:24'),
('5758e3843ae71', '5758e383db954', 'Ibanez TBX150H 150W Guitar Amplifier Head-5', 'i95lDEkp6KLrjBePtWJZ.jpg', '2016-06-09 03:33:24', '2016-06-09 03:33:24'),
('5758e41aa8c7e', '5758e41a9f9cf', 'IsoAcoustics Medium Monitor Isolation Pad (pair) 2016-1', 'SlAbwcN1MVhv7d4EjinT.jpg', '2016-06-09 03:35:54', '2016-06-09 03:35:54'),
('5758e41ab0f13', '5758e41a9f9cf', 'IsoAcoustics Medium Monitor Isolation Pad (pair) 2016-2', 'PrIyw7oTUDbtxOCvXHAc.jpg', '2016-06-09 03:35:54', '2016-06-09 03:35:54'),
('577df92c4eccd', '577df92a32f12', 'duende', 'DEG85VKRyvFiBfmtO3Cn.jpg', '2016-07-07 06:39:40', '2016-07-07 06:39:40'),
('577e4cb85163b', '577e4cb2c7ed0', 'IMG_7987 (1)', '6gTZKebIVC3rhyE4x1au.jpg', '2016-07-07 12:36:08', '2016-07-07 12:36:08'),
('577e4cbc79341', '577e4cb2c7ed0', 'IMG_7991 (1)', 'ABEsrjHv5V3W2tKTGznF.jpg', '2016-07-07 12:36:12', '2016-07-07 12:36:12'),
('577e4cc1433a4', '577e4cb2c7ed0', 'IMG_7995 (1)', 'zSaWKBksgNjIuF1v3Tio.jpg', '2016-07-07 12:36:17', '2016-07-07 12:36:17'),
('577e4cc4c676e', '577e4cb2c7ed0', 'IMG_7996 (1)', 'THhurDoXbMVdU94jmk85.jpg', '2016-07-07 12:36:20', '2016-07-07 12:36:20'),
('577e4f44ae183', '577e4f3d12ccd', 'IMG_7982 (1)', 'tPW3kvdNsG7R8eBl9fhZ.jpg', '2016-07-07 12:47:00', '2016-07-07 12:47:00'),
('5781ce6551fe5', '5781ce616681d', 'IMG_2232', '1V6msSez4pqjKQPCRdfl.jpg', '2016-07-10 04:26:13', '2016-07-10 04:26:13'),
('5781ce691263c', '5781ce616681d', 'IMG_2224', 'XdrToCEyfBGUD2lV3v09.jpg', '2016-07-10 04:26:17', '2016-07-10 04:26:17'),
('5781ce6cd2994', '5781ce616681d', 'IMG_2227', 'xDPSTagIjhEwfsJHzb86.jpg', '2016-07-10 04:26:20', '2016-07-10 04:26:20'),
('5781ce70e8554', '5781ce616681d', 'IMG_2230', 'fhQC9KbtuGqSZNaRimLs.jpg', '2016-07-10 04:26:24', '2016-07-10 04:26:24'),
('5781ce7506f99', '5781ce616681d', 'IMG_2231', 'al1bAMnKegh37QuHwyqO.jpg', '2016-07-10 04:26:29', '2016-07-10 04:26:29'),
('5781ce798eba7', '5781ce616681d', 'IMG_2233', 'Hy9oR8sCrL5WTicdKUjE.jpg', '2016-07-10 04:26:33', '2016-07-10 04:26:33'),
('5781ce7dc94b6', '5781ce616681d', 'IMG_2238', 'a2QlI05Jv4Op8STiKhec.jpg', '2016-07-10 04:26:37', '2016-07-10 04:26:37'),
('5781ce81b9c53', '5781ce616681d', 'IMG_2240', '0XZzrjnCKJQIeaUhA7oH.jpg', '2016-07-10 04:26:41', '2016-07-10 04:26:41'),
('5781ce8493319', '5781ce616681d', 'IMG_2242', 'xzLVe2gdRpCbIcYoOifB.jpg', '2016-07-10 04:26:44', '2016-07-10 04:26:44'),
('5781ce8854253', '5781ce616681d', 'IMG_2243', 'LoeR85KuFNb0DQJdzxBm.jpg', '2016-07-10 04:26:48', '2016-07-10 04:26:48'),
('5781ce8d53008', '5781ce616681d', 'IMG_2261', 'xIl153WN6FQrRpUPiLY8.jpg', '2016-07-10 04:26:53', '2016-07-10 04:26:53'),
('5781d1b8cf218', '5781d1b5ba6c9', 'IMG_2320', '2T3YdmtKZXG6Cf5WvEkg.jpg', '2016-07-10 04:40:24', '2016-07-10 04:40:24'),
('5781d1bbe9493', '5781d1b5ba6c9', 'IMG_2321', 'aVjdNKv3l4gXzAOM7FTU.jpg', '2016-07-10 04:40:27', '2016-07-10 04:40:27'),
('5781d1bf657bf', '5781d1b5ba6c9', 'IMG_2323', 'LYIerXD0Fhga4xyAwPK5.jpg', '2016-07-10 04:40:31', '2016-07-10 04:40:31'),
('5781d1c1a7431', '5781d1b5ba6c9', 'IMG_2326', '8G1EYx7tgNIASQDvfl9d.jpg', '2016-07-10 04:40:33', '2016-07-10 04:40:33'),
('5781d1c421a70', '5781d1b5ba6c9', 'IMG_2327', 'CguUw7LscoWGQ5Fh68I9.jpg', '2016-07-10 04:40:36', '2016-07-10 04:40:36'),
('5781d1c67161c', '5781d1b5ba6c9', 'IMG_2328', '6LkBY3VU0jw5mzexKoTh.jpg', '2016-07-10 04:40:38', '2016-07-10 04:40:38'),
('5781d37007e71', '5781d36b4c056', 'IMG_2311', '1MmvZg4IJuAkTDlSLqyz.jpg', '2016-07-10 04:47:44', '2016-07-10 04:47:44'),
('5781d375980c5', '5781d36b4c056', 'IMG_2297', 'WgFPR18VrlKY3hzwevQT.jpg', '2016-07-10 04:47:49', '2016-07-10 04:47:49'),
('5781d37a52e1a', '5781d36b4c056', 'IMG_2301', 'tjblVsqf0XeSug8Y3L4H.jpg', '2016-07-10 04:47:54', '2016-07-10 04:47:54'),
('5781d37e70d3e', '5781d36b4c056', 'IMG_2302', 'mMKQV6EjrXzAaGZkiTne.jpg', '2016-07-10 04:47:58', '2016-07-10 04:47:58'),
('5781d38281039', '5781d36b4c056', 'IMG_2303', 'URgV3BeOW9ZpLPAC7i4G.jpg', '2016-07-10 04:48:02', '2016-07-10 04:48:02'),
('5781d386c5640', '5781d36b4c056', 'IMG_2304', 'QvkpLNUR1MsFCdfxOBeW.jpg', '2016-07-10 04:48:06', '2016-07-10 04:48:06'),
('5781d38905892', '5781d36b4c056', 'IMG_2307', 's7gBWFbmraDnkYtMPd5u.jpg', '2016-07-10 04:48:09', '2016-07-10 04:48:09'),
('5781d38c3eb56', '5781d36b4c056', 'IMG_2312', 'wF0qgBIQYkjDL8XJUVRc.jpg', '2016-07-10 04:48:12', '2016-07-10 04:48:12'),
('5781d38f247f8', '5781d36b4c056', 'IMG_2313', 'Ql5CZbkRosr8n3m6gzL2.jpg', '2016-07-10 04:48:15', '2016-07-10 04:48:15'),
('5781d394040d8', '5781d36b4c056', 'IMG_2314', 'vJft4F3w8p51Na7c2mTU.jpg', '2016-07-10 04:48:20', '2016-07-10 04:48:20'),
('5781d444b00c4', '5781d43fa649f', 'IMG_2290', 'rVFAfD5zhHBt23Y8x6nZ.jpg', '2016-07-10 04:51:16', '2016-07-10 04:51:16'),
('5781d449665a0', '5781d43fa649f', 'IMG_2284', '9Ud1I0mlNV2C3czFYpng.jpg', '2016-07-10 04:51:21', '2016-07-10 04:51:21'),
('5781d44db1823', '5781d43fa649f', 'IMG_2288', 'lgEM9Xoe1nby6dYRkfjS.jpg', '2016-07-10 04:51:25', '2016-07-10 04:51:25'),
('5781d4520f039', '5781d43fa649f', 'IMG_2291', 'oLCMgcyVxBlsw0r1TIK3.jpg', '2016-07-10 04:51:30', '2016-07-10 04:51:30'),
('5781f5660e311', '5781f56070258', 'IMG_2365', 'kNQvFaV0pYSsf3ML5EGh.jpg', '2016-07-10 07:12:38', '2016-07-10 07:12:38'),
('5781f56b6d2b0', '5781f56070258', 'IMG_2366', 'oha9B6Zx0rwXfYk2bzDp.jpg', '2016-07-10 07:12:43', '2016-07-10 07:12:43'),
('5781f56fab032', '5781f56070258', 'IMG_2367', 'IaLlE61Gh3nzJubMtYDs.jpg', '2016-07-10 07:12:47', '2016-07-10 07:12:47'),
('5781f573707db', '5781f56070258', 'IMG_2368', 'OfPKrWRznoqQmZh8jlEM.jpg', '2016-07-10 07:12:51', '2016-07-10 07:12:51'),
('5781f57728d81', '5781f56070258', 'IMG_2369', 'l8SpMxaVvUI3zuke6TG2.jpg', '2016-07-10 07:12:55', '2016-07-10 07:12:55'),
('5781f57ad35ef', '5781f56070258', 'IMG_2370', 'UIKPi2kbncpx3Tya8SAh.jpg', '2016-07-10 07:12:58', '2016-07-10 07:12:58'),
('5781f57fa478b', '5781f56070258', 'IMG_2371', 'sc58Hkpx1EbiXnSUWQNe.jpg', '2016-07-10 07:13:03', '2016-07-10 07:13:03'),
('5781f5826312a', '5781f56070258', 'IMG_2372', 'NTK1JAzZfeb9IQv2PmiF.jpg', '2016-07-10 07:13:06', '2016-07-10 07:13:06'),
('5781f58835e00', '5781f56070258', 'IMG_2374', 'URhyMNTuxprlw0YQ6ic4.jpg', '2016-07-10 07:13:12', '2016-07-10 07:13:12'),
('5781f58d24f5b', '5781f56070258', 'IMG_2375', 'JnDNMwGWFtkf0KAjBe7o.jpg', '2016-07-10 07:13:17', '2016-07-10 07:13:17'),
('5781f58fc3955', '5781f56070258', 'IMG_2376', 'gTv1ofy73hMEdanwjR9V.jpg', '2016-07-10 07:13:19', '2016-07-10 07:13:19'),
('5781f595e5822', '5781f56070258', 'IMG_2377', 'P49Wi5Nwrg8QITovtOYm.jpg', '2016-07-10 07:13:25', '2016-07-10 07:13:25'),
('578364d1e1c42', '578364d0455cc', 'B-52 ACTPRO-1515 HD-2', 'wVvtK95WhCQ6nLSr3FTg.jpg', '2016-07-11 09:20:17', '2016-07-11 09:20:17'),
('578364d5144e3', '578364d0455cc', 'B-52 ACTPRO-1515 HD-3', 'p43myzP5x9RHrAaiXt7v.jpg', '2016-07-11 09:20:21', '2016-07-11 09:20:21'),
('578364d77ce2e', '578364d0455cc', 'B-52 ACTPRO-1515 HD-1', 'a8nMdHbvQiIxSjzUDy9X.jpg', '2016-07-11 09:20:23', '2016-07-11 09:20:23'),
('578376a1ed3f5', '5783769466e85', 'image', 'BG4fHhuLgzM7eROm0KpU.jpeg', '2016-07-11 10:36:17', '2016-07-11 10:36:17'),
('578376b0366f9', '5783769466e85', 'image', 'qgGNncByDda7IrWKpX46.jpeg', '2016-07-11 10:36:32', '2016-07-11 10:36:32'),
('57837700c33f0', '5783769466e85', 'image', '2AYswfdrK5bZlmNFC08P.jpeg', '2016-07-11 10:37:52', '2016-07-11 10:37:52');
INSERT INTO `gp_products_photos` (`photo_id`, `product_id`, `photo_title`, `photo_filename`, `created_at`, `updated_at`) VALUES
('578377071b4d7', '5783769466e85', 'image', 'gNzwUKvcSxIB57amhVWA.jpeg', '2016-07-11 10:37:59', '2016-07-11 10:37:59'),
('57a17e4525b69', '57a17e4515317', 'Electric-Guitar-2', 'Z3RPobBUOxNnj60CheGc.jpg', '2016-08-03 05:16:53', '2016-08-03 05:16:53'),
('57a84df5626ce', '57a84df448f66', 'Electric-Guitar', 'eRSI3EMO6zrjFVZYUtJX.jpg', '2016-08-08 09:16:37', '2016-08-08 09:16:37'),
('57a84eb568ce0', '57a84eb4eb6a8', 'Electric-Guitar', 'zS917b5Jk2UgqrGud6cE.jpg', '2016-08-08 09:19:49', '2016-08-08 09:19:49'),
('57aacfd1b01e4', '57aacfcbad4c1', '3', 'U0ZgsIvJuhbltwKiNV1f.jpg', '2016-08-10 06:55:13', '2016-08-10 06:55:13'),
('57aacfd9ce780', '57aacfcbad4c1', '2', 'rbaVy41vI8fK3NDkLACu.jpg', '2016-08-10 06:55:21', '2016-08-10 06:55:21'),
('57aacfe366465', '57aacfcbad4c1', '1', 'hBucNGTJMxn79lAW0qey.jpg', '2016-08-10 06:55:31', '2016-08-10 06:55:31'),
('57b1ab12f32d1', '57b1ab11d06ea', 'Fender-DG-60', '9wlHrpVFSPemdMqzJxv5.jpg', '2016-08-15 11:44:19', '2016-08-15 11:44:19'),
('57b1ab26e9d59', '57b1ab11d06ea', '0950801121_gtr_frt_001_rr', 'QNzb2RE91eLuxUnkVmjP.jpg', '2016-08-15 11:44:38', '2016-08-15 11:44:38'),
('57b1ab4c98bb5', '57b1ab11d06ea', '0950801121_gtr_cntbdyright_001_nr', 'CHS2LiVqBTFYzdGUtI4g.jpg', '2016-08-15 11:45:16', '2016-08-15 11:45:16'),
('57c4ff9e32758', '57c4ff9c4955f', 'Electric-Guitar-2', 'LhTsCDxGlHjRtXVoE0Kq.jpg', '2016-08-30 03:38:06', '2016-08-30 03:38:06'),
('57c4ffa389211', '57c4ff9c4955f', 'Electric-Guitar-1', 'DzydaYbchBIGQe8KmFjZ.jpg', '2016-08-30 03:38:11', '2016-08-30 03:38:11'),
('57c5711f0bdd6', '57c5711ca4891', 'IMG_4527', 'K7q6Cf4SibZrYX38NEFI.jpg', '2016-08-30 11:42:23', '2016-08-30 11:42:23'),
('57c5712133d41', '57c5711ca4891', 'IMG_4528', 'NhgjwKcez5MTsER3Z8ir.jpg', '2016-08-30 11:42:25', '2016-08-30 11:42:25'),
('57c5712389121', '57c5711ca4891', 'IMG_4530', 'm7bNeIscPWdJoHh8pM4g.jpg', '2016-08-30 11:42:27', '2016-08-30 11:42:27'),
('57d10cbc5ccfb', '57d10cbbc6e38', 'drumset', 'NwvCidazsn3rJ0D2O7cB.jpg', '2016-09-08 07:01:16', '2016-09-08 07:01:16'),
('57d10cbcc1a85', '57d10cbbc6e38', 'drumset', 'cWjXz7porSN0RUO3ZIbt.jpg', '2016-09-08 07:01:16', '2016-09-08 07:01:16'),
('57d10cbd34b78', '57d10cbbc6e38', 'drumset', 'CSkBnxYpe4JHWfbvswiU.jpg', '2016-09-08 07:01:17', '2016-09-08 07:01:17'),
('57d8aaa8d0f0c', '57d8aaa6edebc', 'drumset', 'X7F5KozJkIEQNn8AVmG1.jpg', '2016-09-14 01:40:56', '2016-09-14 01:40:56'),
('57d8aae278cd1', '57d8aaa6edebc', 'drumset', 'xCl679VfS8DJ4je23RZy.jpg', '2016-09-14 01:41:54', '2016-09-14 01:41:54'),
('57d8aae2f16d1', '57d8aaa6edebc', 'drumset', 'LBegxbw70yTUC4VF5roi.jpg', '2016-09-14 01:41:54', '2016-09-14 01:41:54'),
('581ac73382add', '581ac732d1622', 'Electric-Guitar', 'xR20jVKFJqItXyDbAeH4.jpg', '2016-11-03 05:12:19', '2016-11-03 05:12:19'),
('582014d99e6c3', '582014d381743', 'muffy', 'leuh2BQZYvNXAaoM8PUy.jpg', '2016-11-07 05:44:57', '2016-11-07 05:44:57'),
('582014db3fd50', '582014d381743', 'If you don\'t sacrifice for what you want. What you want becomes the sacrifice', 'pEwQ07TSLkiyfAYto6Ol.jpg', '2016-11-07 05:44:59', '2016-11-07 05:44:59'),
('582014dc277cf', '582014d381743', 'devhugot', 'vM9GA1jiP0NIwalOpryV.jpg', '2016-11-07 05:45:00', '2016-11-07 05:45:00'),
('58564e2fd9b7a', '58564dc921b20', 'IMG_8980', 'hDQf1iL7SbWXz9pKZy6o.jpg', '2016-12-18 08:51:59', '2016-12-18 08:51:59'),
('59818dde341c3', '59818cc9ee61e', 'image', 'DCmjXv8ychJfMSrzQEsR.jpg', '2017-08-02 08:31:26', '2017-08-02 08:31:26'),
('5987cbeb4d6ab', '5987cb6d11f6f', 'trumpet147406', 'GT2CoakidU9Xb8L5EhRs.jpg', '2017-08-07 02:09:47', '2017-08-07 02:09:47'),
('5a6993604857c', '5a699360009db', 'HkvZ9d2', 'GlxPha6pwcOoXReB78rd.jpg', '2018-01-25 00:20:48', '2018-01-25 00:20:48'),
('5a6994eeec03f', '5a6994eea5124', 'numbers-money-calculating-calculation', 'ETgWiVk7JCH6wqFfnOcb.jpg', '2018-01-25 00:27:26', '2018-01-25 00:27:26'),
('5a6ed790e2159', '5a6ed790a7a5d', 'numbers-money-calculating-calculation', 'EMRvgpyGlFVujkHOWtcQ.jpg', '2018-01-29 00:13:04', '2018-01-29 00:13:04'),
('5a6fe1e7870a6', '5a6fe1e751d3e', 'praying-man', 'KFzvhjV4nycoALUme0YP.jpg', '2018-01-29 19:09:27', '2018-01-29 19:09:27'),
('5a6fe62e1fdfd', '5a6fe62de4a85', 'numbers-money-calculating-calculation', 'gtdm5Mlqn48GYw1xNrVH.jpg', '2018-01-29 19:27:42', '2018-01-29 19:27:42');

-- --------------------------------------------------------

--
-- Table structure for table `gp_products_shipping`
--

CREATE TABLE `gp_products_shipping` (
  `product_shipping_id` varchar(13) NOT NULL,
  `product_id` varchar(13) NOT NULL,
  `country` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `policy` text,
  `method` enum('shipping','local pickup','all') DEFAULT 'shipping',
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gp_products_shipping`
--

INSERT INTO `gp_products_shipping` (`product_shipping_id`, `product_id`, `country`, `city`, `state`, `policy`, `method`, `created_at`, `updated_at`) VALUES
('5757b5b0a8f36', '5757b5b0a3478', 'Australia', 'Perth', 'qld', 'Shipping Policy\n\nShips from Murfreesboro, TN to:\nContinental U.S.$50.00\nEverywhere Else$100.00\n\nI will ship with tracking to the continental United States. Alaska, Hawaii, and international shipments may incur additional charges.\n\nReturn Policy\nReturn Window\nThis product can be returned within 7 days of receipt.\n\nGeneral Terms\nItems must be returned in original, as-shipped condition with all original packaging and no signs of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n\nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges.', 'shipping', '2016-06-08 06:05:36', '2016-06-17 07:19:00'),
('5757bc41b0dc6', '5757bc41ae3bc', 'Australia', 'Perth', 'wa', 'Shipping Policy\nAvailable for local pickup from Dickson City, PA\nShips from Dickson City, PA to:Continental U.S.$0.00\nEverywhere Else$100.00\n\nFree UPS Ground shipping to the lower 48 U.S. states. For all  International Orders we ask that you please contact us by email or phone for a Custom Shipping Quote and if the product is available to be shipped to your area. All items ship from Dickson City PA. Orders will \nbe shipped the same business day as payment is received and tracking info will also be provided. We assure you that every item that we ship will be packed with the utmost care from our years of experience. All  items shipped from our store include shipping insurance. \n\nReturn Policy\nReturn Window\nThis product can be returned within 3 days of receipt.\n\nGeneral Terms\nItems must be returned in original, as-shipped condition with all original packaging and no signs of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n\nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges.\nSpecial Conditions We have a 72 hour exchange period on all mail order items (excludes \nlayaways and special orders). Exchanges must be complete and in the  original condition, original packaging with all accessories and complete documentation (owner’s manual, warranty card, hang tags, etc.), show no signs of wear or use, and include the Return Authorization Number provided by your sales associate. Items which are not normally stocked (special orders, oversized or overweight items, discontinued products, close-outs, used products, layaways, etc.) are not returnable. Customer  is responsible for all shipping charges on exchanged items, both to and from.', 'all', '2016-06-08 06:33:37', '2016-06-17 08:52:00'),
('5757bda52e6f4', '5757bda523572', 'Australia', 'Perth', 'wa', 'Shipping Policy\nAvailable for local pickup from Talence, B, FR Ships from Talence, B, FR to:\n - United States $116.52\n - France $0.00\n - European Union $29.13\n - Everywhere Else $174.77\n\nI will ship with tracking to the listed regions. To negotiate shipping rates to other locations, please send me a message.\n\nReturn Policy\nReturn Window This product can be returned within 14 days of receipt. General Terms Items must be returned in original, as-shipped condition with all original packaging and no signs of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as  described.\n\nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges.\n', 'all', '2016-06-08 06:39:33', '2016-06-17 08:51:48'),
('5757be522eec9', '5757be522be2d', 'Australia', 'ST KITTS', 'sa', 'Shipping Policy\n\nShips from Portland, OR to:\nContinental U.S.\n$35.00\nEverywhere Else\n$65.00\nAll overseas shipments will automatically be insured for a maximum of $499 via USPS. Other services, carriers, and additional insurance are the responsibility and expense of the buyer. Some instruments, especially vintage keyboards, will require regular tuning, calibration and service upon receipt and installation. Routine service should be expected.\n\nReturn Policy\nReturn Window\nThis product can be returned within 3 days of receipt.\n\nGeneral Terms\nItems must be returned in original, as-shipped condition with all original packaging and no\nsigns of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n \nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges and a 10% restocking fee.\n\nSpecial Conditions\nReturns are accepted by prior arrangement only. Items shipped internationally are not eligible for return. Vintage electronics and keyboards, because of their uniquely fragile nature, are not eligible for return. Routine service of internal electrical components is frequently required after shipment. For all returns, shipping fees are non-refundable, and return shipping is the sole responsibility of the buyer. Thanks!\n ', 'shipping', '2016-06-08 06:42:26', '2016-06-17 08:34:58'),
('5757bfc15ca75', '5757bfc15b77c', 'Australia', 'Perth', 'wa', 'Ships from St. Louis, MO to: Continental U.S. $0.00\nEverywhere Else $200.00\nFree UPS Ground shipping to the lower 48 U.S. states.  For all  International Orders we ask that you please contact us by email or phone for a Custom Shipping Quote and if the product is available to be shipped to your area. All items ship from St. Louis, MO.  Orders will be shipped the same business day as payment is received and tracking info will also be provided.  We assure you that every item that we ship will be packed with the utmost care from our years of experience. All items shipped from our store include shipping insurance. \n\nReturn Policy\nReturn Window This item is sold As-Is and cannot be returned. ', 'shipping', '2016-06-08 06:48:33', '2016-06-17 08:51:31'),
('5757c05810478', '5757c0580f2d3', 'Australia', 'FRANKTON', 'sa', 'Shipping Policy\n\nAvailable for local\npickup from Los Angeles, CAShips from Los Angeles, CA to:\nContinental U.S.\n$20.00\nEverywhere Else\n$40.00\nI will ship with tracking to the listed regions. To negotiate shipping rates to other locations,\nplease send me a message.\n \nReturn Policy\nReturn Window\nThis item is sold As-Is and cannot be returned.', 'shipping', '2016-06-08 06:51:04', '2016-06-17 08:34:32'),
('5757c285672c7', '5757c28562494', 'Australia', 'FRANKTON', 'sa', 'Shipping Policy\n\nShips from Portland, OR to:\nContinental U.S.\n$16.00\nEverywhere Else\n$22.00\nAll overseas\nshipments will automatically be insured for a maximum of $499 via USPS. Other services, carriers, and additional insurance are the responsibility and expense of the buyer. Some instruments, especially vintage keyboards, will require regular tuning, calibration and service upon receipt and installation. Routine service should be expected.\n \nReturn Policy\nReturn Window\nThis item is sold As-Is and cannot be returned.', 'shipping', '2016-06-08 07:00:21', '2016-06-17 08:34:21'),
('5757c2a0d27ec', '5757c2a0cf7b4', 'Australia', 'Perth', 'wa', 'Shipping Policy\n\nShips from Chester, SC to:\nContinental U.S.\n$70.00\nEverywhere Else\n$200.00\nI will ship with tracking to the continental United States. Alaska,Hawaii, and international shipments may incur additional charges.\n\nReturn Policy\nReturn Window\nThis product can be returned within 14 days of receipt.\n\nGeneral Terms\nItems must be returned in original, as-shipped condition with all original packaging and no signs of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n\nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges.\n', 'all', '2016-06-08 07:00:48', '2016-06-17 07:46:21'),
('5757c4465b90e', '5757c4465aa91', 'Australia', 'ULEY', 'sa', 'Shipping Policy\n\nAvailable for local\npickup from Nashville, TNShips from Nashville, TN to:\nContinental U.S.\n$0.00\nEverywhere Else\n$25.00\nI will ship with tracking to the listed regions. To negotiate shipping rates to other locations, please send me a message.\n \nReturn Policy\nReturn Window\nThis product can be returned within 7 days of receipt.\n\nGeneral Terms\nItems must be returned in original, as-shipped condition with all original packaging and no\nsigns of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n \nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges.\n ', 'shipping', '2016-06-08 07:07:50', '2016-06-17 08:34:08'),
('5757c4949e88c', '5757c4949c432', 'Australia', 'BIGGARA', 'vic', 'Shipping Policy\nAvailable for local pickup from Folsom, CAShips from Folsom, CA to:Continental U.S.$49.99I will ship with tracking to the listed regions. To negotiate shipping rates to other locations, please send me a message.', 'shipping', '2016-06-08 07:09:08', '2016-06-17 03:27:20'),
('5757c57b6099b', '5757c57b5d975', 'Australia', 'ULEY', 'sa', 'Shipping Policy\n\nShips from Portland, OR to:\nContinental U.S.\n$35.00\nEverywhere Else\n$65.00\nAll overseas shipments will automatically be insured for a maximum of $499 via USPS. Other services, carriers, and additional insurance are the responsibility and expense of the buyer. Some instruments, especially vintage keyboards, will require regular tuning, calibration and service upon receipt and installation. Routine service should be expected.\n \nReturn Policy\nReturn Window\nThis item is sold As-Is and cannot be returned.', 'shipping', '2016-06-08 07:12:59', '2016-06-17 08:33:51'),
('5757c69b98884', '5757c69b978d2', 'Australia', 'ULEY ', 'sa', 'Shipping Policy\n\nShips from Portland, OR to:\nContinental U.S.\n$25.00\nEverywhere Else\n$55.00\nAll overseas\nshipments will automatically be insured for a maximum of $499 via USPS. Other services, carriers, and additional insurance are the responsibility and expense of the buyer. Some instruments, especially vintage keyboards, will require regular tuning, calibration and service upon receipt and installation. Routine service should be expected.\n \nReturn Policy\nReturn Window\nThis item is sold As-Is and cannot be returned.', 'shipping', '2016-06-08 07:17:47', '2016-06-17 08:22:41'),
('5757c715e95fb', '5757c715e62a2', 'Australia', 'BANDA BANDA', 'nsw', 'Shipping Policy\n\nShips from Rockford, MI to:Continental U.S.$0.00\nAlaska$39.00\nHawaii$39.00\nPuerto Rico$39.00\nEurope (Non-EU)$89.00\nEuropean Union$89.00\nSouth America$89.00\nCanada$59.00\nJapan$89.00\nUnited Kingdom$89.00\nEverywhere Else$99.00\n\nWe only Sell New & Factory Sealed items, unless clearly listed as Used. As a general rule, we will ship within 24 to 48 hours of your order. There are plenty of items IN STOCK NOW and ready to ship within One Hour of your order - It is always best to Email us: sales@musicfor all.biz or Call us 616-863-8927 for a quick answer about the item(s) you wish to purchase. Thanks! \n\nMatt Kerwin - Owner - Music For All', 'shipping', '2016-06-08 07:19:49', '2016-06-16 03:55:54'),
('5757c76b149c1', '5757c76b0fcf2', 'Australia', 'ULEY ', 'sa', 'Shipping Policy\n\nShips from Brielle, ZH, NL to:\nUnited States $30.29\nNetherlands $11.65\nEuropean Union $18.64\nEverywhere Else $30.29\nI will ship with tracking to the listed regions. To negotiate shipping rates to other locations, please send me a message.\n \nReturn Policy\nReturn Window\nThis product can be returned within 7 days of receipt.\n\nGeneral Terms\nItems must be returned in original, as-shipped condition with all original packaging and no signs of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n \nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges.\n ', 'shipping', '2016-06-08 07:21:15', '2016-06-17 08:22:12'),
('5757c8fb7520d', '5757c8fb7426a', 'Australia', 'ULEY ', 'sa', 'Shipping Policy\n\nAvailable for local\npickup from Wilmington, DEShips from Wilmington, DE to:\nContinental U.S.\n$35.00\nEverywhere Else\n$150.00\n \nReturn Policy\nReturn Window\nThis item is sold As-Is and cannot be returned.', 'shipping', '2016-06-08 07:27:55', '2016-06-17 08:20:52'),
('5757c906aa556', '5757c906a945c', 'Australia', 'LAKE CLIFTON', 'wa', 'Shipping Policy\n\nShips from St. Louis, MO to:\nContinental U.S.$0.00\nEverywhere Else$200.00\n\nFree UPS Ground shipping to the lower 48 U.S. states. For all International Orders we ask that you please contact us by email or phone for a Custom Shipping Quote and if the product is available to be shipped to your area. All items ship from St. Louis, MO. Orders will be shipped the same business day as payment is received and tracking info will also be provided. We assure you that every item that we ship will be packed with the utmost care from our years of experience. All items shipped from our store include shipping insurance.', 'shipping', '2016-06-08 07:28:06', '2016-06-17 07:19:56'),
('5757ca0ce8db3', '5757ca0ce7b0e', 'Australia', 'LOWER TENTHILL', 'qld', 'Shipping Policy\n\nAvailable for local\npickup from Leominster, MA\nShips from Leominster, MA to:\nContinental U.S.\n$10.00\nEverywhere Else\n$20.00\nWe offer three local pickup locations, in Leominster, MA; Southbridge, MA; and East Hartford, CT. On receiving cleared payment we will ship via UPS Ground anywhere in the continental United States for flat rate listed. For any other location, please contact us for a shipping quote.\n \nReturn Policy\nReturn Window\nThis product can be returned within 14 days of receipt.\n\nGeneral Terms\nItems must bereturned in original, as-shipped condition with all original packaging and no\nsigns of use. Buyer assumes responsibility for all return shipping costs unlessthe item was not received as described.\n \nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges.\n \nSpecial Conditions\nWe accept returns within 14 days of receipt.Please inspect gear and call to report damage and we will notify shipper to process the claim.All other returns are subject to a 25%\nrestocking fee.All shipping charges to and from the customer will be deducted from refund as well as the restocking fee.\n ', 'shipping', '2016-06-08 07:32:28', '2016-06-17 08:20:34'),
('5757ca83b72db', '5757ca83b493d', 'Australia', 'Perth', 'wa', 'Shipping Policy\n\nShips from Calgary, AB, CA to:\nUnited States\n$195.00\nCanada\n$145.00\nEverywhere Else\n$345.00\nI will ship with tracking to the listed regions. To negotiate shipping rates to other locations, please send me a message.\n\nReturn Policy\nReturn Window\nThis product can be returned within 7 days of receipt.\n\nGeneral Terms\nItems must be returned in original, as-shipped condition with all original packaging and no signs of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as \ndescribed.\n\nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges.\n', 'all', '2016-06-08 07:34:27', '2016-06-17 07:32:29'),
('5757cad8c63d0', '5757cad8b256c', 'Australia', 'LOWER TENTHILL', 'qld', 'Shipping Policy\n\nShips from Chester, SC to:\nContinental U.S.\n$70.00\nEverywhere Else\n$200.00\nI will ship with tracking to the continental United States. Alaska, Hawaii, and international shipments may incur additional charges.\n \nReturn Policy\nReturn Window\nThis product can be returned within 14 days of receipt.\n\nGeneral Terms\nItems must be returned in original, as-shipped condition with all original packaging and no\nsigns of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n \nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges.\n ', 'shipping', '2016-06-08 07:35:52', '2016-06-17 08:01:42'),
('5757cb901b90c', '5757cb901a907', 'Australia', 'MINCHINBURY', 'nsw', 'Shipping Policy\nAvailable for local pickup from Monroe, NJShips from Monroe, NJ to:\nContinental U.S.$15.00\nEverywhere Else$30.00\n\nI will ship with tracking to the listed regions. To negotiate shipping rates to other locations, please send me a message.', 'shipping', '2016-06-08 07:38:56', '2016-06-17 07:15:55'),
('5757ccb1a601c', '5757ccb1a4f93', 'Australia', 'JERRABATTCULLA', 'nsw', 'Shipping Policy\n\nShips from Weston, FL to:\nContinental U.S.\n$30.00\nEverywhere Else\n$30.00\nI will ship with tracking to the listed regions. To negotiate shipping rates to other locations,\nplease send me a message.\n\nReturn Policy\nReturn Window\nThis product can be returned within 7 days of receipt.\n\nGeneral Terms\nItems must be returned in original, as-shipped condition with all original packaging and no\nsigns of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n \nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges.\n ', 'shipping', '2016-06-08 07:43:45', '2016-06-17 07:49:46'),
('5757ccbde09b6', '5757ccbddfb7d', 'Australia', 'SOUTHPORT', 'qld', 'Shipping Policy\n\nShips from Newington, CT to:\nContinental U.S.\n$47.00\nEverywhere Else\n$225.00\nI will ship with tracking to the listed regions. To negotiate shipping rates to other locations, please send me a message.', 'shipping', '2016-06-08 07:43:57', '2016-06-17 07:47:44'),
('5757cd3170bb3', '5757cd316f107', 'Australia', 'Perth', 'wa', 'Shipping Policy\n\nAvailable for local pickup\nfrom Canton, OH\nShips from Canton, OH to:\nContinental U.S.\n$39.00\nEverywhere Else\n$279.00\nAll shipments will be shipped within 24 hours of payment being received. All shipments will be via UPS Ground, Fed Ex or US Post Office unles otherwise requested. \n\nReturn Policy\nWe operate a 14-day return period. Items must be returned with all original packaging as well as all included items (accessories etc). Items that are not returned in original, resealable condition are subject to a 15% restocking fee. Single items over $1999 must be returned within 5 days from the date of delivery, or a return is not possible. If an item over $1999 is returned after the 5 day period, a 20% restocking fee will apply. No returns accepted after 14 days.Shipping and handling costs are non-refundable, including a reasonable ship cost calculated into free US shipping best offer prices. \n\nAffirm Return Policy\nWhen paying with Affirm, the following return policies apply in addition to the seller\'s return policies: returns are accepted 7 days of receiving new items and 3 days of receiving used items. Read more on the Affirm FAQ.\n', 'shipping', '2016-06-08 07:45:53', '2016-06-17 07:32:03'),
('5757cd8e530cb', '5757cd8e518f5', 'Australia', 'JERRABATTCULLA', 'nsw', 'Shipping Policy\n\nShips from Miami, FL to:\nContinental U.S.$50.00\nEverywhere Else$200.00\nI will ship with tracking to the listed regions. To negotiate shipping rates to other locations, please send me a message.\n\nReturn Policy \nReturn Window\nThis product can be returned within 1 days of receipt.\n\nGeneral Terms \nItems must be returned in original, as-shipped condition with all original packaging and no signs of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n\nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges.', 'shipping', '2016-06-08 07:47:26', '2016-06-17 07:49:23'),
('5757ceb46e031', '5757ceb469e5a', 'Australia', 'WUNGHNU', 'vic', 'Shipping Policy\n\nAvailable for local pickup from Brooklyn, NYShips from Brooklyn, NY to:\nContinental U.S.$20.00\nEverywhere Else$35.00\n\nWe ship within 24 hours of purchase.We ship worldwide, For most countries international shipping for a guitar or bass is $175, but Australia is $200 and Canada is $100. Message for details or a price quote. Thanks.', 'shipping', '2016-06-08 07:52:20', '2016-06-17 08:37:58'),
('5757cf0b55efa', '5757cf0b5171f', 'Australia', 'WILBETREE', 'nsw', 'Shipping Policy\n\nShips from Mississauga, ON, CA to:\nContinental U.S. $69.00\nCanada $69.00\nAustralia $300.00\nCroatia $250.00\nDenmark $250.00\nFrance $250.00 \nGermany $250.00\nUnited Kingdom $250.00\n \nEverywhere Else $400.00\n \nMost Items will ship within one business day. We promptly package and ship all orders as soon as payment has cleared. All instruments are set up and inspected before shipping. We have extensive experience making sure that your instrument arrives at your doorstep in one piece and ready to play. All items are shipped from our warehouse in Ontario Canada. For orders placed outside of Canada, Please refer to your local Country policies to check if there are any additional fees. We do not include any brokerage, duties or taxes owing in our pricing and these cost vary. Most of the product we sell on Reverb are made in North America and are duty free but there may be a brokerage fee or other fees to clear the item into your Country. The buyer is responsible for these charges. It is always a good idea, when ordering items to look into import charges (Brokerage fees, duties and taxes). If a parcel is refused because of duties charges, we will refund the product price once we receive back the product less any associated fees for refusing the shipment. FMIC products only ship domestically which includes EVH, Charvel, Jackson Gretsch, Squier and Fender products. please contact us for more information.\n\nReturn Policy\nReturn Window\nThis product can be returned within 7 days of receipt.\n\nGeneral Terms\nItems must be returned in original, as-shipped condition with all original packaging and no\nsigns of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n\nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges and a 5% restocking fee.\n ', 'shipping', '2016-06-08 07:53:47', '2016-06-17 08:42:35'),
('5757cf25e9690', '5757cf25e884b', 'Australia', 'Perth', 'wa', 'Shipping Policy\n\nAvailable for local pickup\nfrom Mississauga, ON, CA\nShips from Mississauga, ON, CA to:\nUnited States\n$99.00\nCanada\n$99.00\nAustralia\n$400.00\nEverywhere Else\n$500.00\nMost Items will ship within one business day. We promptly package and ship all orders as soon as payment has cleared. All instruments are set up and inspected before shipping. We have extensive experience making sure that your instrument arrives at your doorstep in one piece and ready to play. All items are shipped from our warehouse in Ontario Canada. For orders placed outside of Canada, Please refer to your local Country policies to check if there are any additional fees. We do not include any brokerage, duties or taxes owing in our pricing and these cost vary. Most of the product we sell on Reverb are made in North America and are duty free but there may be a brokerage fee or other fees to clear the item into your Country. The buyer is responsible for these charges. It is always a good idea, when ordering items to look into import charges (Brokerage fees, duties and taxes). If a parcel is refused because of duties charges, we will refund the product price once we receive back the product less any associated fees for refusing the shipment. FMIC products only ship domestically which includes EVH, Charvel, Jackson Gretsch, Squier and Fender products. please contact us for more information.\n\nReturn Policy\nReturn Window\nThis product can be returned within 7 days of receipt.\n\nGeneral Terms\nItems must be returned in original, as-shipped condition with all original packaging and no signs of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n\nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges and a 5% restocking fee.', 'all', '2016-06-08 07:54:13', '2016-06-17 07:31:36'),
('5757d04d143d2', '5757d04d133a2', 'Australia', 'JEERALANG', 'vic', 'Shipping Policy\nAvailable for local pickup from Canton, OHShips from Canton, OH to:Continental U.S.$24.00\nEverywhere Else$99.00\n\nAll shipments will be shipped within 24 hours of payment being received. All shipments will be via UPS Ground, Fed Ex or US Post Office unless otherwise requested.', 'shipping', '2016-06-08 07:59:09', '2016-06-17 08:50:48'),
('5757d0a325a47', '5757d0a322708', 'Australia', 'HACKLINS CORNER', 'sa', 'Shipping Policy\n\nShips from Baltimore, MD to:\nContinental U.S. $10.00\nEverywhere Else $20.00\n\nI will ship with tracking to the listed regions. To negotiate shipping rates to other locations, please send me a message.\n \nReturn Policy\nReturn Window\nThis product can be returned within 14 days of receipt.\n\nGeneral Terms\nItems must be returned in original, as-shipped condition with all original packaging and no\nsigns of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n \nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges.\n ', 'shipping', '2016-06-08 08:00:35', '2016-06-17 08:41:39'),
('5757d1a573556', '5757d1a57267b', 'Australia', 'HACKLINS CORNER', 'sa', 'Shipping Policy\n\nShips from Brooklyn, NY to:\nContinental U.S. $30.00\nEverywhere Else $200.00\nI will ship with tracking to the listed regions. To negotiate shipping rates to other locations, please send me a message.\n \nReturn Policy\nReturn Window\nThis item is sold As-Is and cannot be returned.', 'shipping', '2016-06-08 08:04:53', '2016-06-17 07:13:38'),
('5757daf973827', '5757daf9705c1', 'Australia', 'WEST END', 'qld', 'Ships from Portland, OR to:Continental U.S.$45.00Everywhere Else$85.00All overseas shipments will automatically be insured for a maximum of $499 via USPS. Other services, carriers, and additional insurance are the responsibility and expense of the buyer. Some instruments, especially vintage keyboards, will require regular tuning, calibration and service upon receipt and installation. Routine service should be expected.', 'shipping', '2016-06-08 08:44:41', '2016-06-17 11:19:39'),
('5757db4944183', '5757db493aaa9', 'Australia', 'DEEBING HEIGHTS', 'qld', 'Shipping Policy\n\nShips from New York, NY to:\nContinental U.S. $0.00\nEverywhere Else $60.00\n\nI will ship with tracking to the listed regions. To negotiate shipping rates to other locations, please send me a message.\n \nReturn Policy\nReturn Window This item is sold As-Is and cannot be returned.', 'shipping', '2016-06-08 08:46:01', '2016-06-17 08:41:06'),
('5757dc6b1db2a', '5757dc6b1cc1d', 'Australia', 'PITJANTJATJARA HOMELANDS', 'nsw', 'Shipping Policy\nShips from Stevens Point, WI to:Continental U.S.$27.00\nEverywhere Else$69.00\n\nI will ship with tracking to the listed regions. To negotiate shipping rates to other locations, please send me a message.', 'shipping', '2016-06-08 08:50:51', '2016-06-17 07:18:39'),
('5757dc8030251', '5757dc802f480', 'Australia', 'DEEBING HEIGHTS', 'qld', 'Shipping Policy\n\nShips from Manchester, LAN, GB to:\nUnited States $7.40\nUnited Kingdom $0.00\nEverywhere Else $8.89\n\nI will ship with tracking to the listed regions. To negotiate shipping rates to other locations,\nplease send me a message.\n \nReturn Policy\nReturn Window\nThis product can be returned within 30 days of receipt.\n\nGeneral Terms\nItems must be returned in original, as-shipped condition with all original packaging and no\nsigns of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n \nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges.\n ', 'shipping', '2016-06-08 08:51:12', '2016-06-17 08:40:51'),
('5757ddc6b47cb', '5757ddc6b27e3', 'Australia', 'TALLANGATTA VALLEY', 'vic', 'Shipping Policy\n\nShips from Roslyn, NY to:\nContinental U.S.$0.00\nEverywhere Else$195.00\n\nMost items ship within one business day. Free shipping in the continental US on all orders over $100 with FedEx. Expedited freight is available for an additional charge. All items ship fully insured and professionally packed. If you receive an item that has any issue at all or any damaged not disclosed in the listing, please contact us immediately so we can fix the problem. We offer a 14 day money back guarantee on all new items and 72 hour approval on all used and vintage!\n\nReturn Policy\nReturn Window\nThis product can be returned within 7 days of receipt.General TermsItems must be returned in original, as-shipped condition with all original packaging and no signs of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described. \n\nRefunds \nBuyer receives a full refund in their original payment method less any shipping charges.\n\nSpecial Conditions\n14 Day Money Back Guarantee on all new items. 72 Hour Approval on all used and vintage items. We want you to be completely happy with your purchase from The Music Zoo!', 'shipping', '2016-06-08 08:56:38', '2016-06-17 08:40:30'),
('5757ddfe3a7bb', '5757ddfe391dd', 'Australia', 'BIGGARA', 'vic', 'Shipping Policy\n\nAvailable for local pickup from Miami, FLShips from Miami, FL to:Continental U.S.$0.00Everywhere Else$50.00\n\nTURNAROUND TIME 99% of orders ship within 24-48 Hours after you pay. If you pay on Friday, it will ship on Monday. All items are in stock at the time of the post. In the unlikely event an item is oversold, we will be glad to work with you to find an reasonable resolution for both parties.SERIAL DOWNLOAD ORDERS: Electronic Serial Number Will Be Delivered Within 24-48 Business Hours! All Download Orders must be sent into the Manufacturer for Processing, Business Hours for the majority of Manufacturers are Monday through Friday. Amazingly Fast Delivery!! We email the Download URL and Serial Authorization Number to you immediately after receiving it from the Manufacturer. No Refunds Accepted On SERIAL DOWNLOAD Software Orders. No Returns, No Refunds, No Shipping, No Box, Serial Download Software ONLY eDelivery.', 'shipping', '2016-06-08 08:57:34', '2016-06-17 07:39:29'),
('5757df1ea6b10', '5757df1ea5ddd', 'Australia', 'GREAT MACKEREL BEACH', 'nsw', 'Shipping Policy\n\nShips from Woodland Hills, CA to:\nContinental U.S. $25.00\nEverywhere Else $125.00\n\nI will ship with tracking to the listed regions. To negotiate shipping rates to other locations, please send me a message.\n \nReturn Policy\nReturn Window\nThis item is sold As-Is and cannot be\nreturned.', 'shipping', '2016-06-08 09:02:22', '2016-06-17 08:40:07'),
('5757e08c57828', '5757e08c5348a', 'Australia', 'GREAT MACKEREL BEACH', 'nsw', 'Shipping Policy\n\nShips from Saint Paul, MN to:\nContinental U.S.$20.00\nEverywhere Else $0.00\nI will ship with tracking to the listed regions. To negotiate shipping rates to other locations, please send me a message.\n\nReturn Policy\nReturn Window\nThis item is sold As-Is and cannot be returned.', 'shipping', '2016-06-08 09:08:28', '2016-06-17 08:33:07'),
('5757e1ec650f3', '5757e1ec63fb8', 'Australia', 'GREAT MACKEREL BEACH', 'nsw', 'Shipping Policy\n\nShips from Garden\nGrove, CA to:\nContinental U.S.\n$4.95\nEverywhere\nElse\n$7.95\nI will ship with tracking to the continental United States. Alaska, Hawaii, and international\nshipments may incur additional charges.\n\nReturn Policy\nReturnWindow\nThis product can be returned within 14 days of receipt.\n\nGeneral Terms\nItems must be returned in original, as-shipped condition with all original packaging and no\nsigns of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n \nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges.\n', 'shipping', '2016-06-08 09:14:20', '2016-06-17 08:32:39'),
('5757e3b04ef77', '5757e3b04e109', 'Australia', 'CHITTERING', 'wa', 'Shipping Policy\n\nShips from Lexington, MA to:\nContinental U.S.\n$50.00\nEverywhere Else\n$150.00\nThe Music Emporium has been shipping guitars around the world for 40+ years. Your instrument will be expertly packed and shipped via FedEx, UPS, or US Post, depending on the destination and level of service. If you have a preferred method not listed here, please let us know and we\'ll do our best to accommodate.\n\nReturn Policy\nReturn Window\nThis product can be returned within 3 days of receipt.\n\nGeneral Terms\nItems must be returned in original, as-shipped condition with all original packaging and no\nsigns of use. Buyer assumes responsibility for all return shipping costs unless\nthe item was not received as described.\n\nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges.\n\nSpecial Conditions\nMost instruments ship with a 48-hour approval period during which time you can comfortably evaluate your purchase and determine if it\'s right for you. The prices listed on our website do not include shipping charges or any applicable taxes, duties, or custom fees. Instruments shipped within MA are subject to a 6.25% sales tax. All out-of-state shipments are tax-free though we do recommend you look into your individual state\'s tax codes to determine any tax liabilities. Upon receiving your instrument, please inspect it to insure that the condition of the instrument is as described in your communication with us. Please contact us immediately via phone or email if you find any damage to the case or instrument that wasn\'t previously disclosed at time of purchase. If you decide to return the instrument, you must notify us via phone or email within 48 hours of delivery. We\'ll then issue a Return Authorization and instruct you on how to make the return. We will not receive any returned items that do not have a previously arranged Return Authorization. Please pack the instrument in the same manner in which it was received, using all supplied packing materials. Unless instructed otherwise, you must ship the instrument using the same service in which it was delivered (Next day, Ground etc.) and insure it for its full value. Once the instrument arrives, and provided it arrives in the condition in which it left our shop, The Music Emporium will issue a refund for the full purchase price less a 10% restocking fee for all credit card returns. This fee covers any and all charges we\'ve incurred in processing the transaction. All shipping charges will be the responsibility of the customer.There are instances where the approval period does not apply. If you\'ve played the instrument in our store and we then ship it to you, there is no approval period. If the item is a Sale Item (special discounted price, an item listed on our Specials Page), there is no approval period.\n ', 'shipping', '2016-06-08 09:21:52', '2016-06-17 08:32:24'),
('5757e50fdf4c5', '5757e50fde599', 'Australia', 'COOYA POOYA', 'wa', 'Shipping Policy\n\nShips from Emmen, DR, NL to:\nUnited States\n$249.00\nNetherlands\n$39.00\nEverywhere Else\n$249.00\nI will ship with tracking to the continental United States. Alaska, Hawaii, and international\nshipments may incur additional charges.\n\nReturn Policy \nReturn Window\nThis item is sold As-Is and cannot be returned.', 'shipping', '2016-06-08 09:27:43', '2016-06-17 08:31:57'),
('5757e6379ab1f', '5757e63794d19', 'Australia', 'BEAURY CREEK', 'nsw', 'Shipping Policy\n\nAvailable for local\npickup from Cupertino, CAShips from Cupertino, CA to:\nContinental U.S.\n$10.00\nEverywhere Else\n$35.00\n\nReturn Policy \nReturn Window\nThis item is sold As-Is and cannot be\nreturned.', 'shipping', '2016-06-08 09:32:39', '2016-06-16 03:25:11'),
('5757ff95c7d24', '5757ff95c2c54', 'Australia', 'WOOMBYE', 'qld', 'Shipping Policy\nAvailable for local pickup from Miami, FL\nShips from Miami, FL to:\nEverywhere Else $35.00\n\nTURNAROUND TIME \n99% of orders ship within 24-48 Hours after you pay. If you pay on Friday, it will ship on Monday. All items are in stock at the time of the post. In the unlikely event an item is oversold, we will be glad to work with you to find an reasonable resolution for both parties.\nSERIAL DOWNLOAD ORDERS: \nElectronic Serial Number Will Be Delivered Within 24-48 Business Hours! All Download Orders must be sent into the Manufacturer for Processing, Business Hours for the majority of Manufacturers are Monday through Friday. Amazingly Fast Delivery!! We email the Download URL and Serial Authorization Number to you immediately after receiving it from the Manufacturer. No Refunds Accepted On SERIAL DOWNLOAD Software Orders. No Returns, No Refunds, No Shipping, No Box, Serial Download Software ONLY eDelivery.\n\nReturn Policy\nReturn Window\nThis item is sold As-Is and cannot be returned.', 'all', '2016-06-08 11:20:53', '2016-06-17 06:52:44'),
('575802b3b9890', '575802b3b8829', 'Australia', 'BULL CREEK', 'wa', 'Shipping Policy\nAvailable for local pickup from Miami, FLShips from Miami, FL to:\nContinental U.S. $0.00 \nEverywhere Else $125.00\n\nTURNAROUND TIME\n 99% of orders ship within 24-48 Hours after you pay. If you pay on Friday, it will ship on Monday. All items are in stock at the time of the post. In the unlikely event an item is oversold, we will be glad to work with you to find an reasonable resolution for both parties.SERIAL\nDOWNLOAD ORDERS: Electronic Serial Number Will Be Delivered Within 24-48 Business Hours! All Download Orders must be sent into the Manufacturer for Processing, Business Hours for the majority of Manufacturers are Monday through Friday. Amazingly Fast Delivery!! We email the Download URL and Serial Authorization Number to you immediately after receiving it from the Manufacturer. No Refunds Accepted On SERIAL DOWNLOAD Software Orders. No Returns, No Refunds, No Shipping, No Box, Serial Download Software ONLY eDelivery.\nReturn PolicyReturn Window\nThis product can be returned within 14 days of receipt.\n\nGeneral Terms\nItems must be returned in original, as-shipped condition with all original packaging and no signs of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n\nRefunds Buyer \nreceives a full refund in their original payment method less any shipping charges.Special ConditionsWe will not accept returns on software. We will not accept returns on Opened Goods. If you have a Brand New item, we will accept a return as a credit towards another purchase ONLY. For a Return Authorization, please contact us via Messages. Most Items Are Subject To A Restocking Fee, depending on the Brand, Item, and Condition. Please Email for More Details. Refund will be given as Paypal credit after item has been received by Scitscat Music. Return shipping will be paid and handled by the buyer. Buyer must ship with either FedEx/UPS/USPS and include Tracking and Full Insurance. For more Return Policy Information, please visit our Website: www.scitscat.com', 'shipping', '2016-06-08 11:34:11', '2016-06-17 07:17:48'),
('5758046cba639', '5758046cb97f5', 'Australia', 'MELALEUCA', 'wa', 'Shipping PolicyShips from Brielle, ZH, NL to:United States $30.34 Netherlands $11.67 European Union $18.67 Everywhere Else $30.34I will ship with tracking to the listed regions. To negotiate shipping rates to other locations, please send me a message.Return PolicyReturn WindowThis product can be returned within 7 days of receipt.General TermsItems must be returned in original, as-shipped condition with all original packaging and no signs of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.RefundsBuyer receives a full refund in their original payment method less any shipping charges.', 'shipping', '2016-06-08 11:41:32', '2016-06-15 07:46:12'),
('575805fe8364f', '575805fe82044', 'Australia', 'SPIT JUNCTION', 'nsw', 'Available for local pickup from Cupertino, CA\nShips from Cupertino, CA to:\n- Continental U.S. $10.00\n- Everywhere Else $35.00\n\nReturn Policy\n\nReturn Window\nThis item is sold As-Is and cannot be returned.', 'shipping', '2016-06-08 11:48:14', '2016-06-16 05:42:22'),
('575807dc1c33b', '575807dc11832', 'Australia', 'TILBUSTER', 'nsw', 'Shipping PolicyShips from Columbia City, IN to:Continental U.S.$0.00GearNuts.com offers a no-nonsense shipping policy: fast, FREE shipping on nearly every item we sell. Combined with our low-price guarantee, that means your purchase is an even greater value. We ship Monday through Friday (except holidays). Our same-day shipping cutoff time is the latest in the business. Verified orders for in-stock merchandise will almost always ship the same day you order.For most items we use FedEx Ground for shipment, but we also use other carriers, such as UPS and USPS.Need Your Gear Sooner? Expedited Shipping Is Available! Send us a message and we can let you know the rate!Return PolicyReturn WindowThis product can be returned within 30 days of receipt.General TermsItems must be returned in original, as-shipped condition with all original packaging and no signs of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.RefundsBuyer receives a full refund in their original payment method less any shipping charges and a 5% restocking fee.Special ConditionsAt GearNuts.com, we\'re not happy until you\'re totally happy. If you\'re not completely satisfied with your purchase for any reason, you can return it to us within 30 days. When you return a product, you can request that it be replaced with an identical item, exchange it for a different item, or request a store credit or a refund. If the product you received is defective, we\'ll pay the shipping, both for you to return it to us and to send you a replacement. If you\'re returning a product for any other reason, you must pay for shipping. If your original purchase was eligible for free shipping, the original shipping cost will be deducted from your credit or refund', 'shipping', '2016-06-08 11:56:12', '2016-06-15 08:30:41'),
('5758bd7086f45', '5758bd7085fa4', 'Australia', 'GREEN PIGEON', 'nsw', 'Shipping Policy\n\nShips from Watertown,\nMA to:\nContinental U.S. $15.00\nEverywhere Else $30.00\n\nI will ship with tracking to the listed regions. To negotiate shipping rates to other locations,\nplease send me a message.\n\nReturn Policy\nReturn Window\nThis item is sold As-Is and cannot be returned.', 'shipping', '2016-06-09 00:50:56', '2016-06-17 08:37:25'),
('5758bea4402fb', '5758bea43f4c5', 'Australia', 'GLOUCESTER', 'nsw', 'Shipping Policy\n\nAvailable for local\npickup from Junction City, KSShips from Junction City, KS to:\nContinental U.S. $0.00\nEverywhere Else $0.00\nLocal Pickup only.\n \nReturn Policy\nReturn Window\nThis item is sold As-Is and cannot be returned.', 'shipping', '2016-06-09 00:56:04', '2016-06-17 07:06:15'),
('5758c093c7fab', '5758c093c6e68', 'Australia', 'NETHERBY', 'sa', 'Shipping Policy\nAvailable for local\npickup from Utica, MIShips from Utica, MI to:\nContinental U.S. $100.00\nEverywhere Else $120.00\n\nI will ship with tracking to the listed regions. To negotiate shipping rates to other locations,\nplease send me a message.\n \nReturn Policy\nReturn Window\nThis item is sold As-Is and cannot be returned.', 'shipping', '2016-06-09 01:04:19', '2016-06-17 07:05:46'),
('5758c0e6e7ef8', '5758c0e6e6e5a', 'Australia', 'BIGGARA ', 'vic', 'Shipping Policy\n\nShips from Portland, OR to:Continental U.S.$45.00\nEverywhere Else$85.00\n\nAll overseas shipments will automatically be insured for a maximum of $499 via USPS. Other services, carriers, and additional insurance are the responsibility and expense of the buyer. Some instruments, especially vintage keyboards, will require regular tuning, calibration and service upon receipt and installation. Routine service should be expected.', 'shipping', '2016-06-09 01:05:42', '2016-06-17 07:39:13'),
('5758c1bf78962', '5758c1bf73ef7', 'Australia', 'GLENORAN ', 'wa', 'Shipping Policy\n\nShips from Paris, J, FR to:\nFrance\n$14.93\nEuropean Union\n$17.92\nEverywhere Else\n$29.87\nI will ship worlwide, tracking and insurance included.\n \nReturn Policy\nReturn Window\nThis item is sold As-Is and cannot be returned.', 'shipping', '2016-06-09 01:09:19', '2016-06-17 07:05:14'),
('5758c1da62724', '5758c1da5f662', 'Australia', 'BIGGARA ', 'vic', 'Shipping Policy\nShips from Otterville, ON, CA to:United States$84.85Canada$44.44\nEverywhere Else$202.02\n\nI will ship with tracking to the listed regions. To negotiate shipping rates to other locations, please send me a message.', 'shipping', '2016-06-09 01:09:46', '2016-06-17 07:39:18'),
('5758c2ab55518', '5758c2ab5275a', 'Australia', 'BEAURY CREEK', 'nsw', 'Shipping Policy\n\nAvailable for local\npickup from Smyrna, GAShips from Smyrna, GA to:\nContinental U.S. $0.00\nCanada $24.99\nEverywhere Else $34.99\n\nMegatone Music uses USPS Priority or First Class on all Domestic shipments. On Guitars, Basses, Amps and Pro Audio we use either FedEx or UPS. All items ship within 24-48\nbusiness hours of payment clearing. Tracking will be available and emailed within that time frame. We do not offer free shipping on used or refurbished items unless free shipping is noted on the listing. Most new products over $99.00 include Free Shipping.\n\nInternational Customers:\nWe do not ship International orders for free. I\'m sorry but it\'s to costly. Megatone Music strives to provide our international customers with the most affordable shipping. Our rates are extremely competitive and we\'ll work with you the best we can. All shipments are insured and tracked. We offer combined shipping on multiple item purchases, please email us before you order for a quote. You can save a lot of money on shipping when purchasing multiple items!\nMost additional items will ship at half of that item\'s posted shipping rate. Many countries impose their own customs, duties, tariffs, taxes on items being shipped from the US. These costs are the buyers responsibility. For confirmation of rates or any additional information on International shipping rates available please email us before you purchase.\n \nReturn Policy \nReturn Window\nThis product can be returned within 14 days of receipt.\n\nGeneral Terms\nItems must be returned in original, as-shipped condition with all original packaging and no\nsigns of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n\nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges.\n\nSpecial Conditions\nOn items that have been discounted from MAP prices are not subject for return. This is the trade off for receiving additional discount, On items we ship for free customer will be responsible for paying any shipping fees or charges we incur.\n ', 'shipping', '2016-06-09 01:13:15', '2016-06-17 08:35:51'),
('5758c4d2d986e', '5758c4d2d83fa', 'Australia', 'BIGGARA ', 'vic', 'Shipping Policy\nShips from Reseda, CA to:Continental U.S.$99.99\nEverywhere Else$545.00\n\nI will ship with tracking to the listed regions. To negotiate shipping rates to other locations, please send me a message.', 'shipping', '2016-06-09 01:22:26', '2016-06-17 07:37:43'),
('5758c544639e9', '5758c5446299f', 'Australia', 'Perth', 'wa', 'Shipping Policy\n\nAvailable for local pickup\nfrom West Bridgewater, MA\nShips from West Bridgewater, MA to:\nContinental U.S.\n$0.00\nEverywhere Else\n$200.00\n\nShipping Return Policy\nReturn Window\nThis product can be returned within 7 days of receipt.\n\nGeneral Terms\nItems must be returned in original, as-shipped condition with all original packaging and no signs of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n\nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges and a 10% restocking fee.\n\nSpecial Conditions\nTracking information for all returns must be provided to us within 48 hours of requesting the return. \n', 'all', '2016-06-09 01:24:20', '2016-06-17 08:28:30'),
('5758c5b1dcabf', '5758c5b1dab96', 'Australia', 'BIGGARA ', 'vic', 'Shipping Policy\n\nShips from Denton, TX to:Continental U.S.$16.00\nEverywhere Else$30.00\n\nI will ship with tracking to the listed regions. To negotiate shipping rates to other locations, please send me a message.', 'shipping', '2016-06-09 01:26:09', '2016-06-17 07:49:13'),
('5758c64e839d6', '5758c64e75d1d', 'Australia', 'DEER PARK EAST', 'vic', 'Shipping Policy\n\nShips from Salisbury, MD to:\nContinental U.S.\n$0.00\nEverywhere Else\n$55.00\nI will ship with tracking to the continental United States. Alaska, Hawaii, and international\nshipments may incur additional charges.\n\nReturn Policy \nReturn Window\nThis product can be returned within 14 days of receipt. \n\nGeneral Terms \nItems must be returned in original, as-shipped condition with all original packaging and no signs of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n\nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges.\n ', 'shipping', '2016-06-09 01:28:46', '2016-06-17 09:32:13'),
('5758c6eb51202', '5758c6eb4e101', 'Australia', 'Biggara', 'vic', 'Shipping Policy\nShips from Portland, OR to:Continental U.S.$80.00Canada$145.00\nEverywhere Else$225.00\n\nAll overseas shipments will automatically be insured for a maximum of $499 via USPS. Other services, carriers, and additional insurance are the responsibility and expense of the buyer. Some instruments, especially vintage keyboards, will require regular tuning, calibration and service upon receipt and installation. Routine service should be expected.', 'shipping', '2016-06-09 01:31:23', '2016-06-17 06:29:52');
INSERT INTO `gp_products_shipping` (`product_shipping_id`, `product_id`, `country`, `city`, `state`, `policy`, `method`, `created_at`, `updated_at`) VALUES
('5758c715bbf76', '5758c715baf3b', 'Australia', 'MERRIWA', 'nsw', 'Shipping Policy\n\nAvailable for local\npickup from San Rafael, CA\nShips from San Rafael, CA to:\nContinental U.S.\n$25.00\nAbout Shipping and\nDelivery \nWe utilize legitimate bonded and licensed shipping carriers only (UPS, Fed Ex,\nUSPS etc). We are not the shipping company and therefore cannot be held\nresponsible for faults that lay with the shipping company. By placing orders\nwith us you agree to be present to receive packages sent to you. Bananas cannot\nbe held responsible for lost packages once they are logged as delivered by the\nshipping company. Once a package is logged as delivered by the shipping company\nthe customer is responsible for any issues related to the delivery of the\npackage at that point.\n \nReturn Policy\nBananas Sales and 30 Day Return Policies \nIn order to return a product, Request a Return Authorization Number (RA Number)\nReturn and Customer\n\nSatisfaction Policy: \nAny item (other than as noted below) purchased from Bananas at Large may be\nreturned, within 30 days of the shipment date, for a full refund of the\npurchase price minus shipping costs, or for exchange or credit. Rather than\nmake you click elsewhere for the \'small print\', here it is in not-so-small\nprint:\nThe following items\nmay not be returned: \n• Consumable items such as (but not limited to) strings, picks, drum sticks,\ndrum heads, phono cartridges and replacement speakers. \n• Any item that is worn on the body or is blown into, such as (but not limited\nto) earplugs, wind controllers or harmonicas \n• Computer software, items that can be copied and copyrighted material such as\n(but not limited to) books, videos and CD\'s \n• Installed items such as (but not limited to) guitar pickups, drum heads,\nheadphone parts and other installed parts \n• If you agree to back order or special order a product, you agree that order\nmay not be canceled unless approved by Bananas. Special orders already in\ntransit either from manufacturer or Bananas cannot be canceled. Special order\nitems may be returned if defective for exchange. You will be notified what\nitems qualify as special order status. \nAn Item is considered new if it has not been sold previously, has not been out\nof the store on loan, shows no signs of wear and tear. We open and inspect\nalmost every instrument that comes thru our doors. Often original packaging may\nnot be reused. Just because an item is not shipped in original packaging does\nnot mean it is used. If you are concerned and only want sealed units, please\ncontact us before buying.\n\nOther Things You Need to Know about Returns \nShipping and/or handling charges are not refundable. It is your responsibility\nto pay return shipping and to fully insure the unit(s) you are returning. Also,\nit is your responsibility to cover shipping charges for sending the item to\nyou. If you are quoted free shipping and return the item, you will be required\nto cover the actual shipping costs incurred by your order. Items that are\nreturned must be in the original packaging, with all accessories and paperwork\nincluded in the original shipment, such as owner\'s manual(s) and warranty card.\nItems must be free from signs of use or wear, and both warranty card and\nmanual(s) must not have any writing in them. If your order was shipped to you\nin outer packaging it must be returned in that packaging as well as the inner\nbox or boxes. Returned items that do not arrive in the condition described\nabove are subject to a $25 repackaging charge, plus the cost of replacing\nmissing, soiled, worn or marked-on items. Obvious signs of use that reduce the\nvalue of a returned unit will be subject to additional charges based on how\nmuch we need to mark down the unit to make it sellable.\n \nAbout Shipping and Delivery \nWe utilize legitimate bonded and licensed shipping carriers only (UPS, Fed Ex,\nUSPS etc). We are not the shipping company and therefore cannot be held\nresponsible for faults that lay with the shipping company. By placing orders\nwith us you agree to be present to receive packages sent to you. Bananas cannot\nbe held responsible for lost packages once they are logged as delivered by the\nshipping company. Once a package is logged as delivered by the shipping company\nthe customer is responsible for any issues related to the delivery of the\npackage at that point.\n \nHow Refunds are Processed \nCharges made to a credit card will be refunded, less freight, handling and any\nreconditioning charges, to the same credit card. Payments made by check will be\nrefunded with a check, after sufficient time has passed for your original check\nto completely clear through the banking system.\nNote: All returns and\nrefunds are at the discretion of Bananas at Large management. Damaged,\nobviously used items or abuse of the return privileges may be grounds for a refusal\nof refunds or returns. We will not facilitate free rentals via our return\npolicy.\n \nPrices and Specifications are Subject to Change \nBananas at Large always strives for accuracy in the information and pricing we\npresent. However, we are not responsible for typographical errors, incorrect\nspecifications furnished by manufacturers, or for price changes. Prices are\nsubject to change without notice.\n \nPackaging and\nShipping Information \nDue to conditions beyond our control (weather, shipper\'s handling etc.) some\nboxes that products originally come in cannot be re-used to ship out to\ncustomers. In those rare cases we will remove and inspect all items and\nrepackage them in acceptable boxing to make it safe to you. All items are still\nconsidered brand new, under full warranty and fall under our same guarantees.\nNote: Bananas recycles unused boxes when possible\n ', 'shipping', '2016-06-09 01:32:05', '2016-06-17 08:47:37'),
('5758c7d1791af', '5758c7d1761aa', 'Australia', 'MOUNT GEORGE ', 'sa', 'Shipping Policy\n\nShips from Portland, OR to:\nContinental U.S. $4.00\nEverywhere Else $14.00\n \nAll overseas\nshipments will automatically be insured for a maximum of $499 via USPS. Other\nservices, carriers, and additional insurance are the responsibility and expense\nof the buyer. Some instruments, especially vintage keyboards, will require\nregular tuning, calibration and service upon receipt and installation. Routine\nservice should be expected.\n\nReturn Policy\nReturn Window\nThis product can be returned within 3 days of receipt.\n\nGeneral Terms\nItems must be returned in original, as-shipped condition with all original packaging and no\nsigns of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n \nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges and a 10% restocking fee.\n \nSpecial Conditions\nReturns are accepted by prior arrangement only. Items shipped internationally are not eligible for return. Vintage electronics and keyboards, because of their uniquely fragile nature, are not eligible for return. Routine service of internal electrical components is frequently required after shipment. For all returns, shipping fees are non-refundable, and return shipping is the sole responsibility of the buyer. Thanks!\n ', 'shipping', '2016-06-09 01:35:13', '2016-06-17 08:46:14'),
('5758c849f1ac8', '5758c849f0a87', 'Australia', 'Biggara', 'vic', 'Shipping Policy\nShips from Stevens Point, WI to:Continental U.S.$390.00\n\nEverywhere Else$1,260.00I will ship with tracking to the listed regions. To negotiate shipping rates to other locations, please send me a message.', 'shipping', '2016-06-09 01:37:13', '2016-06-17 06:29:29'),
('5758c87e27745', '5758c87e25bc4', 'Australia', 'BANKSIA', 'nsw', 'Shipping Policy\n\nAvailable for local\npickup from Millersport, OHShips from Millersport, OH to:\nContinental U.S. $3.95\nCanada $9.95\nUnited States $3.95\n\nEverywhere Els $14.95\nI will ship USPS with tracking.\n \nReturn Policy \nReturn Window\nThis product can be returned within 14 days of receipt.\n\nGeneral Terms\nItems must be returned in original, as-shipped condition with all original packaging and no\nsigns of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n\nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges.\n ', 'shipping', '2016-06-09 01:38:06', '2016-06-17 08:46:42'),
('5758c93f1b277', '5758c93f1835c', 'Australia', 'CLIFTON', 'nsw', 'Shipping Policy\nShips from Chicopee, MA to:\nContinental U.S. $4.99\nUnited States $4.99\nEverywhere Else $12.99\n\nWE COMBINE SHIPPING - EVEN IF YOU ACCIDENTALLY OVER-PAY FOR SHIPPING, WE WILL REFUND YOU THE DIFFERENCE!\nOne Day or LESS\nHandling Time Domestic & International buyers can choose USPS First Class Mail or USPS Priority Mail. USPS First Class Mail\nis our default shipping method. Other options available only by request.\n\nRates:\n  - For Domestic (USA)\nbuyers shipping for the first tube, pair, or quad will cost $4.99 + $1.00 for\neach additional tube, pair, or quad.\n - For International\nbuyers, shipping for the first tube, pair, or quad will cost $12.99 + $2.00 for\neach additional tube, pair, or quad.\n\n- Typical transit\ntimes:\nDomestic USPS First\nClass Mail: 3-5 Days, includes tracking.\nDomestic USPS\nPriority Mail: 1-3 Days, includes tracking.\nInternational USPS\nFirst Class Mail: 3-6 weeks but up to 3 months! - NO TRACKING.\nInternational USPS Priority\nMail: 6-10 Days - Includes Tracking.\n\nINTERNATIONAL CUSTOMERS - READ \nALL international orders will be shipped by USPS First Class Mail International by default, which is the cheapest method at $9.99. This shipping service is very cost-effective, but you will not be able to track your package. It usually takes 3-6 weeks for a First Class Mail International package to arrive, but can take UP TO 3 MONTHS FOR DELIVERY.\nWe guarantee all of our shipments, but we do ask that our customers wait 3 months before we grant a full refund due to a lost package. We have shipped thousands of items internationally using First Class Mail and while we have had plenty take months to arrive, we have almost zero packages ever go missing. \n\n - DEPENDING ON YOUR COUNTRY\'S POLICIES, YOU MAY BE RESPONSIBLE FOR IMPORT TAXES\n - WHEN ORDERING OVERSEAS FROM US. WE ARE NOT RESPONSIBLE FOR ANY TAXES, DUTIES,\n - OR CUSTOMS FEES. WE WILL NOT FALSIFY THE VALUE OF AN ITEM TO AVOID CUSTOMS\n - TAXES. DOING SO IS ILLEGAL AND CAN RESULT IN SEIZURE OF YOUR ITEM.\n\nReturnPolicy\nReturn Window\nThis product can be returned within 30 days of receipt.General Terms\nItems must be returned in original, as-shipped condition with all original packaging and no\nsigns of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n\nRefunds\nBuyer receives a full\nrefund in their original payment method less any shipping charges.\nSpecial Conditions\nAll of our tubes are\n100% guaranteed to arrive as described, test as described and to be void of any\nsound defects including microphonics and noise. We offer a 30 day return window\nfrom the day you receive the item. PLEASE NOTE - While we accept returns for\nbasically any fault, we DO NOT accept returns for buyer\'s remorse which includes\nbuyers who decide they don\'t like the subjective tonal signature of a tube. We\nhave had people exploit our generous return policy to tube roll on our dime so\nwe have a strict policy against this behavior.\n ', 'shipping', '2016-06-09 01:41:19', '2016-06-17 08:44:40'),
('5758c96ca09fa', '5758c96c9fa9d', 'Australia', 'Biggara', 'vic', 'Shipping Policy\nShips from Los Angeles, CA to:Continental U.S.$0.00\nEverywhere Else$30.00\n\nFree shipping in the US, for international buyer pays face value. Contact me for international shipping before buying. Local pickup in LA.', 'shipping', '2016-06-09 01:42:04', '2016-06-17 07:36:28'),
('5758ca24cc85f', '5758ca24ca553', 'Australia', 'Biggara', 'vic', 'Shipping Policy\nShips from London, LND, GB to:United Kingdom$7.47\nEverywhere Else$44.80\n\nI will ship with tracking to the listed regions. To negotiate shipping rates to other locations, please send me a message.', 'shipping', '2016-06-09 01:45:08', '2016-06-17 07:36:29'),
('5758ca30e119b', '5758ca30e00fa', 'Australia', 'HILLARYS', 'sa', 'Shipping Policy\nAvailable for local pickup from Berkeley, CAShips from Berkeley, CA to:\nContinental U.S.\n$100.00\nEverywhere Else\n$200.00\nI will ship with tracking to the listed regions. To negotiate shipping rates to other locations,\nplease send me a message.\n \nReturn Policy\nReturn Window\nThis product can be returned within 7 days of receipt.\n\nGeneral Terms \nItems must be returned in original, as-shipped condition with all original packaging and no signs of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n\nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges.\n ', 'shipping', '2016-06-09 01:45:20', '2016-06-17 08:31:30'),
('5758cb12ba6fc', '5758cb12b97cf', 'Australia', 'Biggara', 'vic', 'Shipping Policy\nAvailable for local pickup from ,Ships from , to:Continental U.S.$60.00\nI will ship with tracking to the listed regions. To negotiate shipping rates to other locations, please send me a message.', 'shipping', '2016-06-09 01:49:06', '2016-06-17 07:24:10'),
('5758cc54504e0', '5758cc544f496', 'Australia', 'MONTUMANA', 'tas', 'Shipping Policy \nShips from Irmo, SC to:\nContinental U.S. $7.99\nAlaska $16.99\nHawaii $16.99\nOther U.S. Territories $19.99\nPuerto Rico $19.99\nAsia $49.99\nEuropean Union $49.99\nCanada $39.99\nEverywhere Else $59.99\n\nI will ship with tracking to the listed regions. To negotiate shipping rates to other locations, please send me a message.\n\nReturn Policy \nReturn Window\nThis product can be returned within 14 days of receipt.\n\nGeneral Terms \nItems must be returned in original, as-shipped condition with all original packaging and no signs of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n\nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges and a 5% restocking fee.\n\nSpecial Conditions\nInternational Fees, Taxes, and Restrictions: Please keep in mind that your government or another entity in your country may impose a charge for custom and/or brokerage fees, duties and taxes on items received from the US. These charges do not originate from Front End Audio nor do we benefit from them in any way. You are responsible for payment of all custom and brokerage fees, duties and taxes that may be imposed when these goods are imported into your country. Since these fees will vary from country to country, we do not know what your fees will be and advise you to contact your local government customs office to determine the rate for the products you would like to purchase.\n ', 'shipping', '2016-06-09 01:54:28', '2016-06-17 08:31:12'),
('5758ce269d0a5', '5758ce269848a', 'Australia', 'BANDON', 'nsw', 'Shipping Policy\n\nAvailable for local pickup from Southbridge, MAShips from Southbridge, MA to:\nContinental U.S. $20.00\nEverywhere Else $40.00\n\nUpon receiving cleared payment we will ship via UPS Ground or USPS Priority Mail anywhere in the continental United States for the flat rate listed and will provide tracking information upon shipment. For any other location, please contact us for a shipping quote.  We also offer local pickup at our storefront in East Hartford, CT.\n \nReturn Policy\nReturn Window\nThis product can be returned within 14 days of receipt.\n\nGeneral Terms \nItems must be returned in original, as-shipped condition with all original packaging and no signs of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n \nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges.\n \nSpecial Conditions\nWe accept returns within 14 days of delivery. Please inspect gear when it is received and call as soon as possible to report any damage, and we will notify shipper to process the claim. All other returns are subject to a 25% restocking fee. Customer is responsible for return shipping\ncharges and must provide tracking information.\n ', 'shipping', '2016-06-09 02:02:14', '2016-06-17 08:30:53'),
('5758cef0f2038', '5758cef0d16bc', 'Australia', 'PEAKHURST HEIGHTS', 'nsw', 'Shipping Policy\nShips from Carnation, WA to:\nContinental U.S. $0.00\nEverywhere Else $0.00\n\nI will pay shipping and insurance within CONUS only. Other than ground shipping will be negotiated at time of purchase. Please send me a message.\n \nReturn Policy\nReturn Window\nThis product can be returned within 7 days of receipt.\n\nGeneral Terms\nItems must be returned in original, as-shipped condition with all original packaging and no signs of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n \nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges.\n ', 'shipping', '2016-06-09 02:05:36', '2016-06-17 08:30:37'),
('5758d023bc209', '5758d023ba0f3', 'Australia', 'ALLAMBIE', 'nsw', 'Shipping Policy\n\nShips from Shibuya, 13, JP to:\nJapan$0.00\nAfrica$250.08\nAsia$96.18\nEurope (Non-EU)$153.89\nEuropean Union$153.89\nNorth America$134.66\nSouth America$250.08\nAustralia$134.66\nBahrain$134.66\nBrazil$250.08\nIsrael$134.66\nNew Zealand$134.66\nRussian Federation$153.89\nTurkey$134.66\nUnited Arab Emirates$134.66\nUnited Kingdom$153.89\nFor international shipment we use EMS (Express Mail Service) provided by the International Postal Service if your choice of carrier is not specified.  EMS is safe, fast (approx. 7 to 10 days worldwide), economic and of course fully insured. Usually, it is door to door service. In case our customers (that being you) wish to return the item after receipt of goods & have a change of heart, all shipping, insurance and such related charges are your responsibility. If you have a preferred choice of courier (which you have your own account for) and would like to use that courier, you may talk to one of our International Sales Agent directly. Please note that we do not ship our merchandises by vessels at sea, only by air. If you are located in Japan, we offer free shipping for domestic customers.\n\nReturn Policy\nReturn Window\nThis product can be returned within 7 days of receipt.\n\nGeneral Terms\nItems must be returned in original, as-shipped condition with all original packaging and no signs of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n\nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges.\n\nSpecial Conditions\nWe can accept returns under the following circumstances;\n - What you received does not match the information our international sales staff provided you with. \n -  Damage caused during shipping. Please check for any damage to your package before signing for your shipment and if you find any, report it to the shipping company immediately.  \n -  The product you received is defective. \n -  You received a different item to what our staff\ninformed you they were shipping. In the case of any of these reasons, please email our staff within 24 hours and make the subject of your email \"Return request\". Please be as specific with the details as possible. Do not discard any of the packaging, manuals, papers, parts or accessories the item has been supplied with, and in particular, please do not remove protective film from any of the instruments pickups, pickguards, control plates, etc. A full refund of the purchase price (minus bank costs, PayPal fees, shipping costs, duty and tax) of the goods or exchange will be made once the goods are returned to us in the same condition as when they left our premises. For any other reason, we are not able to accept returns and cancellations.\n ', 'shipping', '2016-06-09 02:10:43', '2016-06-17 08:30:24'),
('5758d1b39f5c7', '5758d1b39dd8a', 'Australia', 'LIDSDALE', 'nsw', 'Shipping Policy\n\nShips from Shibuya, 13, JP to: \nJapan\n$0.00\nAfrica\n$250.08\nAsia\n$96.18\nEurope (Non-EU)\n$153.89\nEuropean Union\n$153.89\nNorth America\n$134.66\nSouth America\n$250.08\nAustralia\n$134.66\nBahrain\n$134.66\nBrazil\n$250.08\nIsrael\n$134.66\nNew Zealand\n$134.66\nRussian Federation\n$153.89\nTurkey\n$134.66\nUnited Arab Emirates\n$134.66\nUnited Kingdom\n$153.89\n\nFor international shipment we use EMS (Express Mail Service) provided by the International Postal Service if your choice of carrier is not specified.  EMS is safe, fast (approx. 7 to 10 days worldwide), economic and of course fully insured. Usually, it is door to door service.\nIn case our customers (that being you) wish to return the item after receipt of goods & have a change of heart, all shipping, insurance and such related charges are your responsibility.\nIf you have a preferred choice of courier (which you have your own account for) and would like to use that courier, you may talk to one of our International Sales Agent directly. Please note that we do not ship our merchandises by vessels at sea, only by air. If you are located in Japan, we offer free shipping for domestic customers.\n\nReturn Policy\nReturn Window \nThis product can be returned within 7 days of receipt.\n\nGeneral Terms \nItems must be returned in original, as-shipped condition with all original packaging and no signs of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n \nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges.\n \nSpecial Conditions\nWe can accept returns under the following circumstances; \n - What you received does not match the information our international sales staff provided you with. \n - Damage caused during shipping. Please check for any damage to your package before signing for your shipment and if you find any, report it to the shipping company immediately.  \n -  The product you received is defective. \n -  You received a different item to what our staff informed you they were shipping. In the case of any of these reasons, please email our staff within 24 hours and make the subject of your email \"Return request\". Please be as specific with the details as possible. Do not discard any of the packaging, manuals, papers, parts or accessories the item has been supplied with, and in particular, please do not remove protective film from any of the instruments pickups, pickguards, control plates, etc. A full refund of the purchase price (minus bank costs, PayPal fees, shipping costs, duty and tax) of the goods or exchange will be made once the goods are returned to us in the same condition as when they left our premises. For any other reason, we are not able to accept returns and cancellations.\n ', 'shipping', '2016-06-09 02:17:23', '2016-06-17 07:18:20'),
('5758d3628a143', '5758d36284ff2', 'Australia', 'THEBARTON', 'sa', 'Shipping Policy\n\nShips from Brookfield, CT to:\nContinental U.S. $55.00\nEverywhere Else $200.00\nWe offer shipping via FedEx (domestic) or USPS(international)\n\nReturn Policy\nReturn Window This product can be returned within 7 days of receipt.\n\nGeneral Terms \nItems must be returned in original, as-shipped condition with all original packaging and no signs of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n\nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges.\n\nSpecial Conditions \nYou can return your purchase from The Guitar Hangar in its original condition for an exchange up to 7 Days from receipt of merchandise. It must be insured for the full value and all paperwork, hang tags original boxes etc... must be returned intact. There are no cash or credit card refunds. Special Orders, Pre orders, layaways, consignments, guitars sold below asking price at offered prices and final sale items are non cancelable and non returnable. Shipping, credit card or paypal fees and deposits for special orders or layaways are non refundable. Any shipping damage must be filed with the carrier within 24hrs of receipt of the merchandise and dealt with in accordance with their policies. All returns must be arranged in advance with the Guitar Hangar and given a return authorization number. Any return after 7 days is subject to a 25% restocking fee or may be refused depending on the situation. The Guitar Hangar is not responsible for typographical errors posted on the website. Now with all THAT out of the way, rest assured that we are here to help you get the gear of your dreams, not to turn your dream into a nightmare. Please let us know if there is anything we can do to make your experience with us a better one. Check out our Testimonial Page for some comforting feedback from some of our many customers! I will do my very best every time. You have it in writing! If you have ANY questions about any of this, call me personally. 203 740 8889 Rick Tedesco.\n ', 'shipping', '2016-06-09 02:24:34', '2016-06-17 07:17:41'),
('5758d4e5ced31', '5758d4e5cd685', 'Australia', 'BIG JACKS CREEK', 'nsw', 'Shipping Policy \n\nShips from Roslyn, NY to:\nContinental U.S. $0.00\nEverywhere Else $195.00\n\nMost items ship within one business day. Free shipping in the continental US on all orders over $100 with FedEx. Expedited freight is available for an additional charge. All items ship fully insured and professionally packed. If you receive an item that has any issue at all or any damaged not disclosed in the listing, please contact us immediately so we can fix the problem. We offer a 14 day money back guarantee on all new items and 72 hour approval on all used and vintage!\n \nReturn Policy\nReturn Window This product can be returned within 14 days of receipt.\n\nGeneral Terms\nItems must be returned in original, as-shipped condition with all original packaging and no signs of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n\nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges. Special Conditions 14 Day Money Back Guarantee on all new items. 72 Hour Approval on all used and vintage items. We want you to be completely happy with your purchase from The Music Zoo!\n ', 'shipping', '2016-06-09 02:31:01', '2016-06-17 07:17:00'),
('5758d7724ae61', '5758d77249ebb', 'Australia', 'WARRIEWOOD', 'nsw', 'Shipping Policy\n\nAvailable for local pickup from Livermore, CAShips from Livermore, CA to:\nContinental U.S. $50.00\nEverywhere Else $200.00\n\nI will ship with tracking to the continental United States. Alaska, Hawaii, and international shipments may incur additional charges.\n\nReturn Policy \nReturn Window \nThis product can be returned within 3 days of receipt.\n\nGeneral Terms \nItems must be returned in original, as-shipped condition with all original packaging and no signs of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n\nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges.\n ', 'shipping', '2016-06-09 02:41:54', '2016-06-17 07:16:10'),
('5758da2901be2', '5758da28f2dcc', 'Australia', 'SUTHERLAND', 'nsw', 'Shipping Policy\n\nShips from Kitaooji, 26, JP to:\nUnited States\n$24.00\nJapan\n$10.00\nEuropean Union\n$36.00\nAustralia\n$24.00\nCanada\n$24.00\nEverywhere Else\n$54.00 \n\nReturn Policy\nReturn Window\nThis product can be returned within 7 days of receipt.\n\nGeneral Terms \nItems must be returned in original, as-shipped condition with all original packaging and no signs of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n\nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges.\n ', 'shipping', '2016-06-09 02:53:29', '2016-06-17 07:15:42'),
('5758db44b5212', '5758db44b4238', 'Australia', 'MOUNT HOOGHLY', 'vic', 'Shipping Policy\n\nAvailable for local pickup from Chicago, IL Ships from Chicago, IL to:\nContinental U.S.\n$10.00\nAlaska\n$14.00\nHawaii\n$14.00\nOther U.S. Territories\n$14.00\nCanada\n$18.00\nEverywhere Else\n$27.00\nWe\'re happy to ship\nanywhere in the world. Most packages will be sent USPS with tracking unless\nrequested otherwise.\n\nReturn Policy\nReturn Window\nThis product can be returned within 7 days of receipt.\n\nGeneral Terms\nItems must be returned in original, as-shipped condition with all original packaging and no\nsigns of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n \nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges.\n ', 'shipping', '2016-06-09 02:58:12', '2016-06-17 08:25:08'),
('5758dc482082a', '5758dc480d86d', 'Australia', 'BANDA BANDA', 'nsw', 'Shipping Policy\n\nI will ship with tracking to the listed regions. To negotiate shipping rates to other locations, please send me a message.', 'shipping', '2016-06-09 03:02:32', '2016-06-16 03:37:32'),
('5758dc526137e', '5758dc525fc7d', 'Australia', 'MOORE', 'qld', 'Shipping Policy\n\nAvailable for local pickup from Brooklyn, NYShips from Brooklyn, NY to:\nContinental U.S.\n$50.00\nEverywhere Else\n$175.00\nWe ship within 24 hours of purchase. We ship worldwide, For most countries international\nshipping for a guitar or bass is $175, but Australia is $200 and Canada is $100. Message for details or a price quote. Thanks.\n\nReturn Policy\nReturn Window\nThis product can be returned within 3 days of receipt.\n\nGeneral Terms\nItems must be returned in original, as-shipped condition with all original packaging and no signs of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n \nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges.\n \nSpecial Conditions\nAll of our guitars come with a 48 hour approval period. The approval period is for the buyer to make sure that the guitar is in satisfactory condition and authentic. No returns will be accepted just because the buyer doesn’t like the way an instrument plays or sounds or other personal preferences. The buyer must notify us that they want to return the item within\n48 hours of receiving the guitar. All returns must be approved by us. Upon safe return of the guitar we will refund the original purchase price minus a 5% restocking fee. The restocking fee will be waived if the original payment was made in cash, or by bank wire transfer. Shipping expenses will not be returned. All amplifiers and effects and other accessories are sold AS IS and no refunds will be issued.\n ', 'shipping', '2016-06-09 03:02:42', '2016-06-17 08:24:38'),
('5758dd33c96ac', '5758dd33c86c2', 'Australia', 'COLINROOBIE', 'nsw', 'Shipping Policy \n\nShips from Edmonds, WA to:\nContinental U.S.\n$150.00\nEverywhere Else\n$400.00\n\nReturn Policy\nReturn Window\nThis product can be returned within 7 days of receipt.\n\nGeneral Terms\nItems must be returned in original, as-shipped condition with all original packaging and no\nsigns of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n \nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges.\n ', 'shipping', '2016-06-09 03:06:27', '2016-06-17 08:24:11'),
('5758dd91cd15a', '5758dd91cc13a', 'Australia', 'BANDA BANDA', 'nsw', 'Shipping Policy\n\nAvailable for local pickup from Portsmouth, NHShips from Portsmouth, NH to:Continental U.S.$6.99\nEverywhere Else$30.00\n\nDomestic Shipping \nFree Shipping offers are only applicable to shipments within the continental United States. If you reside in Alaska, Hawaii, or Puerto Rico, please contact us directly for information on additional shipping charges. If you need to return an item that was originally shipped to you for free, the amount of your store credit or refund will reflect the cost we paid to ship your item. For example, if you choose to return a $400 snare drum that was shipped to you for free, your credit / refund will likely be $15-30 less than the original purchase price ($370-385), reflecting the cost we incurred to ship the drum to you. We ship the vast majority of our domestic orders via FedEx Ground. On occasion, certain shipments may be sent via USPS depending on the item\'s size, weight, and destination. Should you require expedited shipping or prefer to use another carrier such as UPS, please contact us to arrange your shipment. We ship via FedEx Ground on Monday through Friday and via USPS on Monday through Saturday. Most orders placed before 12:00 PM Eastern Time are processed and shipped the same day. In some cases, shipments may take an extra 24 to 48 hours to process. In the event that an ordered item is unavailable (e.g. not in stock, on backorder, out of production, etc), we will notify you immediately to determine what actions are appropriate. No Signature Required: By default, all domestic shipments are sent with No Signature Required for both FedEx Ground and USPS. If you wish to require a signature upon delivery, this may incur an additional charge. In either case, if a package is returned to us after multiple attempts to deliver, the shipping amount will not be refunded. Insurance: All items are automatically insured for $100 per box. If you would like to purchase additional insurance, the cost is $0.80 per $100 of the order (item) total. International Shipping We accept Visa, Mastercard, Discover, and American Express from a variety of countries outside the United States. We accept PayPal (in US dollars) from customers with verified accounts and confirmed shipping addresses. In some cases, we can also accept bank / wire transfers. Please note that all credit card orders require additional verification and must be manually processed. We use USPS Priority Mail for most international shipments. In the case of larger items (or when there are USPS country restrictions), we ship via FedEx. Shipping rates may vary from estimates shown on our website or on our eBay listings. In the event that the actual cost of shipping is different from the initial estimate, your invoice will be adjusted (either lower or higher) to reflect the actual cost of shipping. To help reduce the cost of shipping, we make every effort to consolidate the number of boxes used for purchases of multiple items. Please note that such “combined shipping” is done at our discretion with regard to the safety of the purchased items. If we feel that combining certain items in one package may cause damage during shipment, we will notify you and items can be shipped separately at the appropriate cost. Many governments around the world levy some combination of excise tax, value-added tax (VAT), import duties, and other fees on products sent from the United States. We are not responsible for paying these fees, nor do we have any way of estimating how much these charges may total. We do encourage you to check with local regulations prior to purchasing so you know what responsibilities - if any - you have to your government. Most USPS Priority Mail shipments, regardless of the final destination, arrive within 1-3 weeks of shipment. However, in some cases, your order may get “stuck” in Customs for an extended period of time. We are not responsible for any delays caused by Customs processing in your country. In the event that a package is damaged on arrival or lost, it is the buyer’s responsibility to open an investigation with the shipping courier (e.g. USPS, FedEx) within 2 days of receiving a damaged item, or realizing an item has been lost. These investigations can take several weeks to conclude. We are not able to offer any replacements or refunds until the investigation has been completed. If your country imposes restrictions regarding shipments and returns the item to us, you are responsible for the return shipping cost to us (if any), as well as the return shipping cost back to you. There are no refunds or cancellations. Warranty Information for International Customers: If you need to file a warranty claim for an item you purchased from us, we will be happy to help you through the process, however you will be responsible for paying all shipping charges involved in returning the item to the manufacturer and/or receiving a replacement.', 'shipping', '2016-06-09 03:08:01', '2016-06-16 03:35:37'),
('5758de5227357', '5758de5225dee', 'Australia', 'STRAHAN', 'tas', 'Shipping Policy\n\nAvailable for local\npickup from Binghamton, NYShips from Binghamton, NY to:\nContinental U.S.$650.00\nEverywhere Else$1,250.00\nI will ship with tracking to the listed regions. To negotiate shipping rates to other locations,\nplease send me a message. I will be shipping via Keyboard Carriage\n\nReturn Policy\nReturn Window\nThis item is sold As-Is and cannot be returned.', 'shipping', '2016-06-09 03:11:14', '2016-06-17 08:23:52'),
('5758dfaa6236a', '5758dfaa5d747', 'Australia', 'REYNOLDS NECK', 'tas', 'Shipping Policy \n\nShips from Antwerpen, VLG, BE to:\nUnited States\n$105.34\nBelgium\n$0.00\nEverywhere Else\n$0.00\nI will ship with tracking to the listed regions. To negotiate shipping rates to other locations, please send me a message.\n\nReturn Policy\nReturn Window\nThis product can be returned within 7 days of receipt.\n\nGeneral Terms\nItems must be returned in original, as-shipped condition with all original packaging and no signs of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n\nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges.\n ', 'shipping', '2016-06-09 03:16:58', '2016-06-17 08:23:30'),
('5758e0ec9e781', '5758e0ec9d692', 'Australia', 'CHISWICK', 'nsw', 'Shipping Policy \n\nShips from Nashville, TN to:\nContinental U.S.$85.00\nEverywhere Else$275.00\n\nI will ship with tracking to the listed regions. To negotiate shipping rates to other locations,\nplease send me a message.\n\nReturn Policy \nReturn Window\nThis product can be returned within 7 days of receipt.\n\nGeneral Terms\nItems must be returned in original, as-shipped condition with all original packaging and no\nsigns of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n \nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges.\n ', 'shipping', '2016-06-09 03:22:20', '2016-06-17 08:53:09'),
('5758e1be8473a', '5758e1be8348f', 'Australia', 'YETHERA', 'nsw', 'Shipping Policy\n\nShips from Bend, OR to:\nContinental U.S. $0.00\nEverywhere Else $15.45\n\nShipping Policy\nRGS will ship completed orders Monday-Friday. Orders placed and paid before 9:00am PST will ship that same day. All completed orders after the cutoff time will ship the following business day. RGS does not ship on Saturday or Sunday. Please note that during the holidays we experience a increase in traffic and purchasing and we do out best to fulfill orders as fast as we can. We appreciate your patience and wish you a happy holiday! Free Domestic Shipping on Every item. Voodoo Lab products do not ship internationally.\n\nReturn Policy\nReturn Window\nThis item is sold As-Is and cannot be returned.', 'shipping', '2016-06-09 03:25:50', '2016-08-04 01:55:29'),
('5758e2ac0392f', '5758e2ac027d7', 'Australia', 'BRETTI', 'nsw', 'Shipping Policy\n\nShips from Columbia\nCity, IN to:\nContin', 'shipping', '2016-06-09 03:29:48', '2016-08-04 03:51:37'),
('5758e383dce3e', '5758e383db954', 'Australia', 'KALORAMA', 'vic', 'Shipping Policy\n\nAvailable for local pickup from East Hartford, CTShips from East Hartford, CT to:\nContinental U.S.$30.00\nEverywhere Else$110.00\n\nWe offer three local pickup locations, in Leominster, MA; Southbridge, MA; and East Hartford, CT. On receiving cleared payment we will ship via UPS Ground anywhere in the continental United States for flat rate listed. For any other location, please contact us for a shipping quote.\n\nReturn Policy\nReturn Window\nThis product can be returned within 14 days of receipt.\n\nGeneral Terms\nItems must be returned in original, as-shipped condition with all original packaging and no signs of use. Buyer assumes responsibility for all return shipping costs unless the item was not received as described.\n\nRefunds\nBuyer receives a full refund in their original payment method less any shipping charges.\n\nSpecial Conditions\nWe accept returns within 14 days of receipt.Please inspect gear and call to report damage and we will notify shipper to process the claim.All other returns are subject to a 25% restocking fee.All shipping charges to and from the customer will be deducted from refund as well as the restocking fee.', 'shipping', '2016-06-09 03:33:23', '2016-08-04 03:15:45'),
('5758e41aa100b', '5758e41a9f9cf', 'Australia', 'TARADALE', 'nsw', 'Shipping Policy\n\nShips from New Haven, CT to:\nContinental U.S.$0.00\nI will ship with tracking to the listed regions. To negotiate shipping rates to other locations, please send me a message.\n\nReturn Policy\nReturn Window\nThis item is sold As-Is and cannot be returned.', 'shipping', '2016-06-09 03:35:54', '2016-08-03 08:25:31'),
('577df92a35cd3', '577df92a32f12', 'Australia', 'Perth', 'wa', '', 'shipping', '2016-07-07 06:39:38', '2016-07-07 06:39:38'),
('577e4cb2ca9f3', '577e4cb2c7ed0', 'Australia', 'Mt lawley', 'wa', 'Sent via Pack and send (fully tracked) within 2 business days.', 'all', '2016-07-07 12:36:02', '2016-07-09 02:14:54'),
('577e4f3d160df', '577e4f3d12ccd', 'Australia', 'Mt lawley', 'wa', 'Shipped express within one business day', 'all', '2016-07-07 12:46:53', '2016-07-07 12:46:53'),
('57807765f1a35', '57807765eed8e', 'Australia', 'Perth ', 'wa', '', 'all', '2016-07-09 04:02:45', '2016-07-09 04:02:45'),
('5780e1135896c', '5780e11355edf', 'Australia', 'Perth', 'wa', '$10 Express Domestic', 'all', '2016-07-09 11:33:39', '2016-07-09 11:33:39'),
('5781ce6169698', '5781ce616681d', 'Australia', 'Mount Hawthorn', 'wa', 'Shipped via courier within 2 business days (tracked)', 'all', '2016-07-10 04:26:09', '2016-07-10 04:31:41'),
('5781d1b5bd4b0', '5781d1b5ba6c9', 'Australia', 'Mount Hawthorn', 'wa', 'Please choose carefully, no returns.', 'all', '2016-07-10 04:40:21', '2016-07-10 04:40:21'),
('5781d36b4ea92', '5781d36b4c056', 'Australia', 'Mount Lawley', 'wa', 'Please choose carefully, no returns.', 'all', '2016-07-10 04:47:39', '2016-07-10 04:47:39'),
('5781d43faa309', '5781d43fa649f', 'Australia', 'Mount Lawley', 'wa', 'Please choose carefully, no returns.', 'all', '2016-07-10 04:51:11', '2016-07-10 04:51:11'),
('5781f5607320e', '5781f56070258', 'Australia', 'mount lawley', 'wa', 'No returns', 'all', '2016-07-10 07:12:32', '2016-08-31 01:12:57'),
('578364d048048', '578364d0455cc', 'Australia', 'Perth', 'wa', 'Items purchased with a PO (purchase order) do not qualify for Free Shipping.\nFree Shipping does not apply to special orders and oversized/overweight products.\nFree Shipping may not be combined with other offers or discounts.', 'shipping', '2016-07-11 09:20:16', '2016-11-02 07:59:59'),
('5783769469c6e', '5783769466e85', 'Australia', 'Perth', 'wa', 'Shipped within 2 days', 'all', '2016-07-11 10:36:04', '2016-07-11 10:39:28'),
('57a17e451b6a7', '57a17e4515317', 'Australia', 'Perth', 'wa', 'For shipping policy', 'shipping', '2016-08-03 05:16:53', '2016-08-08 08:58:24'),
('57a1843b6fa15', '57a1843b5f321', 'Australia', 'City', 'qld', 'Test', 'shipping', '2016-08-03 05:42:19', '2016-08-03 05:42:19'),
('57a84df44be33', '57a84df448f66', 'Australia', 'Perth', 'wa', 'This is for the shipping policy', 'shipping', '2016-08-08 09:16:36', '2016-08-08 09:16:36'),
('57a84eb4eed17', '57a84eb4eb6a8', 'Australia', 'Perth', 'wa', 'This is for the shipping policy.', 'shipping', '2016-08-08 09:19:48', '2016-08-23 03:50:51'),
('57aacfcbb0421', '57aacfcbad4c1', 'Australia', 'city001', 'wa', 'brand1 model1 2001 red', 'shipping', '2016-08-10 06:55:07', '2016-08-23 07:26:00'),
('57b1ab11d36f6', '57b1ab11d06ea', 'Australia', 'Mount Hawthorn', 'wa', 'Shipped within 3 days', 'all', '2016-08-15 11:44:17', '2016-08-15 11:44:17'),
('57c4ff9c4c71f', '57c4ff9c4955f', 'Australia', 'Perth', 'wa', 'Test shipping', 'all', '2016-08-30 03:38:04', '2016-10-25 03:23:46'),
('57c5711ca8c11', '57c5711ca4891', 'Australia', 'Mt lawley', 'wa', 'Shipped via courier with 2 business days. Includes tracking!', 'all', '2016-08-30 11:42:20', '2016-08-30 11:59:09'),
('57d10cbbcb82a', '57d10cbbc6e38', 'Australia', 'test', 'wa', '', 'all', '2016-09-08 07:01:15', '2016-09-08 07:02:04'),
('57d8aaa6f0e7f', '57d8aaa6edebc', 'Australia', 'Austria', 'nsw', '', 'all', '2016-09-14 01:40:54', '2016-09-14 02:23:36'),
('581ac732d4593', '581ac732d1622', 'Australia', 'Perth', 'wa', 'No shipping', 'shipping', '2016-11-03 05:12:18', '2016-11-03 05:12:18'),
('58201387b95b5', '58201387b63b3', 'Australia', 'cc', 'wa', '11 22 2001 red', 'all', '2016-11-07 05:39:19', '2016-11-07 05:39:19'),
('582014765c222', '5820147659170', 'Australia', 'cc', 'wa', '11 22 2001 red', 'all', '2016-11-07 05:43:18', '2016-11-07 05:43:18'),
('582014d384720', '582014d381743', 'Australia', 'cc', 'wa', '11 22 2001 red', 'all', '2016-11-07 05:44:51', '2016-11-07 05:44:51'),
('582017d649ebe', '582017d646e57', 'Australia', 'city 001', 'wa', 'metal cowboy 2006 green', 'all', '2016-11-07 05:57:42', '2016-11-07 05:57:42'),
('58564dc924b72', '58564dc921b20', 'Australia', 'Perth', 'wa', 'Sent express via TNT', 'all', '2016-12-18 08:50:17', '2016-12-18 08:52:05'),
('59818cc9f3498', '59818cc9ee61e', 'Australia', 'Perth', 'wa', 'Regular Australia Post', 'shipping', '2017-08-02 08:26:49', '2017-08-02 08:31:34'),
('5987cb6d155d4', '5987cb6d11f6f', 'Australia', 'Perth', 'wa', '', 'all', '2017-08-07 02:07:41', '2017-08-07 02:09:57'),
('5a5efde8c27aa', '5a5efde8bd2b4', 'Australia', 'asd', 'qld', 'testt est', 'all', '2018-01-16 23:40:24', '2018-01-16 23:40:24'),
('5a69432381cd8', '5a6943237bbac', 'Australia', 'bacolod', 'wa', 'aw', 'all', '2018-01-24 18:38:27', '2018-01-24 18:38:27'),
('5a69936006ed7', '5a699360009db', 'Australia', '232', 'qld', 'awdaw', 'local pickup', '2018-01-25 00:20:48', '2018-01-25 00:20:48'),
('5a6994eeacd2e', '5a6994eea5124', 'Australia', '3232', 'qld', 'wew', 'all', '2018-01-25 00:27:26', '2018-01-25 00:27:26'),
('5a6ed790aee5c', '5a6ed790a7a5d', 'Australia', 'awaw', 'wa', 'wewd', 'all', '2018-01-29 00:13:04', '2018-01-29 00:13:04'),
('5a6fe1e756e33', '5a6fe1e751d3e', 'Australia', 'dfdf', 'nsw', '', 'shipping', '2018-01-29 19:09:27', '2018-01-29 19:09:27'),
('5a6fe62de827d', '5a6fe62de4a85', 'Australia', 'aadsa', 'qld', '', 'shipping', '2018-01-29 19:27:41', '2018-01-29 19:39:39');

-- --------------------------------------------------------

--
-- Table structure for table `gp_products_shipping_cost`
--

CREATE TABLE `gp_products_shipping_cost` (
  `product_shipping_cost_id` varchar(13) NOT NULL,
  `product_id` varchar(13) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `cost` decimal(9,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gp_products_shipping_cost`
--

INSERT INTO `gp_products_shipping_cost` (`product_shipping_cost_id`, `product_id`, `location`, `cost`, `created_at`, `updated_at`) VALUES
('576107c440780', '5758046cb97f5', 'Metro Areas', '30.34', '2016-06-15 07:46:12', '2016-06-15 07:46:12'),
('576107c442ca9', '5758046cb97f5', 'Regional Areas', '30.34', '2016-06-15 07:46:12', '2016-06-15 07:46:12'),
('57621efcca09f', '5758dc480d86d', 'Metro Areas', '200.00', '2016-06-16 03:37:32', '2016-06-16 03:37:32'),
('57621efcca638', '5758dc480d86d', 'Regional Areas', '175.00', '2016-06-16 03:37:32', '2016-06-16 03:37:32'),
('57623c3e7243b', '575805fe82044', 'Metro Areas', '35.00', '2016-06-16 05:42:22', '2016-06-16 05:42:22'),
('57623c3e7391a', '575805fe82044', 'Regional Areas', '35.00', '2016-06-16 05:42:22', '2016-06-16 05:42:22'),
('57639e3c9645c', '5757ff95c2c54', 'Metro Areas', '5.00', '2016-06-17 06:52:44', '2016-06-17 06:52:44'),
('57639e3c9772f', '5757ff95c2c54', 'Regional Areas', '5.00', '2016-06-17 06:52:44', '2016-06-17 06:52:44'),
('5763a41cebafe', '575802b3b8829', 'Metro Areas', '125.00', '2016-06-17 07:17:48', '2016-06-17 07:17:48'),
('5763a41cecf0e', '575802b3b8829', 'Regional Areas', '125.00', '2016-06-17 07:17:48', '2016-06-17 07:17:48'),
('5763a4642a896', '5757b5b0a3478', 'Metro Areas', '0.00', '2016-06-17 07:19:00', '2016-06-17 07:19:00'),
('5763a4642bb51', '5757b5b0a3478', 'Regional Areas', '0.00', '2016-06-17 07:19:00', '2016-06-17 07:19:00'),
('5763a7586873e', '5757cf25e884b', 'Metro Areas', '99.00', '2016-06-17 07:31:36', '2016-06-17 07:31:36'),
('5763a75869a5e', '5757cf25e884b', 'Regional Areas', '400.00', '2016-06-17 07:31:36', '2016-06-17 07:31:36'),
('5763a77342197', '5757cd316f107', 'Metro Areas', '39.00', '2016-06-17 07:32:03', '2016-06-17 07:32:03'),
('5763a77343529', '5757cd316f107', 'Regional Areas', '279.00', '2016-06-17 07:32:03', '2016-06-17 07:32:03'),
('5763a78d201bd', '5757ca83b493d', 'Metro Areas', '145.00', '2016-06-17 07:32:29', '2016-06-17 07:32:29'),
('5763a78d216f0', '5757ca83b493d', 'Regional Areas', '345.00', '2016-06-17 07:32:29', '2016-06-17 07:32:29'),
('5763aacd43314', '5757c2a0cf7b4', 'Metro Areas', '200.00', '2016-06-17 07:46:21', '2016-06-17 07:46:21'),
('5763aacd444e6', '5757c2a0cf7b4', 'Regional Areas', '200.00', '2016-06-17 07:46:21', '2016-06-17 07:46:21'),
('5763b4aedd40d', '5758c5446299f', 'Metro Areas', '0.00', '2016-06-17 08:28:30', '2016-06-17 08:28:30'),
('5763b4aede655', '5758c5446299f', 'Regional Areas', '200.00', '2016-06-17 08:28:30', '2016-06-17 08:28:30'),
('5763ba13e9af5', '5757bfc15b77c', 'Metro Areas', '200.00', '2016-06-17 08:51:31', '2016-06-17 08:51:31'),
('5763ba13eb161', '5757bfc15b77c', 'Regional Areas', '200.00', '2016-06-17 08:51:31', '2016-06-17 08:51:31'),
('5763ba247c7d3', '5757bda523572', 'Metro Areas', '174.77', '2016-06-17 08:51:48', '2016-06-17 08:51:48'),
('5763ba247dce4', '5757bda523572', 'Regional Areas', '174.77', '2016-06-17 08:51:48', '2016-06-17 08:51:48'),
('5763ba30882f4', '5757bc41ae3bc', 'Metro Areas', '100.00', '2016-06-17 08:52:00', '2016-06-17 08:52:00'),
('5763ba30896d8', '5757bc41ae3bc', 'Regional Areas', '100.00', '2016-06-17 08:52:00', '2016-06-17 08:52:00'),
('577e4f3d1788a', '577e4f3d12ccd', 'Metro Areas', '0.00', '2016-07-07 12:46:53', '2016-07-07 12:46:53'),
('577e4f3d18f50', '577e4f3d12ccd', 'Regional Areas', '0.00', '2016-07-07 12:46:53', '2016-07-07 12:46:53'),
('57805e1e1c774', '577e4cb2c7ed0', 'Metro Areas', '39.00', '2016-07-09 02:14:54', '2016-07-09 02:14:54'),
('57805e1e1d9e0', '577e4cb2c7ed0', 'Regional Areas', '49.00', '2016-07-09 02:14:54', '2016-07-09 02:14:54'),
('57807765f2f17', '57807765eed8e', 'Metro Areas', '100.00', '2016-07-09 04:02:45', '2016-07-09 04:02:45'),
('578077660000e', '57807765eed8e', 'Regional Areas', '250.00', '2016-07-09 04:02:45', '2016-07-09 04:02:45'),
('5780e11359e5f', '5780e11355edf', 'Metro Areas', '10.00', '2016-07-09 11:33:39', '2016-07-09 11:33:39'),
('5780e1135b801', '5780e11355edf', 'Regional Areas', '10.00', '2016-07-09 11:33:39', '2016-07-09 11:33:39'),
('5781cfadf146f', '5781ce616681d', 'Metro Areas', '20.00', '2016-07-10 04:31:41', '2016-07-10 04:31:41'),
('5781cfadf28ae', '5781ce616681d', 'Regional Areas', '30.00', '2016-07-10 04:31:41', '2016-07-10 04:31:41'),
('5781d1b5be8d3', '5781d1b5ba6c9', 'Metro Areas', '15.00', '2016-07-10 04:40:21', '2016-07-10 04:40:21'),
('5781d1b5bfb35', '5781d1b5ba6c9', 'Regional Areas', '25.00', '2016-07-10 04:40:21', '2016-07-10 04:40:21'),
('5781d36b50056', '5781d36b4c056', 'Metro Areas', '30.00', '2016-07-10 04:47:39', '2016-07-10 04:47:39'),
('5781d36b5168d', '5781d36b4c056', 'Regional Areas', '39.00', '2016-07-10 04:47:39', '2016-07-10 04:47:39'),
('5781d43fab835', '5781d43fa649f', 'Metro Areas', '10.00', '2016-07-10 04:51:11', '2016-07-10 04:51:11'),
('5781d43facace', '5781d43fa649f', 'Regional Areas', '15.00', '2016-07-10 04:51:11', '2016-07-10 04:51:11'),
('5783776065b86', '5783769466e85', 'Metro Areas', '15.00', '2016-07-11 10:39:28', '2016-07-11 10:39:28'),
('5783776066bfd', '5783769466e85', 'Regional Areas', '25.00', '2016-07-11 10:39:28', '2016-07-11 10:39:28'),
('57a1843b9ab12', '57a1843b5f321', 'Metro Areas', '1.00', '2016-08-03 05:42:19', '2016-08-03 05:42:19'),
('57a1843ba5836', '57a1843b5f321', 'Regional Areas', '2.00', '2016-08-03 05:42:19', '2016-08-03 05:42:19');

-- --------------------------------------------------------

--
-- Table structure for table `gp_products_specs`
--

CREATE TABLE `gp_products_specs` (
  `product_specs_id` varchar(13) NOT NULL,
  `product_id` varchar(13) NOT NULL,
  `product_condition` varchar(255) NOT NULL,
  `product_brand` varchar(100) DEFAULT NULL,
  `product_model` varchar(100) DEFAULT NULL,
  `product_finish` varchar(100) DEFAULT NULL,
  `product_categories` varchar(13) NOT NULL,
  `product_subcategories` varchar(13) DEFAULT NULL,
  `product_year` varchar(20) DEFAULT NULL,
  `product_colour` varchar(30) DEFAULT NULL,
  `product_made_in` varchar(100) DEFAULT NULL,
  `product_type` enum('new','pre-owned') DEFAULT 'new',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gp_products_specs`
--

INSERT INTO `gp_products_specs` (`product_specs_id`, `product_id`, `product_condition`, `product_brand`, `product_model`, `product_finish`, `product_categories`, `product_subcategories`, `product_year`, `product_colour`, `product_made_in`, `product_type`, `created_at`, `updated_at`) VALUES
('5757b5b0a89be', '5757b5b0a3478', '56e65261b3fa2', 'Ramirez Classical Guitar', 'Estudio classical', NULL, '56e626f24ce06', '571890184b4bf', '1968', '1968', 'spain', 'new', '2016-06-08 06:05:36', '2016-06-17 07:19:00'),
('5757bc41aeb5d', '5757bc41ae3bc', '56e65261b3799', 'Paul Reed Smith', 'SE Zach Myers', NULL, '56e626f24cdef', '', '2014', '2014', 'united states', 'new', '2016-06-08 06:33:37', '2016-06-17 08:52:00'),
('5757bda523f35', '5757bda523572', '56e65261b3799', 'Fender', 'Master Built 1963 Closet Classic Telecaster Custom J. Cruz', NULL, '56e626f24cdef', '', '2016', '2016', 'united states', 'new', '2016-06-08 06:39:33', '2016-06-17 08:51:48'),
('5757be522c82d', '5757be522be2d', '56e65261b3f97', 'Yamaha', 'Oak custom 12\" tom', NULL, '56e626f24cdff', '', '2014', '2014', 'japan', 'new', '2016-06-08 06:42:26', '2016-06-17 08:34:58'),
('5757bfc15c3a1', '5757bfc15b77c', '56e65261b3799', 'James Tyler', 'Studio Elite HD - Burning Water 2K', NULL, '56e626f24cdef', '', '2016', '2016', 'united states', 'new', '2016-06-08 06:48:33', '2016-06-17 08:51:31'),
('5757c0580fc14', '5757c0580f2d3', '56e65261b3fa2', 'DOD', 'DOD 250 Analog Overdrive Preamp VINTAGE', NULL, '56e626f24cde4', '', '70s', '70s', 'united states', 'new', '2016-06-08 06:51:04', '2016-06-17 08:34:32'),
('5757c28566c38', '5757c28562494', '56e65261b3fa2', 'Ross', 'Stereo analog delay', NULL, '56e626f24cde4', '', '1978', '1978', 'united states', 'new', '2016-06-08 07:00:21', '2016-06-17 08:34:21'),
('5757c2a0d019c', '5757c2a0cf7b4', '56e65261b3799', 'Experience Guitars', 'Eleanor 5', NULL, '56e626f24ce16', '', '2016', '2016', 'united states', 'new', '2016-06-08 07:00:48', '2016-06-17 07:46:21'),
('5757c4465b3aa', '5757c4465aa91', '56e65261b3f8b', 'Zildjian', '22\" K Ride', NULL, '56e626f24cdff', '', '2014', '2014', 'united states', 'new', '2016-06-08 07:07:50', '2016-06-17 08:34:08'),
('5757c4949d2e9', '5757c4949c432', '56e65261b3799', 'Gretsch', 'G6118T-LTV 125th', NULL, '56e626f24cdef', '', '2008', '2008', 'japan', 'new', '2016-06-08 07:09:08', '2016-06-17 03:27:20'),
('5757c57b60199', '5757c57b5d975', '56e65261b3f97', 'Yamaha', 'Oak custom 7 x 14\" snare drum', NULL, '56e626f24cdff', '', '2014', '2014', 'japan', 'new', '2016-06-08 07:12:59', '2016-06-17 08:33:51'),
('5757c69b9821c', '5757c69b978d2', '56e65261b3f97', 'Universal Audio /Urei', 'LA-4 stereo compressor', NULL, '56e626f24ce3d', '', '1980', '1980', 'united states', 'new', '2016-06-08 07:17:47', '2016-06-17 08:22:41'),
('5757c715e8cfd', '5757c715e62a2', '56e65261b3799', 'Sonor', 'AS 12 1405 MB', NULL, '56e626f24cdff', '', '2008', '2008', 'germany', 'new', '2016-06-08 07:19:49', '2016-06-16 03:55:54'),
('5757c76b12359', '5757c76b0fcf2', '56e65261b3f97', 'Grundig', 'GDSM202', NULL, '56e626f24ce3d', '', '1980', '1980', 'germany', 'new', '2016-06-08 07:21:15', '2016-06-17 08:22:12'),
('5757c8fb74bac', '5757c8fb7426a', '56e65261b3f8b', '3rd Power', 'Dream Solo 4 \'68 Plexi', NULL, '56e626f24ce0f', '', '2014', '2014', 'united states', 'new', '2016-06-08 07:27:55', '2016-06-17 08:20:52'),
('5757c906a9e6d', '5757c906a945c', '56e65261b3799', 'Bourgeois', 'L-DBO/S - Sunburst - Madagascar', NULL, '56e626f24ce06', '571890184b4bf', '2010', '2010', 'united states', 'new', '2016-06-08 07:28:06', '2016-06-17 07:19:56'),
('5757ca0ce85fc', '5757ca0ce7b0e', '56e65261b3fa9', 'Angel', 'AX-25N2 Glockenspiel', NULL, '56e626f24cdff', '', '2014', '2014', 'canada', 'new', '2016-06-08 07:32:28', '2016-06-17 08:20:34'),
('5757ca83b5434', '5757ca83b493d', '56e65261b3f97', 'Fender', 'Vibrolux Reverb', NULL, '56e626f24ce0f', '', '1968', '1968', 'united states', 'new', '2016-06-08 07:34:27', '2016-06-17 07:32:29'),
('5757cad8b723a', '5757cad8b256c', '56e65261b3799', 'Experience Guitars', 'Eleanor 5', NULL, '56e626f24ce16', '', '2016', '2016', 'united states', 'new', '2016-06-08 07:35:52', '2016-06-17 08:01:42'),
('5757cb901b24a', '5757cb901a907', '56e65261b3f8b', 'Apogee', 'Duet for iPad, iPod, and Mac', NULL, '56e626f24cdf7', '', '2011', '2011', 'united states', 'new', '2016-06-08 07:38:56', '2016-06-17 07:15:55'),
('5757ccb1a58ea', '5757ccb1a4f93', '56e65261b3fa9', 'Selmer', 'AS500 Alto Saxophone', NULL, '56e626f24ce26', '', '2014', '2014', 'united states', 'new', '2016-06-08 07:43:45', '2016-06-17 07:49:46'),
('5757ccbde0438', '5757ccbddfb7d', '56e65261b3f97', 'Marshall', 'JCM 800 2210 100W Head', NULL, '56e626f24ce0f', '', '80s', '80s', 'united kingdom', 'new', '2016-06-08 07:43:57', '2016-06-17 07:47:44'),
('5757cd3170003', '5757cd316f107', '56e65261b3fa9', 'Fender', 'BASSMAN', NULL, '56e626f24ce0f', '', '1966', '1966', 'united states', 'new', '2016-06-08 07:45:53', '2016-06-17 07:32:03'),
('5757cd8e52935', '5757cd8e518f5', '56e65261b3799', 'Jose Ramirez', '130 Años Spruce', NULL, '56e626f24ce06', '571890184b4bf', '2016', '2016', 'spain', 'new', '2016-06-08 07:47:26', '2016-06-17 07:49:23'),
('5757ceb46a79d', '5757ceb469e5a', '56e65261b3fa2', 'Ibanez', 'AD-80 Analog Delay', NULL, '56e626f24cde4', '', '1980', '1980', 'japan', 'new', '2016-06-08 07:52:20', '2016-06-17 08:37:58'),
('5757cf0b55907', '5757cf0b5171f', '56e65261b3f8b', 'Taylor', '814ce Limited', NULL, '56e626f24ce06', '571890184b4bf', '2010', '2010', 'united states', 'new', '2016-06-08 07:53:47', '2016-06-17 08:42:35'),
('5757cf25e90a8', '5757cf25e884b', '56e65261b3fa9', 'HH Electronic', 'V-S Musician Reverb', NULL, '56e626f24ce0f', '', '1922', '1922', 'united states', 'new', '2016-06-08 07:54:13', '2016-06-17 07:31:36'),
('5757d04d13d7a', '5757d04d133a2', '56e65261b3799', 'Deering', 'GTTUK', NULL, '56e626f24ce1e', '', '2008', '2008', 'united states', 'new', '2016-06-08 07:59:09', '2016-06-17 08:50:48'),
('5757d0a3253b1', '5757d0a322708', '56e65261b3799', 'Native Instruments', 'Native Instruments Maschine MK2 Groove Production Studio, Black', NULL, '56e626f24ce35', '', '2014', '2014', 'united states', 'new', '2016-06-08 08:00:35', '2016-06-17 08:41:39'),
('5757d1a572ee5', '5757d1a57267b', '56e65261b3fa2', 'Pioneer', 'DJM-500', NULL, '56e626f24ce35', '', '2002', '2002', 'japan', 'new', '2016-06-08 08:04:53', '2016-06-17 07:13:38'),
('5757daf9730db', '5757daf9705c1', '56e65261b3fa2', 'Roland', 'System analog synthesizer model 101 controller', NULL, '56e626f24ce2d', '57284a2063532', '1975', '1975', 'japan', 'new', '2016-06-08 08:44:41', '2016-06-17 11:19:39'),
('5757db4943a51', '5757db493aaa9', '56e65261b3799', 'Becker', '2000A', NULL, '56e626f24ce26', '', '2014', '2014', 'canada', 'new', '2016-06-08 08:46:01', '2016-06-17 08:41:06'),
('5757dc6b1d4cf', '5757dc6b1cc1d', '56e65261b3f8b', 'Yamaha', 'YAS 62 ALTO SAXOPHONE', NULL, '56e626f24ce26', '', '20011', '20011', 'japan', 'new', '2016-06-08 08:50:51', '2016-06-17 07:18:39'),
('5757dc802fb10', '5757dc802f480', '56e65261b3799', 'Joyo', 'JMT-03', NULL, '56e626f24cdd8', '57189007d1d38', '2016', '2016', 'united states', 'new', '2016-06-08 08:51:12', '2016-06-17 08:40:51'),
('5757ddc6b3a05', '5757ddc6b27e3', '56e65261b3fa2', 'Mancuso', 'F Model', NULL, '56e626f24ce1e', '', '1991', '1991', 'united states', 'new', '2016-06-08 08:56:38', '2016-06-17 08:40:30'),
('5757ddfe3a0c8', '5757ddfe391dd', '56e65261b3799', 'Sennheiser', 'HD-280 Pro', NULL, '56e626f24ce3d', '', '2012', '2012', 'united states', 'new', '2016-06-08 08:57:34', '2016-06-17 07:39:29'),
('5757df1ea65ba', '5757df1ea5ddd', '56e65261b3fc2', 'Royal Hawaiian', 'Soprano koa ukulele', NULL, '56e626f24ce1e', '', '1930s', '1930s', 'united states', 'new', '2016-06-08 09:02:22', '2016-06-17 08:40:07'),
('5757e08c55fb7', '5757e08c5348a', '56e65261b3fb1', 'Regal', 'Tiple', NULL, '56e626f24ce1e', '', '1930s to \'40s', '1930s to \'40s', 'united states', 'new', '2016-06-08 09:08:28', '2016-06-17 08:33:07'),
('5757e1ec64a04', '5757e1ec63fb8', '56e65261b3799', 'Myers Pickups', 'The Feather', NULL, '56e626f24ce1e', '', '2016', '2016', 'united states', 'new', '2016-06-08 09:14:20', '2016-06-17 08:32:39'),
('5757e3b04e995', '5757e3b04e109', '56e65261b3fa9', 'Gibson', 'J-45', NULL, '56e626f24ce06', '571890184b4bf', '1962', '1962', 'united states', 'new', '2016-06-08 09:21:52', '2016-06-17 08:32:24'),
('5757e50fdee85', '5757e50fde599', '56e65261b3fa2', 'Sonor', 'Phonic', NULL, '56e626f24cdff', '', '1980', '1980', 'germany', 'new', '2016-06-08 09:27:43', '2016-06-17 08:31:57'),
('5757e6379a128', '5757e63794d19', '56e65261b3fa2', 'B&B', 'EQF-1 Parametric Equalizer 500 Series Module', NULL, '56e626f24ce3d', '', '1977', '1977', 'united states', 'new', '2016-06-08 09:32:39', '2016-06-16 03:25:11'),
('5757ff95c56f1', '5757ff95c2c54', '56e65261b3799', 'Platinum Samples', 'Platinum Samples Accent Ocean Way Drums Download Delivery', NULL, '56e626f24cdf7', '', '2015', '2015', 'united states', 'new', '2016-06-08 11:20:53', '2016-06-17 06:52:44'),
('575802b3b91ac', '575802b3b8829', '56e65261b3799', 'BAE', '8CM', NULL, '56e626f24cdf7', '', '2006', '2006', 'united states', 'new', '2016-06-08 11:34:11', '2016-06-17 07:17:48'),
('5758046cb9f99', '5758046cb97f5', '56e65261b3f97', 'Grundig', 'GDSM202', NULL, '56e626f24cdf7', '', '1980', '1980', 'germany', 'new', '2016-06-08 11:41:32', '2016-06-15 07:46:12'),
('575805fe82ec3', '575805fe82044', '56e65261b3fa2', 'B&B', 'EQF-1 Parametric Equalizer 500 Series Module', NULL, '56e626f24cdf7', '', '1977', '1977', 'united states', 'new', '2016-06-08 11:48:14', '2016-06-16 05:42:22'),
('575807dc142d7', '575807dc11832', '56e65261b3799', 'B-52', 'ACTPRO-1515 HD', NULL, '56e626f24cdf7', '', '1515', '1515', 'united states', 'new', '2016-06-08 11:56:12', '2016-06-15 08:30:41'),
('5758bd7086952', '5758bd7085fa4', '56e65261b3f97', 'Behringer', 'Xenyx 1002B', NULL, '56e626f24ce3d', '', '2015', '2015', 'china', 'new', '2016-06-09 00:50:56', '2016-06-17 08:37:25'),
('5758bea43fced', '5758bea43f4c5', '56e65261b3fa2', 'Mackie', 'SA1530z', NULL, '56e626f24ce3d', '', '2015', '2015', 'china', 'new', '2016-06-09 00:56:04', '2016-06-17 07:06:15'),
('5758c093c792a', '5758c093c6e68', '56e65261b3f97', 'rivera', 'los lobottom sub 2', NULL, '56e626f24ce0f', '', '2005', '2005', 'united states', 'new', '2016-06-09 01:04:19', '2016-06-17 07:05:46'),
('5758c0e6e7982', '5758c0e6e6e5a', '56e65261b3f97', 'Jen', 'Sx-1000 synthtone', NULL, '56e626f24ce2d', '57284a2063532', '1980', '1980', 'italy', 'new', '2016-06-09 01:05:42', '2016-06-17 07:39:13'),
('5758c1bf78263', '5758c1bf73ef7', '56e65261b3fa2', 'Korg', 'PX1T', NULL, '56e626f24cde4', '', '1995', '1995', 'japan', 'new', '2016-06-09 01:09:19', '2016-06-17 07:05:14'),
('5758c1da62009', '5758c1da5f662', '56e65261b3f97', 'Korg', 'X50', NULL, '56e626f24ce2d', '57284a2063532', '2000', '2000', 'china', 'new', '2016-06-09 01:09:46', '2016-06-17 07:39:18'),
('5758c2ab53090', '5758c2ab5275a', '56e65261b3799', 'Pig Hog', 'PH186R - 18.6\" Tour Grade 1/4\" Right Angle Guitar Cable', NULL, '56e626f24cdd8', '57189007d1d38', '2015', '2015', 'united states', 'new', '2016-06-09 01:13:15', '2016-06-17 08:35:51'),
('5758c4d2d9366', '5758c4d2d83fa', '56e65261b3f97', 'Korg', 'Triton Extreme 76', NULL, '56e626f24ce2d', '57284a2063532', '2000', '2000', 'china', 'new', '2016-06-09 01:22:26', '2016-06-17 07:37:43'),
('5758c544633fc', '5758c5446299f', '56e65261b3fa2', 'Duesenberg', 'D-Bass', NULL, '56e626f24ce16', '', '2016', '2016', 'germany', 'new', '2016-06-09 01:24:20', '2016-06-17 08:28:30'),
('5758c5b1dc1b4', '5758c5b1dab96', '56e65261b3799', 'Electro Lobotomy', 'Sonic Architecture Lab + // SAL+', NULL, '56e626f24ce2d', '57284a2063532', '2016', '2016', 'united states', 'new', '2016-06-09 01:26:09', '2016-06-17 07:49:13'),
('5758c64e7ab63', '5758c64e75d1d', '56e65261b3799', 'Henry Heller', 'Peruvian Style Vegan Dreadnaught Acoustic Guitar Bag', NULL, '56e626f24cdd8', '57189007d1d38', '2016', '2016', 'united kingdom', 'new', '2016-06-09 01:28:46', '2016-06-17 09:32:13'),
('5758c6eb509c4', '5758c6eb4e101', '56e65261b3fa9', 'RHODES CHROMA POLARIS', 'Arp/ fender', NULL, '56e626f24ce2d', '57284a2063532', '1985', '1985', 'united states', 'new', '2016-06-09 01:31:23', '2016-06-17 06:29:52'),
('5758c715bb8a5', '5758c715baf3b', '56e65261b3799', 'Henry Heller', 'Henry Heller HGB-D1 Level 1 Dreadnought Guitar Soft Case', NULL, '56e626f24cdd8', '57189007d1d38', '2016', '2016', 'united kingdom', 'new', '2016-06-09 01:32:05', '2016-06-17 08:47:37'),
('5758c7d1787c5', '5758c7d1761aa', '56e65261b3799', 'Fender', 'F logo tuners', NULL, '56e626f24c620', '', '1966', '1966', 'united states', 'new', '2016-06-09 01:35:13', '2016-06-17 08:46:14'),
('5758c849f1402', '5758c849f0a87', '56e65261b3799', 'Schiller', 'Leipzig', NULL, '56e626f24ce2d', '57284a2063532', '2016', '2016', 'united states', 'new', '2016-06-09 01:37:13', '2016-06-17 06:29:29'),
('5758c87e26c0c', '5758c87e25bc4', '56e65261b3799', 'Aftermarket', 'MK-0115-003 Guitar/Bass Knobs Flat-Top Barrel Pair USA Solid Shaft Pots - Black', NULL, '56e626f24c620', '', '2014', '2014', 'others', 'new', '2016-06-09 01:38:06', '2016-06-17 08:46:42'),
('5758c93f18cef', '5758c93f1835c', '56e65261b3799', 'National', 'NOS Matched Quad (4) Union NU JAN-CNU- Plate Vacuum Tubes', NULL, '56e626f24c620', '', '7193', '7193', 'others', 'new', '2016-06-09 01:41:19', '2016-06-17 08:44:40'),
('5758c96ca04d5', '5758c96c9fa9d', '56e65261b3f97', 'Thomas Henry', 'Mega Percussive Synth Eurorack', NULL, '56e626f24ce2d', '57284a2063532', '2015', '2015', 'united states', 'new', '2016-06-09 01:42:04', '2016-06-17 07:36:28'),
('5758ca24cb0d6', '5758ca24ca553', '56e65261b3f8b', 'Cyclone Analogic', 'TT-303', NULL, '56e626f24ce2d', '57284a2063532', '2016', '2016', 'united states', 'new', '2016-06-09 01:45:08', '2016-06-17 07:36:29'),
('5758ca30e0b28', '5758ca30e00fa', '56e65261b3f8b', 'Meyer', 'UP Junior', NULL, '56e626f24ce3d', '', '2016', '2016', 'united states', 'new', '2016-06-09 01:45:20', '2016-06-17 08:31:30'),
('5758cb12ba05c', '5758cb12b97cf', '56e65261b3fb1', 'Rheem', 'Kee Bass', NULL, '56e626f24ce2d', '57284a2063532', '1960\'s', '1960\'s', 'united states', 'new', '2016-06-09 01:49:06', '2016-06-17 07:24:10'),
('5758cc544fdea', '5758cc544f496', '56e65261b3799', 'Audix', 'D-Flex', NULL, '56e626f24ce3d', '', '2016', '2016', 'others', 'new', '2016-06-09 01:54:28', '2016-06-17 08:31:12'),
('5758ce269ab10', '5758ce269848a', '56e65261b3fa9', 'Artley', '17S Clarinet', NULL, '56e626f24ce26', '', '2015', '2015', 'united states', 'new', '2016-06-09 02:02:14', '2016-06-17 08:30:53'),
('5758cef0d21b1', '5758cef0d16bc', '56e65261b3f97', 'Vuillaume', 'a’ Paris', NULL, '56e626f24ce26', '', '1900', '1900', 'germany', 'new', '2016-06-09 02:05:36', '2016-06-17 08:30:37'),
('5758d023bb6d6', '5758d023ba0f3', '56e65261b3fa2', 'ESP', 'MA 250 BE STR', NULL, '56e626f24ce16', '', '1990s', '1990s', 'others', 'new', '2016-06-09 02:10:43', '2016-06-17 08:30:24'),
('5758d1b39e986', '5758d1b39dd8a', '56e65261b3fa2', 'ESP', 'Amaze-CTM', NULL, '56e626f24ce16', '', '2007', '2007', 'others', 'new', '2016-06-09 02:17:23', '2016-06-17 07:18:20'),
('5758d36289b11', '5758d36284ff2', '56e65261b3f97', 'Washburn', 'HOBA', NULL, '56e626f24ce16', '', '2015', '2015', 'china', 'new', '2016-06-09 02:24:34', '2016-06-17 07:17:41'),
('5758d4e5ce1c5', '5758d4e5cd685', '56e65261b3799', 'Danelectro', '12-String', NULL, '56e626f24cdef', '', 'Dark Aqua', 'Dark Aqua', 'others', 'new', '2016-06-09 02:31:01', '2016-06-17 07:17:00'),
('5758d7724a7e6', '5758d77249ebb', '56e65261b3f97', 'Martin', 'Cowboy V', NULL, '56e626f24ce06', '571890184b4bf', '2006', '2006', 'others', 'new', '2016-06-09 02:41:54', '2016-06-17 07:16:10'),
('5758da28f3842', '5758da28f2dcc', '56e65261b3fa2', 'Yamaha', 'FL-100 Flanger', NULL, '56e626f24cde4', '', '80s', '80s', 'japan', 'pre-owned', '2016-06-09 02:53:28', '2016-06-17 07:15:42'),
('5758db44b4bf7', '5758db44b4238', '56e65261b3f97', 'TC Electronic', 'Corona Stereo Chorus', NULL, '56e626f24cde4', '', '2015', '2015', 'others', 'pre-owned', '2016-06-09 02:58:12', '2016-06-17 08:25:08'),
('5758dc480e0e7', '5758dc480d86d', '56e65261b3f97', 'Yamaha', 'Absolute Birch', NULL, '56e626f24cdff', '', '2008', '2008', 'united states', 'new', '2016-06-09 03:02:32', '2016-06-16 03:37:32'),
('5758dc5260b2a', '5758dc525fc7d', '56e65261b3f97', 'Fender', 'Jazz Bass', NULL, '56e626f24ce16', '', '1978', '1978', 'united states', 'pre-owned', '2016-06-09 03:02:42', '2016-06-17 08:24:38'),
('5758dd33c9140', '5758dd33c86c2', '56e65261b3f97', 'Barker', 'B1', NULL, '56e626f24ce16', '', '2015', '2015', 'united states', 'pre-owned', '2016-06-09 03:06:27', '2016-06-17 08:24:11'),
('5758dd91ccb00', '5758dd91cc13a', '56e65261b3799', 'S-HOOP', 'S-hoop Drum Hoops : 12\" 6 Hole - Powder Coated Black', NULL, '56e626f24cdff', '', '2016', '2016', 'united states', 'new', '2016-06-09 03:08:01', '2016-06-16 03:35:37'),
('5758de522692a', '5758de5225dee', '56e65261b3f97', 'Hammond', 'B3', NULL, '56e626f24ce2d', '57284a2063532', '1969', '1969', 'united states', 'pre-owned', '2016-06-09 03:11:14', '2016-06-17 08:23:52'),
('5758dfaa5fd92', '5758dfaa5d747', '56e65261b3f97', 'Korg', 'MS-20', NULL, '56e626f24ce2d', '57284a2063532', '2015', '2015', 'japan', 'pre-owned', '2016-06-09 03:16:58', '2016-06-17 08:23:30'),
('5758e0ec9e0a0', '5758e0ec9d692', '56e65261b3fb1', 'Gibson', 'L-00', NULL, '56e626f24ce06', '571890184b4bf', '1941', '1941', 'others', 'pre-owned', '2016-06-09 03:22:20', '2016-06-17 08:53:09'),
('5758e1be83fca', '5758e1be8348f', '56e65261b3799', 'Black Lion Audio', 'Auteur Preamp', NULL, '56e626f24ce3d', '', '2016', '2016', 'others', 'new', '2016-06-09 03:25:50', '2016-08-04 01:55:29'),
('5758e2ac03245', '5758e2ac027d7', '56e65261b3799', 'Meinl', 'Headliner Series Wood Bongos - Vintage Sunburst', NULL, '56e626f24cdff', '', '2016', '2016', 'others', 'new', '2016-06-09 03:29:48', '2016-08-04 03:51:37'),
('5758e383dc5ed', '5758e383db954', '56e65261b3fa2', 'Ibanez', 'TBX150H 150W Guitar Head', NULL, '56e626f24ce0f', '', '2015', '2015', 'others', 'pre-owned', '2016-06-09 03:33:23', '2016-08-04 03:15:45'),
('5758e41aa0970', '5758e41a9f9cf', '56e65261b3f8b', 'IsoAcoustics', 'ISO-L8R155', NULL, '56e626f24cdd8', '57189007d1d38', '', '', 'others', 'pre-owned', '2016-06-09 03:35:54', '2016-08-03 08:25:31'),
('577df92a346a5', '577df92a32f12', '56e65261b3799', 'Bogner', 'Duende', NULL, '56e626f24ce0f', '57591641d0a86', '1991', 'Gold', 'belgium', 'new', '2016-07-07 06:39:38', '2016-07-07 06:39:38'),
('577e4cb2c966a', '577e4cb2c7ed0', '56e65261b3f97', 'Suzuki', 'Omnichord OM27 Autohard', NULL, '56e626f24ce2d', '5759301aed939', '1980\'s', '1980\'s', 'australia', 'pre-owned', '2016-07-07 12:36:02', '2016-07-09 02:14:54'),
('577e4f3d1465d', '577e4f3d12ccd', '56e65261b3fa2', 'BK Butler', 'Tube Drive Overdrive', NULL, '56e626f24cde4', '5759174854e77', '2015', 'White', 'australia', 'pre-owned', '2016-07-07 12:46:53', '2016-07-07 12:46:53'),
('57807765f0498', '57807765eed8e', '56e65261b3fb1', 'Ulbrick', '4x12 Celestion Vintage 30 (Custom Vinyl & Badge)', NULL, '56e626f24ce0f', '5759165bd4940', '2007', 'Black', 'australia', 'pre-owned', '2016-07-09 04:02:45', '2016-07-09 04:02:45'),
('5780e113575be', '5780e11355edf', '56e65261b3fa2', 'Boss', 'BF-2 Flanger', NULL, '56e626f24cde4', '575917917c748', '1983', 'Purple', 'japan', 'pre-owned', '2016-07-09 11:33:39', '2016-07-09 11:33:39'),
('5781ce6168301', '5781ce616681d', '56e65261b3f97', 'Dave Smith Instruments', 'Mopho Desktop Synthesizer', NULL, '56e626f24ce2d', '57593005ad587', 'Mono ', 'Mono ', 'australia', 'pre-owned', '2016-07-10 04:26:09', '2016-07-10 04:31:41'),
('5781d1b5bbf75', '5781d1b5ba6c9', '56e65261b3fb1', 'Akai', 'D-1 Shredomatic Tube Distortion Pedal', NULL, '56e626f24cde4', '5759174854e77', 'Discontinued', 'with Expression', 'australia', 'pre-owned', '2016-07-10 04:40:21', '2016-07-10 04:40:21'),
('5781d36b4d6b6', '5781d36b4c056', '56e65261b3f97', 'Tascam ', ' 424mkii Portastudio', NULL, '56e626f24cdf7', '57591c124a8a0', '4 Track Cassette Tap', 'Blue', 'australia', 'pre-owned', '2016-07-10 04:47:39', '2016-07-10 04:47:39'),
('5781d43fa7b4f', '5781d43fa649f', '56e65261b3f97', 'Teenage Engineering', 'PO-20 Arcade Pocket Operator', NULL, '56e626f24ce2d', '5759303fbde32', '2016', 'Exc', 'australia', 'pre-owned', '2016-07-10 04:51:11', '2016-07-10 04:51:11'),
('5781f56071db8', '5781f56070258', '56e65261b3f97', 'Roland', 'Juno 106 Vintage Analog Polyphonic Synth', NULL, '56e626f24ce2d', '57593005ad587', '1980\'s', '1980\'s', 'australia', 'pre-owned', '2016-07-10 07:12:32', '2016-08-31 01:12:57'),
('578364d046c7c', '578364d0455cc', '56e65261b3799', 'B-52', 'ACTPRO-1515HD', NULL, '56e626f24cdf7', '57591c4c2c618', '1999', 'Black', 'japan', 'new', '2016-07-11 09:20:16', '2016-11-02 07:59:59'),
('57837694687d1', '5783769466e85', '56e65261b3fa2', 'Akai', 'MPC500 drum machine sampler ', NULL, '56e626f24cdff', '57591aa47159e', '2000\'s', '2000\'s', 'japan', 'pre-owned', '2016-07-11 10:36:04', '2016-07-11 10:39:28'),
('57a17e4515f4e', '57a17e4515317', '56e65261b3799', 'Electrika', 'EG-1', NULL, '56e626f24cdef', '5759124f485fc', '', '', 'others', 'new', '2016-08-03 05:16:53', '2016-08-08 08:58:24'),
('57a84df44a8cb', '57a84df448f66', '56e65261b3fa2', 'EG', 'EG-1', NULL, '56e626f24cdef', '5759124f485fc', '', '', 'australia', 'new', '2016-08-08 09:16:36', '2016-08-08 09:16:36'),
('57a84eb4ed270', '57a84eb4eb6a8', '56e65261b3fc2', 'TEG', 'EG-1', NULL, '56e626f24ce45', NULL, '', 'Black', 'australia', 'new', '2016-08-08 09:19:48', '2016-08-23 03:50:51'),
('57aacfcbaeecf', '57aacfcbad4c1', '56e65261b3799', 'brand1', 'model1', NULL, '56e626f24ce45', NULL, '2001', 'red', 'china', 'new', '2016-08-10 06:55:07', '2016-08-23 07:26:00'),
('57b1ab11d2268', '57b1ab11d06ea', '56e65261b3f8b', 'Fender', 'DG100', NULL, '56e626f24ce06', '571890184b4bf', '1977', '', 'united states', 'pre-owned', '2016-08-15 11:44:17', '2016-08-15 11:44:17'),
('57c4ff9c4b15b', '57c4ff9c4955f', '56e65261b3f8b', 'Marsh', 'Mallow-01', NULL, '56e626f24ce35', '575930d62e070', '1999', 'Tangerine', 'australia', 'pre-owned', '2016-08-30 03:38:04', '2016-10-25 03:23:46'),
('57c5711ca7199', '57c5711ca4891', '56e65261b3fa9', 'Tascam', '244 Portastudio', NULL, '56e626f24cdf7', '57591c124a8a0', 'Vintage', '', 'japan', 'pre-owned', '2016-08-30 11:42:20', '2016-08-30 11:59:09'),
('57d10cbbc94c9', '57d10cbbc6e38', '56e65261b3fb1', 'Super D', 'superD500', NULL, '56e626f24cdff', '57591a1f8c761', '1', 'gray', 'australia', 'pre-owned', '2016-09-08 07:01:15', '2016-09-08 07:02:04'),
('57d8aaa6ef7ed', '57d8aaa6edebc', '56e65261b3799', 'MaxD', 'MaxD500', NULL, '56e626f24cdff', '', '1', 'Black/Blue', 'australia', 'new', '2016-09-14 01:40:54', '2016-09-14 02:23:36'),
('581ac732d3036', '581ac732d1622', '56e65261b3f8b', 'T35t', 'Sample', NULL, '56e626f24ce06', '575913c7b6e25', '1999', 'Grey', 'belgium', 'new', '2016-11-03 05:12:18', '2016-11-03 05:12:18'),
('58201387b7e9b', '58201387b63b3', '56e65261b3f8b', '11', '22', NULL, '56e626f24ce06', '575913e1181ce', '2001', 'red', 'china', 'new', '2016-11-07 05:39:19', '2016-11-07 05:39:19'),
('582014765ad6d', '5820147659170', '56e65261b3f8b', '11a', '22asdf', NULL, '56e626f24ce06', '575913e1181ce', '2011', 'red', 'china', 'new', '2016-11-07 05:43:18', '2016-11-07 05:43:18'),
('582014d3832dc', '582014d381743', '56e65261b3f8b', '11ad', '22asdfd', NULL, '56e626f24ce06', '575913e1181ce', '2011', 'red', 'china', 'new', '2016-11-07 05:44:51', '2016-11-07 05:44:51'),
('582017d64892b', '582017d646e57', '56e65261b3f8b', 'metal', 'cowboy', NULL, '56e626f24ce06', '575913d1608b0', '2006', 'green', 'canada', 'pre-owned', '2016-11-07 05:57:42', '2016-11-07 05:57:42'),
('58564dc923672', '58564dc921b20', '56e65261b3f8b', 'Fender', 'AC120 Acoustic', NULL, '56e626f24ce06', '575913d1608b0', '1989', 'Natural', 'united states', 'pre-owned', '2016-12-18 08:50:17', '2016-12-18 08:52:05'),
('59818cc9f072d', '59818cc9ee61e', '56e65261b3f97', 'Armstrong ', '4001 Clarinet USA', NULL, '56e626f24ce26', '5759379f90cbb', '2015', 'Black', 'china', 'pre-owned', '2017-08-02 08:26:49', '2017-08-02 08:31:34'),
('5987cb6d13ddc', '5987cb6d11f6f', '56e65261b3799', 'TROMBA', 'Tromba', NULL, '56e626f24ce26', '57593795cd04a', '2017', 'gold', 'china', 'new', '2017-08-07 02:07:41', '2017-08-07 02:09:57'),
('5a5efde8c0bff', '5a5efde8bd2b4', '56e65261b3799', 'Fender', 'MOD0123', NULL, '56e626f24ce06', '571890184b4bf', '2018', 'red', 'australia', 'new', '2018-01-16 23:40:24', '2018-01-16 23:40:24'),
('5a69432380a48', '5a6943237bbac', '56e65261b3f8b', 'test', '123', NULL, '56e626f24ce06', '571890184b4bf', '2010', 'red', 'australia', 'new', '2018-01-24 18:38:27', '2018-01-24 18:38:27'),
('5a699360053eb', '5a699360009db', '56e65261b3f97', '323', '232', NULL, '56e626f24c620', NULL, '32', '23', 'belgium', 'pre-owned', '2018-01-25 00:20:48', '2018-01-25 00:20:48'),
('5a6994eeaac49', '5a6994eea5124', '56e65261b3f8b', 'wew', 'ewew', NULL, '56e626f24cde4', NULL, 'wew', 'wew', 'belgium', 'pre-owned', '2018-01-25 00:27:26', '2018-01-25 00:27:26'),
('5a6ed790ad036', '5a6ed790a7a5d', '56e65261b3f8b', 'werw', 'wew', NULL, '56e626f24cde4', NULL, 'we', 'wewe', 'belgium', 'new', '2018-01-29 00:13:04', '2018-01-29 00:13:04'),
('5a6fe1e7556ad', '5a6fe1e751d3e', '', '', '', NULL, '56e626f24c620', NULL, '', '', '', '', '2018-01-29 19:09:27', '2018-01-29 19:09:27'),
('5a6fe62de6fc9', '5a6fe62de4a85', '', '', '', NULL, '56e626f24cdff', '', '2121', '', '', '', '2018-01-29 19:27:41', '2018-01-29 19:39:39');

-- --------------------------------------------------------

--
-- Table structure for table `gp_users`
--

CREATE TABLE `gp_users` (
  `user_id` varchar(13) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(60) NOT NULL,
  `login_type` enum('github','facebook','local','google') NOT NULL DEFAULT 'local',
  `api_token` varchar(60) NOT NULL,
  `role_id` varchar(13) NOT NULL,
  `seller` enum('yes','no') DEFAULT 'no',
  `remember_token` varchar(60) DEFAULT '',
  `verification_token` varchar(30) DEFAULT NULL,
  `status` enum('active','inactive','declined') DEFAULT 'inactive',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gp_users`
--

INSERT INTO `gp_users` (`user_id`, `name`, `email`, `password`, `login_type`, `api_token`, `role_id`, `seller`, `remember_token`, `verification_token`, `status`, `created_at`, `updated_at`) VALUES
('5a5ed2729be8b', 'adminforsb adminforsb', 'adminforsb@admin.com', '$2y$10$TJS2IMD.AYhj2gnNdFnUme1sc/KXhiFz6BX2KL0SBHMcQZMEU/RzO', 'local', 'g65rFu87yb9cmnvUCHZALTsaKzBxGi4kf2OQo3W1DpMNeVwEJlRhYtj0PdqX', '56aebc46f281b', 'yes', 'tOmH5rsGkHDfODr5PIqWYBQZPmDji3x9KX9W19BEBvKPtFwkrYypqUiOw8FX', NULL, 'active', '2018-01-16 20:34:58', '2018-02-01 18:59:14'),
('5a5ed593321a5', 'user1 user1', 'user1@user.com', '$2y$10$0k9coS4Tf6XO6bTD6iLA3.Ctm42nCf.6XBjgKYtNmtDDdnIm5ztTq', 'local', 'hKtlb0MA9jvpwWuT1SoYRznPcx4gVICq7UHEdemyrGfJs3QD8XBO65kLFaZN', '56aebc46f2827', 'yes', 'JBBQegyXjXdSli69bHUjkjKgJwFAg2vcVk7JC1JDYL5bQN4pfNAKlU8d8y3U', 'Y7bcO1gElLKh36kp4tex2m9GdDHXfs', 'active', '2018-01-16 20:48:19', '2018-02-01 18:54:35'),
('5a6fcc43a5d86', 'Test name test last name', 'shakegwapo@gmail.com', '$2y$10$q/b1pscnOt/U7Re9Qe6MieuFcXVoJWOSO7oBbOe9Kuvsm8qZEKTve', 'local', 'FHzgpdvZbPcVRjMBKJ8G754NixCtrkf6QqnhWSD21eY9aUOws0XyTAlLEmoI', '56aebc46f2827', 'yes', '2Tb1mDMwpMwQtSISclIhH0jQT8ZPITGcttG9SB52ANIAgIaOX632bz0dbesw', NULL, 'active', '2018-01-29 17:37:07', '2018-01-29 21:39:09'),
('5a7935483d07b', 'Shakey Shake', 'valencia_jr08@yahoo.com', '', 'facebook', 'FLDP7Y8M69fEgKqrOp1eUnkva2icITws5zXxtb4lZSmoNuC0hjHQARVBdWJ3', '56aebc46f2827', 'yes', '3WmXVbVG5CY7T8TwBVCE8QjDmsKvbsHjnjiV7SovJSemh0nfMihkapcImD4k', NULL, 'active', '2018-02-05 20:55:36', '2018-02-05 22:01:57');

-- --------------------------------------------------------

--
-- Table structure for table `gp_users_company`
--

CREATE TABLE `gp_users_company` (
  `company_id` varchar(13) NOT NULL,
  `user_id` varchar(13) NOT NULL,
  `name` varchar(255) NOT NULL,
  `abn` varchar(30) DEFAULT NULL,
  `street` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(10) DEFAULT NULL,
  `post_code` varchar(10) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `logo` varchar(20) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gp_users_company`
--

INSERT INTO `gp_users_company` (`company_id`, `user_id`, `name`, `abn`, `street`, `city`, `state`, `post_code`, `phone`, `logo`, `created_at`, `updated_at`) VALUES
('56e258f915109', '56e258f8ddc6a', 'Syrah Industries', '123456', 'st', 'cc', 'WA', '12345', '123456', '', '2016-03-11 05:34:49', '2016-03-11 05:34:49'),
('56fc9e054e3c1', '56fc9e052d05f', 'RichmundCo.', '98 154 103 483 ', 'street 101', 'Perth City', '', '483103', '123321', '', '2016-03-31 03:48:21', '2016-03-31 03:48:21'),
('5757b02f9dc64', '5757b02f8bd52', 'Handy Dan', NULL, NULL, 'GINDORAN', 'qld', NULL, '(07) 3262 3842', NULL, '2016-06-08 05:42:07', '2016-06-08 05:42:07'),
('5757b08bb90fd', '5757b08ba98b4', 'Brownian', NULL, NULL, 'Perth', 'wa', NULL, '12345678', NULL, '2016-06-08 05:43:39', '2016-06-08 05:43:39'),
('5757b71c3fc99', '5757b71c30095', 'Howard Acoustics', NULL, NULL, 'Perth', 'wa', NULL, '12312312312', NULL, '2016-06-08 06:11:40', '2016-06-08 06:11:40'),
('5757b75b616db', '5757b75b51b9d', 'Lucky Guitars', NULL, NULL, 'Perth', 'wa', NULL, '2342342342', NULL, '2016-06-08 06:12:43', '2016-06-08 06:12:43'),
('5757b761a5dc7', '5757b7619536a', 'Music Plus', NULL, NULL, 'BIGGARA', 'vic', NULL, '(03) 5394 0348', NULL, '2016-06-08 06:12:49', '2016-06-08 06:12:49'),
('5757b79c50ce3', '5757b79c3eee7', 'Els planet', NULL, NULL, 'Perth', 'wa', NULL, '234234234234', NULL, '2016-06-08 06:13:48', '2016-06-08 06:13:48'),
('5757b84fa0761', '5757b84f90556', 'Jewel Mart', NULL, NULL, 'MEBBIN ', 'wa', NULL, '(02) 6674 0885', NULL, '2016-06-08 06:16:47', '2016-06-08 06:16:47'),
('5757b8ec1aea0', '5757b8ebf31a3', 'Musicland', NULL, NULL, 'ZILZIE  ', 'qld', NULL, '(07) 4903 7668', NULL, '2016-06-08 06:19:24', '2016-06-08 06:19:24'),
('5757b94212b88', '5757b94202d2f', 'Mervyn\'s', NULL, NULL, 'GINGHI ', 'wa', NULL, '(02) 4050 7566', NULL, '2016-06-08 06:20:50', '2016-06-08 06:20:50'),
('5757b9ae7cf05', '5757b9ae6ba8a', 'Vitamax Health Food Center', NULL, NULL, 'WYE RIVER', 'vic', NULL, '(03) 5307 9387', NULL, '2016-06-08 06:22:38', '2016-06-08 06:22:38'),
('5757ba32546c8', '5757ba32447f5', 'FlowerTime', NULL, NULL, 'PARADISE', 'wa', NULL, '(08) 9081 4295', NULL, '2016-06-08 06:24:50', '2016-06-08 06:24:50'),
('5757ba73dec2c', '5757ba73cef90', 'White Hen Pantry', NULL, NULL, 'HAPPY VALLEY ', 'qld', NULL, '(07) 3401 6570', NULL, '2016-06-08 06:25:55', '2016-06-08 06:25:55'),
('5757bac778df6', '5757bac743d02', 'Golf Augusta Pro Shops', NULL, NULL, 'COOYAL ', 'sa', NULL, '(02) 4009 4426', NULL, '2016-06-08 06:27:19', '2016-06-08 06:27:19'),
('5757bb219bd3c', '5757bb218a2c3', 'Buttrey Food & Drug', NULL, NULL, 'CHERRY TREE POOL ', 'wa', NULL, '(08) 9059 7678', NULL, '2016-06-08 06:28:49', '2016-06-08 06:28:49'),
('5757bb5b33c29', '5757bb5b2451e', 'Platinum Interior Design', NULL, NULL, 'LAKE CLIFTON', 'wa', NULL, '(08) 9003 5896', NULL, '2016-06-08 06:29:47', '2016-06-08 06:29:47'),
('5757bb6e1858f', '5757bb6decbb1', 'The Happy Bear', NULL, NULL, 'QUARRY HILL', 'vic', NULL, '(03) 5380 7520', NULL, '2016-06-08 06:30:06', '2016-06-08 06:30:06'),
('5757bbce25fae', '5757bbce106d2', 'Pantry Food Stores', NULL, NULL, 'TOM GROGGIN  ', 'wa', NULL, '(03) 5369 3730', NULL, '2016-06-08 06:31:42', '2016-06-08 06:31:42'),
('5757bc734bb7a', '5757bc733bfff', 'Chief Auto Parts', NULL, NULL, 'BANDA BANDA', 'wa', NULL, '(02) 6728 2820', NULL, '2016-06-08 06:34:27', '2016-06-08 06:34:27'),
('5757bcb3599ca', '5757bcb349ea6', 'Enviro Architectural Designs', NULL, NULL, 'MINCHINBURY', 'sa', NULL, '(02) 4744 1090', NULL, '2016-06-08 06:35:31', '2016-06-08 06:35:31'),
('5757bd19397e0', '5757bd1929eed', 'Office Warehouse', NULL, NULL, 'SOUTHPORT', 'qld', NULL, '(07) 5640 7357', NULL, '2016-06-08 06:37:13', '2016-06-08 06:37:13'),
('5757bdfb14c59', '5757bdfb04938', 'VitaGrey', NULL, NULL, 'WUNGHNU', 'vic', NULL, '(03) 5382 6744', NULL, '2016-06-08 06:40:59', '2016-06-08 06:40:59'),
('5757be468f9d9', '5757be467b5cf', 'Master Builder Design Services', NULL, NULL, 'JEERALANG', 'wa', NULL, '(03) 5318 6380', NULL, '2016-06-08 06:42:14', '2016-06-08 06:42:14'),
('5757bec574c1e', '5757bec5647b9', 'Leo\'s Stereo', NULL, NULL, 'WEST END', 'qld', NULL, '(07) 3926 6658', NULL, '2016-06-08 06:44:21', '2016-06-08 06:44:21'),
('5757bf184b898', '5757bf183a788', 'AM/PM Camp', NULL, NULL, 'PITJANTJATJARA HOMELANDS', 'sa', NULL, '(08) 8293 5372', NULL, '2016-06-08 06:45:44', '2016-06-08 06:45:44'),
('5757bfed25c82', '5757bfed16291', 'Fireball', NULL, NULL, 'APPLECROSS', 'wa', NULL, '(08) 9457 6385', NULL, '2016-06-08 06:49:17', '2016-06-08 06:49:17'),
('5757cc0b00323', '5757cc0ae2bf2', 'Home Quarters Warehouse', NULL, NULL, 'WOOMBYE', 'qld', NULL, '(07) 5304 8021', NULL, '2016-06-08 07:40:59', '2016-06-08 07:40:59'),
('5757cca8b0359', '5757cca8a0b11', 'Morville', NULL, NULL, 'BULL CREEK', 'wa', NULL, '(08) 9343 7101', NULL, '2016-06-08 07:43:36', '2016-06-08 07:43:36'),
('5757ccf72c6fd', '5757ccf71cdd1', 'Vinyl Fever', NULL, NULL, 'MELALEUCA', 'wa', NULL, '(08) 9444 3532', NULL, '2016-06-08 07:44:55', '2016-06-08 07:44:55'),
('5757cd6ed28d8', '5757cd6ec2f31', 'Integra Investment Service', NULL, NULL, 'SPIT JUNCTION', 'wa', NULL, '(02) 9309 1068', NULL, '2016-06-08 07:46:54', '2016-06-08 07:46:54'),
('5757cdb97eb32', '5757cdb96f252', 'Dubrow\'s Cafeteria', NULL, NULL, 'TILBUSTER', 'wa', NULL, '(02) 6755 4328', NULL, '2016-06-08 07:48:09', '2016-06-08 07:48:09'),
('5757ce2648952', '5757ce2636dfd', 'Silverwoods', NULL, NULL, 'PIMPAMA', 'qld', NULL, '(07) 3393 5852', NULL, '2016-06-08 07:49:58', '2016-06-08 07:49:58'),
('5757ce72a62ed', '5757ce7294790', 'Northern Star', NULL, NULL, 'CANNING MILLS', 'wa', NULL, '(08) 9302 1308', NULL, '2016-06-08 07:51:14', '2016-06-08 07:51:14'),
('5757cfdb71b5a', '5757cfdb6215a', 'Henry\'s ', NULL, NULL, 'RUSSELL VALE', 'wa', NULL, '(02) 4272 4977 ', NULL, '2016-06-08 07:57:15', '2016-06-08 07:57:15'),
('5757d0a614b38', '5757d0a6052ef', 'System Star', NULL, NULL, 'MARKARANKA', 'sa', NULL, '(08) 8789 8931', NULL, '2016-06-08 08:00:38', '2016-06-08 08:00:38'),
('5757d13f16686', '5757d13f04f58', 'White Hen Pantry ', NULL, NULL, 'TRINITY PARK', 'qld', NULL, '(07) 4018 8186 ', NULL, '2016-06-08 08:03:11', '2016-06-08 08:03:11'),
('57764d5c41de3', '57764d5c2c425', 'SWC Musical Gears', NULL, NULL, 'ASCOTT', 'nsw', NULL, '0412345678', NULL, '2016-07-01 11:00:44', '2016-07-01 11:00:44'),
('57764df8c742e', '57764df8b15ba', 'HSW Studio', NULL, NULL, 'ASCOTT', 'nsw', NULL, '041024567', NULL, '2016-07-01 11:03:20', '2016-07-01 11:03:20'),
('577dcdd150e4b', '577dcdd13b085', 'GT', NULL, NULL, 'Perth', 'wa', NULL, '', NULL, '2016-07-07 03:34:41', '2016-07-07 03:34:41'),
('577dd33cc5001', '577dd33caf66a', 'Optional Comp', NULL, NULL, 'perth', 'nsw', NULL, '', NULL, '2016-07-07 03:57:48', '2016-07-07 03:57:48'),
('577debfe92c5e', '577debfe7d363', 'Guitarzip', NULL, NULL, 'Perth', 'qld', NULL, '', NULL, '2016-07-07 05:43:26', '2016-07-07 05:43:26'),
('577dee360c775', '577dee35ea912', '', NULL, NULL, 'Perth', 'qld', NULL, '', NULL, '2016-07-07 05:52:54', '2016-07-07 05:52:54'),
('577df32a80228', '577df32a7919b', 'no company', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-07-07 06:14:02', '2016-07-07 06:14:02'),
('577df403de2d9', '577df403db1bd', 'no company', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-07-07 06:17:39', '2016-07-07 06:17:39'),
('577df4a3bf312', '577df4a3a9253', '', NULL, NULL, 'Perth', 'qld', NULL, '', NULL, '2016-07-07 06:20:19', '2016-07-07 06:20:19'),
('577dfbb97630a', '577dfbb960547', 'no company', NULL, NULL, 'Perth', 'qld', NULL, '', NULL, '2016-07-07 06:50:33', '2016-07-07 06:50:33'),
('577dfc7cdf365', '577dfc7cc9bcd', 'Guitarz Comp', NULL, NULL, 'Perth', 'wa', NULL, '', NULL, '2016-07-07 06:53:48', '2016-07-07 06:53:48'),
('577dfe6c624cd', '577dfe6c5f8a3', 'no company', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-07-07 07:02:04', '2016-07-07 07:02:04'),
('577dfeca02e44', '577dfec9f0023', 'no company', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-07-07 07:03:38', '2016-07-07 07:03:38'),
('577e48312328e', '577e48310c58d', 'Shaun\'s Gear Emporium', NULL, NULL, 'Mt lawley', 'wa', NULL, '', NULL, '2016-07-07 12:16:49', '2016-07-07 12:16:49'),
('577f89c0e4b00', '577f89c0ca329', 'Groovius House of Groove', NULL, NULL, 'Mount Lawley', 'wa', NULL, '0893776622', NULL, '2016-07-08 11:08:48', '2016-07-08 11:08:48'),
('577f9bcbeb4a5', '577f9bcbd5743', 'no company', NULL, NULL, 'Leederville', 'wa', NULL, '0478101781', NULL, '2016-07-08 12:25:47', '2016-07-08 12:25:47'),
('578073189da0c', '5780731887dff', 'no company', NULL, NULL, 'Perth', 'wa', NULL, '0432063073', NULL, '2016-07-09 03:44:24', '2016-07-09 03:44:24'),
('578330bb38c0f', '56e0d53867459', 'no company', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-07-11 13:40:28', '2016-07-11 05:40:30'),
('578330bb38c14', '56e258f8ddc6a', 'no company', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-07-11 05:40:17', '2016-07-11 05:40:34'),
('578330bb38c16', '5757bacce06da', 'no company', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-07-11 05:40:17', '2016-07-11 05:40:37'),
('578330bb38c18', '5757ca0188272', 'no company', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-07-11 05:40:18', '2016-07-11 05:40:18'),
('578330bb38c1a', '5762964e86b55', 'no company', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-07-11 05:40:18', '2016-07-11 05:40:47'),
('578330bb38c1d', '5763caf04caea', 'no company', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-07-11 05:40:19', '2016-07-11 05:40:53'),
('578330bb38c1f', '5763ccb9cfeb2', 'no company', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-07-11 05:40:19', '2016-07-11 05:40:56'),
('578330bb38c20', '576512da6dd61', 'no company', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-07-11 05:40:20', '2016-07-11 05:40:59'),
('578330bb38c22', '57660b4edaf95', 'no company', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-07-11 05:40:20', '2016-07-11 05:41:01'),
('578330bb38c24', '577dc8e9c8d6a', 'no company', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-07-11 05:40:21', '2016-07-11 05:41:04'),
('578347c1f0b63', '578347c1dad73', 'no company', NULL, NULL, 'Perth', 'wa', NULL, '', NULL, '2016-07-11 07:16:17', '2016-07-11 07:16:17'),
('57834869bfcd8', '57834869a9fc7', 'no company', NULL, NULL, 'Perth', 'wa', NULL, '', NULL, '2016-07-11 07:19:05', '2016-07-11 07:19:05'),
('578348aa0798b', '578348a9e60bf', 'no company', NULL, NULL, '', 'nsw', NULL, '', NULL, '2016-07-11 07:20:10', '2016-07-11 07:20:10'),
('57835a66ae4dd', '57835a6698899', 'no company', NULL, NULL, '', 'nsw', '', '1', NULL, '2016-07-11 08:35:50', '2016-09-07 08:17:50'),
('57835d4dd32db', '57835d4dbd83f', 'no company', NULL, NULL, 'Perth', 'wa', NULL, '', NULL, '2016-07-11 08:48:13', '2016-07-11 08:48:13'),
('5783672f92108', '5783672f7c904', 'Hello Jane', NULL, NULL, 'RUSSELL VALE', 'nsw', NULL, '', NULL, '2016-07-11 09:30:23', '2016-07-11 09:30:23'),
('5783736ac6707', '5783736ab0d23', 'Shaun\'s Gear Emporium', NULL, NULL, 'Mount Hawthorn', 'wa', NULL, '+61893718822', NULL, '2016-07-11 10:22:34', '2016-07-11 10:22:34'),
('57848983edded', '57848983eb4fe', 'no company', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-07-12 06:09:07', '2016-07-12 06:09:07'),
('578489d3c6c18', '578489d3b107c', 'no company', NULL, NULL, 'Perth', 'nsw', NULL, '0412345678', NULL, '2016-07-12 06:10:27', '2016-07-12 06:10:27'),
('57848a3d55d93', '57848a3d5334b', 'no company', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-07-12 06:12:13', '2016-07-12 06:12:13'),
('578493ab51f2e', '578493ab3bab8', 'no company', NULL, NULL, 'Perth', 'wa', NULL, '12345678', NULL, '2016-07-12 06:52:27', '2016-07-12 06:52:27'),
('5786fb7f18351', '5786fb7ef3838', 'Sushi', NULL, NULL, 'perth', 'wa', NULL, '123456', NULL, '2016-07-14 02:39:59', '2016-07-14 02:39:59'),
('578c538e56219', '578c538e46670', 'no company', NULL, NULL, '', 'WA', '', '45222325', NULL, '2016-07-18 03:57:02', '2016-07-18 03:57:02'),
('578d9ba2eb346', '578d9ba2ea31b', 'no company', NULL, NULL, '', '', '', '', NULL, '2016-07-19 03:16:50', '2016-07-19 03:16:50'),
('57a1843b5d561', '57a1843b408e4', 'no company', '', '', '', '', '', '', '', '2016-08-03 05:42:19', '2016-08-03 05:42:19'),
('57abf1d00904a', '57abf1cfe14de', 'Sushi Testing', NULL, NULL, 'Perth', 'wa', NULL, '', NULL, '2016-08-11 03:32:32', '2016-08-11 03:32:32'),
('57ac076b0e4ff', '57ac076aec473', 'Hello Sushi', NULL, NULL, 'Perth', 'wa', NULL, '', NULL, '2016-08-11 05:04:43', '2016-08-11 05:04:43'),
('57b1a777844f7', '57b1a7776e17a', 'Shaun\'s Gear Planet', NULL, NULL, 'Mount Lawley', 'wa', NULL, '', NULL, '2016-08-15 11:28:55', '2016-08-15 11:28:55'),
('57b1aba24afce', '57b1aba23598d', 'no company', NULL, NULL, 'Mt lawley', 'wa', NULL, '', NULL, '2016-08-15 11:46:42', '2016-08-15 11:46:42'),
('57b2afeb603b6', '57b2afeb4a6c3', 'gp123456', NULL, NULL, 'perth', 'nsw', NULL, '', NULL, '2016-08-16 06:17:15', '2016-08-16 06:17:15'),
('57b2b05b04ece', '57b2b05ae3266', 'GP Testing Account', NULL, NULL, 'Perth', 'wa', NULL, '', NULL, '2016-08-16 06:19:07', '2016-08-16 06:19:07'),
('57b2b3c61d400', '57b2b3c607609', 'Pace Membership Warehouse', NULL, NULL, 'EAST BUNBURY', 'wa', NULL, '(08) 8302 2084', NULL, '2016-08-16 06:33:42', '2016-08-16 06:33:42'),
('57b3bb862591b', '57b3bb860fbd2', 'no company', NULL, NULL, 'Spain ', 'nsw', NULL, '0034688265165', NULL, '2016-08-17 01:19:02', '2016-08-17 01:19:02'),
('57b50befeefda', '57b50befd919c', 'Ivan\'s Company', NULL, NULL, 'Perth', 'qld', NULL, '11', NULL, '2016-08-18 01:14:23', '2016-08-18 01:14:23'),
('57bc12c03f50d', '57bc12c029835', 'RegTest', NULL, NULL, 'Perth', 'wa', NULL, '0412345678', NULL, '2016-08-23 09:09:20', '2016-08-23 09:09:20'),
('57bc13858910f', '57bc138572cf7', 'Sushi Leo', NULL, NULL, 'Perth', 'wa', NULL, '0412345678', NULL, '2016-08-23 09:12:37', '2016-08-23 09:12:37'),
('57c7f3e013324', '57c7f3dff0df7', 'E-Muzic Business Services', NULL, NULL, 'MONGANS BRIDGE', 'vic', NULL, '', NULL, '2016-09-01 09:24:48', '2016-09-01 09:24:48'),
('57d109f28c20b', '57d109f273c67', 'test', NULL, NULL, 'test', 'wa', NULL, 'numeric only', NULL, '2016-09-08 06:49:22', '2016-09-08 06:49:22'),
('580d699f6dafa', '580d699f5469a', '7th Damn & Fall', NULL, NULL, 'Perth', 'wa', NULL, '123700', NULL, '2016-10-24 01:53:35', '2016-10-24 01:53:35'),
('581190ad0b54f', '581190ace6cb4', 'SushiTesting', NULL, NULL, 'CANNING MILLS', 'wa', NULL, '0123456789', NULL, '2016-10-27 05:29:17', '2016-10-27 05:29:17'),
('5811930740c11', '5811930728cee', 'SushiTesting2', NULL, NULL, 'Perth', 'wa', NULL, '0412345678', NULL, '2016-10-27 05:39:19', '2016-10-27 05:39:19'),
('5811941d67a2b', '5811941d4f835', 'None', NULL, NULL, 'Perth', 'wa', NULL, '123700', NULL, '2016-10-27 05:43:57', '2016-10-27 05:43:57'),
('58119a2f3f5d8', '58119a2f25e17', 'SushiTesting001', NULL, NULL, 'Perth', 'wa', NULL, '0412345678', NULL, '2016-10-27 06:09:51', '2016-10-27 06:09:51'),
('58119f3b20529', '58119f3b07658', 'SushiTesting', NULL, NULL, 'Perth', 'wa', NULL, '0412345678', NULL, '2016-10-27 06:31:23', '2016-10-27 06:31:23'),
('58119fec2fc81', '58119fec164a2', 'SushiTesting002', NULL, NULL, 'Perth', 'wa', NULL, '0412345678', NULL, '2016-10-27 06:34:20', '2016-10-27 06:34:20'),
('5811a0a6a23a3', '5811a0a68a5cc', 'SushiTesting003', NULL, NULL, 'Perth', 'wa', NULL, '04123456789', NULL, '2016-10-27 06:37:26', '2016-10-27 06:37:26'),
('5811a1462f37c', '5811a14616701', 'SushiTesting004', NULL, NULL, 'Perth', 'wa', NULL, '123456789', NULL, '2016-10-27 06:40:06', '2016-10-27 06:40:06'),
('5812ebf60fad1', '5812ebf5e9ed2', 'Wasbi', NULL, NULL, 'Perth', 'wa', NULL, '123700', NULL, '2016-10-28 06:11:02', '2016-10-28 06:11:02'),
('5981889093ca9', '5981889078112', 'no company', NULL, NULL, '', 'nsw', NULL, '', NULL, '2017-08-02 08:08:48', '2017-08-02 08:08:48'),
('5987c82d4a207', '5987c82d2eed4', 'no company', NULL, NULL, 'Perth', 'wa', NULL, '1300301988', NULL, '2017-08-07 01:53:49', '2017-08-07 01:53:49'),
('5987c8835b170', '5987c88324398', 'no company', NULL, NULL, 'Perth', 'wa', NULL, '1300301988', NULL, '2017-08-07 01:55:15', '2017-08-07 01:55:15'),
('5a5ed272bb9a5', '5a5ed2729be8b', 'adminforsb', NULL, NULL, 'asd', 'nsw', NULL, '', NULL, '2018-01-16 20:34:58', '2018-01-16 20:34:58'),
('5a5ed5935250c', '5a5ed593321a5', 'user1', NULL, NULL, '123456', 'nsw', NULL, '123123', NULL, '2018-01-16 20:48:19', '2018-01-16 20:48:19'),
('5a6fcc43c4975', '5a6fcc43a5d86', 'test store', NULL, NULL, 'test', 'sa', NULL, '123456', NULL, '2018-01-29 17:37:07', '2018-01-29 17:37:07'),
('5a7935484adaa', '5a7935483d07b', 'no company', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-05 20:55:36', '2018-02-05 20:55:36');

-- --------------------------------------------------------

--
-- Table structure for table `gp_users_inbox`
--

CREATE TABLE `gp_users_inbox` (
  `inbox_id` varchar(13) NOT NULL,
  `channel_id` varchar(20) NOT NULL,
  `has_unread_messages` tinyint(1) DEFAULT '0',
  `last_message_received` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gp_users_inbox`
--

INSERT INTO `gp_users_inbox` (`inbox_id`, `channel_id`, `has_unread_messages`, `last_message_received`, `created_at`, `updated_at`) VALUES
('578ee4fea2244', 'emjkSiUqAPOBn7VJutbo', 0, '2016-07-27 01:42:13', '2016-07-20 02:42:06', '2016-07-27 01:42:15'),
('578f1b2d10148', 'Bmb3H2j61LV4nlc0hXCx', 0, '2016-08-23 08:49:41', '2016-07-20 06:33:17', '2016-08-23 08:49:42'),
('57a3f54017514', 'XBGdbUl2qO3hg6Ho58yR', 0, '2016-08-05 02:09:04', '2016-08-05 02:09:04', '2016-08-05 02:09:05'),
('57a3faec36e13', '9kbd2eDoaNGy1clBuS06', 0, '2016-08-08 05:41:25', '2016-08-05 02:33:16', '2016-08-08 05:41:26'),
('57a81be9a92cc', 'LuK7013yYEpsFmTrafkH', 0, '2016-08-08 05:43:05', '2016-08-08 05:43:05', '2016-08-08 05:43:06'),
('57bc163c7f189', 'gfK8eNLjBX9zQZ2cFxmv', 0, '2016-08-24 07:43:28', '2016-08-23 09:24:12', '2016-08-24 07:43:29'),
('57bd4cbe7be38', '9A2yZN7diBQTFeHkCuUD', 0, '2016-08-24 07:29:02', '2016-08-24 07:29:02', '2016-08-24 07:29:03'),
('57c42593b162d', 'mEp7FacGH426qwIdSzlQ', 0, '2016-09-02 12:55:41', '2016-08-29 12:07:47', '2016-09-02 12:55:42'),
('57cfcc199ae15', 'jxptAFC6N8RI9JPcKL4G', 0, '2016-09-07 08:16:56', '2016-09-07 08:13:13', '2016-09-07 08:16:56'),
('57d0de3694af5', 'jHERupd9LGqnIm5FJViK', 0, '2016-09-08 03:43:19', '2016-09-08 03:42:46', '2016-09-08 03:43:19'),
('5a7004def0622', 'IXbxpt2kasgy4GdqKCiB', 0, '2018-01-29 21:38:38', '2018-01-29 21:38:38', '2018-01-29 21:38:40');

-- --------------------------------------------------------

--
-- Table structure for table `gp_users_messages`
--

CREATE TABLE `gp_users_messages` (
  `message_id` varchar(13) NOT NULL,
  `inbox_id` varchar(13) NOT NULL,
  `sender_id` varchar(13) NOT NULL,
  `receiver_id` varchar(13) NOT NULL,
  `message` text,
  `status` enum('read','un-read') DEFAULT 'un-read',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gp_users_messages`
--

INSERT INTO `gp_users_messages` (`message_id`, `inbox_id`, `sender_id`, `receiver_id`, `message`, `status`, `created_at`, `updated_at`) VALUES
('578ee4fea28b8', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', 'hey', 'read', '2016-07-20 02:42:06', '2016-09-08 03:18:17'),
('578ee5117bcfe', '578ee4fea2244', '57835d4dbd83f', '578c538e46670', 'yes?', 'read', '2016-07-20 02:42:25', '2016-09-08 03:18:17'),
('578ee7eac1987', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', 'what time is it now?', 'read', '2016-07-20 02:54:34', '2016-09-08 03:18:17'),
('578ee7ff293dc', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', 'hey?', 'read', '2016-07-20 02:54:55', '2016-09-08 03:18:17'),
('578ee807d6ffb', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', 'are you still there?', 'read', '2016-07-20 02:55:03', '2016-09-08 03:18:17'),
('578ee81a27120', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', 'hey', 'read', '2016-07-20 02:55:22', '2016-09-08 03:18:17'),
('578ee824b3165', '578ee4fea2244', '57835d4dbd83f', '578c538e46670', 'tou ok', 'read', '2016-07-20 02:55:32', '2016-09-08 03:18:17'),
('578ee82e2912f', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', 'really?', 'read', '2016-07-20 02:55:42', '2016-09-08 03:18:17'),
('578ee82e4762d', '578ee4fea2244', '57835d4dbd83f', '578c538e46670', 'dffff', 'read', '2016-07-20 02:55:42', '2016-09-08 03:18:17'),
('578ee83ed1410', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', 'ok', 'read', '2016-07-20 02:55:58', '2016-09-08 03:18:17'),
('578ee846545b0', '578ee4fea2244', '57835d4dbd83f', '578c538e46670', 'fine', 'read', '2016-07-20 02:56:06', '2016-09-08 03:18:17'),
('578ee9d5c1bc3', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', 'BS', 'read', '2016-07-20 03:02:45', '2016-09-08 03:18:17'),
('578ee9db9e174', '578ee4fea2244', '57835d4dbd83f', '578c538e46670', 'weh', 'read', '2016-07-20 03:02:51', '2016-09-08 03:18:17'),
('578ee9f23c710', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', 'hey are you kidding me?\nim not a kid btw.', 'read', '2016-07-20 03:03:14', '2016-09-08 03:18:17'),
('578eea0b3251a', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', 'yeah?', 'read', '2016-07-20 03:03:39', '2016-09-08 03:18:17'),
('578ef200e824e', '578ee4fea2244', '57835d4dbd83f', '578c538e46670', 'yeah?\nreally?\nok?', 'read', '2016-07-20 03:37:36', '2016-09-08 03:18:17'),
('578ef20c50271', '578ee4fea2244', '57835d4dbd83f', '578c538e46670', 'wahahahahahahahahahahahahahahahahahahahahah', 'read', '2016-07-20 03:37:48', '2016-09-08 03:18:17'),
('578ef3ce10f9c', '578ee4fea2244', '57835d4dbd83f', '578c538e46670', 'test\ntest\ntest here', 'read', '2016-07-20 03:45:18', '2016-09-08 03:18:17'),
('578f13333855f', '578ee4fea2244', '57835d4dbd83f', '578c538e46670', 'ghhJshdkdd\ndhdhdhd\ndjdhfhdhd\nhshshdhd\nhshshshdhd', 'read', '2016-07-20 05:59:15', '2016-09-08 03:18:17'),
('578f13d9b1c3d', '578ee4fea2244', '57835d4dbd83f', '578c538e46670', 'llo there', 'read', '2016-07-20 06:02:01', '2016-09-08 03:18:17'),
('578f14518f8a7', '578ee4fea2244', '57835d4dbd83f', '578c538e46670', 'yas...', 'read', '2016-07-20 06:04:01', '2016-09-08 03:18:17'),
('578f14768395d', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', 'no way man.', 'read', '2016-07-20 06:04:38', '2016-09-08 03:18:17'),
('578f147f6193e', '578ee4fea2244', '57835d4dbd83f', '578c538e46670', 'Hello there.', 'read', '2016-07-20 06:04:47', '2016-09-08 03:18:17'),
('578f14bf501b5', '578ee4fea2244', '57835d4dbd83f', '578c538e46670', 'Hello there.', 'read', '2016-07-20 06:05:51', '2016-09-08 03:18:17'),
('578f15d64efc5', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', 'hello there too.', 'read', '2016-07-20 06:10:30', '2016-09-08 03:18:17'),
('578f160c5041a', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', 'hello there too man.', 'read', '2016-07-20 06:11:24', '2016-09-08 03:18:17'),
('578f161242e43', '578ee4fea2244', '57835d4dbd83f', '578c538e46670', 'ok', 'read', '2016-07-20 06:11:30', '2016-09-08 03:18:17'),
('578f161a2535c', '578ee4fea2244', '57835d4dbd83f', '578c538e46670', 'Hello there.', 'read', '2016-07-20 06:11:38', '2016-09-08 03:18:17'),
('578f16c3c91aa', '578ee4fea2244', '57835d4dbd83f', '578c538e46670', 'hello there com.', 'read', '2016-07-20 06:14:27', '2016-09-08 03:18:17'),
('578f16c8e138b', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', 'no way', 'read', '2016-07-20 06:14:32', '2016-09-08 03:18:17'),
('578f16ccd72ba', '578ee4fea2244', '57835d4dbd83f', '578c538e46670', 'Hello there.', 'read', '2016-07-20 06:14:36', '2016-09-08 03:18:17'),
('578f16e66b67f', '578ee4fea2244', '57835d4dbd83f', '578c538e46670', 'Hello there.?', 'read', '2016-07-20 06:15:02', '2016-09-08 03:18:17'),
('578f171e11881', '578ee4fea2244', '57835d4dbd83f', '578c538e46670', 'Hello there 101.?', 'read', '2016-07-20 06:15:58', '2016-09-08 03:18:17'),
('578f1728bf0be', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', '404', 'read', '2016-07-20 06:16:08', '2016-09-08 03:18:17'),
('578f17304c9cc', '578ee4fea2244', '57835d4dbd83f', '578c538e46670', 'yes', 'read', '2016-07-20 06:16:16', '2016-09-08 03:18:17'),
('578f1735ba8ac', '578ee4fea2244', '57835d4dbd83f', '578c538e46670', 'Hello there 102.?', 'read', '2016-07-20 06:16:21', '2016-09-08 03:18:17'),
('578f174036040', '578ee4fea2244', '57835d4dbd83f', '578c538e46670', 'ok', 'read', '2016-07-20 06:16:32', '2016-09-08 03:18:17'),
('578f174409395', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', 'hello', 'read', '2016-07-20 06:16:36', '2016-09-08 03:18:17'),
('578f17499fbde', '578ee4fea2244', '57835d4dbd83f', '578c538e46670', 'Hello there 103.?', 'read', '2016-07-20 06:16:41', '2016-09-08 03:18:17'),
('578f175300ec2', '578ee4fea2244', '57835d4dbd83f', '578c538e46670', 'Hello there 104.?', 'read', '2016-07-20 06:16:51', '2016-09-08 03:18:17'),
('578f1759236d0', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', 'ok', 'read', '2016-07-20 06:16:57', '2016-09-08 03:18:17'),
('578f1b2d10794', '578f1b2d10148', '578d9ba2ea31b', '57835d4dbd83f', 'hey there.', 'read', '2016-07-20 06:33:17', '2016-09-08 03:16:35'),
('578f1b4d89472', '578f1b2d10148', '578d9ba2ea31b', '57835d4dbd83f', 'hey are you still there?', 'read', '2016-07-20 06:33:49', '2016-09-08 03:16:35'),
('578f1b5b650c0', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', 'hey man. still alive and kicking?', 'read', '2016-07-20 06:34:03', '2016-09-08 03:18:17'),
('578f1b67f0e0b', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', 'hey', 'read', '2016-07-20 06:34:15', '2016-09-08 03:18:17'),
('578f1b7ccc17c', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', 'stup', 'read', '2016-07-20 06:34:36', '2016-09-08 03:18:17'),
('578f28234fcb5', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', 'Hello there test something.\nMan to the moon.', 'read', '2016-07-20 07:28:35', '2016-09-08 03:18:17'),
('578f2868085e6', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', 'william01fiaschi@gmail.com', 'read', '2016-07-20 07:29:44', '2016-09-08 03:18:17'),
('578f286eae79f', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', 'william01fiaschi@gmail.com', 'read', '2016-07-20 07:29:50', '2016-09-08 03:18:17'),
('578f2891c9692', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', 'william01fiaschi@gmail.com', 'read', '2016-07-20 07:30:25', '2016-09-08 03:18:17'),
('578f28a2f383f', '578ee4fea2244', '578c538e46670', '578c538e46670', 'hello there', 'read', '2016-07-20 07:30:42', '2016-09-08 03:18:17'),
('578f28a897838', '578ee4fea2244', '57835d4dbd83f', '578c538e46670', 'ok', 'read', '2016-07-20 07:30:48', '2016-09-08 03:18:17'),
('579810e9912c8', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', 'Hallo there.', 'read', '2016-07-27 01:39:53', '2016-09-08 03:18:17'),
('579810eb02cf5', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', 'Hallo there.', 'read', '2016-07-27 01:39:55', '2016-09-08 03:18:17'),
('579810eb2f95c', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', 'Hallo there.', 'read', '2016-07-27 01:39:55', '2016-09-08 03:18:17'),
('57981115af7cd', '578ee4fea2244', '57835d4dbd83f', '578c538e46670', 'yes? why?', 'read', '2016-07-27 01:40:37', '2016-09-08 03:18:17'),
('5798112d6de38', '578ee4fea2244', '57835d4dbd83f', '578c538e46670', 'yes why?', 'read', '2016-07-27 01:41:01', '2016-09-08 03:18:17'),
('5798113c54db5', '578ee4fea2244', '57835d4dbd83f', '578c538e46670', 'yes? why?', 'read', '2016-07-27 01:41:16', '2016-09-08 03:18:17'),
('57981148cde1d', '578ee4fea2244', '57835d4dbd83f', '578c538e46670', 'yes? why? why?', 'read', '2016-07-27 01:41:28', '2016-09-08 03:18:17'),
('57981175cf410', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', 'Yow?', 'read', '2016-07-27 01:42:13', '2016-09-08 03:18:17'),
('579811c916ae9', '578ee4fea2244', '57835d4dbd83f', '578c538e46670', 'yes?', 'read', '2016-07-27 01:43:37', '2016-09-08 03:18:17'),
('579811d4e62d3', '578ee4fea2244', '578c538e46670', '578c538e46670', 'nothing much', 'read', '2016-07-27 01:43:48', '2016-09-08 03:18:17'),
('579811da528aa', '578ee4fea2244', '57835d4dbd83f', '578c538e46670', 'ok', 'read', '2016-07-27 01:43:54', '2016-09-08 03:18:17'),
('57a300a4a1774', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', 'hey there.', 'read', '2016-08-04 08:45:24', '2016-09-08 03:18:17'),
('57a300ae5aa13', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', '1', 'read', '2016-08-04 08:45:34', '2016-09-08 03:18:17'),
('57a300b797171', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', '2', 'read', '2016-08-04 08:45:43', '2016-09-08 03:18:17'),
('57a300c90e79a', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', '22', 'read', '2016-08-04 08:46:01', '2016-09-08 03:18:17'),
('57a300dc7282b', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', '1251', 'read', '2016-08-04 08:46:20', '2016-09-08 03:18:17'),
('57a300e508a74', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', '123123', 'read', '2016-08-04 08:46:29', '2016-09-08 03:18:17'),
('57a301019de4a', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', '522', 'read', '2016-08-04 08:46:57', '2016-09-08 03:18:17'),
('57a301b86fb42', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', 'hello there.', 'read', '2016-08-04 08:50:00', '2016-09-08 03:18:17'),
('57a301c64b845', '578ee4fea2244', '578c538e46670', '578c538e46670', 'hello there.', 'read', '2016-08-04 08:50:14', '2016-09-08 03:18:17'),
('57a301e5113f9', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', 'no.', 'read', '2016-08-04 08:50:45', '2016-09-08 03:18:17'),
('57a3020dbc093', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', 'sup', 'read', '2016-08-04 08:51:25', '2016-09-08 03:18:17'),
('57a302151d347', '578ee4fea2244', '57835d4dbd83f', '578c538e46670', 'hello there.', 'read', '2016-08-04 08:51:33', '2016-09-08 03:18:17'),
('57a3022142fc9', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', 'no...!', 'read', '2016-08-04 08:51:45', '2016-09-08 03:18:17'),
('57a3022977b65', '578ee4fea2244', '578c538e46670', '57835d4dbd83f', 'sup.', 'read', '2016-08-04 08:51:53', '2016-09-08 03:18:17'),
('57a3f54017c5a', '57a3f54017514', '56fc9e052d05f', '57a1843b408e4', 'hey', 'un-read', '2016-08-05 02:09:04', '2016-08-05 02:09:04'),
('57a3faec37544', '57a3faec36e13', '5757ba73cef90', '57835d4dbd83f', 'Hello', 'read', '2016-08-05 02:33:16', '2016-09-08 06:17:28'),
('57a3fb0fb9010', '578f1b2d10148', '57835d4dbd83f', '578d9ba2ea31b', 'test', 'read', '2016-08-05 02:33:51', '2016-08-24 07:46:57'),
('57a3fb2b95d17', '57a3faec36e13', '57835d4dbd83f', '5757ba73cef90', 'hi there.. how can i help you?', 'read', '2016-08-05 02:34:19', '2016-09-08 06:19:32'),
('57a3fbbee49a0', '57a3faec36e13', '5757ba73cef90', '57835d4dbd83f', 'test', 'read', '2016-08-05 02:36:46', '2016-09-08 06:17:28'),
('57a81b857a0c1', '57a3faec36e13', '5757ba73cef90', '57835d4dbd83f', 'hi will!', 'read', '2016-08-08 05:41:25', '2016-09-08 06:17:28'),
('57a81be9aa7b2', '57a81be9a92cc', '5757bac743d02', '57835d4dbd83f', 'woahhhh! is that you???? will???', 'read', '2016-08-08 05:43:05', '2016-08-23 09:34:56'),
('57a81c06e2b05', '57a3faec36e13', '57835d4dbd83f', '5757ba73cef90', 'heyyy', 'read', '2016-08-08 05:43:34', '2016-09-08 06:19:32'),
('57a81c312c7cc', '57a81be9a92cc', '57835d4dbd83f', '5757bac743d02', 'yup.... jay! how r u??', 'read', '2016-08-08 05:44:17', '2016-08-08 05:44:25'),
('57a81c5269c4f', '57a3faec36e13', '5757ba73cef90', '57835d4dbd83f', 'wud???', 'read', '2016-08-08 05:44:50', '2016-09-08 06:17:28'),
('57a81c5336986', '57a81be9a92cc', '5757bac743d02', '57835d4dbd83f', 'fine bout you?? :D', 'read', '2016-08-08 05:44:51', '2016-08-23 09:34:56'),
('57b1542109827', '578f1b2d10148', '578d9ba2ea31b', '57835d4dbd83f', 'heyyy', 'read', '2016-08-15 05:33:21', '2016-09-08 03:16:35'),
('57bc0e25a83f7', '578f1b2d10148', '578d9ba2ea31b', '57835d4dbd83f', 'test', 'read', '2016-08-23 08:49:41', '2016-09-08 03:16:35'),
('57bc163c808bf', '57bc163c7f189', '57bc138572cf7', '57835d4dbd83f', 'yellow', 'read', '2016-08-23 09:24:12', '2016-08-24 07:48:01'),
('57bc164ea02bd', '57bc163c7f189', '57835d4dbd83f', '57bc138572cf7', 'blueeee', 'read', '2016-08-23 09:24:30', '2016-08-24 07:45:32'),
('57bc18adacc4c', '57bc163c7f189', '57835d4dbd83f', '57bc138572cf7', 'weeeeeeeeeeeeeeeeeeeee\n', 'read', '2016-08-23 09:34:37', '2016-08-24 07:45:32'),
('57bc18b3444ae', '57bc163c7f189', '57bc138572cf7', '57835d4dbd83f', 'yeppppp', 'read', '2016-08-23 09:34:43', '2016-08-24 07:48:01'),
('57bc18c6e9a00', '57bc163c7f189', '57835d4dbd83f', '57bc138572cf7', 'hmmmm', 'read', '2016-08-23 09:35:02', '2016-08-24 07:45:32'),
('57bc18cd5eb01', '57bc163c7f189', '57bc138572cf7', '57835d4dbd83f', 'yes?', 'read', '2016-08-23 09:35:09', '2016-08-24 07:48:01'),
('57bc1c2dc11f3', '57bc163c7f189', '57835d4dbd83f', '57bc138572cf7', 'pweh', 'read', '2016-08-23 09:49:33', '2016-08-24 07:45:32'),
('57bc1c469aa8f', '57bc163c7f189', '57bc138572cf7', '57835d4dbd83f', 'pweh', 'read', '2016-08-23 09:49:58', '2016-08-24 07:48:01'),
('57bd4cbe7d660', '57bd4cbe7be38', '5757bbce106d2', '5757b9ae6ba8a', 'This is just a test message\n\nPlease Try', 'un-read', '2016-08-24 07:29:02', '2016-08-24 07:29:02'),
('57bd4cd962c57', '57bd4cbe7be38', '5757bbce106d2', '5757b9ae6ba8a', 'hey hey hey hey!\n\nhey!\n\nthis is just a test', 'un-read', '2016-08-24 07:29:29', '2016-08-24 07:29:29'),
('57bd4cf05c092', '57bd4cbe7be38', '5757bbce106d2', '5757b9ae6ba8a', 'test1&lt;br /&gt;&lt;br /&gt;', 'un-read', '2016-08-24 07:29:52', '2016-08-24 07:29:52'),
('57bd4cf73a90e', '57bd4cbe7be38', '5757bbce106d2', '5757b9ae6ba8a', 'asfdf1&lt;br /&gt;&lt;br /&gt;gasd12&lt;br /&gt;&lt;br /&gt;asd1 test', 'un-read', '2016-08-24 07:29:59', '2016-08-24 07:29:59'),
('57bd4cff95bcb', '57bd4cbe7be38', '5757bbce106d2', '5757b9ae6ba8a', 'yesy', 'un-read', '2016-08-24 07:30:07', '2016-08-24 07:30:07'),
('57bd4d0cb3cb2', '57bd4cbe7be38', '5757bbce106d2', '5757b9ae6ba8a', 'asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1asd1', 'un-read', '2016-08-24 07:30:20', '2016-08-24 07:30:20'),
('57bd5020ec37f', '57bc163c7f189', '57bc138572cf7', '57835d4dbd83f', 'hi', 'read', '2016-08-24 07:43:28', '2016-08-24 07:48:01'),
('57bd5046eae92', '57bc163c7f189', '57835d4dbd83f', '57bc138572cf7', 'hello', 'read', '2016-08-24 07:44:06', '2016-08-24 07:45:32'),
('57bd504e99338', '57bc163c7f189', '57835d4dbd83f', '57bc138572cf7', 'yup?&lt;br /&gt;New line test', 'read', '2016-08-24 07:44:14', '2016-08-24 07:45:32'),
('57bd5060ccead', '57bc163c7f189', '57835d4dbd83f', '57bc138572cf7', 'this is for testing only&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;series of new line of texts', 'read', '2016-08-24 07:44:32', '2016-08-24 07:45:32'),
('57bd507390a72', '57bc163c7f189', '57835d4dbd83f', '57bc138572cf7', '&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;. _____________________________________________________________________________________________________.', 'read', '2016-08-24 07:44:51', '2016-08-24 07:45:32'),
('57bd507b73ff7', '57bc163c7f189', '57835d4dbd83f', '57bc138572cf7', 'i&#039;m a whale', 'read', '2016-08-24 07:44:59', '2016-08-24 07:45:32'),
('57bd508b47bf9', '578f1b2d10148', '57835d4dbd83f', '578d9ba2ea31b', '&lt;br /&gt;', 'read', '2016-08-24 07:45:15', '2016-08-24 07:46:57'),
('57bd508f1848d', '578f1b2d10148', '57835d4dbd83f', '578d9ba2ea31b', '&lt;br /&gt;', 'read', '2016-08-24 07:45:19', '2016-08-24 07:46:57'),
('57bd509179181', '578f1b2d10148', '57835d4dbd83f', '578d9ba2ea31b', '&lt;br /&gt;', 'read', '2016-08-24 07:45:21', '2016-08-24 07:46:57'),
('57bd50c2722d1', '578f1b2d10148', '57835d4dbd83f', '578d9ba2ea31b', '.__________________________________________________________________________________________________________.', 'read', '2016-08-24 07:46:10', '2016-08-24 07:46:57'),
('57bd50d494a21', '578f1b2d10148', '57835d4dbd83f', '578d9ba2ea31b', 'this is a test&lt;br /&gt;New line', 'read', '2016-08-24 07:46:28', '2016-08-24 07:46:57'),
('57bd50f73a409', '578f1b2d10148', '578d9ba2ea31b', '57835d4dbd83f', 'hi', 'read', '2016-08-24 07:47:03', '2016-09-08 03:16:35'),
('57bd50ff9acf7', '578f1b2d10148', '578d9ba2ea31b', '57835d4dbd83f', 'hello :3&lt;br /&gt;Yeah', 'read', '2016-08-24 07:47:11', '2016-09-08 03:16:35'),
('57bd512ae152c', '578f1b2d10148', '578d9ba2ea31b', '57835d4dbd83f', 'this is a new&lt;br /&gt;Line of text', 'read', '2016-08-24 07:47:54', '2016-09-08 03:16:35'),
('57bd5134a49c1', '57bc163c7f189', '57835d4dbd83f', '57bc138572cf7', 'yep?', 'read', '2016-08-24 07:48:04', '2016-08-24 07:48:05'),
('57bd5143028f3', '57bc163c7f189', '57bc138572cf7', '57835d4dbd83f', 'hi there this is a &lt;br /&gt;new line of text', 'read', '2016-08-24 07:48:19', '2016-08-24 07:48:20'),
('57c42593b7dfb', '57c42593b162d', '577e48310c58d', '5783736ab0d23', 'Hmmm nice MPC', 'read', '2016-08-29 12:07:47', '2016-09-02 12:56:01'),
('57c425b9f078f', '57c42593b162d', '5783736ab0d23', '577e48310c58d', 'Ok yes very good, thank you', 'read', '2016-08-29 12:08:25', '2016-09-02 13:04:28'),
('57c62ee35b3e9', '57c42593b162d', '577e48310c58d', '5783736ab0d23', 'MMM nice', 'read', '2016-08-31 01:12:03', '2016-09-02 12:56:01'),
('57c976cd55cf3', '57c42593b162d', '5783736ab0d23', '577e48310c58d', 'too expensive...', 'read', '2016-09-02 12:55:41', '2016-09-02 13:04:28'),
('57c9783ecf02f', '57c42593b162d', '577e48310c58d', '5783736ab0d23', 'yo', 'read', '2016-09-02 13:01:50', '2016-09-02 13:01:51'),
('57c9784b7bba4', '57c42593b162d', '577e48310c58d', '5783736ab0d23', 'yoyo', 'read', '2016-09-02 13:02:03', '2016-09-02 13:02:04'),
('57ce94fcc4b01', '578f1b2d10148', '57835d4dbd83f', '578d9ba2ea31b', 'Yup', 'un-read', '2016-09-06 10:05:48', '2016-09-06 10:05:48'),
('57cfcc199c63e', '57cfcc199ae15', '57835a6698899', '5757ba73cef90', 'hi Abbey, I&#039;m interested to you item', 'read', '2016-09-07 08:13:13', '2016-09-07 09:10:32'),
('57cfcc7104da2', '57cfcc199ae15', '57835a6698899', '5757ba73cef90', 'Hi Abbey', 'read', '2016-09-07 08:14:41', '2016-09-07 09:10:32'),
('57cfccf800c82', '57cfcc199ae15', '57835a6698899', '5757ba73cef90', 'Hi Abbey!', 'read', '2016-09-07 08:16:56', '2016-09-07 09:10:32'),
('57cfcd1c02e40', '57cfcc199ae15', '5757ba73cef90', '57835a6698899', 'hello Rich', 'read', '2016-09-07 08:17:32', '2016-09-07 09:09:24'),
('57cfcd3f92947', '57cfcc199ae15', '5757ba73cef90', '57835a6698899', 'r u sure?', 'read', '2016-09-07 08:18:07', '2016-09-07 09:09:24'),
('57cfcd6cee71e', '57cfcc199ae15', '5757ba73cef90', '57835a6698899', '??', 'read', '2016-09-07 08:18:52', '2016-09-07 09:09:24'),
('57cfcd7a7a9d2', '57cfcc199ae15', '5757ba73cef90', '57835a6698899', 'u there?', 'read', '2016-09-07 08:19:06', '2016-09-07 09:09:24'),
('57cfcddb718b1', '57cfcc199ae15', '5757ba73cef90', '57835a6698899', 'still there?', 'read', '2016-09-07 08:20:43', '2016-09-07 09:09:24'),
('57cfce0722c27', '57cfcc199ae15', '5757ba73cef90', '57835a6698899', 'hello?', 'read', '2016-09-07 08:21:27', '2016-09-07 09:09:24'),
('57cfce4a1a888', '57cfcc199ae15', '5757ba73cef90', '57835a6698899', 'hi?', 'read', '2016-09-07 08:22:34', '2016-09-07 09:09:24'),
('57cfcffa2e6af', '57cfcc199ae15', '5757ba73cef90', '57835a6698899', 'se?', 'read', '2016-09-07 08:29:46', '2016-09-07 09:09:24'),
('57cfd02aa0dea', '57cfcc199ae15', '5757ba73cef90', '57835a6698899', 'rw', 'read', '2016-09-07 08:30:34', '2016-09-07 09:09:24'),
('57cfd0b5a18b7', '57cfcc199ae15', '5757ba73cef90', '57835a6698899', 'sss', 'read', '2016-09-07 08:32:53', '2016-09-07 09:09:24'),
('57cfd9b33bc3c', '57cfcc199ae15', '5757ba73cef90', '57835a6698899', 'Hi Richmund,', 'read', '2016-09-07 09:11:15', '2016-09-07 09:11:16'),
('57cfda525912e', '57cfcc199ae15', '57835a6698899', '5757ba73cef90', 'hey!', 'read', '2016-09-07 09:13:54', '2016-09-07 09:13:55'),
('57d0d778e4ab6', '57a3faec36e13', '5757ba73cef90', '57835d4dbd83f', 'testing 0001', 'read', '2016-09-08 03:14:00', '2016-09-08 06:17:28'),
('57d0d7fc246f0', '57a3faec36e13', '5757ba73cef90', '57835d4dbd83f', 'testing 0002', 'read', '2016-09-08 03:16:12', '2016-09-08 06:17:28'),
('57d0d835735e9', '57a3faec36e13', '5757ba73cef90', '57835d4dbd83f', 'testing 0003', 'read', '2016-09-08 03:17:09', '2016-09-08 06:17:28'),
('57d0d84d90530', '57a3faec36e13', '5757ba73cef90', '57835d4dbd83f', 'testing 0004', 'read', '2016-09-08 03:17:33', '2016-09-08 06:17:28'),
('57d0d8737be29', '57a3faec36e13', '5757ba73cef90', '57835d4dbd83f', 'testing 0005', 'read', '2016-09-08 03:18:11', '2016-09-08 06:17:28'),
('57d0de3696259', '57d0de3694af5', '56fc9e052d05f', '5757bbce106d2', 'hey', 'read', '2016-09-08 03:42:46', '2016-09-08 07:46:37'),
('57d0de408229d', '57d0de3694af5', '5757bbce106d2', '56fc9e052d05f', 'hi kuya', 'read', '2016-09-08 03:42:56', '2016-09-08 07:46:37'),
('57d0de5719a09', '57d0de3694af5', '56fc9e052d05f', '5757bbce106d2', 'test', 'read', '2016-09-08 03:43:19', '2016-09-08 07:46:37'),
('57d0dea0f1589', '57d0de3694af5', '5757bbce106d2', '56fc9e052d05f', 'hehea12', 'read', '2016-09-08 03:44:32', '2016-09-08 07:46:37'),
('57d0deccb7e48', '57d0de3694af5', '5757bbce106d2', '56fc9e052d05f', '12w312', 'read', '2016-09-08 03:45:16', '2016-09-08 07:46:37'),
('57d0ded7ca1b6', '57d0de3694af5', '5757bbce106d2', '56fc9e052d05f', 'chat ha', 'read', '2016-09-08 03:45:27', '2016-09-08 07:46:37'),
('57d0e16fc0792', '57d0de3694af5', '5757bbce106d2', '56fc9e052d05f', 'sure &lt;3', 'read', '2016-09-08 03:56:31', '2016-09-08 07:46:37'),
('57d0e1a53dbc4', '57d0de3694af5', '5757bbce106d2', '56fc9e052d05f', 'lllll', 'read', '2016-09-08 03:57:25', '2016-09-08 07:46:37'),
('57d0e1d333a5e', '57d0de3694af5', '56fc9e052d05f', '5757bbce106d2', '123', 'read', '2016-09-08 03:58:11', '2016-09-08 07:46:37'),
('57d0f0ebe552d', '57a3faec36e13', '5757ba73cef90', '57835d4dbd83f', 'testing 0005', 'read', '2016-09-08 05:02:35', '2016-09-08 06:17:28'),
('57d0f1070166a', '57a3faec36e13', '57835d4dbd83f', '5757ba73cef90', '001', 'read', '2016-09-08 05:03:03', '2016-09-08 06:19:32'),
('57d0f132e95c6', '57a3faec36e13', '5757ba73cef90', '57835d4dbd83f', 'testing 0006', 'read', '2016-09-08 05:03:46', '2016-09-08 06:17:28'),
('57d0f13e2793b', '57a3faec36e13', '57835d4dbd83f', '5757ba73cef90', '002', 'read', '2016-09-08 05:03:58', '2016-09-08 06:19:32'),
('57d0f19bc7f46', '57a3faec36e13', '5757ba73cef90', '57835d4dbd83f', 'testing 0007', 'read', '2016-09-08 05:05:31', '2016-09-08 06:17:28'),
('57d101e2e14a5', '57d0de3694af5', '5757bbce106d2', '56fc9e052d05f', 'hi', 'read', '2016-09-08 06:14:58', '2016-09-08 07:46:37'),
('57d101f9af06f', '57a3faec36e13', '5757ba73cef90', '57835d4dbd83f', 'abbey01tayler@gmail.com', 'read', '2016-09-08 06:15:21', '2016-09-08 06:17:28'),
('57d1021d3e37d', '57d0de3694af5', '5757bbce106d2', '56fc9e052d05f', 'hhh', 'read', '2016-09-08 06:15:57', '2016-09-08 07:46:37'),
('57d1021f377c9', '57a3faec36e13', '5757ba73cef90', '57835d4dbd83f', 'abbey01tayler@gmail.com&lt;br /&gt;abbey01tayler@gmail.com', 'read', '2016-09-08 06:15:59', '2016-09-08 06:17:28'),
('57d102890e93e', '57d0de3694af5', '5757bbce106d2', '56fc9e052d05f', '123', 'read', '2016-09-08 06:17:45', '2016-09-08 07:46:37'),
('57d1028e5cad0', '57a3faec36e13', '57835d4dbd83f', '5757ba73cef90', 'What is wrong with you?', 'read', '2016-09-08 06:17:50', '2016-09-08 06:19:32'),
('57d102b01d889', '57d0de3694af5', '56fc9e052d05f', '5757bbce106d2', 'hey', 'read', '2016-09-08 06:18:24', '2016-09-08 07:46:37'),
('57d102cd90cd0', '57d0de3694af5', '56fc9e052d05f', '5757bbce106d2', '222', 'read', '2016-09-08 06:18:53', '2016-09-08 07:46:37'),
('57d102e16737f', '57a3faec36e13', '57835d4dbd83f', '5757ba73cef90', 'abbey01tayler@gmail.com&lt;br /&gt;abbey01tayler@gmail.com', 'read', '2016-09-08 06:19:13', '2016-09-08 06:19:32'),
('57d103249ee19', '57a3faec36e13', '57835d4dbd83f', '5757ba73cef90', 'What is wrong with you?&lt;br /&gt;What is wrong with you?', 'un-read', '2016-09-08 06:20:20', '2016-09-08 06:20:20'),
('57d1045f53e2c', '57d0de3694af5', '5757bbce106d2', '56fc9e052d05f', '3sadfasdf', 'read', '2016-09-08 06:25:35', '2016-09-08 07:46:37'),
('5a7004def23ca', '5a7004def0622', '5a6fcc43a5d86', '5a5ed593321a5', 'hi', 'read', '2018-01-29 21:38:38', '2018-02-01 18:54:29'),
('5a700527ce88d', '5a7004def0622', '5a5ed593321a5', '5a6fcc43a5d86', 'hahaha', 'read', '2018-01-29 21:39:51', '2018-02-07 17:32:01');

-- --------------------------------------------------------

--
-- Table structure for table `gp_users_profile`
--

CREATE TABLE `gp_users_profile` (
  `profile_id` varchar(13) NOT NULL DEFAULT '',
  `user_id` varchar(13) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `telephone` varchar(30) DEFAULT NULL,
  `mobile` varchar(30) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gp_users_profile`
--

INSERT INTO `gp_users_profile` (`profile_id`, `user_id`, `first_name`, `last_name`, `telephone`, `mobile`, `created_at`, `updated_at`) VALUES
('5a5ed272ba3b8', '5a5ed2729be8b', 'adminforsb', 'adminforsb', '', NULL, '2018-01-16 20:34:58', '2018-01-16 20:34:58'),
('5a5ed5934fc6a', '5a5ed593321a5', 'user1', 'user1', '123123', NULL, '2018-01-16 20:48:19', '2018-01-16 20:48:19'),
('5a6fcc43c3159', '5a6fcc43a5d86', 'Test name', 'test last name', '123456', NULL, '2018-01-29 17:37:07', '2018-01-29 17:37:07'),
('5a79354848413', '5a7935483d07b', 'Shakey Shake', '', NULL, NULL, '2018-02-05 20:55:36', '2018-02-05 20:55:36');

-- --------------------------------------------------------

--
-- Table structure for table `gp_users_roles`
--

CREATE TABLE `gp_users_roles` (
  `role_id` varchar(13) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gp_users_roles`
--

INSERT INTO `gp_users_roles` (`role_id`, `name`, `description`, `created_at`, `updated_at`) VALUES
('56aebc46f281b', 'Super Admin', 'Super Admin Role', '2016-03-10 10:00:38', '2016-03-10 09:55:45'),
('56aebc46f2823', 'Administrator', 'Administrator Role', '2016-03-10 10:00:38', '2016-03-10 09:55:46'),
('56aebc46f2827', 'User', 'User Role', '2016-03-10 10:00:38', '2016-03-10 09:55:47');

-- --------------------------------------------------------

--
-- Table structure for table `gp_users_saved_search`
--

CREATE TABLE `gp_users_saved_search` (
  `search_id` varchar(13) NOT NULL,
  `keyword` varchar(255) DEFAULT NULL,
  `url` text,
  `created_by` varchar(13) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gp_users_saved_search`
--

INSERT INTO `gp_users_saved_search` (`search_id`, `keyword`, `url`, `created_by`, `created_at`, `updated_at`) VALUES
('5757e46a7ae4f', 'gear', 'http://gearplanet.sushidigital.ph/search?keyword=gear&location=nsw&brand=3rd+power&price_min=100&price_max=999&type=new&sort=created_at%7Cdesc', '56fc9e052d05f', '2016-06-08 09:24:58', '2016-06-08 09:24:58'),
('5757e47dee5b6', 'gear', 'http://gearplanet.sushidigital.ph/search?keyword=gear&location=&brand=&price_min=100&price_max=&type=new&sort=created_at%7Cdesc', '56fc9e052d05f', '2016-06-08 09:25:17', '2016-06-08 09:25:17'),
('5759138ca0e59', 'gui', 'http://gearplanet.sushidigital.ph/search?brand=&keyword=gui&location=&price_max=&price_min=&sort=product_price%7Casc&type=', '5757d13f04f58', '2016-06-09 06:58:20', '2016-06-09 06:58:20'),
('5759149217628', 'gui', 'http://gearplanet.sushidigital.ph/search?keyword=gui&location=nsw&brand=aftermarket&price_min=1&price_max=20&type=&sort=product_price%7Casc', '5757d13f04f58', '2016-06-09 07:02:42', '2016-06-09 07:04:37'),
('575915b4b5672', 'strings', 'http://gearplanet.sushidigital.ph/search?keyword=strings&location=all&brand=becker&price_min=1&price_max=500&type=&sort=product_price%7Cdesc', '5757d13f04f58', '2016-06-09 07:07:32', '2016-06-09 07:07:32'),
('575915d18dde8', 'key', 'http://gearplanet.sushidigital.ph/search?keyword=key&location=&brand=korg&price_min=&price_max=&type=&sort=product_price%7Casc', '5757d13f04f58', '2016-06-09 07:08:01', '2016-06-09 07:08:01'),
('57591607ea726', 'key', 'http://gearplanet.sushidigital.ph/search?keyword=key&location=vic&brand=korg&price_min=20&price_max=123456&type=&sort=product_price%7Cdesc', '5757d13f04f58', '2016-06-09 07:08:55', '2016-06-09 07:08:55'),
('576215fa92a54', 'ha', 'http://gearplanet.sushidigital.ph/search?keyword=ha&location=&brand=&price_min=&price_max=&type=new&sort=', '5757bb6decbb1', '2016-06-16 02:59:06', '2016-06-16 02:59:06'),
('576216029cd22', 'ha', 'http://gearplanet.sushidigital.ph/search?keyword=ha&location=&brand=&price_min=&price_max=&type=new&sort=product_price%7Casc', '5757bb6decbb1', '2016-06-16 02:59:14', '2016-06-16 02:59:14'),
('576281e9a663e', 'fe', 'http://gearplanet.sushidigitaldemo.info/search?keyword=fe', '5757cc0ae2bf2', '2016-06-16 10:39:37', '2016-06-16 10:39:37'),
('57628265ddc7e', 'fe', 'http://gearplanet.sushidigitaldemo.info/search?keyword=fe&location=&brand=&price_min=&price_max=&type=new&sort=', '5757cc0ae2bf2', '2016-06-16 10:41:41', '2016-06-16 10:41:41'),
('576283e23b963', 'ge', 'http://gearplanet.sushidigitaldemo.info/search?keyword=ge', '5757ce2636dfd', '2016-06-16 10:48:02', '2016-06-16 10:48:02'),
('576357450cb25', 'guitar', 'http://gearplanet.sushidigitaldemo.info/search?keyword=guitar', '5757cc0ae2bf2', '2016-06-17 01:49:57', '2016-06-17 01:49:57'),
('576362933b75f', 'tes', 'http://gearplanet.sushidigitaldemo.info/search?keyword=tes&page=2', '5757cc0ae2bf2', '2016-06-17 02:38:11', '2016-06-17 02:38:23'),
('576362a80c283', 'tes', 'http://gearplanet.sushidigitaldemo.info/search?keyword=tes&location=&brand=&price_min=&price_max=&type=&sort=product_price%7Casc', '5757cc0ae2bf2', '2016-06-17 02:38:32', '2016-06-17 02:38:32'),
('5782039f05738', 'tascam', 'http://gearplanet.sushidigitaldemo.info/search?keyword=tascam', '577e48310c58d', '2016-07-10 08:13:19', '2016-12-18 08:39:14'),
('578dd15014182', 'gea', 'http://gearplanet.sushidigital.ph/search?keyword=gea', '56fc9e052d05f', '2016-07-19 07:05:52', '2016-07-19 07:05:52'),
('57982f862751c', 'gear1', 'http://gearplanet.sushidigital.ph/search?keyword=gear1&location=nsw&brand=3rd+power&price_min=100&price_max=999&type=new&sort=created_at%7Cdesc', '56fc9e052d05f', '2016-07-27 03:50:30', '2016-07-27 03:50:50'),
('57982ffee5f67', 'akai', 'http://gearplanet.sushidigital.ph/search?keyword=akai&location=all&brand=all&type=all&sort=created_at|desc', '578c538e46670', '2016-07-27 03:52:30', '2016-07-27 03:52:30'),
('5798300df34cc', 'akai', 'http://gearplanet.sushidigital.ph/search?keyword=akai&location=&brand=&price_min=&price_max=&type=new&sort=', '578c538e46670', '2016-07-27 03:52:45', '2016-07-27 03:53:03'),
('5798302fc700d', 'akai', 'http://gearplanet.sushidigital.ph/search?keyword=akai&location=all&brand=all&price_min=&price_max=&type=all&sort=product_price%7Casc', '578c538e46670', '2016-07-27 03:53:19', '2016-07-27 03:53:40'),
('57aaba594b07a', 't', 'http://gearplanet.sushidigitaldemo.info/search?keyword=t&location=wa&brand=&price_min=&price_max=&type=&sort=product_price%7Casc', '57835d4dbd83f', '2016-08-10 05:23:37', '2016-08-10 05:23:37'),
('57bc190a69bcb', 'black', 'http://gearplanet.sushidigitaldemo.info/search?keyword=black&location=&brand=&price_min=&price_max=&type=&sort=product_price%7Casc', '57bc138572cf7', '2016-08-23 09:36:10', '2016-08-23 09:36:10'),
('57bc194674e06', 'black', 'http://gearplanet.sushidigitaldemo.info/search?keyword=black&location=&brand=&price_min=&price_max=&type=new&sort=product_price%7Casc', '57bc138572cf7', '2016-08-23 09:37:10', '2016-08-23 09:37:10'),
('57bc194f5cb28', 'black', 'http://gearplanet.sushidigitaldemo.info/search?keyword=black&location=&brand=&price_min=&price_max=&type=all&sort=product_price%7Casc', '57bc138572cf7', '2016-08-23 09:37:19', '2016-08-23 09:37:19'),
('57bc195b94c73', 'black', 'http://gearplanet.sushidigitaldemo.info/search?keyword=black&location=&brand=&price_min=&price_max=&type=pre-owned&sort=product_price%7Casc', '57bc138572cf7', '2016-08-23 09:37:31', '2016-08-23 09:37:31'),
('57bc1b6f74a12', 'black', 'http://gearplanet.sushidigitaldemo.info/search?keyword=black&location=wa&brand=fender&price_min=800&price_max=1000&type=all&sort=product_price%7Casc', '57bc138572cf7', '2016-08-23 09:46:23', '2016-08-23 09:46:23'),
('57c7f32008d42', 'ger', 'http://gearplanet.sushidigitaldemo.info/search?keyword=ger', '5757bbce106d2', '2016-09-01 09:21:36', '2016-09-01 09:21:36'),
('57c94ac7bd405', 'tyler', 'http://gearplanet.sushidigitaldemo.info/search?keyword=tyler', '57835d4dbd83f', '2016-09-02 09:47:51', '2016-09-02 10:17:20'),
('57c9553f82df6', 'amp', 'http://gearplanet.sushidigitaldemo.info/search?keyword=amp', '57835d4dbd83f', '2016-09-02 10:32:31', '2016-09-02 10:32:37'),
('57c96ed435597', 'mpc', 'http://gearplanet.sushidigitaldemo.info/search?keyword=mpc', '577e48310c58d', '2016-09-02 12:21:40', '2016-09-02 12:23:03'),
('580ed3cc3bb37', 'amp', 'http://gearplanet.sushidigitaldemo.info/search?keyword=amp&location=qld&brand=&price_min=&price_max=&type=&sort=', '57835d4dbd83f', '2016-10-25 03:38:52', '2016-10-25 03:38:52'),
('581174af8b1c1', 'test test', 'http://gearplanet.sushidigitaldemo.info/search?keyword=test+test', '56e0d53867459', '2016-10-27 03:29:51', '2016-10-27 03:29:51'),
('581992318ff83', 'find gear', 'http://gearplanet.sushidigitaldemo.info/search?keyword=find+gear', '57c7f3dff0df7', '2016-11-02 07:13:53', '2016-11-02 07:13:53'),
('5819932a8d79f', 'amps', 'http://gearplanet.sushidigitaldemo.info/search?keyword=amps', '57c7f3dff0df7', '2016-11-02 07:18:02', '2016-11-02 07:18:02'),
('5819933b4d247', 'electric guitar', 'http://gearplanet.sushidigitaldemo.info/search?keyword=electric+guitar', '57c7f3dff0df7', '2016-11-02 07:18:19', '2016-11-02 07:18:19'),
('58199348a5c68', 'electric', 'http://gearplanet.sushidigitaldemo.info/search?keyword=electric', '57c7f3dff0df7', '2016-11-02 07:18:32', '2016-11-02 07:18:32'),
('5819935f0443b', 'sound', 'http://gearplanet.sushidigitaldemo.info/search?keyword=sound', '57c7f3dff0df7', '2016-11-02 07:18:55', '2016-11-02 07:18:55'),
('581c25e16809b', 'guitars', 'http://gearplanet.sushidigitaldemo.info/search?keyword=guitars', '577dc8e9c8d6a', '2016-11-04 06:08:33', '2016-11-04 06:08:33');

-- --------------------------------------------------------

--
-- Table structure for table `gp_users_settings`
--

CREATE TABLE `gp_users_settings` (
  `settings_id` varchar(13) NOT NULL,
  `user_id` varchar(13) DEFAULT NULL,
  `email_message_notification` enum('off','on') NOT NULL DEFAULT 'on',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gp_users_settings`
--

INSERT INTO `gp_users_settings` (`settings_id`, `user_id`, `email_message_notification`, `created_at`, `updated_at`) VALUES
('57cd062ae9ab7', '5757bbce106d2', 'on', '2016-09-05 05:44:10', '2016-09-05 05:44:11'),
('57cfcd2ee924b', '57835a6698899', 'on', '2016-09-07 08:17:50', '2016-09-07 08:17:50'),
('57d109f288736', '57d109f273c67', 'on', '2016-09-08 06:49:22', '2016-09-08 06:49:22'),
('580d699f69bbf', '580d699f5469a', 'on', '2016-10-24 01:53:35', '2016-10-24 01:53:35'),
('581190ad07b3a', '581190ace6cb4', 'on', '2016-10-27 05:29:17', '2016-10-27 05:29:17'),
('581193073cfd9', '5811930728cee', 'on', '2016-10-27 05:39:19', '2016-10-27 05:39:19'),
('5811941d63ef0', '5811941d4f835', 'on', '2016-10-27 05:43:57', '2016-10-27 05:43:57'),
('58119a2f3b482', '58119a2f25e17', 'on', '2016-10-27 06:09:51', '2016-10-27 06:09:51'),
('58119f3b1c643', '58119f3b07658', 'on', '2016-10-27 06:31:23', '2016-10-27 06:31:23'),
('58119fec2b5eb', '58119fec164a2', 'on', '2016-10-27 06:34:20', '2016-10-27 06:34:20'),
('5811a0a69e9ae', '5811a0a68a5cc', 'on', '2016-10-27 06:37:26', '2016-10-27 06:37:26'),
('5811a1462b59d', '5811a14616701', 'on', '2016-10-27 06:40:06', '2016-10-27 06:40:06'),
('5812ebf60b50c', '5812ebf5e9ed2', 'on', '2016-10-28 06:11:02', '2016-10-28 06:11:02'),
('598188908ecbf', '5981889078112', 'on', '2017-08-02 08:08:48', '2017-08-02 08:08:48'),
('5987c82d4556a', '5987c82d2eed4', 'on', '2017-08-07 01:53:49', '2017-08-07 01:53:49'),
('5987c883395ce', '5987c88324398', 'on', '2017-08-07 01:55:15', '2017-08-07 01:55:15'),
('5a5ed272b6810', '5a5ed2729be8b', 'on', '2018-01-16 20:34:58', '2018-01-16 20:34:58'),
('5a5ed5934c957', '5a5ed593321a5', 'on', '2018-01-16 20:48:19', '2018-01-16 20:48:19'),
('5a6fcc43be633', '5a6fcc43a5d86', 'on', '2018-01-29 17:37:07', '2018-01-29 17:37:07'),
('5a7935484485f', '5a7935483d07b', 'on', '2018-02-05 20:55:36', '2018-02-05 20:55:36');

-- --------------------------------------------------------

--
-- Table structure for table `gp_users_shipping_address`
--

CREATE TABLE `gp_users_shipping_address` (
  `shipping_id` varchar(13) NOT NULL,
  `user_id` varchar(13) DEFAULT NULL,
  `address_1` varchar(255) DEFAULT NULL,
  `address_2` varchar(255) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(10) DEFAULT NULL,
  `post_code` varchar(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gp_users_shipping_address`
--

INSERT INTO `gp_users_shipping_address` (`shipping_id`, `user_id`, `address_1`, `address_2`, `city`, `state`, `post_code`, `created_at`, `updated_at`) VALUES
('573a8960dd98d', '56fc9e052d05f', 'address 1', 'address 2', 'city', 'qld', '6532', '2016-05-18 10:22:23', '2016-06-18 03:27:03'),
('57455cc447c13', '56e258f8ddc6a', 'add 11', 'add 22', 'city', 'wa', '1234', '2016-05-25 08:05:24', '2016-05-25 08:08:02'),
('5757dd3304773', '5757cc0ae2bf2', 'Test St.', '', 'Perth', 'wa', '6000', '2016-06-08 08:54:11', '2016-06-17 01:57:14'),
('575a81c404d1a', '5757ce7294790', '85 Chatsworth Drive', '', 'CANNING MILLS', 'wa', '6111', '2016-06-10 09:00:52', '2016-06-10 09:00:52'),
('575e45e41c9ca', '5757bc733bfff', '', '', '', '', '', '2016-06-13 05:34:28', '2016-06-13 05:34:28'),
('576101f550daa', '5757cca8a0b11', '6 Ranworth Road', NULL, 'BULL CREEK', 'wa', '6149', '2016-06-15 07:21:25', '2016-06-15 07:21:25'),
('5761212040708', '5757cfdb6215a', '17 Jacabina Court', '', 'RUSSELL VALE', 'nsw', '2517', '2016-06-15 09:34:24', '2016-06-15 09:36:43'),
('576122377475b', '5757ccf71cdd1', '59 Jacolite Street', '', 'MELALEUCA', 'wa', '6065', '2016-06-15 09:39:03', '2016-06-15 09:39:03'),
('576218bec89a3', '5757bb6decbb1', '90 Chatsworth Drive', '', 'ASCOT', 'wa', '6104', '2016-06-16 03:10:54', '2016-06-16 03:29:52'),
('5762199057e13', '5757bbce106d2', 'test', '', 'Perth', 'nsw', '6000', '2016-06-16 03:14:24', '2016-09-07 07:33:17'),
('577e50119eab7', '577e48310c58d', '77 sixth', '', 'mount lawley', 'wa', '6050', '2016-07-07 12:50:25', '2016-12-18 09:00:27'),
('577e516589084', '5763caf04caea', '89 hobart st', '', 'Mount hawthorn', 'wa', '6050', '2016-07-07 12:56:05', '2016-07-07 12:56:15'),
('577e66b571ac3', '576512da6dd61', '762 Beaufort St', '', 'Mt Lawley', 'wa', '6050', '2016-07-07 14:27:01', '2016-07-07 14:27:01'),
('57a97e684b317', '5757ce2636dfd', '92 Banksia Street', '', 'ZUYTDORP', 'wa', '6536', '2016-08-09 06:55:36', '2016-08-09 06:55:36'),
('57b1a624b0ea7', '5783736ab0d23', '762 Beaufort st', '', 'Mount Lawley', 'wa', '6050', '2016-08-15 11:23:16', '2016-08-30 11:43:09'),
('57b1a8695edfc', '57b1a7776e17a', '762 Beaufort st', '', 'mount lawley', 'wa', '6050', '2016-08-15 11:32:57', '2016-08-20 09:53:16'),
('57b1ae4aee1ae', '57b1aba23598d', '762 Beaufort st', '', 'Mount Lawley', 'wa', '6050', '2016-08-15 11:58:02', '2016-08-15 12:07:51'),
('57b28f03566db', '57835d4dbd83f', 'Sushi Testing', '', 'Perth', 'nsw', '6041', '2016-08-16 03:56:51', '2016-08-16 03:56:51'),
('57bc2811c5ea2', '57bc138572cf7', '1233 Horsea St', '', 'Perth', 'wa', '6041', '2016-08-23 10:40:17', '2016-08-23 10:40:17'),
('57c7d20b05410', '578d9ba2ea31b', 'Sushi Digital St.', '', 'Perth', 'wa', '6041', '2016-09-01 07:00:27', '2016-09-01 07:01:49'),
('57ce63e70f422', '57c7f3dff0df7', '58 Moruya Road', '', 'GUNDILLION', 'nsw', '2622', '2016-09-06 06:36:23', '2016-11-03 04:09:39'),
('57cfc3ae29297', '57835a6698899', 'carol street', 'carolina street', 'Sydney', 'nsw', '2148', '2016-09-07 07:37:18', '2016-09-14 02:46:26'),
('57db9d3a918c9', '5757ba73cef90', 'Address one and final', 'Address two and final', 'sydney', 'wa', '5646', '2016-09-16 07:20:26', '2016-09-16 08:33:12'),
('5809c3b87a9c7', '5757b7619536a', 'test', 'test', 'test', 'wa', '12345', '2016-10-21 07:28:56', '2016-10-21 07:28:56'),
('58199abe2c7b5', '5811a0a68a5cc', '20 Seninis Road', '', 'RITE ISLAND', 'qld', '4807', '2016-11-02 07:50:22', '2016-11-02 08:42:40'),
('598189260f33d', '5981889078112', '351 Hay Street', '', 'Subiaco', 'wa', '6008', '2017-08-02 08:11:18', '2017-08-02 08:11:18'),
('5987cdb594306', '5987c88324398', '99 Hay Street', '', 'Subiaco', 'wa', '6008', '2017-08-07 02:17:25', '2017-08-07 02:17:25'),
('5a5f0a40727aa', '5a5ed593321a5', 'dada', 'dada', '3213', 'nsw', '21313', '2018-01-17 00:33:04', '2018-01-17 00:33:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gp_blogs`
--
ALTER TABLE `gp_blogs`
  ADD PRIMARY KEY (`blog_id`);

--
-- Indexes for table `gp_blogs_comments`
--
ALTER TABLE `gp_blogs_comments`
  ADD PRIMARY KEY (`blog_comment_id`);

--
-- Indexes for table `gp_builds`
--
ALTER TABLE `gp_builds`
  ADD PRIMARY KEY (`build_id`);

--
-- Indexes for table `gp_cart`
--
ALTER TABLE `gp_cart`
  ADD PRIMARY KEY (`cart_id`);

--
-- Indexes for table `gp_categories`
--
ALTER TABLE `gp_categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `gp_failed_jobs`
--
ALTER TABLE `gp_failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gp_jobs`
--
ALTER TABLE `gp_jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_reserved_reserved_at_index` (`queue`,`reserved`,`reserved_at`);

--
-- Indexes for table `gp_orders`
--
ALTER TABLE `gp_orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `gp_orders_history`
--
ALTER TABLE `gp_orders_history`
  ADD PRIMARY KEY (`order_history_id`);

--
-- Indexes for table `gp_orders_payment_info`
--
ALTER TABLE `gp_orders_payment_info`
  ADD PRIMARY KEY (`order_payment_id`);

--
-- Indexes for table `gp_orders_products`
--
ALTER TABLE `gp_orders_products`
  ADD PRIMARY KEY (`order_product_id`);

--
-- Indexes for table `gp_orders_shipping_info`
--
ALTER TABLE `gp_orders_shipping_info`
  ADD PRIMARY KEY (`order_shipping_id`);

--
-- Indexes for table `gp_orders_status`
--
ALTER TABLE `gp_orders_status`
  ADD PRIMARY KEY (`order_status_id`);

--
-- Indexes for table `gp_password_resets`
--
ALTER TABLE `gp_password_resets`
  ADD PRIMARY KEY (`password_reset_id`);

--
-- Indexes for table `gp_products`
--
ALTER TABLE `gp_products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `gp_products_condition`
--
ALTER TABLE `gp_products_condition`
  ADD PRIMARY KEY (`condition_id`);

--
-- Indexes for table `gp_products_photos`
--
ALTER TABLE `gp_products_photos`
  ADD PRIMARY KEY (`photo_id`);

--
-- Indexes for table `gp_products_shipping`
--
ALTER TABLE `gp_products_shipping`
  ADD PRIMARY KEY (`product_shipping_id`);

--
-- Indexes for table `gp_products_shipping_cost`
--
ALTER TABLE `gp_products_shipping_cost`
  ADD PRIMARY KEY (`product_shipping_cost_id`);

--
-- Indexes for table `gp_products_specs`
--
ALTER TABLE `gp_products_specs`
  ADD PRIMARY KEY (`product_specs_id`);

--
-- Indexes for table `gp_users`
--
ALTER TABLE `gp_users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `gp_users_company`
--
ALTER TABLE `gp_users_company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `gp_users_inbox`
--
ALTER TABLE `gp_users_inbox`
  ADD PRIMARY KEY (`inbox_id`);

--
-- Indexes for table `gp_users_messages`
--
ALTER TABLE `gp_users_messages`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `gp_users_profile`
--
ALTER TABLE `gp_users_profile`
  ADD PRIMARY KEY (`profile_id`);

--
-- Indexes for table `gp_users_roles`
--
ALTER TABLE `gp_users_roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `gp_users_saved_search`
--
ALTER TABLE `gp_users_saved_search`
  ADD PRIMARY KEY (`search_id`);

--
-- Indexes for table `gp_users_settings`
--
ALTER TABLE `gp_users_settings`
  ADD PRIMARY KEY (`settings_id`);

--
-- Indexes for table `gp_users_shipping_address`
--
ALTER TABLE `gp_users_shipping_address`
  ADD PRIMARY KEY (`shipping_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gp_failed_jobs`
--
ALTER TABLE `gp_failed_jobs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gp_jobs`
--
ALTER TABLE `gp_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
