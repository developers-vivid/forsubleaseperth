<?php

return [
	/*
	|--------------------------------------------------------------------------
	| Global
	|--------------------------------------------------------------------------
	*/
		"welcome" => 'Welcome to Forusblease',
		"storage_not_found" => 'Image not upload because the storage path not found',

		"user_signup" 	=> [
			"signedup" 	=> 'Success. You have signed up to Forusblease! Please check your email for a verification link to get started!',
			"loggedin" 	=> 'Success. You have signed in! Redirecting ...',
		],

		"resend_verification_email" => 'Success. Please check your email for a verification link!',
	/*
	|--------------------------------------------------------------------------
	| User Account
	|--------------------------------------------------------------------------
	*/
		"user_not_activated"    => 'Your email address has not been verified. Click to <a href="javascript:void(0);" class="btn-resend-verification">re-send verification email</a>.',
		"user_declined"         => 'Your account has been declined.',
		"user_type_not_allowed" => 'Error! your account is not allowed to login',
		"user_type_not_seller"  => 'Error! your account is not a seller type',
		"account_missing"       => 'Account is missing. If you have already an account please enter <strong>Email</strong> and <strong>Password</strong> fields only',
		"account_not_match"     => 'Error! your account credentials do not match our records',
		"email_taken"           => 'The Email Address has already been taken',
		"user" => [
			"must_agree" => [
				"on_signup" => "You must agree to the terms of use and privacy policy",
			],
			"change_pass" => [
				"email_sent" => "Email has been sent to your account",
				"email_not_exist" => "Email address does not exist",
				"error" => "Unable to reset. Please try again",
				"invalid_token" => "Token does not exist or already expired",
				"success" => "Password has been changed",
			],
		],

	/*
	|--------------------------------------------------------------------------
	| Cart
	|--------------------------------------------------------------------------
	*/
		"cart" => [
			"gear_not_exist" => 'Item does not exist',
			"added" => 'Item has been added to your cart',
			"updated" => 'Your cart has been updated',
			"empty" => 'Your cart is empty. Browse now!',
			"product" => [
				"not_removed" => 'Unable to remove product from cart',
				"removed" => 'Product has been successfully removed',
			],
			"product_qty" => [
				"updated" => 'Product qty has been updated',
				"not_updated" => 'Unable to update product qty',
			]
		],

	/*
	|--------------------------------------------------------------------------
	| Checkout
	|--------------------------------------------------------------------------
	*/
		"checkout" => [
			"logged_in_users" => 'To checkout, please login',
			"completed" => 'Thank you, your order has been placed. An email has been sent to you.',
			"cancelled" => 'Your order has been cancelled.',
		],

	/*
	|--------------------------------------------------------------------------
	| Account Settings
	|--------------------------------------------------------------------------
	*/
		"account_settings" => [
			"successfully_saved" => 'Successfully Saved',
		],

	/*
	|--------------------------------------------------------------------------
	| Blogs
	|--------------------------------------------------------------------------
	*/
		"blog" => [
			"created" => "Blog has been successfully created",
			"updated" => "Blog has been successfully updated",
			"not_created" => "Unable to create blog. Please try again.",
			"exist" => "Blog has already been exist",
			"deleted" => "Blog has been deleted",
			"not_deleted" => "Unable to delete blog. Please try again",
		],

	/*
	|--------------------------------------------------------------------------
	| Gears
	|--------------------------------------------------------------------------
	*/
		"gear_created"     => 'Your ad has been successfully posted',
		"gear_not_updated" => 'Unable to update gear. Please try again.', 
		"gear" => [
			"created" => "Your ad has been successfully posted.",
			"not_created" => "Unable to post your ad. Please try again.",
			"updated" => "You ad has been successfully updated",
			"update_not_allowed" => "You are not allowed to update this ad",
			"not_found" => "Sorry! The gear you were looking for does not exist or already been sold",
			"not_owner" => "Sorry! You are not the owner of this gear",
			"listing_title_taken" => "Ad listing title was already been taken",
			"cancel" => [
				"success" => "Your ad has been cancelled",
				"error" => "Unable to cancel your ad. Please try again.",
				"not_allowed" => "You are not allowed to cancel this ad.",
			],
			"delete" => [
				"success" => "Your ad has been deleted",
				"error" => "Unable to delete your ad. Please try again.",
				"not_allowed" => "You are not allowed to delete this ad.",
			],
			"activate" => [
				"success" => "Your ad has been activated",
				"error" => "Unable to activate your ad. Please try again.",
				"not_allowed" => "You are not allowed to activate this ad.",
			],
			"photo" => [
				"removed" => "Photo has been successfully removed.",
				"not_removed" => "Could not remove Product Primary Photo.",
				"nothing_to_remove" => "Nothing to remove",
				"primary" => [
					"updated" => "Updated product primary photo.",
					"not_updated" => "Could not set Product Primary Photo.",
				],
			],
		],
		"ad" => [
			"activated" => 'Ad has been activated',
			"not_activated" => 'Unable to activate ad',
			"cancelled" => 'Ad has been Cancelled',
			"not_cancelled" => 'Unable to cancel ad',
			"deleted" => 'Ad has been Deleted',
			"not_deleted" => 'Unable to delete ad',
		],

	/*
	|--------------------------------------------------------------------------
	| API
	|--------------------------------------------------------------------------
	*/
 	"api" => [
		"method_not_found" => 'Method is missing.',
		"token_required"   => 'Token is required.',
		"invalid_user" => 'You are not allowed to perform this operation.',
		"agreement" => "You need to agree to the terms of use and privacy policy",
		"settings" => [
			"all" => "Settings are loaded",
			"categories_loaded" => 'Categories has been loaded',
		],
		"user" => [
			'not_verified' => 'Account is not yet verified',
            'role_not_allowed' => 'Account is not allowed to access',
			"inbox" => "Inbox has been loaded",
			"inbox_message" => "Messages has been loaded",
			"message_details" => "Message details has been loaded",
			"login" => "User has been successfully signed in",
			"not_found" => "These credentials do not match our records",
			"ads" => [
				"loaded" => "User ads has been loaded",
			],
			"sign_up" => [
				"local" => "A verification email has been sent to you. Please check your inbox and follow the instructions to get verified and start using your account.",
				"social_media" => "You have successfully sign up",
			],
		],
		"messages" => [
			"sent" => 'Message has been successfully sent',
			"not_sent" => 'Message not sent. Please try again later.',
		],
		"search" => [
			"loaded" => 'Search list has been loaded',
			"saved" => 'Search has been saved',
			"deleted" => 'Search has been deleted',
			"not_exist" => 'Search does not exist',
			"not_owner" => 'Unable to remove saved search. You are not the owner.',
		],
		'deprecated' => 'THIS API IS DEPRECATED OR NO LONGER IN USE',
        'success' => 'Success',
        'error' => [
            'page_not_found' => "Sorry, this page isn't available",
            'unrecognized_method' => "We couldn't recognized this method",
            'went_wrong' => 'Whoops, looks like something went wrong. Please try again.',
            'no_record' => 'These credentials do not matched our records',
        ],
    	'method' => [
    		'mismatched' => 'Method mismatched and/or not found',
    		'requires_auth_users' => 'This method requires authenticated users. Access token is required.',
    	],
        'request' => [
            'invalid' => 'Invalid request',
            'no_data' => 'Request has no data',
            'not_json' => 'Invalid request content type',
            'unknown_method' => 'Unknown and/or invalid method',
            'build' => [
            	'missing' => 'Build missing',
            	'unknown' => 'No build added to system',
            	'mismatched' => 'Build mismatched. Please install the latest build',
            	'update' => [
            		'not_allowed' => 'You are not allowed to update app build.',
            		'success' => 'App build version has been updated.',
            		'denied' => 'Unable to update build app version.',
            	],
            ],
            'device' => [
            	"invalid" => 'Invalid build device type'
            ],
        ],
        "gears" => [
        	"loaded" => "Gears has been loaded",
        	"not_found" => "Gears not found",
        ],
        "orders" => [
        	"success" => "Your order been successfully done",
        	"error" => "Unable to process your order. Please try again.",
       	]
 	],
];