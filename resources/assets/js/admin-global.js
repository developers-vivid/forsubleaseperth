$(document).ready(function(){

	isRegisteredSeller();

	$('input[name="registered_seller"]').on('click', function(){
		isRegisteredSeller();
	});

	function isRegisteredSeller(){
		if( $('input[name="registered_seller"]').is(':checked') )
			$('.company-details').removeClass('hidden');
		else
			$('.company-details').addClass('hidden');		
	}
});