var showToastr;

( function( $ ) {

	showToastr = function showToastr(status, message, noClear){
		if( typeof noClear == 'undefined' || ! noClear )
			toastr.clear();

		if( typeof message == 'object' ) {
			for(var i = (message.length - 1); i >= 0; i--) {
				toastr[status](message[i]);
			}
		}
		else {
			toastr[status](message);
		}
	}

} )( jQuery );