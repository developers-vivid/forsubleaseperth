/*
* GLOBAL TOOLS AND UTILITIES SCRIPTS
* Place all custom js/jquery scripts here.
*
*/


/* Initialize and/or execute, code jQuery Scripts here */
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
 
$(document).ready(function() {
            
		
	if ($(window).width() <= 767) {
		$(".clicker").on('click', function() {
		       $(this).siblings('.hide-results').slideToggle(100);
		});
	}
    
	// $('.drawer').drawer();

	if (isMobile()){
		console.log('mobile');
	}else {
		console.log('not');
	}

	$('nav#menu').mmenu({
		"extensions": [
			"pagedim-black" 
        ],
		"offCanvas": {
			"position"	: "right",
			"zposition"	: "front"
		},
		"slidingSubmenus": false
	}); 

	// $(".drawer-menu input").on('focusout', function() {
	// 	$('.drawer').drawer({
	// 		state: false
	// 	});
	// });


	// $('.drawer-dropdown-menu-item').on('click', function() {
	// 	console.log("")
	// });

	$(".user-circle").on('click', function() {
		$(this).siblings('.sub-container').slideToggle(100);
	});

	$('#myCarousel').carousel({
      interval: 0
    });
	
	//Banner Slider
	$('.banner-container').slick({
		dots: false,
		arrows: false,
		infinite: true,
		speed: 2000,
		autoplay: true,
		fade: true,
		cssEase: 'linear'
	});

    var $container_1 = $('.gridviewlayout');
	$container_1.imagesLoaded(function(){
	  $container_1.masonry({
	      itemSelector: '.product-item'
	      
	  });
	  $('.product-item img').addClass('not-loaded');
	  $('.product-item img.not-loaded').lazyload({
	      effect: 'fadeIn',
	      load: function() {
	          // Disable trigger on this image
	          $(this).removeClass("not-loaded");
	          $container_1.masonry('reload');
	      }
	  });
	  $('.product-item img.not-loaded').trigger('scroll');
	});

	var $container_2 = $('.gridviewlayout.product-gear');
	$container_2.imagesLoaded(function(){
	  $container_2.masonry({
	      itemSelector: '.product-item'
	      
	  });
	  $('.product-item img').addClass('not-loaded');
	  $('.product-item img.not-loaded').lazyload({
	      effect: 'fadeIn',
	      load: function() {
	          // Disable trigger on this image
	          $(this).removeClass("not-loaded");
	          $container_2.masonry('reload');
	      }
	  });
	  $('.product-item img.not-loaded').trigger('scroll');
	});

	//Categories Accordion
 //   	$('a.drawer-menu-item').on('click',function( ){
	// 	if($(this).siblings('.drawer-dropdown-menu').is(":hidden")){
	// 		$(this).children().addClass('fa-minus');
	// 		$(this).children().removeClass('fa-plus');
	// 	}else{
	// 		$(this).children().removeClass('fa-minus');
	// 		$(this).children().addClass('fa-plus');
	// 	}		
	// });

	$('.categories li a.dropdownbtn').on('click',function( ){
		if($(this).prev().is(":hidden")){
			$(this).prev().addClass('diplayitem');
			$(this).addClass('fa-minus');
			$(this).removeClass('fa-plus');
		}else{
			$(this).prev().removeClass('diplayitem');
			$(this).removeClass('fa-minus');
			$(this).addClass('fa-plus');
		}		
	});

   	$('.categories li.active ul li.active').parent().css('display', 'block');

  	//Register Seller field
	isRegisteredSeller();

	$('input[name="registered_seller"]').on('click', function(){
		isRegisteredSeller();
	});

	function isMobile(){
		var isMobile = false; //initiate as false
        // device detection
        if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;
        return isMobile;
    }
	function isRegisteredSeller(){
		if( $('input[name="registered_seller"]').is(':checked') ) {
			$('.company-info-heading').removeClass('hidden');
			$('.company-info-details').removeClass('hidden');
		}
		else {
			$('.company-info-heading').addClass('hidden');
			$('.company-info-details').addClass('hidden');	
		}
	}

	//register agree field
	$('.sign-up input[name="agree"]').change(function(){
	   if( $(this).is(":checked") ) {
	       $(this).attr('value', 1);
	   } else {
	       $(this).attr('value', 0);
	   }
	});

	//Upload field
	$('#uploadBtn').change(function() {
    var upload_path = $(this).val();
    $('#uploadFile').val(upload_path);
  });

	
	//Products
	$(function(){
		//Product Container - Home
		$('.products-home').mixItUp();

		//Product Container - Gear
		var //layout = 'list', // Store the current layout as a variable
			$container_gear = $('.product-gear'), // Cache the MixItUp container
			$ListLayout = $('#ListLayout'), // Cache the ListLayout button
			$GridLayout = $('#GridLayout'); // Cache the GridLayout button

		// Instantiate MixItUp with some custom options:
		$container_gear.mixItUp({
			animation: {
			    duration: 400,
			    effects: '',
			    easing: ''
			}
		});
	});	
	
	//Gears Tabbing
	$('.new-pre-owned-container ul li').on('click',function(){
		$(this).addClass('selected').siblings().removeClass('selected');
	});

	$('.view-type-container button').on('click',function(){
		$(this).addClass('selected').siblings().removeClass('selected');
	});

	//Gears Details
	$('.tab ul.tabs').addClass('active').find('> li:eq(0)').addClass('current');
		
	$('.tab ul.tabs li a').click(function (g) { 
		var tab = $(this).closest('.tab'), 
			index = $(this).closest('li').index();
		
		tab.find('ul.tabs > li').removeClass('current');
		$(this).closest('li').addClass('current');
		
		//if( !$(this).parent().hasClass('current') ) {
			tab.find('.tab_content').find('div.tabs_item').not('div.tabs_item:eq(' + index + ')').hide();
			tab.find('.tab_content').find('div.tabs_item:eq(' + index + ')').show();
		//}
		
		g.preventDefault();
	} );

	$('input[name="brand"], input[name="model"], input[name="year"], input[name="colour"]').on('keyup', function(){
		var listingTitle = $('input[name="brand"]').val() +' '+ $('input[name="model"]').val() +' '+ $('input[name="year"]').val() +' '+ $('input[name="colour"]').val();
		$('input[name="listing_title"]').val(listingTitle);
	});

	//Select 
	$(".chosen").chosen();

	// Adding Field 		
		$(".add-field").click(function(e) {
	        $('.multi-field-option').removeClass('hidden');
	        $('.add-container').addClass('hidden');    
	    });

	    $('.save-field').on('click', function() {
	    	var location_select = $( '.multi-field-option #location');
	    	var location = $( '.multi-field-option #location option:selected' ).val();
			var shipping_cost = $( '.multi-field-option .currency-field input.input-field' ).val();

			if( location != '' && shipping_cost != '' ) {
				var wrapper  =	"<div class='col-lg-12 col-md-12 col-xs-12 shipping-to multi-field-add'><label class='col-lg-6 col-md-6 col-xs-6 control-label'>" + location.toUpperCase() + "</label><div class='col-lg-5 col-md-5 col-xs-5 currency-field'><i class='fa fa-usd' aria-hidden='true'></i><input class='form-control shipping-costs' placeholder='AUD' name='metro_areas_cost' type='number' value='" + shipping_cost + "'></div><div class='col-lg-1 col-md-1 col-xs-1 optionsbtn'><button type='button' class='remove-field fa fa-times'></button></div></div>"

				//Append container
				$(wrapper).appendTo('.multi-fields').insertBefore('.multi-field-option');

				//Reset value of .multi-field-option
				$('.multi-field-option .currency-field input.input-field').val(''); 
				$(".multi-field-option #location option[value='"+location+"']").remove();
				location_select.val('').trigger("liszt:updated");

				getShippingCostList();
			}

		});

		$('body').on('keyup', '.shipping-costs', function(){
			getShippingCostList();
		});

		function getShippingCostList(){
			var shippingCosts = [];
			$('.shipping-costs').each(function(){
				var cost = $(this).val();
				shippingCosts.push({
					"location" : $(this).parents('.shipping-to').find('.control-label').text(),
					"cost" : (cost != '' ? cost : 0)
				});
			});
			$('#shipping-cost').val(JSON.stringify(shippingCosts));
		}

		$('body').on('click', 'button.remove-field', function(){
			var multi_field_to_remove = $(this).closest('.multi-field-add');
			var retrieveOption = $(multi_field_to_remove).find('label.control-label').text();

			var appendToSelect = $('<option value="'+ retrieveOption +'">' + retrieveOption + '</option>');
				$( '.multi-field-option #location').append(appendToSelect);
				$( '.multi-field-option #location').val('').trigger("liszt:updated");
			$(this).closest('.multi-field-add').remove();
			getShippingCostList();
		});

	//Primary Photo
	$('.photo-container .img-preview-cont.dz-file-preview:first-child').addClass('primary-img-con');
	$('.photo-container .img-preview-cont.dz-file-preview:first-child span').addClass('primary-photo');

	$(".photo-container .img-preview-cont.dz-preview.dz-file-preview.img-con.col-md-2.col-sm-6.col-xs-12.removable").mouseover(function() { 
			$(this).find('.add-primary-photo').css("visibility","visible");
	});
	$(".photo-container .img-preview-cont.dz-file-preview").mouseout(function() { 
		$(this).find('.add-primary-photo').css("visibility","hidden");
	});

	/* Text Editor */
	$('.mytextarea').trumbowyg({
		fullscreenable: false,
		closable: true,
		removeformatPasted: true
	});

	/*
  |--------------------------------------------------------------------------
  | Create Gear
  |--------------------------------------------------------------------------
  */
 		$('input[name="agree"]').on('click', function(){
 			console.log('checked:', $(this).is(':checked'));
 			if( $(this).is(':checked') )
 				$('#btn-submit-gear').prop('disabled', false);
 			else
 				$('#btn-submit-gear').prop('disabled', true);
 		});

		if( $('body').find('#upload-gear-photos').length > 0 ) {
			Dropzone.autoDiscover = false;
			var myDropzone = new Dropzone("div#upload-gear-photos", {
				url         : baseUrl + '/gears/upload-photos',
				maxFilesize : 1,
				headers : {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				paramName          : "gearphoto",
				parallelUploads    : 1,
				maxFilesize        : 2,
				autoProcessQueue   : false,	// change to false to have it manual upload
				acceptedFiles      : 'image/*',
				previewsContainer  : "#imagePreviews",
				dictDefaultMessage : "Drop images here to upload",
				thumbnail: function(file, dataUrl) {
		      $(file.previewElement).find('.dz-image').find('img').remove();
		      $(file.previewElement).find('.dz-image').attr('style', "background:url("+ dataUrl +") no-repeat top center / cover;");
		      $(file.previewElement).find('.dz-details').remove();
		    },
		    init: function() {
		      var myDropzone = this;

		      myDropzone.on("addedfile", function(file) { 
		        $(file.previewElement).addClass('img-con col-md-2 col-sm-6 col-xs-12 removable');
		        $(file.previewElement)
		      			.attr('data-toggle', 'tooltip')
		      			.attr('data-placement', 'top')
		      			.attr('title', 'Click to remove image');
		      		$('[data-toggle="tooltip"]').tooltip();
		        file.previewElement.addEventListener("click", function() {
		          myDropzone.removeFile(file);
		          $('.tooltip').hide();
		        });
		      });

		      myDropzone.on('sending', function(file, xhr, formData){
		        // formData.append('_cp_upload_pictures_nonce' , $('input[name="_cp_upload_pictures_nonce"]').val());
		        // formData.append('_wp_http_referer'          , $('input[name="_wp_http_referer"]').val());
		        // formData.append('timestamp'                 , $('input[name="timestamp"]').val());
		        // formData.append('token'                     , $('input[name="token"]').val());
		      });

		      myDropzone.on('success', function(file, response){
		      	var marks = {
		      		"success" : $(file.previewElement).find('.dz-success-mark'),
		      		"error" : $(file.previewElement).find('.dz-error-mark')
		      	};
		      	marks.success.hide();
		      	marks.error.hide();

		      	if( response.status == "error" ){
		      		$(file.previewElement).addClass('upload-error');
		      		$(file.previewElement)
		      			.attr('data-toggle', 'tooltip')
		      			.attr('data-placement', 'top')
		      			.attr('title', response.message);
		      		$('[data-toggle="tooltip"]').tooltip();
		      	}
		      });

		      this.on("success", function() {
		       	myDropzone.options.autoProcessQueue = true; 
		      });
		    }
			});		
		}

		$('#btn-submit-gear').on('click', function(e){
			var slf = $(this);
			var btn = $(slf).button('loading');
			var $form = slf.parents('form');


			// myDropzone.processQueue();	// execute file upload

			// var btn = $(self).button('loading');
			e.preventDefault();
			e.stopImmediatePropagation();		

			$form.ajaxSubmit({
			url : baseUrl + '/gears/create',
		    type: 'post',
		    data: {
		    	"accept_offers": ($('input[name="accept_offers"]').is(':checked') ? 'yes' : 'no'),
		    	"photos": myDropzone.files.length
		    },
		    success: function(data){
		    	if( data.status == "success" ) {
			    	myDropzone.on('sending', function(file, xhr, formData){
			    		formData.append('gear_id', data.data.gear_id);
			    	});

			    	myDropzone.processQueue();	// execute file upload

			    	myDropzone.on("complete", function (file) {
				      if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
				      	showToastr('success', data.message + ". Redirecting...");
				      	window.setTimeout(function(){
				        	window.location.href = data.data.redirect_url;			      		
				      	}, 1000);
				      }
				    });
		    	}
		    	else {
		    		showToastr('error', data.message);
		    		btn.button('reset');

		    		if( $('body').find('input[name="'+ data.data.target +'"]').length > 0 ) {
			    		var errorTarget = $('input[name="'+ data.data.target +'"]');
	                    errorTarget.focus();
	                    errorTarget.parents('.form-group').addClass('has-error');		    			
		    		}
		    	}
		    }
			});
		});

	/*
  |--------------------------------------------------------------------------
  | Update Gear
  |--------------------------------------------------------------------------
  */
 		if( $('body').find('#update-photos').length > 0 ) {
			Dropzone.autoDiscover = false;
			var myDropzone = new Dropzone("div#update-photos", {
				url         : baseUrl + '/gears/update-gear-photos',
				maxFilesize : 1,
				headers : {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				paramName          : "gearphoto",
				parallelUploads    : 1,
				maxFilesize        : 2,
				autoProcessQueue   : true,	// change to false to have it manual upload
				acceptedFiles      : 'image/*',
				previewsContainer  : "#imagePreviews",
				dictDefaultMessage : "Drop images here to upload",
				thumbnail: function(file, dataUrl) {
		      $(file.previewElement).find('.dz-image').find('img').remove();
		      $(file.previewElement).find('.dz-image').attr('style', "background:url("+ dataUrl +") no-repeat top center / cover;");
		      $(file.previewElement).find('.dz-details').remove();
		    },
		    init: function() {
		      var myDropzone = this;

		      myDropzone.on('sending', function(file, xhr, formData){
		        formData.append('gear_id' , $('input[name="gear_id"]').val());
		      });

			    //When there's additional img added
	       	myDropzone.on("addedfile", function(file) { 
		        $(file.previewElement).addClass('img-con col-md-2 col-sm-6 col-xs-12 removable');
		        $(file.previewElement)
		      			.attr('data-toggle', 'tooltip')
		      			.attr('data-placement', 'top')
		      			.attr('title', 'Click to remove image');
		      		$('[data-toggle="tooltip"]').tooltip();
		        file.previewElement.addEventListener("click", function() {
		          myDropzone.removeFile(file);
		          $('.tooltip').hide();
		        });
		     });

		      myDropzone.on('success', function(file, response){
		      	myDropzone.removeFile(file);
		      	$('.photo-container').append(response.photo);
		      });

		      myDropzone.on("complete", function (file) {
			      if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
			      	showToastr('success', "Gear photos has been successfully added");
			      }
			    });

		      this.on("success", function() {
		       	myDropzone.options.autoProcessQueue = true; 
		      });
		    }
			});		
		}

		/*
	  |--------------------------------------------------------------------------
	  | BOF Settings
	  |--------------------------------------------------------------------------
	  */
	 		$('.change-search-view').on('click', function(){
	 			var self = $(this);
	 			var btn = $('.change-search-view').button('loading');

	 			$.ajax({
	 				url: baseUrl + '/settings/change-search-view',
	 				type: 'post',
	 				data: {
	 					"view" : self.data('view')
	 				}
	 			}).done(function(result){
	 				btn.button('reset');
	 				
	 				if( result.status == 'success' )
	 					window.location.reload();
	 			});
	 		});
	 
	 	/*
	  |--------------------------------------------------------------------------
	  | EOF Settings
	  |--------------------------------------------------------------------------
	  */

	  	$('#category').on('change', function(){
	  		var self = $(this);
	  		var subCat = $('#sub-category');

	  		subCat.html('');
	  		subCat.trigger('liszt:updated');

	  		$.ajax({
	  			"url" : baseUrl + '/settings/get-subcategory',
	  			"type" : 'post',
	  			"data" : {
	  				"category" : self.val()
	  			}
	  		}).done(function(response){
	  			console.log('response ->>', response);
	  			if( response.status == 'success' ) {
	  				subCat.html( response.data.html );
	  				subCat.trigger('liszt:updated');
	  			}
	  		});
		});

		/*
	  |--------------------------------------------------------------------------
	  | BOF Filters
	  |--------------------------------------------------------------------------
	  */
	 
		 	$('body').on('click', '.refined-filter-type', function(){
		 		$('.filter-type').val($(this).data('value'));
		 		getRefinedFilters();
		 	});

		 	$('body').on('click', '.refined-filter-status', function(){
		 		$('.filter-status').val($(this).data('value'));
		 		getRefinedFilters();
		 	});

		 	$('body').on('change', '.refined-filter-sort', function(){
		 		$('.sort-results').val($(this).val());
		 		getRefinedFilters();
		 	});

		 	$('.refined-filter-locations, .refined-filter-brands, .refined-filter-type').on('change', function(){
		 		getRefinedFilters();
		 	});

		 	//Sidebar Slider
				if( $('body').find('#price_slider').length > 0 ){
					$("#price_slider").slider({
						tooltip: 'always'
					});

					$("#price_slider").on("slideStop", function(slideEvt) {
						$("#price_sliderVal").text(slideEvt.value);

						getRefinedFilters();
					});

					var min = $("#price_slider").attr('data-slider-min');
					$("<span id='price_sliderminVal'>"+ min +"-</span>").insertBefore('#price_sliderVal');

					$("<i class='fa fa-usd dollar-icon' aria-hidden='true'></i>").insertBefore('.tooltip-inner');

				}

			function getRefinedFilters(){
				$('#refined-filter-form').submit();
			}

		/*
	  |--------------------------------------------------------------------------
	  | EOF Filters
	  |--------------------------------------------------------------------------
	  */

		$('#btn-update-gear').on('click', function(e){
			var slf = $(this);
			var btn = $(slf).button('loading');
			var $form = slf.parents('form');

			e.preventDefault();
			e.stopImmediatePropagation();		

			$form.ajaxSubmit({
				url : baseUrl + '/mygears/update-gear',
		    type: 'post',
		    success: function(data){
		    	
		    	btn.button('reset');

		    	if( data.status == 'error' ) {		    		
			    	if( $('body').find('input[name="'+ data.data.target +'"]').length > 0 ) {
			    		var errorTarget = $('input[name="'+ data.data.target +'"]');
	                    errorTarget.focus();
	                    errorTarget.parents('.form-group').addClass('has-error');		    			
		    		}
		    	} else{
		    		showToastr(data.status, data.message);
		    		window.setTimeout(function(){
						window.location.href = data.data.redirect;
					}, 500);
					console.log(data.redirect);
		    	}
		    }
			});
		});

	/*
  |--------------------------------------------------------------------------
  | Pop-up Login
  |--------------------------------------------------------------------------
  */
		$('.login-modal').on('click', function(){
			var self = $(this);
			var btn = self.button('loading');

			$.ajax({
				'type' : 'post',
				'url' : baseUrl + '/get-login',
				'data' : {
					'ajax' : 1,
					'redirect_url' : self.data('redirect-url')
				},
				'success' : function(data){
					btn.button('reset');
					bootbox.dialog({
					  message: data.html,
					  size: 'small'
					});				
				}
			});
		});

		$('body').on('submit', '.login', function(e){
			var self = $(this);
			var btn = self.find('.btn-login').button('loading');

			e.preventDefault();
			e.stopImmediatePropagation();

			self.ajaxSubmit({
				"url": baseUrl + '/post-login',
				"type": 'post',
				"success": function(data){

					showToastr(data.status, data.message);
					// toastr[data.status](data.message);

					if( data.status == "success" ){
						toastr.clear()
						btn.remove();
						
						window.setTimeout(function(){
							window.location.href = data.data.redirect;
						}, 1000);	
					}
					else {
						btn.button('reset');
					}
				}
			});

			// toastr.options.timeOut = 0;
			// toastr.options.extendedTimeOut = 0;
			// toastr.options.tapToDismiss = false;

		});

		toastr.options = { 
			// tapToDismiss: false,
			onclick: function () {
				var resend = $('body').find('a.btn-resend-verification').length;
				if(resend > 0){
					$('form.login').ajaxSubmit({
						url 	: baseUrl + '/resend-verification',
		                type 	: 'post',
		            	success : function(data){
		            		showToastr(data.status, data.message);
		            	}
					});
					// alert('hi');
				} else {
					// alert('oopss');
				}
			} 
		};

	/*
  |--------------------------------------------------------------------------
  | Pop-up Make an Offer
  |--------------------------------------------------------------------------
  */
 
 	/*
  |--------------------------------------------------------------------------
  | Subscribe Newsletter
  |--------------------------------------------------------------------------
  */
 	$('.subscribe-btn').on('click', function(e){
 		e.preventDefault();
 		var slf = $(this);
 		var email = $('.subscribe-email').val();

 		if( email == "" ) {
 			showToastr('error', "Please enter your Email Address");
 		}
 		else {
 			var btn = slf.button('loading');
 			$.ajax({
				"type" : 'post',
				"url" : baseUrl + '/subscribe',
				"data" : {
					"email" : email
				},
				"success" : function(data){
					showToastr(data.status, data.message);
					btn.button('reset');

					if( data.status == "success" ) {
						$('.subscribe-email').val('');
					}
				}
			});
 		}

 	});

 	//Bottom Section
 	setTimeout(function(){
		$('.rowEQ > [class*="col-"]').eqHeights({parentSelector:'.rowEQ'});
	},1000);


 	if($(window).width() < 767 ) {
		$("footer#main-footer .row").removeClass( "rowEQ" )
	}
	else {
		$("footer#main-footer .row").addClass( "rowEQ" )
	}

	/*
	|--------------------------------------------------------------------------
	| Cart
	|--------------------------------------------------------------------------
	*/
	$('.add-to-cart').on('click', function(){
		var self = $(this);
		var gearID = self.data('product-id');

		var btn = self.button('loading');
		$.ajax({
			"url" : baseUrl + '/cart/add',
			"type" : 'post',
			"data" : {
				"gear" : gearID
			}
		}).done(function(response){
			if( self.hasClass('buy-now') ){
				window.location.href = baseUrl + '/cart';
			}
			else {
				btn.button('reset');
				showToastr(response.status, response.message);

				$('.cart-count').text( response.data.total > 0 ? ' ('+ response.data.total +')' : '' );				
			}
		});
	});

	// buy now. Only for one-to-one purchase only
	$('.buy-now').on('click', function(){
		var self = $(this);
		var gearID = self.data('product-id');

		var btn = self.button('loading');
		$.ajax({
			"url" : baseUrl + '/cart/add',
			"type" : 'post',
			"data" : {
				"gear" : gearID
			}
		}).done(function(response){
			window.location.href = baseUrl + '/cart';
		});
	});

	$('body').on('click', '.remove-search-tag', function(){
        var self = $(this);
        var trgt = self.data('input-tag');
        console.log('trgt ->>', trgt);

        if( trgt == 'location' || trgt == 'brand' ) {
            $('body').find('select[name="'+ trgt +'"] option:selected').val('');
        }
        else {
            $('body').find('input[name="'+ trgt +'"]').val('');
        }

        window.setTimeout(function(){
            $('body').find('#refined-filter-form').submit();
        });
    });

  /*
  |--------------------------------------------------------------------------
  | Top Search bar section
  |--------------------------------------------------------------------------
  */

  $(document).ready(function() {
  		// $("#main-header").before($("#main-header").clone().addClass("fixed"));
	    //  $(window).on("scroll", function () {
	    //      $("#main-header").toggleClass("down", ($(window).scrollTop() > 100));
	    //  });
  		$(window).on("scroll", function () {

		 // if ($(window).scrollTop() > 80) {
		 //        // $('#main-header').addClass('fixed');
		 //        $('.header').animate({ padding: '0' }, 20 );
		 //        $('.header-info-container').animate({ paddingBottom: '0' }, 20 );
		 //    } else {
		 //        // $('#main-header').removeClass('fixed');
		 //        // $('#main-header').animate({ top: '-208px' }, 0 )
		 //        $('.header').animate({ padding: '15px 0 15px 0' }, 20 );
		 //        $('.header-info-container').animate({ paddingBottom: '16px' }, 20 );
		 //    }
		 // $(".header").toggleClass("down", ($(window).scrollTop() > 80));

  		});
  });
		
});


/*
* END OF FILE
* global.js
*/
