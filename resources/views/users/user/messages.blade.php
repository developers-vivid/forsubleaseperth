<?php $userDetails = \App\User::whereUserId( (Auth::user()->user_id == $inbox->receiver_id ? $inbox->sender_id : $inbox->receiver_id ) )->first(); ?>
<div class="receiver-user">
	<div class="receiver-user-pic-container">
		<img class="receiver-user-pic" src="{{ url('images/thumb-pic.png') }}" alt="avatar" title="avatar" />
	</div>
	
	<h3 class="receiver-user-name">{{ $userDetails->name }}<span><a href="mailto:{{ $userDetails->email }}">{{-- $userDetails->email --}}</a></span></h3>
</div>

<hr>
<div class="messages-container inbx-{{ $inbox->inbox_id }}">
    @if( count($messages) > 0 )
    	<!-- <div class="date">June 1</div> -->
        @foreach($messages as $msg)
            @include('layouts.single-message')
        @endforeach
    @else
        <div class="alert alert-danger text-center">
            No record found
        </div>
    @endif
</div>