@extends('layouts.master')

@section('title', urldecode($category))

@section('content')

    <?php $categories = \App\Helper::getCategories(); ?>
    <div id="main-container" class="single-page gears">
        <div class="container">
            <div class="row main-row">
                <div class="col-lg-3 col-sm-3 col-xs-12 sidebar-container">
                     @include('layouts.includes.gp_categories')
                    <div class="product-search-container">
                        <h3>Refined results</h3>
                        <div class="location-container">
                            <h4>Location</h4>
                            <div class="select-chosen">
                                <select class="form-control chosen">
                                    <option value="">All Locations</option>
                                    <option value="Test1">Test1</option>
                                    <option value="Test2">Test2</option>
                                    <option value="Test3">Test3</option>
                                </select>
                            </div>
                        </div>
                        <div class="brand-container">
                            <h4>Brand</h4>
                            <div class="select-chosen">
                                <select name="category" id="category" class="form-control chosen">
                                    <option value="">All Brands</option>
                                        @if( count($categories) > 0 )
                                          @foreach($categories as $category)
                                            <option value="{{ $category->category_id }}">{{ $category->name }}</option>
                                          @endforeach
                                        @endif
                                </select>
                            </div>
                        </div>
                        <div class="type-container">
                            <h4>Type</h4>
                            <div class="select-chosen">
                                <select class="form-control chosen">
                                    <option value="">All Types</option>
                                    <option value="Test1">Test1</option>
                                    <option value="Test2">Test2</option>
                                    <option value="Test3">Test3</option>
                                </select>
                            </div>
                        </div>
                        <div class="price-range-container">
                            <h3>Price Range</h3>
                            <input id="price_slider" data-slider-id='ex1Slider' type="text" data-slider-min="50" data-slider-max="50000" data-slider-step="1" data-slider-value="25000" data-slider-handle="triangle"/>
                            <label>$50</label>
                            <label>$50,000</label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-sm-9 col-xs-12 items-container">
                    <div class="row options-container">
                        <div class="col-lg-8 col-sm-7 col-xs-6 new-pre-owned-container">
                            <ul>
                                <li class="selected"> <button class="filter" data-filter="all">All</button></li>
                                <li><button class="filter" data-filter=".new">New</button></li>
                                <li><button class="filter active" data-filter=".pre-owned">Pre-Owned</button></li>
                            </ul>
                        </div>
                        <div class="col-lg-4 col-sm-5 col-xs-6 most-recent-view-container">
                            <div class="col-lg-8 col-sm-8 col-xs-8 most-recent">
                                <div class="custom-select">
                                    {!! Form::select('state', [
                                       'wa' => 'WA',
                                       'test1' => 'test1',
                                       'test2' => 'test2'], old('state'), ['class' => 'form-control'] ) !!}
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-4 col-xs-4 view-type-container">
                                <button id="ListLayout" class="selected"><i class="fa fa-th-list"></i></button>
                                <button id="GridLayout"><i class="fa fa-th"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="row product-gear">
                        <div class="col-lg-12">
                            <small class="text-muted">{{ urldecode(\Request::segment(3)) }} / {{ urldecode($filter->sub_category) }} / {{ $filter->location }} / {{ $filter->brand }} / {{ $filter->type }} / {{ $filter->pricerange }}</small>
                            <h3 class="title-area">
                                {{ \Request::route()->getName() == "gears::categories" ? urldecode(\Request::segment(3)) : 'All' }}
                                {{ (\Request::route()->getName() == "gears::categories" && \Request::segment(4)) ? ' &raquo; ' . urldecode(\Request::segment(4)) : '' }}
                            </h3>
                        </div>
                        @if( count($gears) > 0 )
                            @foreach( $gears as $gear )
                                <div class="mix new col-lg-4 col-sm-4 col-xs-12 product-item" data-my-order="1">
                                    <div class="row listview">
                                        <div class="col-lg-4 col-sm-4 col-xs-12 img-container">
                                            <a href="{{ route('gears::marketplace', [$gear->product_blurb]) }}" title="">
                                                <img src="{{ url('images', ['gears', \App\Helper::getGearPrimaryPhoto($gear->product_id)]) }}" class="img-responsive" alt="" title="" />
                                            </a>
                                        </div>
                                        <div class="col-lg-8 col-sm-8 col-xs-12 content-container">
                                            <h3 class="col-lg-8 col-sm-8 col-xs-8 item-title clearfix">
                                                <a href="{{ route('gears::marketplace', [$gear->product_blurb]) }}" title="">{{ $gear->product_title }}</a>
                                                <span>Listed {{ \Carbon\Carbon::createFromTimeStamp(strtotime($gear->created_at))->diffForHumans() }} by <span class="contact-name">{{ \App\User::whereUserId($gear->created_by)->value('name') }}</span></span>
                                            </h3>
                                            <span class="col-lg-4 col-sm-4 col-xs-4 price clearfix">${{ $gear->product_price }} <span>AUD</span></span>
                                            <p class="col-lg-12 col-ms-12 col-xs-12 item-desc clearfix">
                                                {!! str_limit(htmlspecialchars_decode($gear->product_description), 200) !!}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="gridview hidden">
                                        <a href="{{ route('gears::marketplace', [$gear->product_blurb]) }}" class="gear-img" title=""><img src="{{ url('images', ['gears', \App\Helper::getGearPrimaryPhoto($gear->product_id)]) }}" class="img-responsive" alt="" title="" /></a>
                                        <h3><a href="{{ route('gears::marketplace', [$gear->product_blurb]) }}" title="">{{ $gear->product_title }}</a></h3>
                                        <div class="price">${{ $gear->product_price }} <span>AUD</span></div>
                                    </div>
                                    
                                </div>
                            @endforeach
                            {!! $gears->render() !!}
                        @else
                        <div class="col-lg-12">
                            <div class="alert alert-danger text-center">
                                No items found
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div><!-- .container -->
    </div><!-- #main-container -->
@endsection
