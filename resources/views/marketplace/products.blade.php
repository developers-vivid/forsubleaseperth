@extends('layouts.master')

@section('title', \App\Helper::getCategoryBySlug( $category ) . (\Request::segment(3) ? ' &raquo; '. \App\Helper::getCategoryBySlug( \Request::segment(3) ) : ''))

@section('content')

    <?php $categories = \App\Helper::getCategories(); ?>
    <div id="main-container" class="single-page gears">
        <div class="container">
            <div class="row main-row">
                <div class="col-lg-3 col-sm-3 col-xs-12 sidebar-container">
                    @include('layouts.includes.gp_categories')
                    @include('layouts.filters.refined-filter')
                </div>
                <div class="col-lg-9 col-sm-9 col-xs-12 items-container">
                    @include('layouts.filters.gear-filter')
                    <div class="row product-gear{{ (\Session::has('searchview') && \Session::get('searchview') == 'list') ? ' list' : ' grid' }}">
                        <div class="found-result clearfix">
                            <div class="col-lg-10 col-md-10 col-xs-12 result-detail">
                                <p>({{ $gears->total() }}) Results found 
                                    @if( \Input::has('keyword') && \Input::get('keyword') != '' )
                                        for “{{ \Input::get('keyword') }}”
                                    @endif
                                </p>
                                <br/>
                                <ul class="search-listing">
                                    @if( \Input::has('location') && \Input::get('location') != '' )
                                        <li>{{ \Input::get('location') == 'all' ? 'All Locations' : \Input::get('location') }} <i class="fa fa-times fa-fw remove-search-tag" data-input-tag="location" title="Remove Location" aria-hidden="true"></i></li>
                                    @endif
                                    @if( \Input::has('brand') && \Input::get('brand') != '' )
                                        <li>{{ \Input::get('brand') == 'all' ? 'All Brands' : \Input::get('brand') }} <i class="fa fa-times fa-fw remove-search-tag" data-input-tag="brand" title="Remove Brand" aria-hidden="true"></i></li>
                                    @endif
                                    @if( \Input::has('price_min') && \Input::get('price_min') != '' )
                                        <li><small class="text-muted">AUD</small> {{ \Input::get('price_min') }} <i class="fa fa-times fa-fw remove-search-tag" data-input-tag="price_min" title="Remove Minimum Price" aria-hidden="true"></i></li>
                                    @endif
                                    @if( \Input::has('price_max') && \Input::get('price_max') != '' )
                                        <li><small class="text-muted">AUD</small> {{ \Input::get('price_max') }} <i class="fa fa-times fa-fw remove-search-tag" data-input-tag="price_max" title="Remove Maximum Price" aria-hidden="true"></i></li>
                                    @endif
                                    @if( \Input::has('type') && \Input::get('type') != '' )
                                        <li>{{ \Input::get('type') == 'all' ? 'All Types' : \Input::get('type') }} <i class="fa fa-times fa-fw remove-search-tag" data-input-tag="type" title="Remove Type" aria-hidden="true"></i></li>
                                    @endif
                                    @if( \Input::has('sort') && \Input::get('sort') != '' )
                                        <?php
                                            $sortName = "";
                                            if( \Input::get('sort') == "product_price|asc" ) 
                                                $sortName = "Price Low to High";
                                            else if( \Input::get('sort') == "product_price|desc" )
                                                $sortName = "Price Hight to Low";
                                            else
                                                $sortName = "Most Recent First";
                                        ?>
                                        <li>{{ $sortName }} <i class="fa fa-times fa-fw remove-search-tag" data-input-tag="sort" title="Remove Sort" aria-hidden="true"></i></li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        @if( count($gears) > 0 )
                        <div class="{{ (\Session::has('searchview') && \Session::get('searchview') == 'list') ? ' list' : ' gridviewlayout' }}" >
                                @foreach( $gears as $gear )
                                     @if( \Session::has('searchview') && strtolower(\Session::get('searchview')) == 'list' )
                                        <div class="col-xs-12 product-item" data-my-order="1">
                                            @include('layouts.gears.list')
                                        </div>
                                    @else
                                        <div class="col-lg-4 col-sm-6 col-xs-12 product-item" data-my-order="1">
                                            @include('layouts.gears.grid')
                                        </div>
                                    @endif
                                @endforeach
                        </div>
                        <div class="col-lg-12 pagination-container">
                            {!! $gears->appends([
                                'location'  => Input::get('location'),
                                'brand'     => Input::get('brand'),
                                'price_min' => Input::get('price_min'),
                                'price_max' => Input::get('price_max'),
                                'type'      => Input::get('type'),
                                'sort'      => Input::get('sort')
                            ])->links() !!}
                        </div>
                        @else
                            <div class="col-lg-12">
                                <div class="alert message-alert text-center">
                                    <div class="icon-empty">
                                        <i class="fa fa-file-image-o" aria-hidden="true"></i>
                                    </div>
                                   <p>No items found</p>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div><!-- .container -->
    </div><!-- #main-container -->

@endsection
