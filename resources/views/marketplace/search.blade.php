@extends('layouts.master')

@section('title', (Input::get('keyword') ? Input::get('keyword') : 'Search'))

@section('content')

	<div id="main-container" class="single-page gears">
        <div class="container">
        	<div class="row main-row">
                <div class="col-lg-3 col-sm-3 col-xs-12 sidebar-container">
                    @include('layouts.includes.gp_categories')
                    @include('layouts.filters.refined-filter', ['action' => url('search')])
                </div>
                <div class="col-lg-9 col-sm-9 col-xs-12 items-container">
                	@include('layouts.filters.gear-filter')
                    <div class="product-gear">
                        <div class="found-result clearfix">
                            <div class="col-lg-10 col-md-10 col-xs-12 result-detail">
                                <p>({{ $gears->total() }}) Results found 
                                    @if( \Input::has('keyword') && \Input::get('keyword') != '' )
                                        for “{{ \Input::get('keyword') }}”
                                    @endif
                                </p>
                                <br/>
                                <ul class="search-listing">
                                    @if( \Input::has('location') && \Input::get('location') != '' )
                                        <li>{{ \Input::get('location') == 'all' ? 'All Locations' : \Input::get('location') }} <i class="fa fa-times fa-fw remove-search-tag" data-input-tag="location" title="Remove Location" aria-hidden="true"></i></li>
                                    @endif
                                    @if( \Input::has('brand') && \Input::get('brand') != '' )
                                        <li>{{ \Input::get('brand') == 'all' ? 'All Brands' : \Input::get('brand') }} <i class="fa fa-times fa-fw remove-search-tag" data-input-tag="brand" title="Remove Brand" aria-hidden="true"></i></li>
                                    @endif
                                    @if( \Input::has('price_min') && \Input::get('price_min') != '' )
                                        <li><small class="text-muted">AUD</small> {{ \Input::get('price_min') }} <i class="fa fa-times fa-fw remove-search-tag" data-input-tag="price_min" title="Remove Minimum Price" aria-hidden="true"></i></li>
                                    @endif
                                    @if( \Input::has('price_max') && \Input::get('price_max') != '' )
                                        <li><small class="text-muted">AUD</small> {{ \Input::get('price_max') }} <i class="fa fa-times fa-fw remove-search-tag" data-input-tag="price_max" title="Remove Maximum Price" aria-hidden="true"></i></li>
                                    @endif
                                    @if( \Input::has('type') && \Input::get('type') != '' )
                                        <li>{{ \Input::get('type') == 'all' ? 'All Types' : \Input::get('type') }} <i class="fa fa-times fa-fw remove-search-tag" data-input-tag="type" title="Remove Type" aria-hidden="true"></i></li>
                                    @endif
                                    @if( \Input::has('sort') && \Input::get('sort') != '' )
                                        <?php
                                            $sortName = "";
                                            if( \Input::get('sort') == "product_price|asc" ) 
                                                $sortName = "Price Low to High";
                                            else if( \Input::get('sort') == "product_price|desc" )
                                                $sortName = "Price Hight to Low";
                                            else
                                                $sortName = "Most Recent First";
                                        ?>
                                        <li>{{ $sortName }} <i class="fa fa-times fa-fw remove-search-tag" data-input-tag="sort" title="Remove Sort" aria-hidden="true"></i></li>
                                    @endif
                                </ul>
                            </div>
                            <div class="col-lg-2 col-md-2 col-xs-12 result-btn">
                                <button type="button" class="btn btn-save-results" data-loading-text="Please Wait ..." data-search-keyword="{{ \Input::get('keyword') }}" data-search-url="{{ "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" }}">
                                    <i class="fa fa-save fa-fw"></i> Save Results
                                </button>
                            </div>
                        </div>
                        @if( count($gears) > 0 )
                            <div class="row{{ (\Session::has('searchview') && \Session::get('searchview') == 'list') ? ' list' : ' grid gridviewlayout' }}">
                                 @foreach( $gears as $gear )
                                    @if( \Session::has('searchview') && strtolower(\Session::get('searchview')) == 'list' )
                                        <div class="col-xs-12 product-item">
                                            @include('layouts.gears.list')
                                        </div>
                                    @else
                                        <div class="col-lg-4 col-sm-6 col-xs-12 product-item">
                                            @include('layouts.gears.grid')
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                            <div class="col-lg-12 pagination-container">
                                {!! $gears->appends([
                                    'keyword'   => Input::get('keyword'),
                                    'brand'     => Input::get('brand'),
                                    'price_min' => Input::get('price_min'),
                                    'price_max' => Input::get('price_max'),
                                    'type'      => Input::get('type'),
                                    'sort'      => Input::get('sort')
                                ])->links() !!}        
                            </div>
                        @else
                            <div class="col-lg-12">
                                <div class="alert message-alert text-center">
                                    <div class="icon-empty">
                                        <i class="fa fa-file-image-o" aria-hidden="true"></i>
                                    </div>
                                    <p>Oops, no results! Try a saved search, we’ll let you know when something gets posted.</p>
                                </div>
                            </div>
                        @endif
                    </div>
                                   
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.btn-save-results').on('click', function (event) {
                var self = $(this);
                var btn = self.button('loading');

                $.ajax({
                    'type': 'post',
                    'url': '{{ route("search::save") }}',
                    'data': {
                        "url": self.data('search-url'),
                        "keyword": self.data('search-keyword')
                    }
                }).done(function(data){
                    btn.button('reset');
                    showToastr(data.status, data.message);

                    if( data.status == 'success' ) {
                        bootbox.dialog({
                          message: data.data.html,
                          title: "Search Results",
                          backdrop: true,
                          onEscape: true,
                          className: 'custom-saveresults'
                        });
                    }
                });
            });

            $('body').on('click', '.btn-remove-search', function(){
                var self = $(this);
                var btn = self.button('loading');

                $.ajax({
                    "url": "{{ route('search::remove') }}",
                    "type": 'post',
                    "data": {
                        "id": self.data('search')
                    }
                }).done(function(response){
                    btn.button('reset');
                    showToastr(response.status, response.message);

                    if( response.status == 'success' ){
                        self.parents('.section').fadeOut(300, function() {
                            $(this).remove();

                            if( response.data.total == 0 )
                                $('body').find('.bootbox-body').html(response.data.html);
                        });
                    }
                });
            }); 
        });
    </script>
@endsection