<div class="modal-save-results">
	<div class="modal-body-custom">
		@if( count($searches) > 0 )
			@foreach($searches as $search)
				<div class="section clearfix">
					<div class="link-label pull-left">
						You searched for <a href="{{ $search->url }}" target="_blank">{{ \App\Helper::appendAllUrlParams($search->url) }}</a><br/>
						<small class="text-muted">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($search->updated_at))->diffForHumans() }}</small>
					</div>
				</div>
			@endforeach
		@else
			<div class="no-results">
				<div class="search-icon-container">
					<i class="fa fa-search"></i>
				</div>
				<p>No results found.</p>
			</div>
		@endif
	</div>
</div>