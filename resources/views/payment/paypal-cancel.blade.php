@extends('layouts.master')

@section('title', 'PayPal Payment Cancelled')

@section('content')
	
	<div class="single-page">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12">
        	PayPal Payment Cancelled
        </div>
      </div>
    </div>
  </div>

@endsection