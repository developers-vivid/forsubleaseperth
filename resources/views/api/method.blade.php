@extends('layouts.api')

@section('title', $title)
@section('token', (isset($token) ? $token : ''))

@section( 'content' )
	<script src="{{ url('https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js') }}"></script>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4>
				{{ $title }}
				<a href="{{ route('api::apiReference') }}" class="pull-right">&laquo; Back</a>
			</h4>
		</div>
		<div class="panel-body">
			@if( isset($notes) )
				<h5><strong>Notes</strong></h5>
				<ul>
					@foreach($notes as $note)
						<li>{!! $note !!}</li>
					@endforeach
				</ul>
				<hr>
			@endif

		  	<h5><strong>Request</strong></h5>
			<pre class="request">loading...</pre>

		  	<hr/>
		  	<h5><strong>Response</strong></h5>
			<pre class="response">loading...</pre>
		</div>
	</div>

	<script>
		$(document).ready(function(){
			$.ajax({
				"url": "{{ route(Route::currentRouteName(), [$method]) }}",
				"type": 'post'
			}).done(function(response){
				$('.request').html(syntaxHighlight(JSON.stringify(response.data.request, undefined, 4)));
				$('.response').html(syntaxHighlight(JSON.stringify(response.data.response, undefined, 4)));
			});

			function syntaxHighlight(json) {
			    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
			    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
			        var cls = 'number';
			        if (/^"/.test(match)) {
			            if (/:$/.test(match)) {
			                cls = 'key';
			            } else {
			                cls = 'string';
			            }
			        } else if (/true|false/.test(match)) {
			            cls = 'boolean';
			        } else if (/null/.test(match)) {
			            cls = 'null';
			        }
			        return '<span class="' + cls + '">' + match + '</span>';
		    	});
			}
		});
	</script>

@endsection