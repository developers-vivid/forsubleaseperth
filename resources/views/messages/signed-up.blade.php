@extends('layouts.404.page_not_found_master')

@section('title', 'Successful Sign Up')

@section('content-404')
    
    <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-8 col-md-offset-2 col-xs-12 page-not-found-container">

            <!--<div class="logo-container">
                <a href="{{ route('home') }}">
                    <img src="{{ url('images/splash/splash-logo.png') }}" class="img-responsive" alt="" title="" />
                    <h1 class="slogan">Your Office Just A Click Away</h1>
                </a>
            </div>
            <div class="alert alert-danger text-center">
                <br/>
                <h3>
                    <p><strong style="font-weight: bold;">{{ \Session::get('alert')->message }}</strong></p>
                </h3>
                <a href="{{ route('home') }}" class="homepage-link" >Go To Homepage</a>
                <br/><br/>
            </div>-->

            <div class="logo-container" style="margin-top:100px;">
                <a href="{{ route('home') }}">
                    <center>
                        <img src="{{ url('images/splash/gp-logo.png') }}" class="img-responsive" alt="" title="" />
                        <!--<h1 class="slogan">Your Office Just A Click Away</h1></br>-->
                    </center>
                </a>
            </div>
            <div class="alert alert-danger text-center" style="margin-bottom:100px; padding:50px; background-color: #6ed5d4;">
                <h3>
                    <p><strong style="font-weight: bold; color:black;">{{ \Session::get('alert')->message }}</strong></p>
                </h3></br>
                <a href="{{ route('home') }}" class="homepage-link" style="padding-top:15px; padding-bottom:15px; padding-left:20px; padding-right:20px; color: #336666; font-weight: bold; background-color: #ffffff; border-radius: 10px;">Go To Homepage</a>
            </div>

          </div>
        </div>
    </div>

@endsection