@extends('layouts.master')

@section('title', $title)

@section('content')

    <div id="main-container" class="single-page gears">
        <div class="container">
            <div class="row main-row">
                <div class="col-lg-3 col-sm-3 col-xs-12 sidebar-container">
                    @include('layouts.includes.gp_categories')
                    @include('layouts.filters.refined-filter', ['action' => url('search')])
                </div>
                <div class="col-lg-9 col-sm-9 col-xs-12 items-container">
                    @include('layouts.filters.gear-filter')
                    <div class="alert alert-{{ $status ? 'success' : 'danger' }}">
                        <h3>{{ $status ? 'Success' : 'Error' }}!</h3><br/>
                        <p>{{ $message }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection