@extends('layouts.master')

@section('content')
<div class="single-page sign-up">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <div class="panel panel-default" >

                  <div class="container reg-container" style="background: #fff">
                    <div class="row panel-heading"  >
                            <div class="col-lg-6 col-md-10 col-xs-8 heading-title"><h2>Sign Up</h2></div>
                            <div class="col-lg-6 col-md-2 col-xs-4 logo-thumb"><img src="/images/sign_in_logo.png" class="img-responsive" alt="Forsublease logo" title="Forusblease Logo"></div>
                    </div>
                    <div class="clr"></div>
                    <div class="panel-body" style="background-color: #e2f7f6 !important;">
                        {!! Form::open(['url' => 'sign-up', 'class' => 'form-horizontal', 'role' => 'form', 'file' => true]) !!}
                            <div class="row">
                                <div class="col-lg-11 col-md-12 col-xs-12 section-title">Personal Details</div>
                            </div>
                            <div class="row section-form">
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-xs-12 colmn">
                                            <div class="form-group">
                                                <label class="col-md-12 control-label">First name*</label>
                                                <div class="col-md-12">
                                                    {!! Form::text('first_name', old('first_name'), ['class' => 'form-control', 'placeholder' => 'Your first name', 'autofocus' => true ]) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-xs-12 colmn">
                                            <div class="form-group">
                                                <label class="col-md-12 control-label">Last name*</label>
                                                <div class="col-md-12">
                                                    {!! Form::text('last_name', old('last_name'), ['class' => 'form-control', 'placeholder' => 'Your last name']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-xs-12 colmn">
                                    <!-- Email -->
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Email*</label>
                                        <div class="col-md-12">
                                            {!! Form::text('email', old('email'), ['class' => 'form-control', 'placeholder' => 'Your email address']) !!}
                                        </div>
                                    </div>

                                    <!-- Telephone -->
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Telephone</label>
                                        <div class="col-md-12">
                                            {!! Form::text('telephone', old('telephone'), ['class' => 'form-control']) !!}
                                        </div>
                                    </div>

                                </div>
                                <div class="col-lg-6 col-md-6 col-xs-6 colmn">

                                    <!-- Password -->
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Password*</label>
                                        <div class="col-md-12">
                                            {!! Form::password('password', ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                    
                                </div>

                                <div class="col-lg-6 col-md-6 col-xs-6 colmn">

                                <!-- Confirm password -->
                                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                        <label class="col-md-12 control-label">Confirm Password*</label>
                                        <div class="col-md-12">
                                            {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 hidden">
                                    <!-- Register seller -->
                                    <div class="form-group">
                                        <div class="col-md-6 col-lg-6">
                                            {!! Form::checkbox('registered_seller', null, false, ['class' => 'registered_seller', 'checked' => 'checked']) !!}<label class="control-label regseller-label">Register as Seller</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row company-info-heading{{ config('gp_conf.allseller') ? '' : ' hidden' }}">
                                <div class="col-lg-11 col-md-12 col-xs-12 section-title">Company Details</div>
                            </div>
                            <div class="row section-form company-info-details{{ config('gp_conf.allseller') ? '' : ' hidden' }}">
                                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                    <!-- Company Name -->
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Username / Store Name <small class="text-muted">(optional)</small></label>
                                        <div class="col-md-12">
                                            {!! Form::text('company_name', old('company_name'), ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-xs-12 colmn comp-address">
                                    

                                    <!-- Company State -->
                                    <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12 compaddr-input">
                                        <div class="form-group">
                                            <label class="control-label">State*</label>
                                            <div class="select-chosen">
                                                {!! Form::select('company_state', \App\Helper::getStates(), old('company_state'), ['class' => 'form-control chosen'] ) !!}
                                            </div>
                                        </div>
                                    </div>

                                     <!-- Company City -->
                                    <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12 compaddr-input">
                                        <div class="form-group">
                                            <label class="control-label">City*</label>
                                            <div>
                                                {!! Form::text('company_city', old('company_city'), ['class' => 'form-control']) !!}
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Company postcode -->
                                    <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12 compaddr-input no-mr no-pr">
                                        <div class="form-group">
                                            <label class="control-label">Postcode</label>
                                            <div>
                                                {!! Form::text('company_postcode', old('company_postcode'), ['class' => 'form-control']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                
                            </div>

                            <!-- Agreement -->
                            <div class="form-group">
                                <div class="col-lg-12 col-md-12 col-xs-12 agreement text-center">
                                    <input type="checkbox" name="agree" value="0">
                                    <label class="control-label">I agree to the <a href="{{ route('pages::termsAndCondition') }}" target="_blank">terms of use</a> and <a href="{{ route('pages::policy') }}" target="_blank">privacy policy</a></label>
                                </div>
                            </div>

                             <!-- Sign Up -->
                            <div class="form-group">
                                <div class="col-lg-12 col-md-12 col-xs-12 center-block">
                                    <button type="button" class="btn center-block btn-success btn-sign-up">Sign up</button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
    
                  </div><!-- /.reg-container -->

                </div>
            </div>
        </div>
    </div>
</div>
    <script>
        $(document).ready(function(){

            $('input[name="agree"]').on('change', function(){
                $(this).val( $(this).is(':checked') ? 1 : 0 );
            });

            $('.btn-sign-up').on('click', function(e){
                var self = $(this);
                var $form = self.parents('form');

                e.preventDefault();
                e.stopImmediatePropagation();
                $form.find('.form-group').removeClass('has-error');

                var btn = self.button('loading');
                $form.ajaxSubmit({
                    url: baseUrl + '/process-signup',
                    type: 'post',
                    success: function(response) {
                        toastr[response.status](response.message);

                        if( response.status == 'success' ) {
                            window.setTimeout(function(){
                                window.location.href = "{{ route('messages::messageSuccess') }}";
                            }, 1500);
                        }
                        else {
                            btn.button('reset');
                            var errorTarget = $('input[name="'+ response.data.target +'"]');
                            errorTarget.focus();
                            errorTarget.parents('.form-group').addClass('has-error');
                        }
                    }
                });
            });
        });
    </script>
@endsection
