@extends('layouts.master')

@section('content')
<div class="single-page login">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Login</div>
                    <div class="panel-body">
                        {!! Form::open(['url' => 'sign-in', 'class' => 'form-horizontal', 'role' => 'form']) !!}
                            <div class="form-group{{ $errors->has('email') || session()->has('error_message') ? ' has-error' : '' }}">
                                <label class="col-md-4 col-md-offset-1 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => 'E-Mail Address', 'autofocus' => true]) !!}

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @elseif(session()->has('error_message'))
                                        <span class="help-block">
                                            <strong>{{ session()->get('error_message') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="col-md-4 col-md-offset-1 control-label">Password</label>

                                <div class="col-md-6">
                                    {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) !!}

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-5">
                                    <div class="checkbox">
                                           <label class="control-label"> {!! Form::checkbox('remember', null) !!} Remember me</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-5">
                                    <div class="row">
                                        <button type="submit" class="col-lg-5 col-md-6 col-xs-12 btn btn-primary">Login</button>
                                        <a href="{{ url('register') }}" title="" class="col-lg-5 col-lg-offset-1 col-md-6 col-xs-12 btn btn-default">Sign Up</a>
                                    </div>
                                    
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
