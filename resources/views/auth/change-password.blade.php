@extends('layouts.master')

@section('content')
<div class="single-page forgot-password">
    <div class="container">
        <div class="row">
        	<div class="col-lg-12">

        		<div class="forgot-password-section">
                    {!! Form::open(['url' => route("user::updateAccountPassword", [$token]), 'id' => 'change-password', 'role' => 'form', 'method' => 'post']) !!}
            			<div class="header-section">
            				<h3>Forgot Password</h3>
            			</div>
            			<div class="form-group">
            				{!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'New Password']) !!}
            			</div>
            			<div class="form-group">
            				{!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Confirm New Password']) !!}
            			</div>

            			<div class="form-group">
            				<button type="submit" class="btn button-forgot-password" data-loading-text="Processing..">Submit</button>
            			</div>
                    {!! Form::close() !!}
        		</div>

        	</div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#change-password').on('submit', function(e){
                e.preventDefault();
                e.stopImmediatePropagation();

                var self = $(this),
                    btn = self.find('.btn').button('loading');

                self.ajaxSubmit({
                    success: function(response){
                        showToastr( response.status, response.message );

                        if( response.status == 'success' ) {
                            window.setTimeout(function(){
                                window.location.href = response.data.redirect;
                            }, 1500);
                        }
                        else {
                            btn.button('reset');
                        }
                    }
                });
            });
        });
    </script>

    @include('layouts.includes.gp_bottom')
</div>
@endsection