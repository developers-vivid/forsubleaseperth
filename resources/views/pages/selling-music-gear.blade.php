@extends('layouts.master')

@section('title', 'Selling Music Gear')

@section('content')
	
<div id="main-container" class="single-page selling-music-gear">
      <div class="container">
          <div class="row">
          
              <div class="selling-music-gear-content">
                <div class="image-container">
                  <img src="{{ url('/images/image-banner-aboutus.jpg') }}">
                </div>

                <div class="header-title col-lg-12 col-sm-12 col-xs-12">
                  <h1>Selling Music Gear</h1>
                </div>
                <div class="section">
                  <h3>Best Practices for Selling your gear on Forusblease</h3>
                  <br/>
                  <h3>Include as much information about your item as possible</h3>
                  <p>This sounds like an obvious one, but so many people over look the power of a well written blurb. You don’t need to be Shakespeare; you just have to include as much information as you have. I generally try to answer the most common questions in the ad for example: Year of manufacture, General condition, Make/Model, any accessories it may come with, any modifications, any specific points of interest for your item - does your guitar have Hendrix’s pickups or was your synth the one used on Tame Impalas last record?</p>
                  

                  <h3>Describe any faults, or mention the item is ‘un-tested’</h3>
                  <p>Many problems and disputes can be totally prevented, by the seller accurately describing the condition of their item. Buyers will also appreciate knowing exactly what they are getting, and will be more likely to purchase an item with a detailed description. It’s always best to properly test your gear before selling. If not, describe functions or the whole unit as ‘untested’, and price your item accordingly.</p>

                  <h3>Clean/Service your item!</h3>
                  <p>If you want to maximise the return for the item that you’re selling, clean it! It can be off putting as a buyer when you read things like “could probably do with a service” or “it should work, but I cant be bothered to try it”. Take the time to get it in great condition - even if it just means giving your guitar pedal a dust, or your guitar a new set of strings.</p>

                  <h3>Take Decent Photographs</h3>
                  <p>This is probably the most important point. Take as many as you can, and make them look good. If (like me) you are not the best photographer, Shaun has written a great article on how to take great snaps <a href="{{ route('pages::gearPhotography') }}" target="_blank">here</a>.</p>

                  <h3>Check the going price for your item</h3>
                  <p>I usually have a quick scout on the usual sites before I post an item. There’s no point in asking for $1000 when everybody else has the same thing listed for $800. Be realistic with your pricing, that way you can be firm in your negotiations and get as quick a sale as possible.</p>
                </div>
              </div>
          </div>
      </div>
  </div>

@endsection