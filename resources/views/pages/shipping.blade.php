@extends('layouts.master')

@section('title', 'Shipping')

@section('content')
	
<div id="main-container" class="single-page shipping">
      <div class="container">
          <div class="row">
              <div class="aboutus-content">
                <div class="image-container">
                  <img src="{{ url('/images/image-banner-aboutus.jpg') }}">
                </div>

                <div class="page-icons">
                  <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="icon-img">
                      <img src="{{ url('/images/icons/antenna.png') }}" alt="Check shipping prices" title="Check shipping prices"/>
                    </div>
                    <span class="icon-title">Check shipping prices</span>
                  </div>
                  <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="icon-img">
                      <img src="{{ url('/images/icons/delivery-cart.png') }}" alt="Pack your item carefully" title="Pack your item carefully"/>
                    </div>
                    <span class="icon-title">Pack your item carefully</span>
                  </div>
                  <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="icon-img">
                      <img src="{{ url('/images/icons/rocket.png') }}" alt="Book a courier or send via post" title=""/>
                    </div>
                    <span class="icon-title">Book a courier or send via post</span>
                  </div>
                  <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="icon-img">
                      <img src="{{ url('/images/icons/lighthouse.png') }}" alt="Send tracking info to buyer" title="Send tracking info to buyer"/>
                    </div>
                    <span class="icon-title">Send tracking info to buyer</span>
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="header-title col-lg-12 col-sm-12 col-xs-12">
                  <h1>Shipping Your Item</h1>
                </div>
                <div class="section">
                  <h3>Shipping Your Item - Who and How?</h3>
                  <p>With 10+ years of retail experience behind us, it’s fair to say we’ve shipped our fair share of gear. So here I’d just like to out line what has worked for us in the past and hopefully make shipping your items a little easier.</p>

                  <h3>Shipping Small items (Effects pedals, Mics etc)</h3>
                  <p>I hear a lot of people being wary of using Australia Post - but I have sent literally hundreds of parcels with them and never had a problem. Make sure your item is wrapped properly and pop it in a satchel and away you go. You get an affordable method of posting (between $10-15) and a tracking number. Nice and simple. You can also send extra-important small items via courier (see below).</p>

                  <h3>Medium - Large Size items</h3>
                  <p> There are a bunch of companies now that will help you with packaging and shipping larger items. I use Pack and Send (www.packsend.com.au) because they are the most affordable (a guitar usually costs between $25-45 depending on how regional its destination) and they offer tracking. Don’t bother with insuring your parcel however as they will only cover times in their “factory packaging”. I would again like to repeat though - I have sent literally hundreds of items with this company and have never once had an issue. So long as you package your item properly you shouldn’t have an issue. They’ll even pick the parcel up from your house/work if you want!</p>
                
                  <h3>Packaging your item</h3>
                  <p>For small items like pedals etc normally a well packaged box with plenty of stuffing does the trick. That could be bubble wrap, old newspaper, foam - anything you have thats not going to damage the item. So long as the item is packaged firmly, you should minimise any chance of damage in transit.</p>

                  <p> For larger items like Keyboards and Guitars it can be a little trickier. If your guitar is in a good hard case, then that will do most of the protection for you. Make sure you package each end of the box, and stuff the box so the case cannot move inside. If you can’t find a box the right size - ask your local music store! They’ll always help if they can. Buying something small (and being grateful) while you’re there to say thanks will mean you’ll be welcome to future boxes if needed.</p>

                  <p>For items without a hard case - bubble wrap is your best friend. Make sure you secure any wires and plugs so they cant rattle and knock anything. A few layers of bubble wrap and a well stuffed box should see you out of strife. With larger parcels, always mark them as fragile - just in case. If you need to buy a whole roll of bubble wrap to ship an item, you’re well within your rights to charge the buyer for that - make sure you’re open in your communications and negotiate the shipping price accordingly.</p>

                  <h3>Sending a lot of items?</h3>
                  <p>Try a Dymo label printer and print your shipping labs quickly! Buy your satchels in bulk online or at the post office, and save time by packing at home before dropping off. Different size packing boxes are available cheaply online, get cool custom boxes made! The Box Man is a good place to start. Aus Post also has a decent selection of small packing boxes and bags available in bulk.</p>

                </div>
              </div>
          </div>
      </div>
  </div>

@endsection