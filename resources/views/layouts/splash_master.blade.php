<!DOCTYPE html>
<html>
<head>
	<title>forsublease | @yield('title', 'Australia\'s Music Marketplace')</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="icon" href="/favicon.png" type="image/x-icon" />

	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="{{ elixir('css/plugins.css') }}">
	<link rel="stylesheet" href="{{ elixir('css/app.css') }}">

	<script type="text/javascript" src="{{ elixir('js/master.js') }}"></script>
	<script type="text/javascript">
		var baseUrl = "{{ url('/') }}";
	</script>
</head>

<body class="splash-page">
	<div id="main-wrapper">
		<!-- BOF Body -->
		@yield('content-splash')
		<!-- EOF Body -->
	</div><!-- #main-wrapper -->

	<script type="text/javascript" src="{{ elixir('js/plugins.js') }}"></script>
	<script type="text/javascript" src="{{ elixir('js/app.js') }}"></script>
	<script>
    $(document).ready(function(){
      @if( session()->has('alert') )
        toastr["{{ session()->get('alert')['status'] ? session()->get('alert')['status'] : 'info' }}"]("{{ session()->get('alert')['message'] }}");
      @endif
    });
  </script>	
</body>
</html>
