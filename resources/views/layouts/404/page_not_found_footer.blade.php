<div id="branding-container">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-8 col-md-offset-2 col-xs-12 branding-content">
				<p class="col-lg-6 col-xs-12">Copyright &copy; 2017 <b><a href="#">Forsublease</a></b>. All Right Reserved.</p>
				<p class="col-lg-6 col-xs-12">Website Design Perth <a href="#" target="_blank"><img src="{{ url('images/core-logo.png') }}" class="img-responsive" alt="" title="" /></a></p> 
			</div>
		</div>
	</div><!-- /.container -->
</div><!-- /#branding-container -->