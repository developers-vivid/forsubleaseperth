@extends('layouts.master')

@section('title', "Page Not Found")

@section('content')
    
    <div id="main-container" class="page-not-found-page">
        <div class="container">
          
			<!-- BOF Body -->
			@yield('content-404')
			<!-- EOF Body -->
                
        </div>
    </div>
@endsection