@if( count($options) > 0 )
	@foreach( $options as $option )
		<option value="{{ $option->category_id }}">{{ $option->name }}</option>
	@endforeach
@endif