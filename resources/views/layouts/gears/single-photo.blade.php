<div class="img-preview-cont dz-preview dz-file-preview img-con col-md-2 col-sm-6 col-xs-12 removable">
    <img src="{{ url('images', ['gears', 'thumbs', $photo_filename]) }}" alt="" title="" />
    <span data-photo-id="{{ $photo_id }}" class="delete-img fa fa-times-circle"></span>
    <span data-photo-id="{{ $photo_id }}" class="add-primary-photo fa fa-picture-o"></span>
</div>