<div>
</div>
<div id="desktop-sidebar" class="categories-container" style="background-color:#ebebeb; border-radius: 10px !important;">
    <h3 style="margin-top:-20px;">Type</h3>

    <ul class="categories" style="width:80%; margin:0 auto;">
        @if( count($categories = \App\Helper::getCategories()) > 0 )
            @foreach($categories as $category)
                <?php
                    $currentMenu = (Route::currentRouteName() == 'gears::gearmarketplace' && \Request::segment(2) == $category->slug);
                    $catCntr = App\Products::getTotalProducts($category->category_id);
                ?>
                <li {!! $currentMenu ? "class='active'" : "" !!}>
                    {!! link_to_route('gears::gearmarketplace', ($category->name .' '. ($catCntr > 0 ? '('. $catCntr .')' : '')), [$category->slug, null, 'location' => \Input::get('location'), 'brand' => \Input::get('brand'), 'price_min' => \Input::get('price_min'), 'price_max' => \Input::get('price_max'), 'type' => \Input::get('type'), 'sort' => \Input::get('sort')]) !!}
                    @if( count($category->children) > 0 )
                        <ul class="sub-menu dropdown">
                            @foreach($category->children as $sc)
                                <?php $subCatCntr = App\Products::getTotalProducts($category->category_id, $sc->category_id); ?>
                                <li class="menu-item{{ $currentMenu && \Request::segment(3) == $sc->slug ? ' active' : '' }}">
                                    {!! link_to_route('gears::gearmarketplace', ($sc->name .' '. ($subCatCntr > 0 ? '('. $subCatCntr .')' : '')), [$category->slug, $sc->slug, 'location' => \Input::get('location'), 'brand' => \Input::get('brand'), 'price_min' => \Input::get('price_min'), 'price_max' => \Input::get('price_max'), 'type' => \Input::get('type'), 'sort' => \Input::get('sort')]) !!}
                                    <!-- <a href="{{ route('gears::gearmarketplace', [$category->slug, $sc->slug]) }}">{{ $sc->name }} {{ $subCatCntr > 0 ? '('. $subCatCntr .')' : '' }}</a> -->
                                </li>
                             @endforeach
                        </ul>
                        <a href="javascript:void(0);" class="dropdownbtn fa fa-plus"></a>
                    @endif
                </li>
            @endforeach
        @endif
    </ul>
</div>