@extends('layouts.admin.app')

@section('title', 'Create Category')

@section('content')
    
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Create Blog</strong>
                    </div>
                    <?php $faker = Faker\Factory::create(); ?>
                    <div class="panel-body">
                        {!! Form::open(['route' => 'admin::createBlog', 'class' => 'form-horizontal', 'id' => 'createBlog', 'files' => true]) !!}

                            <!-- Title -->
                            <div class="form-group">
                                <label for="blogTitle" class="col-sm-2 control-label">Title</label>
                                <div class="col-sm-8">
                                    {!! Form::text('title', substr($faker->sentence($nbWords = 6, $variableNbWords = true), 0, -1), ['class' => 'form-control', 'id' => 'blogTitle', 'placeholder' => 'Title here']) !!}
                                </div>
                            </div>

                            <!-- Description -->
                            <div class="form-group">
                                <label for="blogContent" class="col-sm-2 control-label">Content</label>
                                <div class="col-sm-8">
                                    {!! Form::textarea('content', implode("\n\n", $faker->paragraphs($nb = 10, $asText = false)), ['class' => 'form-control', 'id' => 'blogContent', 'placeholder' => 'Content here...']) !!}
                                </div>
                            </div>

                            <!-- Photo -->
                            <div class="form-group">
                                <label class="col-sm-2 control-label">&nbsp;</label>
                                <div class="col-sm-8">
                                    <label for="blogPhoto" class="control-label">Photo</label>
                                    <input type="file" name="photo" id="blogPhoto" accept="image/x-png, image/gif, image/jpeg">
                                </div>
                            </div>

                            <!-- Status -->
                            <div class="form-group">
                                <label for="blogStatus" class="col-sm-2 control-label">Status</label>
                                <div class="col-sm-3">
                                    <select name="status" id="blogStatus" class="form-control">
                                        <option value="active">Active</option>
                                        <option value="inactive">Inactive</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-8">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-save fa-fw"></i> Save
                                    </button>
                                </div>
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#createBlog').on('submit', function(e){
                var self = $(this);
                var btn = self.find('.btn').button('loading');

                e.preventDefault();
                self.ajaxSubmit({
                    url: "{{ route('admin::createBlog') }}",
                    type: 'post',
                    success: function(response){
                        showToastr(response.status, response.message);

                        if( response.status == 'success' ) {
                            window.setTimeout(function(){
                                window.location.href = response.data.redirect;
                            }, 1500);
                        }
                        else
                            btn.button('reset');

                    }
                });

            });
        });
    </script>

@endsection
