@extends('layouts.admin.app')

@section('title', 'Users')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">        
        <div class="panel panel-default">
          <div class="panel-heading">Users <small class="pull-right"><a href="{{ route('admin::user-register') }}">Register?</a></small></div>

          <div class="panel-body">
            <blockquote>
              <small>List of all users</small>
            </blockquote>
          </div>

          <table class="table table-striped table-hovered">
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Status</th>
                <th>Created</th>
              </tr>
            </thead>
            <tbody>
              @if( count($users) > 0 )
                @foreach($users as $user)
                  <tr>
                    <td><code>{{ $user->user_id }}</code></td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                      <a href="{{ route('admin::change-user-status', [$user->user_id]) }}" title="">
                        <span class="label label-{{ $user->status == 'inactive' ? 'danger' : 'success' }}">{{ $user->status }}</span>
                      </a>
                    </td>
                    <td>
                      <small class="text-muted">
                        <i class="fa fa-clock-o fa-fw"></i> <span data-livestamp="{{ strtotime($user->created_at) }}"></span>
                      </small>
                    </td>
                  </tr>
                @endforeach
              @else
                <tr>
                  <td colspan="5" class="text-center">No record found</td>
                </tr>
              @endif
            </tbody>
          </table>
        </div>
        {{ $users->render() }}
      </div>
    </div>
  </div>
@endsection
