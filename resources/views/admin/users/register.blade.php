@extends('layouts.admin.app')

@section('title', 'Register User')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
          <div class="panel-heading">Users</div>

          <div class="panel-body">
            {!! Form::open(['url' => 'sign-up', 'class' => 'form-horizontal', 'role' => 'form', 'file' => true]) !!}
              <!-- First name -->
              <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">First Name</label>

                <div class="col-md-6">
                  {!! Form::text('first_name', old('first_name'), ['class' => 'form-control', 'placeholder' => 'First Name', 'autofocus' => true]) !!}

                  @if ($errors->has('first_name'))
                    <span class="help-block">
                      <strong>{{ $errors->first('first_name') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <!-- Last Name -->
              <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Last Name</label>

                <div class="col-md-6">
                  {!! Form::text('last_name', old('last_name'), ['class' => 'form-control', 'placeholder' => 'Last Name']) !!}

                  @if ($errors->has('last_name'))
                    <span class="help-block">
                      <strong>{{ $errors->first('last_name') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <!-- Email Address -->
              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Email Address</label>

                <div class="col-md-6">
                  {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => 'Email Address']) !!}

                  @if ($errors->has('email'))
                    <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <!-- Telephone -->
              <div class="form-group{{ $errors->has('telephone') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Telephone</label>

                <div class="col-md-6">
                  {!! Form::text('telephone', old('telephone'), ['class' => 'form-control', 'placeholder' => 'Telephone']) !!}

                  @if ($errors->has('telephone'))
                    <span class="help-block">
                      <strong>{{ $errors->first('telephone') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <!-- Mobile -->
              <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Mobile</label>

                <div class="col-md-6">
                  {!! Form::text('mobile', old('mobile'), ['class' => 'form-control', 'placeholder' => 'Mobile']) !!}

                  @if ($errors->has('mobile'))
                    <span class="help-block">
                      <strong>{{ $errors->first('mobile') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <!-- Password -->
              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Password</label>

                <div class="col-md-6">
                  {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) !!}

                  @if ($errors->has('password'))
                    <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <!-- Password Confirmation -->
              <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Re-type password</label>

                <div class="col-md-6">
                  {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Re-type password']) !!}

                  @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                      <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-4 control-label"></label>

                <div class="col-md-6">                  
                  <div class="checkbox">
                    <label>
                      {!! Form::checkbox('registered_seller', null, false) !!} Registered seller?
                    </label>
                  </div>
                </div>
              </div>

              <hr/>

              <!-- Company Name -->
              <div class="company-details hidden form-group{{ $errors->has('company_name') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Company Name</label>

                <div class="col-md-6">
                  {!! Form::text('company_name', old('company_name'), ['class' => 'form-control', 'placeholder' => 'Company Name']) !!}

                  @if ($errors->has('company_name'))
                    <span class="help-block">
                      <strong>{{ $errors->first('company_name') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <!-- Company ABN -->
              <div class="company-details hidden form-group{{ $errors->has('company_abn') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Company ABN</label>

                <div class="col-md-6">
                  {!! Form::text('company_abn', old('company_abn'), ['class' => 'form-control', 'placeholder' => 'Company ABN']) !!}

                  @if ($errors->has('company_abn'))
                    <span class="help-block">
                      <strong>{{ $errors->first('company_abn') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <!-- Company Street -->
              <div class="company-details hidden form-group{{ $errors->has('company_street') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Company Street</label>

                <div class="col-md-6">
                  {!! Form::text('company_street', old('company_street'), ['class' => 'form-control', 'placeholder' => 'Company Street']) !!}

                  @if ($errors->has('company_street'))
                    <span class="help-block">
                      <strong>{{ $errors->first('company_street') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <!-- Company City -->
              <div class="company-details hidden form-group{{ $errors->has('company_city') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Company City</label>

                <div class="col-md-6">
                  {!! Form::text('company_city', old('company_city'), ['class' => 'form-control', 'placeholder' => 'Company City']) !!}

                  @if ($errors->has('company_city'))
                    <span class="help-block">
                      <strong>{{ $errors->first('company_city') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <!-- Company State -->
              <div class="company-details hidden form-group{{ $errors->has('company_state') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Company State</label>

                <div class="col-md-6">
                  {!! Form::text('company_state', old('company_state'), ['class' => 'form-control', 'placeholder' => 'Company State']) !!}

                  @if ($errors->has('company_state'))
                    <span class="help-block">
                      <strong>{{ $errors->first('company_state') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <!-- Company Postcode -->
              <div class="company-details hidden form-group{{ $errors->has('company_postcode') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Company Postcode</label>

                <div class="col-md-6">
                  {!! Form::text('company_postcode', old('company_postcode'), ['class' => 'form-control', 'placeholder' => 'Company Postcode']) !!}

                  @if ($errors->has('company_postcode'))
                    <span class="help-block">
                      <strong>{{ $errors->first('company_postcode') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <!-- Company Phone -->
              <div class="company-details hidden form-group{{ $errors->has('company_phone') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Company Phone</label>

                <div class="col-md-6">
                  {!! Form::text('company_phone', old('company_phone'), ['class' => 'form-control', 'placeholder' => 'Company Phone']) !!}

                  @if ($errors->has('company_phone'))
                    <span class="help-block">
                      <strong>{{ $errors->first('company_phone') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <!-- Company Logo -->
              <div class="company-details hidden form-group">
                <label class="col-md-4 control-label">Company Logo</label>

                <div class="col-md-6">
                  {!! Form::file('logo') !!}
                </div>
              </div>

              <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                  <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-user fa-fw"></i> Signup
                  </button>
                </div>
              </div>
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
