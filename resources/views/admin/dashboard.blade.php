@extends('layouts.admin.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Welcome to your dashboard {{ Auth::user()->name }}</div>

                    <div class="panel-body">
                        <p>Dashboard Page</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
