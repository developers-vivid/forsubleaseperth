@extends('layouts.admin.app')

@section('title', 'Create Category')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
          <div class="panel-heading">
            <strong>Categories</strong>
          </div>

          <div class="panel-body">
            {!! Form::open(['route' => 'admin::post-category', 'class' => 'form-horizontal']) !!}

              <!-- Name -->
              <div class="form-group{{ $errors->has('category_name') ? ' has-error' : '' }}">
                <label for="category_name" class="col-sm-4 control-label">Category Name</label>
                <div class="col-sm-6">
                  {!! Form::text('category_name', old('category_name'), ['class' => 'form-control', 'id' => 'category_name', 'placeholder' => 'Category Name']) !!}
                  @if ($errors->has('category_name'))
                    <span class="help-block">
                      <strong>{{ $errors->first('category_name') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <!-- Description -->
              <div class="form-group{{ $errors->has('category_description') ? ' has-error' : '' }}">
                <label for="category_description" class="col-sm-4 control-label">Category Description</label>
                <div class="col-sm-6">
                  {!! Form::textarea('category_description', old('category_description'), ['class' => 'form-control', 'id' => 'category_description', 'placeholder' => 'Category Description', 'rows' => '4']) !!}
                  @if ($errors->has('category_description'))
                    <span class="help-block">
                      <strong>{{ $errors->first('category_description') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <!-- Parent -->
              <div class="form-group">
                <label for="category_parent" class="col-sm-4 control-label">Parent</label>
                <div class="col-sm-6">
                  <select name="category_parent" id="category_parent" class="form-control">
                    <option value="-">None</option>
                    @if( count($categories) > 0 )
                      @foreach($categories as $category)
                        <option value="{{ $category->category_id }}">{{ $category->name }}</option>
                      @endforeach
                    @endif
                  </select>
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-offset-4 col-sm-6">
                  <button type="submit" class="btn btn-primary">
                    <i class="fa fa-save fa-fw"></i> Save
                  </button>
                </div>
              </div>
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
