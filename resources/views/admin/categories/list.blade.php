@extends('layouts.admin.app')

@section('title', 'Categories')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
          <div class="panel-heading">
            <strong>Categories</strong>
            <small class="pull-right"><a href="{{ route('admin::create-category') }}"><i class="fa fa-plus"></i> Create category</a></small>
          </div>

          <div class="panel-body">
            <blockquote>
              <small>List of all categories</small>
            </blockquote>
          </div>

          <table class="table table-striped table-hovered">
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Parent</th>
                <th>Created</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @if( count($categories) > 0 )
                @foreach($categories as $category)
                  <tr>
                    <td><code>{{ $category->category_id }}</code></td>
                    <td>{{ $category->name }}</td>
                    <td>
                      @if( $category->parent == "-" )
                        {{ $category->parent }}
                      @else
                        <strong class="text-muted">{{ \App\Categories::whereCategoryId($category->parent)->value('name') }}</strong>
                      @endif
                    </td>
                    <td>
                      <small class="text-muted">
                        <i class="fa fa-clock-o fa-fw"></i> <span data-livestamp="{{ strtotime($category->created_at) }}"></span>
                      </small>
                    </td>
                    <td>
                      <a href="{{ route('admin::edit-category', [$category->category_id, 'edit']) }}" class="btn btn-default btn-xs" title="">
                        <i class="fa fa-pencil fa-fw"></i> Edit
                      </a>
                    </td>
                  </tr>
                @endforeach
              @else
                <tr>
                  <td colspan="5" class="text-center">No record found</td>
                </tr>
              @endif
            </tbody>
          </table>

          {!! $categories->render() !!}
        </div>
      </div>
    </div>
  </div>
@endsection
