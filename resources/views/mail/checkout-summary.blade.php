@extends('mail.layout')

@section('mail-content')
	<?php
		$tdHeaderLabel = "border-left:1px solid #eaeaea; border-right:1px solid #eaeaea; margin:0; text-align:left;padding:5px 5px 5px 10px;font-family:tahoma,sans-serif;";
		$tdLabel   = "border-left:1px solid #eaeaea; margin:0; text-align:left;padding:5px 0 5px 10px;font-family:tahoma,sans-serif;";
		$tdText    = "border-right:1px solid #eaeaea; margin:0; text-align:left;padding:5px 10px 5px 0;font-family:tahoma,sans-serif;";
		$h4Label   = "margin:0; padding:0; font-size: 18px;color:#616161;";
		$textStyle = "margin:0; padding:0; font-size: 15px;color:#616161;";
		$td155     = "border-right:1px solid #eaeaea; margin:0; text-align:left;padding:5px 10px 5px 5px;font-family:tahoma,sans-serif;";
		$style001 = "border: 0px solid #dbdbdb;border-collapse:separate;box-shadow: 0px 0 10px #B5B5B5;border-radius: 10px; margin: 0 auto 0 auto;";
		$style002 = "background-color:#4ab0cf;padding: 0 0 0 0; text-align:center; font-size: 29px;color:#fff;padding:40px 10px;border-radius: 6px 6px 0 0;font-family:tahoma,sans-serif;";
		$style003 = "background-color: #fff;font-size: 15px;padding: 15px;margin: 0;color: #333;font-weight: normal;font-family: tahoma,sans-serif;";
		$style004 = "background-color:#fff; font-size:15px;padding:15px 15px 7px;margin: 0;color:#{{ strtolower($orderDetails->order_status) != 'completed' ? 'd9534f' : '5cb85c' }};font-weight:normal;font-family:tahoma,sans-serif;";
	?>

	<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="{{ $style001 }}">
		<tr>
			<td style="{{ $style002 }}">
				Order Summary
			</td>
		</tr>
		<tr>
			<td>
				<p style="{{ $style003 }}">
				   Thanks for your order! Payment has been sent to the seller. You should receive your new gear soon! The seller should get in touch soon with the postage
				   information. You can always contact them for more information regarding delivery. Enjoy your new purchase!
				</p>
			</td>
		</tr>
		<tr>
			<td>
				<p style="{{ $style004 }}">{{ $orderMessage }}</p>
			</td>
		</tr>
		<tr>
			<td style="background-color:#fff;padding: 0 0 0 0; text-align:center;padding:7px 15px 15px;border-radius: 0 0 6px 6px;">
				<!-- Order Information -->
	    		<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:separate;margin: 0 auto; text-align:left;">
	    			<thead>
	    				<tr>
	                        <th colspan="2" style="border:1px solid #eaeaea; margin:0; text-align:left;padding:10px;font-family:tahoma,sans-serif;"><h4 style="{{ $h4Label }}">Order Information</h4></th>
	        			</tr>
	    			</thead>
	    			<tbody>
	    				<tr>
	    					<td width="230" style="{{ $tdLabel }}"><span style="{{ $textStyle }}">Order ID</span> :</td>
	    					<td width="392" style="{{ $tdText }}"><p style="{{ $textStyle }}">{{ $orderDetails->order_id }}</p></td>
	    				</tr>
	    				<tr>
	    					<td width="230" style="{{ $tdLabel }}"><span style="{{ $textStyle }}">Invoice ID</span> :</td>
	    					<td width="392" style="{{ $tdText }}"><p style="{{ $textStyle }}"><strong>{{ $orderDetails->invoice_id }}</strong></p></td>
	    				</tr>
	    				<tr>
	    					<td width="230" style="{{ $tdLabel }}"><span style="{{ $textStyle }}">Date {{ $orderDetails->order_status }}</span> :</td>
	    					<td width="392" style="{{ $tdText }}"><p style="{{ $textStyle }}">{{ \Carbon\Carbon::parse(strtolower($orderDetails->order_status) != 'completed' ? $orderDetails->date_cancelled : $orderDetails->date_completed )->format('F j, Y @ h:i A') }}</p></td>
	    				</tr>
	    				<tr>
	    					<td width="230" style="{{ $tdLabel }}"><span style="{{ $textStyle }}">Status</span> :</td>
	    					<td width="392" style="{{ $tdText }}"><p style="margin:0; padding:0; font-size: 15px;color:#{{ strtolower($orderDetails->order_status) != 'completed' ? 'd9534f' : '5cb85c' }};">{{ $orderDetails->order_status }}</p></td>
	    				</tr>
	    				@if( strtolower($orderDetails->order_status) == 'completed' )
		    				<tr>
		    					<td width="230" style="{{ $tdLabel }}"><span style="{{ $textStyle }}">Paypal Email</span> :</td>
		    					<td width="392" style="{{ $tdText }}"><p style="{{ $textStyle }}">{{ $orderDetails->paypal_payer_email ?: 'n/a' }}</p></td>
		    				</tr>
		    			@endif
	    				<tr>
	    					<td width="230" style="border-left:1px solid #eaeaea; border-bottom:1px solid #eaeaea; margin:0; text-align:left;padding:5px 0 5px 10px;font-family:tahoma,sans-serif;"><span style="{{ $textStyle }}">Total</span> :</td>
	    					<td width="392" style="border-right:1px solid #eaeaea; border-bottom:1px solid #eaeaea; margin:0; text-align:left;padding:5px 10px 5px 0;font-family:tahoma,sans-serif;"><p style="{{ $textStyle }}"><small>AUD</small> <strong>{{ number_format($orderDetails->total, 2) }}</strong></p></td>
	    				</tr>
	    			</tbody>
	    		</table>
	    	</td>
	    </tr>
	    {{--
	    <tr>
			<td style="background-color:#fff;padding: 0 0 0 0; text-align:center;padding:7px 15px 15px;border-radius: 0 0 6px 6px;">
	            <!-- Paypal Payment Information -->
	    		<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:separate;margin: 0 auto; text-align:left;">
	    			<thead>
	    				<tr>
	                        <th colspan="2" style="border:1px solid #eaeaea; margin:0; text-align:left;padding:10px;font-family:tahoma,sans-serif;"><h4 style="{{ $h4Label }}">Paypal Payment Information</h4></th>
	        			</tr>
	    			</thead>
	    			<tbody>
	    				<tr>
	    					<td width="200" style="{{ $tdLabel }}"><span style="{{ $textStyle }}">PayKey</span> :</td>
	    					<td width="422" style="{{ $tdText }}"><p style="{{ $textStyle }}">{{ $orderDetails->paypal_pay_key }}</p></td>
	    				</tr>
	    				<tr>
	    					<td width="200" style="{{ $tdLabel }}"><span style="{{ $textStyle }}">Payer Email Address</span> :</td>
	    					<td width="422" style="{{ $tdText }}"><p style="{{ $textStyle }}">{{ $orderDetails->paypal_payer_email ?: 'n/a' }}</p></td>
	    				</tr>
	    				<tr>
	    					<td width="200" style="border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea; margin:0; text-align:left;padding:5px 0 5px 10px;font-family:tahoma,sans-serif;"><span style="{{ $textStyle }}">Payer Account ID</span> :</td>
	    					<td width="422" style="border-right:1px solid #eaeaea; border-bottom:1px solid #eaeaea;margin:0; text-align:left;padding:5px 10px 5px 0;font-family:tahoma,sans-serif;"><p style="{{ $textStyle }}">{{ $orderDetails->paypal_payer_id ?: 'n/a' }}</p></td>
	    				</tr>
	    			</tbody>
	    		</table>
	    	</td>
	    </tr>
	    --}}
	    <tr>
	    	<td style="background-color:#fff;padding: 0 0 0 0; text-align:center;padding:7px 15px 15px;border-radius: 0 0 6px 6px;">
	            <!-- Item -->
	            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:separate;margin: 0 auto; text-align:left;">
	                <thead>
	                   	<tr>
	                        <th colspan="5" style="border:1px solid #eaeaea; margin:0; text-align:left;padding:10px;font-family:tahoma,sans-serif;"><h4 style="{{ $h4Label }}">Item <small>in AUD</small></h4></th>
	        			</tr>
	                </thead>
	                <tbody>
	                    <tr>
	                        <td width="311" style="border-left:1px solid #eaeaea; margin:0; text-align:left;padding:5px 5px 5px 10px;font-family:tahoma,sans-serif;"><h4  style="{{ $textStyle }}">Product Name</h4></td>
	                        <td width="155" style="{{ $td155 }}"><h4  style="{{ $textStyle }}">Price</h4></td>
	                        <td width="155" style="{{ $td155 }}"><h4  style="{{ $textStyle }}">Shipping</h4></td>
	                        <td width="155" style="{{ $td155 }}"><h4  style="{{ $textStyle }}">Sub Total</h4></td>
	                        <td width="155" style="{{ $td155 }}"><h4  style="{{ $textStyle }}">Commission</h4></td>
	                    </tr>
	                    <?php
	                    	$ot = 0;
	                    	$totalCommission = 0;
	                    ?>
	                    @if( count($orderDetails->ordered_products) > 0 )
                            @foreach($orderDetails->ordered_products as $prod)
                                <?php
                                	$ot+=$total = (($prod->price + $prod->shipping_cost) * $prod->qty);
                                	$totalCommission += $prod->commission;
                                ?>
                                <tr>
			                        <td width="311" style="border-left:1px solid #eaeaea; margin:0; text-align:left;padding:5px 5px 5px 10px;font-family:tahoma,sans-serif;"><p style="{{ $textStyle }}">{{ $prod->product_title }}</p></td>
			    					<td style="{{ $td155 }}"><span style="{{ $textStyle }}">{{ $prod->price }}</span></td>
			    					<td style="{{ $td155 }}"><span style="{{ $textStyle }}">{{ $prod->shipping_cost }}</span></td>
			    					<td style="{{ $td155 }}"><span style="{{ $textStyle }}">{{ $total }}</span></td>
			    					<td width="311" style="{{ $td155 }}"><span style="{{ $textStyle }}">{{ !is_null($prod->commission) ? $prod->commission : 0 }}</span></td>
			                    </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5" class="text-center">You did not placed an order</td>
                            </tr>
                        @endif
	                    <tr>
	                        <td width="311" style="border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea; margin:0; text-align:left;padding:5px 0 5px 10px;font-family:tahoma,sans-serif;"><h4 style="{{ $textStyle }}">Total:</h4></td>
	    					<td width="155" style="border-right:1px solid #eaeaea;border-bottom:1px solid #eaeaea; margin:0; text-align:left;padding:5px 10px 5px 5px;font-family:tahoma,sans-serif;" colspan="3"><span style="{{ $textStyle }}">AUD {{ $ot }}</span></td>
	    					<td width="155" style="border-right:1px solid #eaeaea;border-bottom:1px solid #eaeaea; margin:0; text-align:left;padding:5px 10px 5px 5px;font-family:tahoma,sans-serif;"><span style="{{ $textStyle }}">AUD {{ $totalCommission }}</span></td>
	                    </tr>
	                </tbody>
	            </table>  
	        </td>
	    </tr>
	    <tr>
			<td style="background-color:#fff;padding: 0 0 0 0; text-align:center;padding:7px 15px 10px 15px;border-radius: 0 0 6px 6px;">
	            <!-- Paypal Payment Information -->
	    		<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:separate;margin: 0 auto; text-align:left;">
	    			<thead>
	    				<tr>
	                        <th colspan="2" style="border:1px solid #eaeaea; margin:0; text-align:left;padding:10px;font-family:tahoma,sans-serif;"><h4 style="{{ $h4Label }}">Customer and Shipping Information</h4></th>
	        			</tr>
	    			</thead>
	    			<tbody>
	    				<!-- Customer -->
	    				<tr>
	                        <td colspan="2" style="{{ $tdHeaderLabel }}"><h4  style="{{ $textStyle }}">Customer</h4></td>
	                    </tr>
	    				<tr>
	    					<td width="125" style="{{ $tdLabel }}"><span style="{{ $textStyle }}">Name</span> :</td>
	    					<td width="497" style="{{ $tdText }}"><p style="{{ $textStyle }}">{{ $orderDetails->customer_name }}</p></td>
	    				</tr>
	    				<tr>
	    					<td width="125" style="{{ $tdLabel }}"><span style="{{ $textStyle }}">Email</span> :</td>
	    					<td width="497" style="{{ $tdText }}"><p style="{{ $textStyle }}">{{ $orderDetails->customer_email }}</p></td>
	    				</tr>
	    				<tr>
	    					<td width="125" style="{{ $tdLabel }}"><span style="{{ $textStyle }}">Telephone</span> :</td>
	    					<td width="497" style="{{ $tdText }}"><p style="{{ $textStyle }}">{{ $orderDetails->customer_telephone ?: 'n/a' }}</p></td>
	    				</tr>
	    				<tr>
	    					<td width="125" style="{{ $tdLabel }}"><span style="{{ $textStyle }}">Mobile</span> :</td>
	    					<td width="497" style="{{ $tdText }}"><p style="{{ $textStyle }}">{{ $orderDetails->customer_mobile ?: 'n/a' }}</p></td>
	    				</tr>

	    				<!-- Seller -->
	    				<tr>
	                        <td colspan="2" style="{{ $tdHeaderLabel }}"><h4  style="{{ $textStyle }}">Seller</h4></td>
	                    </tr>
	    				<tr>
	    					<td width="125" style="{{ $tdLabel }}"><span style="{{ $textStyle }}">Name</span> :</td>
	    					<td width="497" style="{{ $tdText }}"><p style="{{ $textStyle }}">{{ $orderDetails->seller->seller_name }}</p></td>
	    				</tr>
	    				<tr>
	    					<td width="125" style="{{ $tdLabel }}"><span style="{{ $textStyle }}">Email</span> :</td>
	    					<td width="497" style="{{ $tdText }}"><p style="{{ $textStyle }}">{{ $orderDetails->seller->seller_email }}</p></td>
	    				</tr>
	    				<tr>
	    					<td width="125" style="{{ $tdLabel }}"><span style="{{ $textStyle }}">Telephone</span> :</td>
	    					<td width="497" style="{{ $tdText }}"><p style="{{ $textStyle }}">{{ $orderDetails->seller->seller_telephone ?: 'n/a' }}</p></td>
	    				</tr>
	    				<tr>
	    					<td width="125" style="{{ $tdLabel }}"><span style="{{ $textStyle }}">Mobile</span> :</td>
	    					<td width="497" style="{{ $tdText }}"><p style="{{ $textStyle }}">{{ $orderDetails->seller->seller_mobile ?: 'n/a' }}</p></td>
	    				</tr>

	    				<!-- Shipping -->
	    				<tr>
	                        <td colspan="2" style="{{ $tdHeaderLabel }}"><h4  style="{{ $textStyle }}">Shipping</h4></td>
	                    </tr>
	    				<tr>
	    					<td width="125" style="{{ $tdLabel }}"><span style="{{ $textStyle }}">Address 1</span> :</td>
	    					<td width="497" style="{{ $tdText }}"><p style="{{ $textStyle }}">{{ $orderDetails->shipping_address_1 }}</p></td>
	    				</tr>
	    				<tr>
	    					<td width="125" style="{{ $tdLabel }}"><span style="{{ $textStyle }}">Address 2</span> :</td>
	    					<td width="497" style="{{ $tdText }}"><p style="{{ $textStyle }}">{{ $orderDetails->shipping_address_2 }}</p></td>
	    				</tr>
	    				<tr>
	    					<td width="125" style="{{ $tdLabel }}"><span style="{{ $textStyle }}">City</span> :</td>
	    					<td width="497" style="{{ $tdText }}"><p style="{{ $textStyle }}">{{ $orderDetails->shipping_city }}</p></td>
	    				</tr>
	    				<tr>
	    					<td width="125" style="{{ $tdLabel }}"><span style="{{ $textStyle }}">State</span> :</td>
	    					<td width="497" style="{{ $tdText }}"><p style="{{ $textStyle }}">{{ $orderDetails->shipping_state }}</p></td>
	    				</tr>
	    				<tr>
	    					<td width="125" style="border-left:1px solid #eaeaea; border-bottom:1px solid #eaeaea; margin:0; text-align:left;padding:5px 0 5px 10px;font-family:tahoma,sans-serif;"><span style="{{ $textStyle }}">Post Code</span> :</td>
	    					<td width="497" style="border-right:1px solid #eaeaea; border-bottom:1px solid #eaeaea; margin:0; text-align:left;padding:5px 10px 5px 0;font-family:tahoma,sans-serif;"><p style="{{ $textStyle }}">{{ $orderDetails->shipping_post_code }}</p></td>
	    				</tr>
	    			</tbody>
	    		</table>
			</td>
		</tr>

		{{--<tr>
			<td style="background-color:#fff;padding: 0 0 0 0; text-align:center;padding:7px 15px 10px 15px;border-radius: 0 0 6px 6px;">
	            <!-- Paypal Payment Information -->
	    		<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:separate;margin: 0 auto; text-align:left;">
	    			<thead>
	    				<tr>
	                        <th colspan="2" style="border:1px solid #eaeaea; margin:0; text-align:left;padding:10px;font-family:tahoma,sans-serif;"><h4 style="{{ $h4Label }}">Commision Information</h4></th>
	        			</tr>
	    			</thead>
	    			<tbody>
	    				
	    				<tr>
	    					<td width="125" style="{{ $tdLabel }}"><span style="{{ $textStyle }}">Commision Amount</span> :</td>
	    					<td width="497" style="border-right:1px solid #eaeaea; border-bottom:1px solid #eaeaea; margin:0; text-align:left;padding:5px 10px 5px 0;font-family:tahoma,sans-serif;"><p style="{{ $textStyle }}">$ xxx.xx</p></td>
	    				</tr>
	    				
	    			</tbody>
	    		</table>
			</td>
		</tr>--}}


		<tr>
			<td style="background-color:#fff; margin:0; padding:0 0 15px 0;border-bottom: 0;">
				<p style="background-color:#fff; font-size:15px;padding:15px 15px 7px;margin: 0;color:#616161;font-weight:normal;font-family:tahoma,sans-serif;">Notes: {{ $orderDetails->notes ?: '-' }}</p>
			</td>
		</tr>

		<tr>
			<td style="background-color:#fff; margin:0; padding:0 0 15px 0;border:0;">
				<button type="submit" class="contact-seller btn btn-success btn-contact-seller" style="color: #fff;background-color: #5cb85c;border-color: #4cae4c;display: inline-block;font-weight: normal;text-align: center;vertical-align: middle;-ms-touch-action: manipulation;touch-action: manipulation;cursor: pointer;background-image: none;border: 1px solid transparent;white-space: nowrap;padding: 10px 15px;font-size: 15px;line-height: 1.428571429;border-radius: 4px;-webkit-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;margin: 15px 15px 7px;">Message Seller</button>
			</td>
		</tr>
	</table>	
@endsection