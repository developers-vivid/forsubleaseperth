@if( count($user) > 0 )
    <form>
        <div class="form-group">
            <label for="inputTo" class="control-label">To</label>
            <h5 class="form-control"><strong>{{ $user->name }}</strong><small class="text-muted"><!-- &lt; -->{{-- $user->email --}}<!-- &gt; --></small></h5>
        </div>
        <div class="form-group">
            <label for="inputMessage" class="control-label">Message</label>
            {!! Form::textarea('message', null, ['class' => 'form-control', 'rows' => 5, 'id' => 'inputMessage', 'placeholder' => 'Your message here...']) !!}
        </div>
    </form>
@else
    <div class="alert alert-danger text-center">
        Seller does not exist
    </div>
@endif