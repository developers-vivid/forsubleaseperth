@extends('mail.layout')

@section('mail-content')
	<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #dbdbdb;border-collapse:separate;box-shadow: 0px 0 10px #B5B5B5;border-radius: 10px; margin: 0 auto 0 auto;">
		<tr>
			<td style="background-color:#4ab0cf;padding: 0 0 0 0; text-align:center; font-size: 29px;color:#fff;padding:40px 10px;border-radius: 6px 6px 0 0;font-family:tahoma,sans-serif;">
				New Account Registered
			</td>
		</tr>

		<tr>
			<td style="background-color:#fff;padding: 0 0 0 0; text-align:center;color:#7e7e7e;padding:45px 20px;border-radius: 0 0 6px 6px;font-family:tahoma,sans-serif;text-align:left;">
				<p style="font-size:17px;line-height: 23px;padding:0;margin:0 0 30px 0;">Hi <strong>{{ $user->name }}</strong>,</p>
				<p style="font-size:17px;line-height: 23px;padding:0;margin:0 0 30px 0;">You have successfully created a Forsublease account. Please click on the link below to verify your email address and complete your registration.</p>
				<p style="font-size:17px;line-height: 23px;padding:0;"><a href="{{ url('/verify-account') }}?token={{ $user->verification_token }}" title="Verify your email" style="color:#5ab5d2; text-decoration: none;">Verify your email</a></p>
				<p style="font-size:17px;line-height: 23px;padding:0;">or copy and paste this link into your browser:</p>
				<p style="font-size:17px;line-height: 23px;padding:0;margin:0 0 30px 0;">{{ url('/verify-account') }}?token={{ $user->verification_token }}</p>
				<p style="font-size:17px;line-height: 23px;padding:0;margin:0 0 30px 0;">Didn't create a Forsublease account? It's likely someone just typed in your email address by accident. Feel free to ignore this email.</p>
				<p style="font-size:17px;line-height: 23px;padding:0;margin:0 0 30px 0;"><span style="font-style:italic;">Thanks</span>,<br/><span style="font-weight:600;">Forsublease Team</span></p>
			</td>
		</tr>
	</table>	
@endsection