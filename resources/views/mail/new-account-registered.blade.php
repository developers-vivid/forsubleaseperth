@extends('mail.layout')

@section('mail-content')
	<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #dbdbdb;border-collapse:separate;box-shadow: 0px 0 10px #B5B5B5;border-radius: 10px; margin: 0 auto 0 auto;">
		<tr>
			<td style="background-color:#4ab0cf;padding: 0 0 0 0; text-align:center; font-size: 29px;color:#fff;padding:40px 10px;border-radius: 6px 6px 0 0;font-family:tahoma,sans-serif;">
				New Account Registered
			</td>
		</tr>

		<tr>
			<td style="background-color:#fff;padding: 0 0 0 0; text-align:center;padding:60px 20px 20px 20px;border-radius: 0 0 6px 6px;">
				<p style="font-size:19px;color:#7e7e7e;font-family:tahoma,sans-serif;"><span style="color:#5ab5d2;">{{ $user->name }}</span> has just registered an <strong style="font-weight:600;">account</strong> on your website and is awaiting your approval.</p>
				<table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 25px 0 0 0;">
					<tr>
						<td width="50%" style="text-align:right;">
							<a href="{{ url('/verify-account') }}?token={{ $user->verification_token }}&admin=true" style="text-decoration:none;font-size:18px;color:#fff;font-family:tahoma,sans-serif;">
								<img src="{{ url('/images/approved.png') }}" style="float:right; margin-right:5px;">
							</a>
						</td>
						<td width="50%" style="text-align:left;">
							<a href="{{ url('/decline-account') }}?token={{ $user->verification_token }}" style="text-decoration:none;font-size:18px;color:#fff;font-family:tahoma,sans-serif;">
								<img src="{{ url('/images/decline.png') }}" style="float:left; margin-left:5px;">
							</a>
						</td>
					</tr>
				</table>
				<table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin:35px 0 0 0;">
					<tr>
						<td width="50%" style="font-family:tahoma,sans-serif;font-size:13px;color:#b7b7b7;text-align:left;">Email: {{ $user->email }}</td>
						<td width="50%" style="font-family:tahoma,sans-serif;font-size:13px;color:#b7b7b7;text-align:right;">Seller: {{ \App\UsersCompany::whereUserId($user->user_id)->count() > 0 ? 'Yes' : 'No' }}</td>
					</tr>
				</table>

			</td>
		</tr>
	</table>	
@endsection