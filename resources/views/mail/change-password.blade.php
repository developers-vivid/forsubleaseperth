@extends('mail.layout')

@section('mail-content')
	<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #dbdbdb;border-collapse:separate;box-shadow: 0px 0 10px #B5B5B5;border-radius: 10px; margin: 0 auto 0 auto;">
		<tr>
			<td style="background-color:#4ab0cf;padding: 0 0 0 0; text-align:center; font-size: 29px;color:#fff;padding:40px 10px;border-radius: 6px 6px 0 0;font-family:tahoma,sans-serif;">
				Forsublease password reset
			</td>
		</tr>
		<tr>
			<td style="background-color:#fff;padding: 0 0 0 0; text-align:center;color:#7e7e7e;padding:45px 20px;border-radius: 0 0 6px 6px;font-family:tahoma,sans-serif;text-align:left;">
				<p style="font-size:17px;line-height: 23px;padding:0;margin:0 0 30px 0;">Dear {{ $user->name }},</p>
				<p style="font-size:17px;line-height: 23px;padding:0;margin:0 0 30px 0;">You're receiving this e-mail because you requested a password reset for your account. Please go to the following link and choose a new password: <a href="{{ $url }}" target="_blank" title="Change password">{{ $url }}</a>.</p>
				<p style="font-size:17px;line-height: 23px;padding:0;margin:0 0 30px 0;">If you didn't make this request, disregard this email.</p>
				<p style="font-size:17px;line-height: 23px;padding:0;margin:0 0 30px 0;"><span style="font-style:italic;">Thanks</span>,<br/><span style="font-weight:600;">Forsublease Team</span></p>
			</td>
		</tr>
	</table>
@endsection