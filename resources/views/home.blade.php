@extends('layouts.master')

@section('content')
    <?php $categories = \App\Helper::getCategories(); ?>
<style>
    @media only screen and (max-width: 768px) and (min-width: 413px)  {
    .customPageIconsText{
    position:relative;
	left:10px;
    }
	}
    @media only screen and (max-width: 412px) and (min-width: 385px)  {
    .customPageIconsText{
    position:relative;
	left:10px;
    }
	}		
    @media only screen and (max-width: 384px) and (min-width: 376px)  {
    .customPageIconsText{
    position:relative;
	left:10px;
    }
	}		
    @media only screen and (max-width: 375px) and (min-width: 361px)  {
    .customPageIconsText{
    position:relative;
	left:5px;
    }
	}
    @media only screen and (max-width: 360px) and (min-width: 320px)  {
    .customPageIconsText{
    position:relative;
	left:15px;
    }
	}		
    @media only screen and (max-width: 319px) and (min-width: 150px)  {
    .customPageIconsText{
    position:relative;
	left:10px;
    }
	}	
</style>		
    <div id="banner-container">
        <div class="row banner-row">
            <div class="banner-container">
                <div class="col-lg-12 col-md-12 col-xs-12 banner-item">
                    <div class="container">
                        <div class="heading-container">
                            <!-- <img class="banner-logo" src="{{ url('images/banner-gp-logo.png') }}" title="Gear Planet" alt="Gear Planet" /> -->
                            <h2>YOUR OFFICE JUST A CLICK AWAY</h2>
                            <!-- <p>Join a growing community of <b>buyers & sellers</b> across Australia <b>Browse</b>, <b>Purchase</b> and <b>List</b> your items for sale Purpose build for the Australian music community!</p> -->
                        </div>
                    </div>
                    <!-- <div class="img-container"><img src="{{ url('images/homepage_banner1.jpg') }}" class="img-responsive" alt="" title="" /></div> -->
                    <div class="img-container" style="background: url({{ url('images/homepage_banner1.jpg') }}) 0 0 no-repeat; background-size: cover; background-position: center;"></div>
                </div>
                <!-- <div class="col-lg-12 col-md-12 col-xs-12 banner-item">
                    <div class="container">
                        <div class="heading-container">
                            <img class="banner-logo" src="{{ url('images/banner-gp-logo.png') }}" title="Gear Planet" alt="Gear Planet" />
                            <h2>Welcome to Gear Planet!</h2>
                            <p>Join a growing community of <b>buyers & sellers</b> across Australia <b>Browse</b>, <b>Purchase</b> and <b>List</b> your items for sale Purpose build for the Australian music community!</p>
                        </div>
                    </div>
                    <div class="img-container" style="background: url({{ url('images/homepage_banner1.jpg') }}) 0 0 no-repeat; background-size: cover; background-position: center;"></div>
                </div> -->
            </div>
            
        </div>
    </div><!-- #banner-container -->
    <div id="main-container">
        <div class="container">

                <div class="container" >
                    <div id="test-sana" class="col-lg-12 col-sm-12 col-xs-12 heading-title text-center" style="margin-top: 3em;">
                        <h3 class="customPageIconsText">ADVERTISE YOUR AVAILABLE SPACE TODAY</h3>
                    </div>
                </div>
   
            <div class="page-icons  text-center">
              <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="icon-img">
                  <img src="{{ url('/images/icons/simple_registration.png') }}" alt="Buy and sell music gear!" title="Buy and sell music gear!"/>
                </div>
                <span class="icon-title">1. Simple Registration</span>
              </div>
              <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="icon-img">
                  <img src="{{ url('/images/icons/ads_space.png') }}" alt="Free to join list items" title="Free to join list items"/>
                </div>
                <span class="icon-title">2. Advertise your Space</span>
              </div>
              <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="icon-img">
                  <img src="{{ url('/images/icons/rec_enq.png') }}" alt="Great deals on new &amp; used gear" title="Great deals on new &amp; used gear"/>
                </div>
                <span class="icon-title">3. Receive enquiries!</span>
              </div>
              <div class="clearfix"></div>
            </div>

           

            <div class="row main-row" style="margin-top:80px;">
                <div class="col-lg-3 col-sm-3 col-xs-12 sidebar-container" style="background-color:#ffffff; margin-top:5px;">
                    @include('layouts.includes.gp_categories')
                    @include('layouts.filters.refined-filter', ['action' => url('search')])
                </div>
                <div class="col-lg-9 col-sm-9 col-xs-12 items-container">
                    @include('layouts.filters.gear-filter', ['hideResultsLayouts' => true])

                    @if( ! is_null($savedSearch) )
                        <div class="product-gear">
                            <div class="found-result clearfix">
                                <div class="col-lg-10 col-md-10 col-xs-12 result-detail">
                                    <p>Saved Search</p>
                                </div>
                                <div class="col-lg-2 col-md-2 col-xs-12 result-btn">
                                    <button type="button" class="btn btn-get-save-results btn-save-results" data-loading-text="Please Wait ..." data-search-keyword="{{ \Input::get('keyword') }}" data-search-url="{{ "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" }}">
                                        <i class="fa fa-save fa-fw"></i> Search History
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="row products-home">  
                            <div class="product-container">
                                @if( count($savedSearch) > 0 )
                                    <div class="gridviewlayout grid"> 
                                        @foreach( $savedSearch as $gear )
                                            <div class="col-lg-4 col-sm-6 col-xs-12 product-item" data-my-order="1">
                                                @include('layouts.gears.grid', ['gear' => $gear])
                                            </div>
                                        @endforeach
                                    </div>
                                    
                                @else
                                    <div class="col-lg-12">
                                        <div class="alert alert-danger text-center">
                                            No items found
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    @endif
                   
                    <div class="border-custom"></div>

                    <div class="heading-title">
                        <!--<h3>Recent Listings</h3>-->
                    </div>
                    <div class="row products-home">  
                        <div class="product-container">
                            @if( count($gears) > 0 )
                                <div class="gridviewlayout grid"> 
                                    @foreach( $gears as $gear )
                                        <div class="col-lg-4 col-sm-6 col-xs-12 product-item" data-my-order="1">
                                            @include('layouts.gears.grid', ['gear' => $gear])
                                        </div>
                                    @endforeach
                                </div>

                                <!--{!! $gears->links() !!}-->
                            @else
                                <div class="col-lg-12">
                                    <div class="alert alert-danger text-center">
                                        No items found
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .container -->
    </div><!-- #main-container -->
    <script type="text/javascript">
        $(document).ready(function() {
            
            $('.btn-get-save-results').on('click', function (event) {
                var self = $(this);
                var btn = self.button('loading');

                $.ajax({
                    'type': 'post',
                    'url': '{{ route("search::getSavedResults") }}',
                }).done(function(data){
                    btn.button('reset');

                    if( data.status == 'success' ) {
                        bootbox.dialog({
                          message: data.data.html,
                          title: "Search History",
                          backdrop: true,
                          onEscape: true,
                          className: 'custom-saveresults'
                        });
                    }
                });
            });

        });

    </script>
    @include('layouts.includes.gp_bottom')
@endsection
