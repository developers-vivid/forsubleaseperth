<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CategoriesTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $categories = [
      // ["category_id" => \App\Helper::getUID(), "name" => 'Accoustic', "description" => 'Accoustic', "created_at" => Carbon::now()],
      // ["category_id" => \App\Helper::getUID(), "name" => 'Amps', "description" => 'Amps', "created_at" => Carbon::now()],
      // ["category_id" => \App\Helper::getUID(), "name" => 'Bass', "description" => 'Bass', "created_at" => Carbon::now()],
      // ["category_id" => \App\Helper::getUID(), "name" => 'Dj', "description" => 'Dj', "created_at" => Carbon::now()],
      // ["category_id" => \App\Helper::getUID(), "name" => 'Drums', "description" => 'Drums', "created_at" => Carbon::now()],
      // ["category_id" => \App\Helper::getUID(), "name" => 'Effects', "description" => 'Effects', "created_at" => Carbon::now()],
      // ["category_id" => \App\Helper::getUID(), "name" => 'Electric', "description" => 'Electric', "created_at" => Carbon::now()],
      // ["category_id" => \App\Helper::getUID(), "name" => 'Folk', "description" => 'Folk', "created_at" => Carbon::now()],
      // ["category_id" => \App\Helper::getUID(), "name" => 'Keyboards', "description" => 'Keyboards', "created_at" => Carbon::now()],
      // ["category_id" => \App\Helper::getUID(), "name" => 'Orchestra', "description" => 'Orchestra', "created_at" => Carbon::now()],
      // ["category_id" => \App\Helper::getUID(), "name" => 'Pro-Audio', "description" => 'Pro-Audio', "created_at" => Carbon::now()],
      // ["category_id" => \App\Helper::getUID(), "name" => 'Parts', "description" => 'Parts', "created_at" => Carbon::now()],
      // ["category_id" => \App\Helper::getUID(), "name" => 'Accessories', "description" => 'Accessories', "created_at" => Carbon::now()],
      // ["category_id" => \App\Helper::getUID(), "name" => 'Others', "description" => 'Others', "created_at" => Carbon::now()],

    	["category_id" => \App\Helper::getUID(), "name" => "Parts", "description" => 'Parts', "created_at" => Carbon::now()],
    	["category_id" => \App\Helper::getUID(), "name" => "Accessories", "description" => 'Accessories', "created_at" => Carbon::now()],
    	["category_id" => \App\Helper::getUID(), "name" => "Effects and Pedals", "description" => 'Effects and Pedals', "created_at" => Carbon::now()],
    	["category_id" => \App\Helper::getUID(), "name" => "Electric Guitars", "description" => 'Electric Guitars', "created_at" => Carbon::now()],
    	["category_id" => \App\Helper::getUID(), "name" => "Pro-Audio", "description" => 'Pro-Audio', "created_at" => Carbon::now()],
    	["category_id" => \App\Helper::getUID(), "name" => "Drum and Percussion", "description" => 'Drum and Percussion', "created_at" => Carbon::now()],
    	["category_id" => \App\Helper::getUID(), "name" => "Acoustic Guitars", "description" => 'Acoustic Guitars', "created_at" => Carbon::now()],
    	["category_id" => \App\Helper::getUID(), "name" => "Amps", "description" => 'Amps', "created_at" => Carbon::now()],
    	["category_id" => \App\Helper::getUID(), "name" => "Bass Guitars", "description" => 'Bass Guitars', "created_at" => Carbon::now()],
    	["category_id" => \App\Helper::getUID(), "name" => "Folk Instruments", "description" => 'Folk Instruments', "created_at" => Carbon::now()],
    	["category_id" => \App\Helper::getUID(), "name" => "Band and Orchestra", "description" => 'Band and Orchestra', "created_at" => Carbon::now()],
    	["category_id" => \App\Helper::getUID(), "name" => "Keyboards", "description" => 'Keyboards', "created_at" => Carbon::now()],
    	["category_id" => \App\Helper::getUID(), "name" => "DJ and Lighting Gear", "description" => 'DJ and Lighting Gear', "created_at" => Carbon::now()],
    	["category_id" => \App\Helper::getUID(), "name" => "Home Audio", "description" => 'Home Audio', "created_at" => Carbon::now()],
    	["category_id" => \App\Helper::getUID(), "name" => "Other", "description" => 'Other', "created_at" => Carbon::now()]
    ];

    DB::table('categories')->insert($categories);
    $this->command->info('Categories has been successfully seeded');
  }
}
