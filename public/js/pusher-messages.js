$(document).ready(function(){

	var socketMsgCntrId, socketId, activeInbox, currReceiver;
    var inbxInfo = {
        "inboxId": ((typeof chat != 'undefined' && chat.inboxId != '') ? chat.inboxId : ''),
        "receiverId": ((typeof chat != 'undefined' && chat.receiverId != '') ? chat.receiverId : '')
    };
	var pshrMsgCntr = new Pusher(pshMsg.id);
	var pusherMsgs = new Pusher( pshMsg.id );

    activeInbox = ((typeof chat != 'undefined' && chat.activeInbox != '') ? chat.activeInbox : '');
    console.log('activeInbox ->>', activeInbox);
    currReceiver = "";

	/**
	 * Pusher message counter
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-16T09:34:34+0800]
	 * @param  {[type]} ) {                   socketMsgCntrId [description]
	 * @return {[type]}   [description]
	 */
	pshrMsgCntr.connection.bind('connected', function() {
        socketMsgCntrId = pshrMsgCntr.connection.socket_id;

        var pshrChannel = pshrMsgCntr.subscribe(pshMsg.msg_chnl);
	    pshrChannel.bind(pshr.msgcntr, function(data){
            var obj = jQuery.parseJSON(data);

            $.ajax({
                "url" : pshMsg.msg_cntr_url,
                "type" : 'post',
                "data": {
                    "inbox": obj.inbox,
                    "msg_id": (obj.inbox == activeInbox ? obj.message : '')
                }
            }).done(function(response){
                var msgContainer = $('body').find('#total-unread-messages');
                if( obj.socket != socketId ) {
                    if( obj.receiver == pshr.msgreceiver )
                        toastr.info('<a href="'+ (baseUrl  +'/inbx/'+ response.data.channel) +'">You have received a message from <strong>'+ obj.user_name +'</strong></a>');

                    if( obj.inbox != activeInbox )
                        setTotalMessages( response.data.total );
                }
	    	});
	    });
    });

    /**
     * To contact gear owner
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-13T11:50:13+0800]
     * @param  {[type]} ){                     } [description]
     * @return {[type]}     [description]
     */
    // if( pshMsg.isGearInfo ) {
        pshrMsgCntr.connection.bind('connected', function() {
            $('.btn-contact-seller').prop('disabled', false);

            $('.btn-contact-seller').on('click', function(){
                var self = $(this);
                var gear = {
                    "seller" : self.data('seller-id'),
                    "socket_id" : socketMsgCntrId
                };

                var btn = self.button('loading');
                $.ajax({
                    "url" : pshMsg.cntct_seller,
                    "type" : "post",
                    "data" : gear
                }).done(function(response){
                    btn.button('reset');
                    
                    if(response.status == 'success') {
                        bootbox.dialog({
                          message: response.data.contact_form,
                          title: "Contact Seller",
                          buttons: {
                            success: {
                                label: "Send",
                                className: "btn-success",
                                callback: function() {
                                    gear.message = $('.bootbox').find('#inputMessage').val().replace(/\n/g, '<br />');

                                    if( gear.message != '' ) {
                                        var bootboxBtn = $('.bootbox').find('.modal-footer').find('.btn').button('loading');

                                        $.ajax({
                                            "url" : pshMsg.snd_msg_to_seller,
                                            "type" : 'post',
                                            "data" : gear
                                        }).done(function(data){
                                            toastr[data.status](data.message);
                                            bootbox.hideAll();
                                        });                                    
                                    }

                                    return false;
                                }
                            },
                            cancel: {
                                label: "Cancel",
                                className: "btn-danger"
                            }
                          }
                        });
                    }
                    else {
                        toastr[response.status](response.message);
                    }
                });
            });


        });        
    // }

	/**
	 * Pusher for messages page
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-16T09:34:42+0800]
	 * @param  {[type]} ) {                   socketId [description]
	 * @return {[type]}   [description]
	 */
    // if( pshMsg.isMessaging ) {
        pusherMsgs.connection.bind('connected', function() {
            socketId = pusherMsgs.connection.socket_id;
            getUserInbox();
        });

        /**
         * To show messages per inbox
         * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
         * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-13T09:01:30+0800]
         * @param  {[type]} e){                         var self [description]
         * @return {[type]}      [description]
         */
        $('body').on('click', '.show-messages', function(e){
            var self = $(this);
            var inboxId = self.data('inbox-id');

            $('.show-messages').removeClass('active');
            self.addClass('active');

            $('.chat-box-container').removeClass('hidden');
            $('.chat-box-container').find('.message-area-container').html('');

            $.ajax({
                "url" : pshMsg.inbx_msg,
                "type" : 'post',
                "data" : {
                    "inbox" : inboxId
                }
            }).done(function(response){
                $('.chat-box-container').find('.message-area-container').html(response.data.msg_html);
                $('.textbox-area-container .btn-send').attr('data-inbox-id', inboxId).attr('data-user-id', response.data.user);

                // set flag active inbox
                activeInbox = inboxId;
                inbxInfo.inboxId = response.data.inbox;
                inbxInfo.receiverId = response.data.user;
                console.log('inbxInfo -->>', inbxInfo);

                setTotalMessages( response.data.total_unread_messages );
                scrollBottom();
            });
        });

        /**
         * To send message per inbox
         * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
         * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-13T09:01:41+0800]
         * @param  {[type]} ){                         var self [description]
         * @return {[type]}     [description]
         */
        $('body').on("click", '.btn-send', function(){
            var self = $(this);
            var inboxId = self.data('inbox-id');
            var userId = self.data('user-id');
            var message = self.siblings('textarea').val().replace(/\n/g, '<br />');

            if( message != '' ) {
                var btn = self.button('loading');
                sendMessage( inboxId, userId, message  );
                console.log(message);
            }
        });

        $('body').on("keypress", '#message-textarea', function(e){
            if( e.keyCode == 13 && !e.shiftKey || e.keyCode == 10 ){

                var self = $(this);
                var inboxId =self.siblings('.btn-send').data('inbox-id');
                var userId = self.siblings('.btn-send').data('user-id');
                var message = self.val().replace(/\n/g, '<br />');

                if( message != '' ) {
                    var btn = self.siblings('.btn-send').button('loading');
                    sendMessage( inboxId, userId, message  );
                }
            }
        });
        
    	var eventName = pshr.msgevnt;
        var callback = function(data){
            var msgObj = jQuery.parseJSON(data);
            console.log('msgObj ->>', msgObj);
            $.ajax({
                "url" : pshMsg.recvr_msg,
                "type" : 'post',
                "data" : {
                    "message_id": msgObj.message_id,
                    "update_msg": (msgObj.inbox == activeInbox ? 'yes' : 'no' )
                }
            }).done(function(response){
                var inbxCntr = $('body').find('.messages-container.inbx-'+msgObj.inbox);

                if( inbxCntr.length > 0 ) {
                    inbxCntr.append(response.data.message);
                    scrollBottom();
                }
            });
        }

    	/**
    	 * To get user inbox
    	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
    	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-16T09:22:28+0800]
    	 * @return {[type]} [description]
    	 */

        function sendMessage( inboxId, userId, message ){
            $('body').find('#message-textarea').val('');
            $('body').find('#message-textarea').prop('disabled', true);
            $.ajax({
                "url" : pshMsg.snd_msg,
                "type" : 'post',
                "data" : {
                    "inbox" : inbxInfo.inboxId,
                    "receiver" : inbxInfo.receiverId,
                    "message" : message,
                    "socket_id" : socketId
                }
            }).done(function(response){
                $('.btn-send').button('reset');
                $('.btn-send').siblings('textarea').val('');

                if( response.status == "success" ) {
                    $('body').find('#message-textarea').prop('disabled', false);

                    // toastr[response.status](response.message);
                    $('.chat-box-container').find('.messages-container').append(response.data.message);
                    scrollBottom();
                }
                else {
                    showToastr(response.status, response.message );
                }
            });
        }

    	function getUserInbox(){
            $.ajax({
                "url" : pshMsg.recvr_inbox,
                "type" : 'post'
            }).done(function(response){
                $('.users-container').html(response.data.sender_html);
                if( response.data.inbox.length > 0 ){
                    var channels = [];
                    $.each(response.data.inbox, function(key, value){
                        eval("theChannel"+ value.channel_id +" = pusherMsgs.subscribe('"+ value.channel_id +"')");
                        eval("theChannel"+ value.channel_id +".bind(eventName, callback)");
                    });
                }
            });            
        }
    // }


	/**
	 * Set total messages
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-16T09:19:15+0800]
	 * @param  {[type]} total [description]
	 */
    function setTotalMessages( total ){
        var msgContainer = $('body').find('#total-unread-messages');
        if( total > 0 )
            msgContainer.html(' ('+ total +')').removeClass('hidden');
        else
            msgContainer.html('').addClass('hidden');
    }

    /**
     * [scrollBottom description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-16T09:19:26+0800]
     * @return {[type]} [description]
     */
    function scrollBottom() {
        var container = document.getElementsByClassName("messages-container");
        if (container.length){
            var height = container[0].scrollHeight;
            $(".messages-container").animate({ scrollTop: height }, "fast");
        }
    }

    // Enable pusher logging - don't include this in production
    if( pshr.mode == 'local' || pshr.mode == 'development' ) {
        Pusher.log = function(message) {
            if (window.console && window.console.log) {
                window.console.log(message);
            }
        };        
    }
});